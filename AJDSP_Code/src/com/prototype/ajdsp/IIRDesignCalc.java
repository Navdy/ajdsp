package com.prototype.ajdsp;


import android.os.Bundle;

public class IIRDesignCalc {
	int Order, iirfilter, ftype;
	double W1,W2,Wp1,Wp2,Ws1,Ws2,Rp,Rs;

    double[] aCoeff = new double[12];
    double[] bCoeff = new double[12];
    public Bundle getFilterCoeff(Bundle inputBundle){
        Rp=(int)inputBundle.getDouble("pb");
        Rs=(int)inputBundle.getDouble("sb");
        Wp1=inputBundle.getDouble("Wp1");
        Ws1=inputBundle.getDouble("Ws1");
        Wp2=inputBundle.getDouble("Wp2");
        Ws2=inputBundle.getDouble("Ws2");
        ftype=inputBundle.getInt("filterType");
        iirfilter=(int)inputBundle.getLong("iirType");
        FilterDesign();
        Bundle filterCoeff=new Bundle();
  	  filterCoeff.putDoubleArray("filterCoeffB", bCoeff);

        filterCoeff.putDoubleArray("filterCoeffA", aCoeff);
        return filterCoeff;
    }
    public void FilterDesign()
    {
  	  for (int i=0;i<=10;i++)
  		{
  			aCoeff[i]=0.0;
  			bCoeff[i]=0.0;
  		}

  	  if (iirfilter==0) //Butterworth
  	  {
//  		  System.out.println("I selected the Butterworth filter");
  		  Butterworth IIRfilter = new Butterworth();
  		  IIRfilter.setFilterParameters(ftype, Wp1, Ws1, Wp2, Ws2, Rp, Rs);
  		  Order=IIRfilter.getOrder();
  		  if(Order==-1){
  			/*OrderWarningIIRDialog orderWarningDialog = new OrderWarningIIRDialog(this.);
     		 orderWarningDialog.setText("Max Order of the Filter is 64");
     		 orderWarningDialog.show();	*/
  		  }
  		  W1=IIRfilter.getFreq1();
  		  W2=IIRfilter.getFreq2();
  		  for (int i=0;i<=Order;i++)
  		  {
  			  bCoeff[i]=IIRfilter.getACoeff(i);
  			  aCoeff[i]=IIRfilter.getBCoeff(i);
  		  }
  		  int a=0;
  		  a=a+1;
  	  }
  	  else if (iirfilter==1) //Chebyshev I
  	  {
  		  Chebyshev IIRfilter = new Chebyshev();
  		  IIRfilter.setFilterParameters(ftype, Wp1, Ws1, Wp2, Ws2, Rp, Rs);
  		  Order=IIRfilter.getOrder();
  		  W1=IIRfilter.getFreq1();
  		  W2=IIRfilter.getFreq2();
  		  for (int i=0;i<=Order;i++)
  		  {
  			  aCoeff[i]=IIRfilter.getBCoeff(i);
  			  bCoeff[i]=IIRfilter.getACoeff(i);
  		  }
  		 int a=0;
 		  a=a+1;
  	  }
  	  else if (iirfilter==2) //Chebyshev II
  	  {
  		  Chebyshev2 IIRfilter = new Chebyshev2();
  		  IIRfilter.setFilterParameters(ftype, Wp1, Ws1, Wp2, Ws2, Rp, Rs);
  		  Order=IIRfilter.getOrder();
  		  W1=IIRfilter.getFreq1();
  		  W2=IIRfilter.getFreq2();
  		  for (int i=0;i<=Order;i++)
  		  {
  			  aCoeff[i]=IIRfilter.getACoeff(i);
  			  bCoeff[i]=IIRfilter.getBCoeff(i);
  		  }
  	  }
  	  else if (iirfilter==3) // Elliptic
  	  {
  		  Elliptic IIRfilter = new Elliptic();
  		  IIRfilter.setFilterParameters(ftype, Wp1, Ws1, Wp2, Ws2, Rp, Rs);
  		  Order=IIRfilter.getOrder();
  		  W1=IIRfilter.getFreq1();
  		  W2=IIRfilter.getFreq2();
  		  for (int i=0;i<=Order;i++)
  		  {
  			  aCoeff[i]=IIRfilter.getACoeff(i);
  			  bCoeff[i]=IIRfilter.getBCoeff(i);
  		  }
  	  }
  	  for (int i=Order+1;i<=10;i++)
  	  {
  		  aCoeff[i]=0.0;
  		  bCoeff[i]=0.0;
  	  }
    }

}
