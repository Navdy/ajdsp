package com.prototype.ajdsp;

import com.prototype.ajdsp.PartObject.PinSignalTypes;
import com.prototype.ajdsp.PartObject.PinTypes;


import android.app.Activity;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.view.View.OnClickListener;
import android.view.WindowManager;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;

public class AdderMenu extends Activity implements OnClickListener {
	RelativeLayout layout;
	RelativeLayout topbar;
	Button addParts;
	Button back;	
	Bundle inputValues;
	Bundle inputTopBundle;
	Bundle inputBottomBundle;
	TextView title;

	Spinner topPinSpinner;
	Spinner bottomPinSpinner;
	Boolean create=true;



	public static int adderNumber=0;
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
		setContentView(R.layout.adder_menu);
		//getWindow().setFeatureInt(Window.FEATURE_CUSTOM_TITLE, R.layout.row);
		layout=(RelativeLayout)findViewById(R.id.adder_menu);
		layout.setBackgroundColor(Color.WHITE);
		
		title= (TextView)findViewById(R.id.Title);
		title.setText("Adder");
		//topbar.setBackgroundColor(Color.GRAY);

		topPinSpinner = (Spinner) findViewById(R.id.adderUpperPinType);
		ArrayAdapter<CharSequence> topPinAdapter = ArrayAdapter.createFromResource(
				this, R.array.adder_input_type, android.R.layout.simple_spinner_item);
		topPinAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		//topPinSpinner.setBackgroundColor(Color.WHITE);
		topPinSpinner.setAdapter(topPinAdapter);

		bottomPinSpinner = (Spinner) findViewById(R.id.adderLowerPinType);
		ArrayAdapter<CharSequence> bottomPinAdapter = ArrayAdapter.createFromResource(
				this, R.array.adder_input_type, android.R.layout.simple_spinner_item);
		bottomPinAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		//bottomPinSpinner.setBackgroundColor(Color.WHITE);
		bottomPinSpinner.setAdapter(bottomPinAdapter); 
		back=(Button)findViewById(R.id.back_select_parts);
		back.setOnClickListener(this);
		addParts=(Button)findViewById(R.id.add_part);
		addParts.setOnClickListener(this);

		Bundle extras=getIntent().getExtras();
		if(extras!=null){
			create=false;
			inputValues = extras.getBundle("blockValues");
			topPinSpinner.setSelection(((int) inputValues.getLong("topPinType")));
			bottomPinSpinner.setSelection(((int) inputValues.getLong("bottomPinType")));
			addParts.setText("Update");
			back.setText("Back");
			inputTopBundle=inputValues.getBundle("leftTopPin");
		    inputBottomBundle=inputValues.getBundle("leftBottomPin");
			

		}	else {
			topPinSpinner.setSelection(0);
			bottomPinSpinner.setSelection(0); 
		}


	}
	@Override
	 public void onStop() {
		super.onStop();
				finish();
				
	 }
	@Override
	public void onBackPressed() {
	/*	if(create){
			Intent i = new Intent(this, PartList.class);
			startActivity(i);}
		else {
			Intent i = new Intent(this, StartView.class);
			startActivity(i);
		}*/
	}
	@Override
	public void onClick(View v) {
		if (v == addParts){
			double []input=new double[256];
			Bundle adderInput=new Bundle();	 		

			adderInput.putLong("topPinType",  topPinSpinner.getSelectedItemId());
			adderInput.putLong("bottomPinType",  bottomPinSpinner.getSelectedItemId());		   


			Bundle outPutArray=new Bundle();
			outPutArray.putDoubleArray("signalMag", input);
			outPutArray.putDoubleArray("signalPhase", input);

			PinSignalTypes adderSigType=new PinSignalTypes();

			adderInput.putSerializable("pinSignalType", adderSigType);
			
			adderInput.putBundle("rightPin", outPutArray);			
			adderInput.putString("signalType","time");

			if(create){
				adderNumber++;
				PinTypes types=new PinTypes();
				types.leftTopPin=true;
				types.leftBottomPin=true;
				types.rightPin=true;
				adderInput.putSerializable("PinTypes",types);
				adderInput.putBundle("leftTopPin", outPutArray);
				adderInput.putBundle("leftBottomPin", outPutArray);
				adderInput.putString("title", "Mixer" + " " +adderNumber);
				PartsController.createBlock(adderInput);
			} else{
				adderInput.putBundle("leftTopPin", inputTopBundle);
				adderInput.putBundle("leftBottomPin",inputBottomBundle );
				adderInput.putString("title", inputValues.getString("title"));
				PartsController.updateBlock(adderInput);
			}

			Intent i = new Intent(this, StartView.class);
			startActivity(i);
		}
		else if(v== back){			
			if(create){
				Intent i = new Intent(this, PartList.class);
				startActivity(i);}
			else {
				Intent i = new Intent(this, StartView.class);
				startActivity(i);
			}
		}
	}
}





