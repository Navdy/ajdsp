package com.prototype.ajdsp;
/*Copyright (c) 1999 Andreas Spanias, Arizona State University
JDSP Concept by Andreas Spanias

Development Team:
Director: Andreas Spanias
Research Assistants/Associates: Argyris Constantinou, Axel Clausen, Maya Tampi

For questions regarding use of this software write or
call Prof. Andreas Spanias  (spanias@asu.edu,  (480) 965 1837))
********************************************************************************

DESCRIPTION:  Implements Chebyshev type II filter design


KEY VARIABLES:
INPUTS:
LP - lowpass filter
HP - highpass filter
BP - bandpass filter
filterType - filter type (LP, HP, BP)
wp1 - passband edge frequency
ws1 - stopband edge frequency
wp2 - second passband edge frequency
ws1 - second stopband edge frequency
rp  - passband ripple
rs  - stopband ripple
OUTPUTS:
order - order of the filter
fp1 - first cut-off frequency
fp2 - second cut-off frequency
*/


class Chebyshev2 {


  public static final int LP = 0;
  public static final int HP = 1;
  public static final int BP = 2;

  private int order, nz;
  private int filterType;
  private float fp1, fp2,fN, rate, ripple;
  private double[] pReal;
  private double[] pImag;
  private double[] z;
  private double[] aCoeff;
  private double[] bCoeff;
  private double Wp1, Ws1, Wp2, Ws2, Rp, Rs, kk, WN, BW=1;


  public  Chebyshev2()
  {

    rate = 8000.0f;
    fN = 0.5f*rate;
  }

  public void setFilterParameters(int ft, double wp1, double ws1, double wp2, double ws2, double rp, double rs)
  {
	  double wa,tempord;

	  filterType=ft;
	  Wp1=Math.tan(Math.PI*wp1/2);
	  Ws1=Math.tan(Math.PI*ws1/2);
	  Wp2=Math.tan(Math.PI*wp2/2);
	  Ws2=Math.tan(Math.PI*ws2/2);
	  Rs=rs;
	  Rp=rp;
	  ripple=(float)rs;
	  // calculates band-pass (fp1-fp2) and order
	  switch (filterType){
	  case LP: wa=Ws1/Wp1;break;
	  case HP: wa=Wp1/Ws1;break;
	  case BP: wa=Math.min(Math.abs((Ws1*Ws1-Wp1*Wp2)/(Ws1*(Wp1-Wp2))),Math.abs((Ws2*Ws2-Wp1*Wp2)/(Ws2*(Wp1-Wp2))));break;
	  default: wa=0;
	  }
	  tempord=acosh(Math.sqrt((Math.pow(10,0.1*Math.abs(Rs))-1)/(Math.pow(10,0.1*Math.abs(Rp))-1)))/acosh(wa);
	  order=(int)(tempord+0.999999);


	  double new_wp=1/cosh(1/(double)(order)*acosh(Math.sqrt((Math.pow(10,0.1*Math.abs(Rs))-1)/(Math.pow(10,0.1*Math.abs(Rp))-1))));



	  if (filterType == BP)
		  order*=2;

	  switch (filterType){
	  case LP: fp1=0;fp2=(float)((2/Math.PI)*Math.atan(Wp1/new_wp));break;
	  case HP: fp1=(float)((2/Math.PI)*Math.atan(Wp1*new_wp));fp2=1;break;
	  case BP: double Wn1=(Wp1-Wp2)/(2*new_wp)+Math.sqrt(Math.pow(Wp2-Wp1,2)/(4*Math.pow(new_wp,2))+Wp1*Wp2);
			   double Wn2=Wp1*Wp2/Wn1;
			   fp1=(float)((2/Math.PI)*Math.atan(Wn1));
			   fp2=(float)((2/Math.PI)*Math.atan(Wn2));break;
	  default: fp1=0;fp2=0;
	  }
	  if (order>0 && order<=10)
		  design();
	  else
		  order=-1;

  }

  private double cosh(double x)
  {
	  return (Math.exp(x)+Math.exp(-x))/2;
  }
  private double sinh(double x)
  {
	  return (Math.exp(x)-Math.exp(-x))/2;
  }
  private double acosh(double x)
  {
	  double v;
	  v=Math.max(1,x);
	  return Math.log(x+v*Math.sqrt(Math.pow(x/v,2)-Math.pow(1/v,2)));
  }
  private double asinh(double x)
  {
	  double v;
	  v=Math.max(1,x);
	  return Math.log(x+v*Math.sqrt(Math.pow(x/v,2)+Math.pow(1/v,2)));
  }

  public int getOrder()
  {
    return order;
  }

  public double getFreq1() {
    return fp1;
  }

  public double getFreq2() {
    return fp2;
  }

  public double getACoeff(int i) {
    // returns IIR filter numerator coefficient with index i
    return aCoeff[i];
  }

  public double getBCoeff(int i) {
    // returns IIR filter denominator coefficient with index i
    return bCoeff[i];
  }

  float sqr(float x) {
    return x * x;
  }

  double sqr(double x) {
    return x * x;
  }

  boolean odd(int n) {
    // returns true if n odd
    return (n % 2) != 0;
  }




  public void locatePolesAndZeros()
  {

	  pReal  = new double[order + 1];
      pImag  = new double[order + 1];
      z      = new double[order + 1];
	  double w1=4*Math.tan(Math.PI*fp1/2);
	  double w2=4*Math.tan(Math.PI*fp2/2);
	  int n=order;
	  double[] zx = new double[2*order];
	  double[] pr = new double[2*order];
	  double[] pi = new double[2*order];


	  WN=1;
	  if (filterType==LP)
		  WN=w2;
	  else if (filterType==BP)
	  {
		  BW=w2-w1;WN=Math.sqrt(w1*w2);
	  }
	  else if (filterType==HP)
		  WN=w1;
	  double delta=1/Math.sqrt(Math.pow(10,0.1*ripple)-1);
	  double mu = asinh(1/delta)/(double)order;
	  int k;
	  if (order % 2==0)
	  {
		  k=1;
		  for (int i=1;i<=2*order-1;i+=2)
		  {
			  zx[k]=1/Math.cos(i*Math.PI/(2*(double)(order)));
			  k++;
		  }
		  nz=order;
	  }
	  else
	  {
		  nz=order-1;
		  k=1;
		  for (int i=1;i<=order-2;i+=2)
		  {
			  zx[k]=1/Math.cos(i*Math.PI/(2*(double)(order)));
			  k++;
		  }
		  for (int i=order+2;i<=2*order-1;i+=2)
		  {
			  zx[k]=1/Math.cos(i*Math.PI/(2*(double)(order)));
			  k++;
		  }
	  }
	  k=1;
	  double prt,pit;
	  for (int i=1;i<=2*order-1;i+=2)
	  {
		  prt=Math.cos(Math.PI*i/(2*(double)(order))+Math.PI/2)*sinh(mu);
		  pit=Math.sin(Math.PI*i/(2*(double)(order))+Math.PI/2)*cosh(mu);
		  pr[k]=prt/(prt*prt+pit*pit);
		  pi[k]=-pit/(prt*prt+pit*pit);
		  k++;
	  }

	  prt=-pr[1];pit=-pi[1];
	  //double zt=-zx[1];
	  double zt=1;
	  double prtn,pitn;
	  for (int i=2;i<=n;i++)
	  {
		  prtn=prt*(-pr[i])-pit*(-pi[i]);
		  pitn=prt*(-pi[i])+pit*(-pr[i]);
		  prt=prtn;
		  pit=pitn;
	  }
	  for (int i=1;i<=nz;i++)
		  zt=zt*(-zx[i]);


	  kk=prt/(zt*Math.pow(-1,(double)nz/2.0));

	    // rearange poles & zeros in comjugate pairs
	  if (n%2!=0)
	  {
		  int nn=n-1;
		  pReal[n]=pr[nn/2+1];pImag[n]=pi[nn/2+1];
		  int ii=1;
		  for (int i=1;i<=nn;i+=2)
		  {
			  pReal[i]=pr[nn/2+1-ii];pImag[i]=pi[nn/2+1-ii];
			  pReal[i+1]=pr[nn/2+1+ii];pImag[i+1]=pi[nn/2+1+ii];
			  ii++;
		  }
	  }
	  else
	  {
		  int ii=1;
		  for (int i=1;i<=n;i+=2)
		  {
			  pReal[i]=pr[n/2+1-ii];pImag[i]=pi[n/2+1-ii];
			  pReal[i+1]=pr[n/2+ii];pImag[i+1]=pi[n/2+ii];
			  ii++;
		  }
	  }
	  int ii=1;
	  for (int i=1;i<=nz;i+=2)
	  {
		  z[i+1]=zx[nz/2+1-ii];
		  z[i]=zx[nz/2+ii];
		  ii++;
	  }
  }

  private double[] realpoly (double r1, double i1, double r2, double i2)
  {
	  double pc[] = new double[3];
	  pc[0] = 1;
	  pc[1] = -(r1+r2);
	  pc[2] = r1*r2-i1*i2;
	  return pc;
  }

  private void design()
  {
	  UtilityFunctions uf = new UtilityFunctions();
	  aCoeff = new double[11];
      bCoeff = new double[11];
	  for (int i=0;i<=10;i++)
	  {aCoeff[i]=0;bCoeff[i]=0;}
	  if (filterType==BP)
		 order=order/2;

	  locatePolesAndZeros();
	   double a[][] = new double[order][order];
	  double b[][] = new double[order][1];
	  double c[][] = new double[1][order];
	  double d[][] = new double[1][1];
	  double num[] = new double[3];
	  double den[] = new double[3];
	  double wn;
	  int j=0;
	  int cp=0;
	  b[0][0]=1;d[0][0]=1;c[0][0]=0;
	  for (int i=0;i<order;i++)
		  for (int i2=0;i2<order;i2++)
				a[i][i2]=0;
	  for (int i=1;i<order;i++)
			 b[i][0]=1-i%2;

	  if (order % 2 !=0)
	  {
	  	 a[j][0]=pReal[order];d[0][0]=0;c[0][0]=1;
		 j++;
		 for (int i=1;i<order;i++)
			 b[i][0]=0;

	  }

	  for (int i=1;i<nz;i+=2)
	  {
		  num=realpoly(0,z[i],0,z[i+1]);
		  den=realpoly(pReal[i],pImag[i],pReal[i+1],pImag[i+1]);
		  wn=Math.sqrt(Math.sqrt(pReal[i]*pReal[i]+pImag[i]*pImag[i])*Math.sqrt(pReal[i+1]*pReal[i+1]+pImag[i+1]*pImag[i+1]));
	      if (wn==0) wn=1;
		  double[][] a1={{-den[1],-den[2]/wn},{wn,0}};
		  double[] c1={num[1]-den[1],(num[2]/wn-den[2]/wn)};
		  for (int k=0;k<=cp;k++)
			  a[j][k]=c[0][k];
		  cp++;
		  if (order%2==0 && i==1)
			  cp=cp-1;
		  for (int k=cp;k<=cp+1;k++)
			  a[j][k]=a1[0][k-cp];
		  j++;
		  for (int k=cp;k<=cp+1;k++)
			  a[j][k]=a1[1][k-cp];
		  j++;
	      for (int k=cp;k<=cp+1;k++)
			  c[0][k]=c1[k-cp];
		  cp++;
	  }
	  d[0][0]=d[0][0]*kk;
	  c[0][0]=c[0][0]*kk;
	  for (int i=1;i<order;i++)
		  c[0][i]=c[0][i]*kk;




	  if (filterType==BP) order=order*2;
	  double at[][] = new double[order][order];
	  double bt[][] = new double[order][1];
	  double ct[][] = new double[1][order];
	  double dt[][] = new double[1][1];
	  int n=order;

	  if (filterType==LP)
	  {
		  bt=uf.sxM(b,WN,n,1);
		  at=uf.sxM(a,WN,n,n);
		  for (int i=0;i<order;i++)
			  ct[0][i]=c[0][i];
		  dt[0][0]=d[0][0];
	  }
	  else if (filterType==HP)
	  {
		  double[][] dtemp=uf.MxM(uf.MxM(c,uf.invM(a,n),1,n,n),b,1,n,1);
		  dt[0][0]=d[0][0]-dtemp[0][0];
		  ct=uf.MxM(c,uf.invM(a,n),1,n,n);
		  bt=uf.sxM(uf.MxM(uf.invM(a,n),b,n,n,1),-WN,n,1);
		  at=uf.sxM(uf.invM(a,n),WN,n,n);
	  }
	  else if (filterType==BP)
	  {
		  double q=WN/BW;
		  for (int i=0;i<order;i++)
			  for (j=0;j<order;j++)
				at[i][j]=0;
		  for (int i=0;i<order/2;i++)
		  {
			  bt[i][0]=WN*b[i][0]/q;bt[i+order/2][0]=0;
			  ct[0][i]=c[0][i];ct[0][i+order/2]=0;
		  }
		  for (int i=0;i<order/2;i++)
			  for (j=0;j<order/2;j++)
			  at[i][j]=WN*a[i][j]/q;
		  for (int i=0;i<order/2;i++)
		  {
			  at[i+order/2][i]=-WN;
			  at[i][i+order/2]=WN;
		  }
		  dt[0][0]=d[0][0];

	  }
	  // bilinear transformation
	  double ab[][] = new double[order][order];
	  double bb[][] = new double[order][1];
	  double cb[][] = new double[1][order];
	  double db[][] = new double[1][1];
	  double t=0.5;
	  double r= Math.sqrt(t);
	  double[][] t1 = new double[order][order];
	  double[][] t2 = new double[order][order];
	  for (int i=0;i<order;i++)
	  {
		  for (j=0;j<order;j++)
		  {
			  t1[i][j]=at[i][j]*t/2;
			  t2[i][j]=-at[i][j]*t/2;
			  if (i==j)
			  {t1[i][j]++;t2[i][j]++;}
		  }

		  ab=uf.MxM(uf.invM(t2,order),t1,order,order,order);
		  bb=uf.sxM(uf.MxM(uf.invM(t2,order),bt,order,order,1),t/r,order,1);
		  cb=uf.sxM(uf.MxM(ct,uf.invM(t2,order),1,order,order),r,1,order);
		  db=uf.MxM(uf.MxM(ct,uf.invM(t2,order),1,order,order),bt,1,order,1);
		  db[0][0]=db[0][0]*t/2+dt[0][0];
	  }
	  aCoeff=uf.polyMat(ab,order);
	  bCoeff=uf.polyMat(uf.MmM(ab,uf.MxM(bb,cb,order,1,order),order,order),order);
	  for (int i=0;i<=order;i++)
		  bCoeff[i]=bCoeff[i]+(db[0][0]-1)*aCoeff[i];

  }




}
