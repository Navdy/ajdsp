package com.prototype.ajdsp;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.widget.Button;
import android.widget.RelativeLayout;
import android.widget.TextView;

public class InfoView extends Activity implements OnClickListener{
	
	RelativeLayout layout;
	TextView disclaimerText;
	Button back;
	TextView infoLink;
	Bundle inputValues;
	String returnClass;
	
	@Override
    public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		//requestWindowFeature(Window.FEATURE_CUSTOM_TITLE);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.info_view);
		Bundle extras = getIntent().getExtras();
		//getWindow().setFeatureInt(Window.FEATURE_CUSTOM_TITLE, R.layout.row);
		layout=(RelativeLayout)findViewById(R.id.infoPage);
		layout.setBackgroundColor(Color.BLACK);
		
		
		//disclaimerText = (TextView)findViewById(R.id.disclaimerText);
		//infoLink = (TextView)findViewById(R.id.infoLink);
		//infoLink.setOnClickListener(this);
	
		back=(Button) findViewById(R.id.back_select_parts_black);
		back.setOnClickListener(this);
		
		if(extras !=null)
		{			
			inputValues = extras.getBundle("blockValues");
			returnClass=inputValues.getString("returnClass");
		}
		
	}
	
	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		if (v == infoLink){
			String url = "http://jdsp.asu.edu";
			Intent i = new Intent(Intent.ACTION_VIEW);
			i.setData(Uri.parse(url));
			startActivity(i);
		     }
		else
			if(v == back){
				
				if(returnClass.equalsIgnoreCase("StartView")){
					Intent i = new Intent(this, StartView.class);
					startActivity(i);
				}else
			    if(returnClass.equalsIgnoreCase("SplashView")){
						Intent i = new Intent(this, SplashView.class);
						startActivity(i);
				}
		
			}
	}
	
	
	 @Override
	 public void onStop() {
		 super.onStop();	
				finish();
				
	 }
	 
	 @Override
	    public void onBackPressed() {
	    /*	Intent i = new Intent(this, MainActivity.class);
	        startActivity(i);*/
	    }
}
