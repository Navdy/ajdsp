package com.prototype.ajdsp;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.Toast;


import com.prototype.ajdsp.FIRDesignMenu.FilterTypeOnItemSelectedListener;
import com.prototype.ajdsp.PartObject.PinSignalTypes;
import com.prototype.ajdsp.PartObject.PinTypes;

public class IIRDesignMenu extends Activity implements OnClickListener {

	RelativeLayout layout;
	RelativeLayout topbar;
	Button addParts;
	Button back;
	EditText pb;
	EditText sb;
	EditText Wp1;
	EditText Ws1;
	EditText Wp2;
	EditText Ws2;

	Bundle inputValues;
	TextView Wp1Text;
	TextView Ws1Text;
	TextView sbText;
	TextView cutOffText;
	TextView Wp2Text;
	TextView Ws2Text;


	Spinner filterTypeSpinner;
	Spinner iirTypeSpinner;
	Boolean create=true,update=false;


	public static int iirDesignNumber=0;
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		//requestWindowFeature(Window.FEATURE_CUSTOM_TITLE);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
		setContentView(R.layout.iir_design_layout);
		//getWindow().setFeatureInt(Window.FEATURE_CUSTOM_TITLE, R.layout.row);
		layout=(RelativeLayout)findViewById(R.id.iirDesignMenu);
		layout.setBackgroundColor(Color.WHITE);
		back=(Button) findViewById(R.id.back_select_parts);
		back.setOnClickListener(this);
		TextView title= (TextView)findViewById(R.id.Title);
		title.setText("IIR Design");
	addParts=(Button) findViewById(R.id.add_part);
		addParts.setOnClickListener(this);
	


		filterTypeSpinner = (Spinner) findViewById(R.id.iirDesignFilterType);
		ArrayAdapter<CharSequence> filterTypeAdapter = ArrayAdapter.createFromResource(
				this, R.array.filter_type_array_iir, android.R.layout.simple_spinner_item);
		filterTypeAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		//filterTypeSpinner.setBackgroundColor(Color.WHITE);
		filterTypeSpinner.setAdapter(filterTypeAdapter);
		filterTypeSpinner.setOnItemSelectedListener(new MyOnItemSelectedListener());


		iirTypeSpinner = (Spinner) findViewById(R.id.iirFilterType);
		ArrayAdapter<CharSequence> iirTypeAdapter = ArrayAdapter.createFromResource(
				this, R.array.iir_type_array, android.R.layout.simple_spinner_item);
		iirTypeAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		//iirTypeSpinner.setBackgroundColor(Color.WHITE);
		iirTypeSpinner.setAdapter(iirTypeAdapter);
		

		Wp1=(EditText)findViewById(R.id.Wp1iirDesign);
		Ws1=(EditText)findViewById(R.id.Ws1IirDesign);
		Wp2=(EditText)findViewById(R.id.Wp2IirDesign);
		Ws2=(EditText)findViewById(R.id.Ws2IirDesign);
		Wp2Text=(TextView)findViewById(R.id.Wp2IirDesignText);
		Ws2Text=(TextView)findViewById(R.id.Ws2IirDesignText);
		pb=(EditText) findViewById(R.id.pbIirDesign);
		sb=(EditText) findViewById(R.id.sbIirDesign);




	


		Bundle extras = getIntent().getExtras(); 
		if(extras !=null)
		{
			inputValues = extras.getBundle("blockValues");
			filterTypeSpinner.setSelection(inputValues.getInt("filterType"));
			iirTypeSpinner.setSelection((int) inputValues.getLong("iirType"));
			Double pbValue=inputValues.getDouble("pb");
			Double sbValue= inputValues.getDouble("sb");
			Double Wp1Value= inputValues.getDouble("Wp1");
			Double Ws1Value=inputValues.getDouble("Ws1");
			Double Wp2Value= inputValues.getDouble("Wp2");
			Double Ws2Value=inputValues.getDouble("Ws2");
			
			pb.setText(pbValue.toString());
			sb.setText(sbValue.toString());
			Wp1.setText(Wp1Value.toString());
			Ws1.setText(Ws1Value.toString());
			Wp2.setText(Wp2Value.toString());
			Ws2.setText(Ws2Value.toString());
			create=false;
			update=true;
			addParts.setText("Update");
			back.setText("Back");



		}else{

			filterTypeSpinner.setSelection(0);
			iirTypeSpinner.setSelection(0);

		Double pbValue=5.0;
			Double sbValue=20.0;
			/*Double Wp1Value= 0.4;
			Double Ws1Value=0.2;
			Double Wp2Value= 0.6;
			Double Ws2Value=0.8;*/


			pb.setText(pbValue.toString());
			sb.setText(sbValue.toString());
			/*Wp1.setText(Wp1Value.toString());
			Ws1.setText(Ws1Value.toString());
			Wp2.setText(Wp2Value.toString());
			Ws2.setText(Ws2Value.toString());*/
		}


		



	}
	public class MyOnItemSelectedListener implements OnItemSelectedListener {

		public void onItemSelected(AdapterView<?> parent,
				View view, int pos, long id) {
			Wp2Text.setVisibility(View.GONE);
			Ws2Text.setVisibility(View.GONE);
			Wp2.setVisibility(View.GONE);
			Ws2.setVisibility(View.GONE);

			switch((int)id){
			case 0:
				if((!update)||(inputValues.getInt("filterType")!=0))	{
				Wp1.setText(".2");
				Ws1.setText(".4");
				}else{
					update=false;
				}
				break;
			case 1:
				if((!update)||(inputValues.getInt("filterType")!=1)){
				Wp1.setText(".4");
				Ws1.setText(".2");
				}else{
					update=false;
				}
				break;
			case 2:
				Wp2Text.setVisibility(View.VISIBLE);
				Ws2Text.setVisibility(View.VISIBLE);
				Wp2.setVisibility(View.VISIBLE);
				Ws2.setVisibility(View.VISIBLE);
				if((!update)||(inputValues.getInt("filterType")!=2)){
				Wp1.setText(".4");
				Ws1.setText(".2");
				Wp2.setText(".6");
				Ws2.setText(".8");
				}else{
					update=false;
				}
				break;

		/*	case 3:
				Wp2Text.setVisibility(View.VISIBLE);
				Ws2Text.setVisibility(View.VISIBLE);
				Wp2.setVisibility(View.VISIBLE);
				Ws2.setVisibility(View.VISIBLE);
				break;*/


			default:
				break;
			}
		}

		public void onNothingSelected(AdapterView parent) {
			// Do nothing.
		}
	}

	@Override
	public void onBackPressed() {
		/*if(create){
			Intent i = new Intent(this, PartList.class);
			startActivity(i);}
		else {
			Intent i = new Intent(this, StartView.class);
			startActivity(i);
		}*/
	}
	@Override
	 public void onStop() {
		         super.onStop();	
				finish();
				
	 }

	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		if (v == addParts){

			Bundle iirDesignInput=new Bundle();


			double pbValue=value(pb.getText().toString());
			double sbValue=value(sb.getText().toString());
			double Wp1Value=value(Wp1.getText().toString());
			double Ws1Value=value(Ws1.getText().toString());
			double Wp2Value=value(Wp2.getText().toString());
			double Ws2Value=value(Ws2.getText().toString());
			int filterType=(int)filterTypeSpinner.getSelectedItemId();


			iirDesignInput.putInt("filterType",filterType);
			iirDesignInput.putLong("iirType", iirTypeSpinner.getSelectedItemId());

			iirDesignInput.putDouble("pb",pbValue);
			iirDesignInput.putDouble("sb",sbValue);
			iirDesignInput.putDouble("Wp1", Wp1Value);
			iirDesignInput.putDouble("Ws1",Ws1Value);
			iirDesignInput.putDouble("Wp2", Wp2Value);
			iirDesignInput.putDouble("Ws2",Ws2Value);
			IIRDesignCalc iirDesignCalc=new IIRDesignCalc();
			Bundle filterCoeff =iirDesignCalc.getFilterCoeff(iirDesignInput);
			int order =iirDesignCalc.Order;
			if((filterType==0)&&(Wp1Value>=Ws1Value)){
				Toast.makeText(getApplicationContext(), "Non-Valid inputs.Allowable inputs conform to:\nWp1 < Ws1", Toast.LENGTH_LONG).show();
			}else if((filterType==1)&&(Wp1Value<=Ws1Value)){
				Toast.makeText(getApplicationContext(), "Non-Valid inputs.Allowable inputs conform to:\nWp1 > Ws1", Toast.LENGTH_LONG).show();
			} else if((filterType==2)&&((Wp1Value<=Ws1Value)||(Wp2Value>=Ws2Value)||(Wp1Value>=Wp2Value))){
				Toast.makeText(getApplicationContext(), "Non-Valid inputs.Allowable inputs conform to:\nWp1 > Ws1 \n Wp2 < Ws2 \n Wp1 < Wp2", Toast.LENGTH_LONG).show();
			} 
			else if(order==-1){
				Toast.makeText(getApplicationContext(), "Non-Valid filter order.\nMaximum order = 64 \n Please redefine parameters.", Toast.LENGTH_LONG).show();
			} else if(pbValue>=sbValue){
				Toast.makeText(getApplicationContext(), "Invalid Tolerace Values. PB < SB", Toast.LENGTH_LONG).show();
			} else if((Wp1Value>1)||(Ws1Value>1)||(Wp2Value>1)||(Ws2Value>1)){
				Toast.makeText(getApplicationContext(), "Cut-off frequencies must be less than 1", Toast.LENGTH_LONG).show();
			}
			else{

			
			iirDesignInput.putBundle("topPin", filterCoeff);
			if(create){
				iirDesignNumber=iirDesignNumber+1;
				PinSignalTypes iirSigType=new PinSignalTypes();
				iirSigType.topPin="frequency";
				iirDesignInput.putSerializable("pinSignalType", iirSigType);
				PinTypes types=new PinTypes();
				types.topPin=true;
				iirDesignInput.putSerializable("PinTypes",types);
				iirDesignInput.putString("title", "IIR Design" +" "+iirDesignNumber);
				PartsController.createBlock(iirDesignInput);
			} else{
				iirDesignInput.putString("title", inputValues.getString("title"));
				PartsController.updateBlock(iirDesignInput);
			}

			Intent i = new Intent(this, StartView.class);
			startActivity(i);
			}
		}
		else if (v == back){
			if(create){
				Intent i = new Intent(this, PartList.class);
				startActivity(i);}
			else {
				Intent i = new Intent(this, StartView.class);
				startActivity(i);
			}

		}

	}
	 private Double value(String input){
		  if (input.equalsIgnoreCase("")){
			  return 0.0;
		  }
		  else return Double.parseDouble(input);
	  }
	
	public void orderDialog(){
		Toast.makeText(getApplicationContext(), "Max Order of the Filter is 64", Toast.LENGTH_LONG).show();

	}
}

