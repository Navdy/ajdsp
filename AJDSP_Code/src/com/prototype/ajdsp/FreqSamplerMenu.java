package com.prototype.ajdsp;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.TextView;

import com.prototype.ajdsp.PartObject.PinTypes;


public class FreqSamplerMenu extends Activity implements OnClickListener {
	RelativeLayout layout;
	RelativeLayout topbar;
	Button draw;
	Button back;

	Bundle inputValues;
	Spinner normalSpinner;
	TextView normalModeText;
	TextView modeText;



	Spinner lineSegmentSpinner;
	Spinner samplesSpinner;
	Spinner filterTypeSpinner;
	Spinner modeSpinner;
	public int lineSegments;
	public int samples,filterType,mode;
	public float [] coordinates;
	static Boolean create=true;




	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		//requestWindowFeature(Window.FEATURE_CUSTOM_TITLE);
		setContentView(R.layout.freq_sampling_menu);
		//getWindow().setFeatureInt(Window.FEATURE_CUSTOM_TITLE, R.layout.row);
		layout=(RelativeLayout)findViewById(R.id.freqSampler_menu);
		layout.setBackgroundColor(Color.WHITE);
		
		normalSpinner = (Spinner) findViewById(R.id.freqSamplerNormalModeType);
		ArrayAdapter<CharSequence> normalSegmentAdapter = ArrayAdapter.createFromResource(
				this, R.array.freqResp_Norm_Mode, android.R.layout.simple_spinner_item);
		normalSegmentAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		normalSpinner.setBackgroundColor(Color.WHITE);
		normalSpinner.setAdapter(normalSegmentAdapter);
 
		back=(Button) findViewById(R.id.back_select_parts);
		back.setOnClickListener(this);
		
	    draw=(Button) findViewById(R.id.add_part);
		draw.setOnClickListener(this);
		draw.setText("Draw");

		normalModeText=(TextView)findViewById(R.id.freqSamplerNormalModeText);
		modeText=(TextView)findViewById(R.id.freqSamplerModeText);

		lineSegmentSpinner = (Spinner) findViewById(R.id.freqSamplerLineSegment);
		ArrayAdapter<CharSequence> lineSegmentAdapter = ArrayAdapter.createFromResource(
				this, R.array.freqsamp_line_segment, android.R.layout.simple_spinner_item);
		lineSegmentAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		lineSegmentSpinner.setBackgroundColor(Color.WHITE);
		lineSegmentSpinner.setAdapter(lineSegmentAdapter);

		samplesSpinner = (Spinner) findViewById(R.id.freqSamplerSamples);
		ArrayAdapter<CharSequence> samplesAdapter = ArrayAdapter.createFromResource(
				this, R.array.freqSampling_samples, android.R.layout.simple_spinner_item);
		samplesAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		samplesSpinner.setBackgroundColor(Color.WHITE);
		samplesSpinner.setAdapter(samplesAdapter); 
		modeSpinner = (Spinner) findViewById(R.id.freqSamplerModeType);
		ArrayAdapter<CharSequence> modeAdapter = ArrayAdapter.createFromResource(
				this, R.array.freqResp_Mode, android.R.layout.simple_spinner_item);
		modeAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		modeSpinner.setBackgroundColor(Color.WHITE);
		modeSpinner.setAdapter(modeAdapter);

		filterTypeSpinner = (Spinner) findViewById(R.id.freqSamplerFilterType);
		ArrayAdapter<CharSequence> filterTypeAdapter = ArrayAdapter.createFromResource(
				this, R.array.freqsamp_filterType, android.R.layout.simple_spinner_item);
		filterTypeAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		filterTypeSpinner.setBackgroundColor(Color.WHITE);
		filterTypeSpinner.setAdapter(filterTypeAdapter);
		filterTypeSpinner.setOnItemSelectedListener(new MyOnItemSelectedListener());
		Bundle extras = getIntent().getExtras(); 

		if(extras !=null)
		{
			inputValues= extras.getBundle("blockValues");
			if(inputValues!=null){
				lineSegmentSpinner.setSelection((int) inputValues.getInt("numLinSeg")-1);
				filterTypeSpinner.setSelection((int) inputValues.getInt("filterType"));
				modeSpinner.setSelection((int) inputValues.getInt("mode"));
				samplesSpinner.setSelection((int) inputValues.getInt("numSamp"));
				coordinates=inputValues.getFloatArray("freqSampData");
				create=false;
			} else {
				inputValues= extras.getBundle("backValues");
				lineSegmentSpinner.setSelection((int) inputValues.getInt("lineSegmentsId")-1);
				filterTypeSpinner.setSelection((int) inputValues.getInt("filterTypeId"));
				modeSpinner.setSelection((int) inputValues.getInt("modeId"));
				samplesSpinner.setSelection((int) inputValues.getInt("samplesId"));
				coordinates=inputValues.getFloatArray("freqSampData");

			}


		} else{


			lineSegmentSpinner.setSelection(0);
			filterTypeSpinner.setSelection(0);
			modeSpinner.setSelection(0);
			samplesSpinner.setSelection(0);	
		}

		

	}
	@Override
	 public void onStop() {
		        super.onStop();	
				finish();
				
	 }
	public class MyOnItemSelectedListener implements OnItemSelectedListener {

		public void onItemSelected(AdapterView<?> parent,
				View view, int pos, long id) {
			modeSpinner.setVisibility(View.GONE); 
			modeText.setVisibility(View.GONE);
			normalSpinner.setVisibility(View.VISIBLE);
			normalModeText.setVisibility(View.VISIBLE);
			switch((int)id){
			case 1:
				modeSpinner.setVisibility(View.VISIBLE); 
				modeText.setVisibility(View.VISIBLE);
				normalSpinner.setVisibility(View.GONE);
				normalModeText.setVisibility(View.GONE);

				break;

			default:
				break;
			}
		}

		public void onNothingSelected(AdapterView parent) {
			// Do nothing.
		}
	}

	@Override
	public void onBackPressed() {
		/*if(create){
			Intent i = new Intent(this, PartList.class);
			startActivity(i);}
		else {
			Intent i = new Intent(this, StartView.class);
			startActivity(i);
		}*/
	}
	@Override
	public void onClick(View v) {
		if (v == draw){
			Bundle freqSampValues=new Bundle();		
			lineSegments=(int)lineSegmentSpinner.getSelectedItemId()+1;
			samples=(int)samplesSpinner.getSelectedItemId();
			mode=(int)modeSpinner.getSelectedItemId();
			filterType=(int)filterTypeSpinner.getSelectedItemId();
			freqSampValues.putInt("lineSegmentsId", lineSegments);
			freqSampValues.putInt("samplesId", samples);
			freqSampValues.putInt("modeId", mode);
			freqSampValues.putInt("filterTypeId", filterType);
			if(!create){
				freqSampValues.putString("title","Freq Samp"+FreqSamplerDraw.freqSamplerNumber);
				freqSampValues.putFloatArray("coordinatesArray", coordinates);

			}

			Intent i = new Intent(this, FreqSamplerDraw.class);
			i.putExtra("parameterValues",freqSampValues );
			startActivity(i);
		}
		else if(v== back){			
			if(create){
				Intent i = new Intent(this, PartList.class);
				startActivity(i);}
			else {
				Intent i = new Intent(this, StartView.class);
				startActivity(i);
			}
		}
	}

}



