package com.prototype.ajdsp;


import android.app.Activity;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.view.ViewPager.LayoutParams;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;

	public class SplashView extends Activity implements OnClickListener{
	
		LinearLayout layout;
		//RelativeLayout bottombar;
	    Button delall;
		Button tips;
		Button facebookLogin;
		SplashImage imageView;
	    ImageView imageInfo;
		//public int buttonHeight;
		ImageView plusView;
		
		
	    @Override
	    public void onCreate(Bundle savedInstanceState) {
	    	super.onCreate(savedInstanceState);
	    	requestWindowFeature(Window.FEATURE_NO_TITLE);
	        setContentView(R.layout.splash_view);
	        layout= (LinearLayout) findViewById(R.id.home_screen);
	        layout.setBackgroundColor(Color.WHITE);
	        imageView= new SplashImage(this) ;
	        
	        imageView.setOnClickListener(this);
	        layout.addView(imageView, 0,new LinearLayout.LayoutParams(LayoutParams.WRAP_CONTENT,LayoutParams.WRAP_CONTENT));
	     //   layout.addView(imageView, 0);
	        plusView= (ImageView) findViewById(R.id.add_plus);
	        plusView.setOnClickListener(this);
	        
	        imageInfo=(ImageView)findViewById(R.id.info_view);
	        imageInfo.setOnClickListener(this);
	      //  facebookLogin=(Button)findViewById(R.id.fb_login);
	        //facebookLogin.setOnClickListener(this);
	        
	        
	        
	        tips=(Button)findViewById(R.id.tips_button);
	        tips.setText("Tips");
	        tips.setOnClickListener(this);  
	        delall=(Button) findViewById(R.id.delete_all);
	        delall.setText("Del All");
			delall.setOnClickListener(this);
			
			
	        
			 // 
	        
	    }

	  /*  @Override
	    public boolean onCreateOptionsMenu(Menu menu) {
	        getMenuInflater().inflate(R.menu.activity_main, menu);
	        return true;
	    }*/
	
	/*    public SplashView(final Context context, final AttributeSet attrs) {
			this(context, attrs, 0);
		}
	    
	    public SplashView(final Context context, final AttributeSet attrs,
				final int defStyle) {
			super();   
		   
		    buttonHeight=BitmapFactory.decodeResource(getResources(), R.drawable.test).getHeight();
	    
	    }*/
	    
	    
	    
	   
/*
		@Override
		protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
			super.onMeasure(widthMeasureSpec, heightMeasureSpec);
	       //String parentClass=  getContext().getClass().getName();
	       //int height=StartView.button_height;
			int parentWidth = MeasureSpec.getSize(widthMeasureSpec);
			int parentHeight = MeasureSpec.getSize(heightMeasureSpec);
			this.setMeasuredDimension(
				parentWidth, parentHeight-buttonHeight);
			
		}*/

	    @Override
		 public void onStop() {
			 super.onStop();	
					finish();
					
		 }
	    
	    @Override
	    public void onBackPressed() {
	    /*	Intent i = new Intent(this, MainActivity.class);
	        startActivity(i);*/
	    }

	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		/*	if (v == imageView) {
			Intent i = new Intent(this, StartView.class);
			startActivity(i);
			// PartsController partsController =new PartsController(this);
		} else*/

		if (v == plusView) {
			Intent i = new Intent(this, PartList.class);
			startActivity(i);
		} else

		if (v == delall) {
			// do nothing.

			/*
			 * AllPartDeleteDialog allPartDelete = new
			 * AllPartDeleteDialog(this,partView);
			 * 
			 * allPartDelete.setCancelable(true); allPartDelete.setTitle(
			 * "This will delete all of the blocks. Are you sure?");
			 * 
			 * allPartDelete.show(); WindowManager.LayoutParams lp = new
			 * WindowManager.LayoutParams();
			 * lp.copyFrom(allPartDelete.getWindow().getAttributes());
			 * 
			 * 
			 * 
			 * lp.flags &= ~WindowManager.LayoutParams.FLAG_DIM_BEHIND;
			 * 
			 * 
			 * allPartDelete.getWindow().setAttributes(lp);
			 */

		} else if (v == imageInfo) {
			Intent i = new Intent(this, InfoView.class);// specify the info view
														// class that will be
														// created
			Bundle splashValues = new Bundle();
			splashValues.putString("returnClass", "SplashView");
			i.putExtra("blockValues", splashValues);
			startActivity(i);
			// PartsController partsController =new PartsController(this);
		} else

		if (v == tips) {
			Intent i = new Intent(this, TipsView.class);
			Bundle splashValues = new Bundle();
			splashValues.putString("returnClass", "SplashView");
			i.putExtra("blockValues", splashValues);
			startActivity(i);
			// PartsController partsController =new PartsController(this);
		}

	}
	}

	
	

