package com.prototype.ajdsp;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;

import com.prototype.ajdsp.InputSignalType.SequenceData;
import com.prototype.ajdsp.InputSignalType.SequenceDataList;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Paint.Style;
import android.os.Bundle;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

public class ConvDemoContDisplay extends Activity implements OnClickListener {
	LinearLayout layout;
	RelativeLayout topBar;
	MyContSurfaceView plotView;
	Bundle inputValues;
	Button back;
	public int  i=0;
	public List<SequenceData> sequenceList=new ArrayList<SequenceData>();
	public double []input1=new double[17*2];
	public double []input2=new double[17*2];
	public double []output;
	public Double maxOutput;
	public Double maxInput;
	public Double ratio;
	private Object mPauseLock = new Object();  
    private boolean mPaused;
    public boolean backCalled=false;
    public String outputType;
    public String input1Type;
    public String input2Type;
    Bundle spinnerData=new Bundle();
    Button add;
    
    TextView title;
    int width,height,step;
	@Override

	public void onCreate(Bundle savedInstanceState) {
		//requestWindowFeature(Window.FEATURE_CUSTOM_TITLE);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
		super.onCreate(savedInstanceState);
		//getWindow().setFeatureInt(Window.FEATURE_CUSTOM_TITLE, R.layout.row);
		setContentView(R.layout.convdemoplot);
		layout=(LinearLayout)findViewById(R.id.conv_demo);
		layout.setBackgroundColor(Color.WHITE);
		
		Bundle extras = getIntent().getExtras();
		title= (TextView)findViewById(R.id.Title);
		title.setText("Conv Demo");
		 add=(Button) findViewById(R.id.add_part);
		 add.setText("Stop");
		 add.setOnClickListener(this);
		back=(Button) findViewById(R.id.back_select_parts);
		back.setOnClickListener(this);

		if(extras !=null)
		{
			inputValues = extras.getBundle("inputValues");
			//SequenceDataList sequence=	 (SequenceDataList) inputValues.getSerializable("sequenceData");
			input1=inputValues.getDoubleArray("input1"); 
			input2=inputValues.getDoubleArray("input2");
			spinnerData=inputValues.getBundle("spinnerData");
            for(int i=0;i<input1.length;i++){
            	if((i==0||i==(input1.length-1)/2||i==(input1.length-1))){
            		if((input1[i]==.01))
            			input1[i]=1;
            		if((input2[i]==.01))
            			input2[i]=1;
            	}
            		
            }
			output=inputValues.getDoubleArray("output");
			outputType=inputValues.getString("outputType");
			input1Type=inputValues.getString("input1Type");
			input2Type=inputValues.getString("input2Type");
			maxInput=max(input1);
			maxOutput= max(output);		
		
			sequenceList=InputSignalType.contSequence;
		} 
		plotView = new MyContSurfaceView(this);
		layout.addView(plotView); 

	}
	@Override
	public void onBackPressed() {
		/*if(back){
			Intent i = new Intent(this, PartList.class);
			startActivity(i);}
		else {
			Intent i = new Intent(this, StartView.class);
			startActivity(i);
		}
					
		Intent i = new Intent(this, InputSignalType.class);
		i.putExtra("signalType", "continuous");
		startActivity(i);

		}
	}*/
	}
	public double max(double[] t) {
		double maximum = t[0];   // start with the first value
		for (int i=1; i<t.length; i++) {
			if (t[i] > maximum) {
				maximum = t[i];   // new maximum
			}
		}
		DecimalFormat twoDForm = new DecimalFormat("#.##");
		return Double.valueOf(twoDForm.format(maximum));
		
	}
	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		if(v==back)
		{
			backCalled=true;
			 sequenceList=new ArrayList<SequenceData>();
			Intent i = new Intent(this, InputSignalType.class);
		
			i.putExtra("signalType", "continuous");
			i.putExtra("spinnerData", spinnerData);
			finish();
			startActivity(i);
			
		} else if(v==add)
				{
					backCalled=true;
					Intent i = new Intent(this, StartView.class);			
				    sequenceList=new ArrayList<SequenceData>();
					finish();
					startActivity(i);
				}
		

	}
	
	class MyContSurfaceThread extends Thread {
		private SurfaceHolder myThreadSurfaceHolder;
		private MyContSurfaceView view;
		private boolean myThreadRun = false;
		public MyContSurfaceThread(SurfaceHolder surfaceHolder, MyContSurfaceView myContSurfaceView) {
			myThreadSurfaceHolder = surfaceHolder;
			view = myContSurfaceView;
		}
		public void setRunning(boolean b) {
			myThreadRun = b;
		}
		@Override
		public void run() {
			while (myThreadRun&&!backCalled) {
				Canvas c = null;
				try {
					c = myThreadSurfaceHolder.lockCanvas(null);
					synchronized (myThreadSurfaceHolder) {						
						if(i<sequenceList.size()){
							view.getSequenceData(sequenceList.get(i));
							i++;	
						}	
						
						view.onDraw(c);
						Thread.sleep(50);
						
						
					}
					
					 synchronized (mPauseLock) {
		                    while (mPaused) {                       
		                            mPauseLock.wait();
		                        
		                    }
		                }
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				finally {
					if (c != null) {
						myThreadSurfaceHolder.unlockCanvasAndPost(c);
					}
				}
			}
		}
		 public void onPause() {
		        synchronized (mPauseLock) {
		            mPaused = true;
		        }
		    }
		    public void onResume() {
		        synchronized (mPauseLock) {
		            mPaused = false;
		            mPauseLock.notifyAll();
		        }
		    }
	}
	@Override
	 public void onStop() {
		super.onStop();
		 sequenceList=new ArrayList<SequenceData>();
				finish();
				
	 }
	class MyContSurfaceView extends SurfaceView implements SurfaceHolder.Callback{
		private MyContSurfaceThread thread;
		Paint paintAxes=new Paint();
		Paint paintInput1 = new Paint();
		Paint paintInput2 = new Paint();
		Paint paintOutput = new Paint();
		SequenceData sequenceData=new SequenceData();
		float[] xAxis=new float[input1.length*4];
		int []xCoordFlip;
		double[]inputArrayFlip;
		int k=0;
		@Override
		protected void onDraw(Canvas canvas){
			if(canvas!=null){
			canvas.drawColor(Color.WHITE);
			height=getHeight();
			ratio=.85*height/Math.max(maxInput,maxOutput);
			if(maxInput.doubleValue()>=maxOutput.doubleValue())
				ratio=ratio/2;
			
			canvas.drawLine(width/2, 0, width/2, height, paintAxes);
			canvas.drawLine(0, 9*height/10, width, 9*height/10, paintAxes);
		//	canvas.drawLine((float)(width/2-5),(float)(.9*height-ratio*maxOutput),(float)(width/2+5),(float)(.9*height-ratio*maxOutput),paintOutput);
			// canvas.drawText(maxOutput.toString(), (float)(width/2-5-paintAxes.measureText(maxOutput.toString())),(float)(.9*height-ratio*maxOutput+paintAxes.getTextSize()/2), paintOutput);
			canvas.drawLine((float)(width/2-5),(float)((.9*height-(ratio*maxInput))),(float)(width/2+5),(float)(.9*height-(ratio*maxInput)),paintInput1);
			canvas.drawText(maxInput.toString(), (float)(width/2-5-paintAxes.measureText(maxOutput.toString())),(float)(.9*height-(ratio*maxInput)+paintAxes.getTextSize()/2), paintInput1);
			
		/*	canvas.drawLine((float)(width/2),(float)(9*height/10+5),(float)(width/2),(float)(9*height/10-5),paintAxes);
			canvas.drawText("0", (float)(width/2-paintAxes.measureText("0")),(float)(.9*height+paintAxes.getTextSize()), paintAxes);
			canvas.drawLine((float)(width/2+step*80),(float)(9*height/10+5),(float)(width/2+step*80),(float)(9*height/10-5),paintAxes);
			canvas.drawText("80", (float)(width/2+step*80-paintAxes.measureText("80")/2),(float)(.9*height+paintAxes.getTextSize()), paintAxes);
			canvas.drawLine((float)(width/2-step*80),(float)(9*height/10+5),(float)(width/2-step*80),(float)(9*height/10-5),paintAxes);
			canvas.drawText("-80", (float)(width/2-step*80-paintAxes.measureText("-80")/2),(float)(.9*height+paintAxes.getTextSize()), paintAxes);
			canvas.drawLine((float)(width/2+step*120),(float)(9*height/10+5),(float)(width/2+step*120),(float)(9*height/10-5),paintAxes);
			canvas.drawText("120", (float)(width/2+step*120-paintAxes.measureText("120")/2),(float)(.9*height+paintAxes.getTextSize()), paintAxes);
			canvas.drawLine((float)(width/2-step*120),(float)(9*height/10+5),(float)(width/2-step*120),(float)(9*height/10-5),paintAxes);
			canvas.drawText("-120", (float)(width/2-step*120-paintAxes.measureText("-120")/2),(float)(.9*height+paintAxes.getTextSize()), paintAxes);
			canvas.drawLine((float)(width/2+step*160),(float)(9*height/10+5),(float)(width/2+step*160),(float)(9*height/10-5),paintAxes);
			canvas.drawText("160", (float)(width/2+step*160-paintAxes.measureText("160")/2),(float)(.9*height+paintAxes.getTextSize()), paintAxes);
			canvas.drawLine((float)(width/2-step*160),(float)(9*height/10+5),(float)(width/2-step*160),(float)(9*height/10-5),paintAxes);
			canvas.drawText("-160", (float)(width/2-step*160-paintAxes.measureText("-160")/2),(float)(.9*height+paintAxes.getTextSize()), paintAxes);
			canvas.drawLine((float)(width/2+step*40),(float)(9*height/10+5),(float)(width/2+step*40),(float)(9*height/10-5),paintAxes);
			canvas.drawText("40", (float)(width/2+step*40-paintAxes.measureText("40")/2),(float)(.9*height+paintAxes.getTextSize()), paintAxes);
			canvas.drawLine((float)(width/2-step*40),(float)(9*height/10+5),(float)(width/2-step*40),(float)(9*height/10-5),paintAxes);
			canvas.drawText("-40", (float)(width/2-step*40-paintAxes.measureText("-40")/2),(float)(.9*height+paintAxes.getTextSize()), paintAxes);*/
			
			if(sequenceData.stage==0){
				canvas.drawLine(xAxis[input1.length],9*height/10,xAxis[input1.length],(float)(-(ratio)*input1[0]+9*height/10) ,paintInput1);
				//float oldY=0;
				for(int j=0;j<sequenceData.input1-1;j++){
               if(input1Type.equalsIgnoreCase("dr")){
				if((input1[j]==0)&&(input1[j+1]!=0)&&(j!=0)&&(j!=(input1.length-1)/2)){
						canvas.drawLine(xAxis[j+input1.length],9*height/10,xAxis[j+input1.length+1],9*height/10 ,paintInput1);
						canvas.drawLine(xAxis[j+input1.length+1],9*height/10,xAxis[j+input1.length+1],(float)(-(ratio)*input1[j+1]+9*height/10) ,paintInput1);
					}else if((input1[j]!=0)&&(input1[j+1]==0)&&(j!=input1.length-2)){
						canvas.drawLine(xAxis[j+input1.length],(float)(-(ratio)*input1[j]+9*height/10),xAxis[j+input1.length],9*height/10 ,paintInput1);
						canvas.drawLine(xAxis[j+input1.length],9*height/10,xAxis[j+input1.length+1],9*height/10 ,paintInput1);
					}else
						canvas.drawLine(xAxis[j+input1.length],(float)(-(ratio)*input1[j]+9*height/10),xAxis[j+input1.length+1],(float)(-(ratio)*input1[j+1]+9*height/10) ,paintInput1);
               }
               else
            	   canvas.drawLine(xAxis[j+input1.length],(float)(-(ratio)*input1[j]+9*height/10),xAxis[j+input1.length+1],(float)(-(ratio)*input1[j+1]+9*height/10) ,paintInput1);
					if(j==input1.length-2)
						canvas.drawLine(xAxis[input1.length+input1.length-1],9*height/10,xAxis[input1.length+input1.length-1],(float)(-(ratio)*input1[input1.length-1]+9*height/10) ,paintInput1);
              
					
					//canvas.drawLine(xAxis[j+input1.length],9*height/10-oldY,xAxis[j+input1.length],(float)(-(ratio)*input1[j]+9*height/10), paintInput1);
					//canvas.drawLine(xAxis[j+input1.length+1],9*height/10-oldY,xAxis[j+input1.length],(float)(-(ratio)*input1[j]+9*height/10), paintInput1);
					
		          // 	oldY=(float)((ratio)*input1[j]);
				}
			}else if(sequenceData.stage==1){
				canvas.drawLine(xAxis[input1.length+sequenceData.xCoord[0]],9*height/10,xAxis[input1.length+sequenceData.xCoord[0]],(float)(-(ratio)*sequenceData.inputArray[0]+9*height/10) ,paintInput1);
				for(int j=0;j<sequenceData.xCoord.length-1;j++){
					 if(input1Type.equalsIgnoreCase("dr")){
					if((sequenceData.inputArray[j]==0)&&(sequenceData.inputArray[j+1]!=0)&&(j!=0)){
						canvas.drawLine(xAxis[input1.length+sequenceData.xCoord[j]],9*height/10,xAxis[input1.length+1+sequenceData.xCoord[j]],9*height/10 ,paintInput1);
						canvas.drawLine(xAxis[input1.length+1+sequenceData.xCoord[j]],9*height/10,xAxis[input1.length+1+sequenceData.xCoord[j]],(float)(-(ratio)*sequenceData.inputArray[j+1]+9*height/10) ,paintInput1);
					}else if((sequenceData.inputArray[j]!=0)&&(sequenceData.inputArray[j+1]==0)&&(j!=input1.length-2)){
						canvas.drawLine(xAxis[input1.length+sequenceData.xCoord[j]],(float)(-(ratio)*sequenceData.inputArray[j]+9*height/10),xAxis[input1.length+sequenceData.xCoord[j]],9*height/10 ,paintInput1);
						canvas.drawLine(xAxis[input1.length+sequenceData.xCoord[j]],9*height/10,xAxis[input1.length+1+sequenceData.xCoord[j]],9*height/10 ,paintInput1);
					}
					else if((sequenceData.inputArray[j]==0)&&(sequenceData.inputArray[j+1]!=0)&&(sequenceData.xCoord.length==3)){
						canvas.drawLine(xAxis[input1.length+sequenceData.xCoord[j]],9*height/10,xAxis[input1.length+1+sequenceData.xCoord[j]],9*height/10 ,paintInput1);
						canvas.drawLine(xAxis[input1.length+1+sequenceData.xCoord[j]],9*height/10,xAxis[input1.length+1+sequenceData.xCoord[j]],(float)(-(ratio)*sequenceData.inputArray[j+1]+9*height/10) ,paintInput1);
					}else if((sequenceData.inputArray[j]!=0)&&(sequenceData.inputArray[j+1]==0)&&(sequenceData.xCoord.length==3)){
						canvas.drawLine(xAxis[input1.length+sequenceData.xCoord[j]],(float)(-(ratio)*sequenceData.inputArray[j]+9*height/10),xAxis[input1.length+sequenceData.xCoord[j]],9*height/10 ,paintInput1);
						canvas.drawLine(xAxis[input1.length+sequenceData.xCoord[j]],9*height/10,xAxis[input1.length+1+sequenceData.xCoord[j]],9*height/10 ,paintInput1);
					}else
						canvas.drawLine(xAxis[input1.length+sequenceData.xCoord[j]],(float)(-(ratio)*sequenceData.inputArray[j]+9*height/10),xAxis[input1.length+1+sequenceData.xCoord[j]],(float)(-(ratio)*sequenceData.inputArray[j+1]+9*height/10) ,paintInput1);
					 } 
					 else {
						 canvas.drawLine(xAxis[input1.length+sequenceData.xCoord[j]],(float)(-(ratio)*sequenceData.inputArray[j]+9*height/10),xAxis[input1.length+1+sequenceData.xCoord[j]],(float)(-(ratio)*sequenceData.inputArray[j+1]+9*height/10) ,paintInput1);
					 }
					if(j==sequenceData.xCoord.length-2)
						canvas.drawLine(xAxis[input1.length+sequenceData.xCoord.length+sequenceData.xCoord[0]-1],9*height/10,xAxis[input1.length+sequenceData.xCoord.length+sequenceData.xCoord[0]-1],(float)(-(ratio)*sequenceData.inputArray[sequenceData.inputArray.length-1]+9*height/10) ,paintInput1);
				}
					 
			}
			else if(sequenceData.stage==2){
				canvas.drawLine(xAxis[input1.length+sequenceData.xCoord[0]],9*height/10,xAxis[input1.length+sequenceData.xCoord[0]],(float)(-(ratio)*sequenceData.inputArray[0]+9*height/10) ,paintInput1);
				for(int j=0;j<sequenceData.xCoord.length-1;j++){
					 if(input1Type.equalsIgnoreCase("dr")){
					if((sequenceData.inputArray[j]==0)&&(sequenceData.inputArray[j+1]!=0)&&(j!=0)){
						canvas.drawLine(xAxis[input1.length+sequenceData.xCoord[j]],9*height/10,xAxis[input1.length+1+sequenceData.xCoord[j]],9*height/10 ,paintInput1);
						canvas.drawLine(xAxis[input1.length+1+sequenceData.xCoord[j]],9*height/10,xAxis[input1.length+1+sequenceData.xCoord[j]],(float)(-(ratio)*sequenceData.inputArray[j+1]+9*height/10) ,paintInput1);
					}else if((sequenceData.inputArray[j]!=0)&&(sequenceData.inputArray[j+1]==0)&&(j!=sequenceData.inputArray.length-2)){
						canvas.drawLine(xAxis[input1.length+sequenceData.xCoord[j]],(float)(-(ratio)*sequenceData.inputArray[j]+9*height/10),xAxis[input1.length+sequenceData.xCoord[j]],9*height/10 ,paintInput1);
						canvas.drawLine(xAxis[input1.length+sequenceData.xCoord[j]],9*height/10,xAxis[input1.length+1+sequenceData.xCoord[j]],9*height/10 ,paintInput1);
					}else
						canvas.drawLine(xAxis[input1.length+sequenceData.xCoord[j]],(float)(-(ratio)*sequenceData.inputArray[j]+9*height/10),xAxis[input1.length+1+sequenceData.xCoord[j]],(float)(-(ratio)*sequenceData.inputArray[j+1]+9*height/10) ,paintInput1);
					 }
					 else{
						 canvas.drawLine(xAxis[input1.length+sequenceData.xCoord[j]],(float)(-(ratio)*sequenceData.inputArray[j]+9*height/10),xAxis[input1.length+1+sequenceData.xCoord[j]],(float)(-(ratio)*sequenceData.inputArray[j+1]+9*height/10) ,paintInput1);
					 }
					if(j==sequenceData.xCoord.length-2)
						canvas.drawLine(xAxis[input1.length+sequenceData.xCoord.length+sequenceData.xCoord[0]-1],9*height/10,xAxis[input1.length+sequenceData.xCoord.length+sequenceData.xCoord[0]-1],(float)(-(ratio)*sequenceData.inputArray[sequenceData.inputArray.length-1]+9*height/10) ,paintInput1);
				}
				
			}
			else if(sequenceData.stage==3){
				canvas.drawLine(xAxis[input1.length+sequenceData.xCoord[0]],9*height/10,xAxis[input1.length+sequenceData.xCoord[0]],(float)(-(ratio)*sequenceData.inputArray[0]+9*height/10) ,paintInput1);
				for(int j=0;j<sequenceData.xCoord.length-1;j++){
					 if(input1Type.equalsIgnoreCase("dr")){
					if((sequenceData.inputArray[j]==0)&&(sequenceData.inputArray[j+1]!=0)&&(j!=0)){
						canvas.drawLine(xAxis[input1.length+sequenceData.xCoord[j]],9*height/10,xAxis[input1.length+1+sequenceData.xCoord[j]],9*height/10 ,paintInput1);
						canvas.drawLine(xAxis[input1.length+1+sequenceData.xCoord[j]],9*height/10,xAxis[input1.length+1+sequenceData.xCoord[j]],(float)(-(ratio)*sequenceData.inputArray[j+1]+9*height/10) ,paintInput1);
					}else if((sequenceData.inputArray[j]!=0)&&(sequenceData.inputArray[j+1]==0)&&(j!=sequenceData.inputArray.length-2)){
						canvas.drawLine(xAxis[input1.length+sequenceData.xCoord[j]],(float)(-(ratio)*sequenceData.inputArray[j]+9*height/10),xAxis[input1.length+sequenceData.xCoord[j]],9*height/10 ,paintInput1);
						canvas.drawLine(xAxis[input1.length+sequenceData.xCoord[j]],9*height/10,xAxis[input1.length+1+sequenceData.xCoord[j]],9*height/10 ,paintInput1);
					}else
						canvas.drawLine(xAxis[input1.length+sequenceData.xCoord[j]],(float)(-(ratio)*sequenceData.inputArray[j]+9*height/10),xAxis[input1.length+1+sequenceData.xCoord[j]],(float)(-(ratio)*sequenceData.inputArray[j+1]+9*height/10) ,paintInput1);
					 }
					 else{
						 canvas.drawLine(xAxis[input1.length+sequenceData.xCoord[j]],(float)(-(ratio)*sequenceData.inputArray[j]+9*height/10),xAxis[input1.length+1+sequenceData.xCoord[j]],(float)(-(ratio)*sequenceData.inputArray[j+1]+9*height/10) ,paintInput1); 
					 }
					if(j==sequenceData.xCoord.length-2)
						canvas.drawLine(xAxis[input1.length+sequenceData.xCoord.length+sequenceData.xCoord[0]-1],9*height/10,xAxis[input1.length+sequenceData.xCoord.length+sequenceData.xCoord[0]-1],(float)(-(ratio)*sequenceData.inputArray[sequenceData.inputArray.length-1]+9*height/10) ,paintInput1);
				}

				canvas.drawLine(xAxis[input2.length],9*height/10,xAxis[input2.length],(float)(-(ratio)*input2[0]+9*height/10) ,paintInput2);
				for(int j=0;j<sequenceData.input2-1;j++){
					 if(input2Type.equalsIgnoreCase("dr")){
					if((input2[j]==0)&&(input2[j+1]!=0)&&(j!=0)){
						
						canvas.drawLine(xAxis[j+input2.length],9*height/10,xAxis[j+input2.length+1],9*height/10 ,paintInput2);
						
						canvas.drawLine(xAxis[j+input2.length+1],9*height/10,xAxis[j+input2.length+1],(float)(-(ratio)*input2[j+1]+9*height/10) ,paintInput2);
					}else if((input2[j]!=0)&&(input2[j+1]==0)&&(j!=input2.length-2)){
						canvas.drawLine(xAxis[j+input2.length],(float)(-(ratio)*input2[j]+9*height/10),xAxis[j+input2.length],9*height/10 ,paintInput2);
						canvas.drawLine(xAxis[j+input2.length],9*height/10,xAxis[j+input2.length+1],9*height/10 ,paintInput2);
					}else
						canvas.drawLine(xAxis[j+input2.length],(float)(-(ratio)*input2[j]+9*height/10),xAxis[j+input2.length+1],(float)(-(ratio)*input2[j+1]+9*height/10) ,paintInput2);
					 }
					 else {
						 canvas.drawLine(xAxis[j+input2.length],(float)(-(ratio)*input2[j]+9*height/10),xAxis[j+input2.length+1],(float)(-(ratio)*input2[j+1]+9*height/10) ,paintInput2);
					 }
					if(j==input2.length-2)
						canvas.drawLine(xAxis[input2.length+input2.length-1],9*height/10,xAxis[input2.length+input2.length-1],(float)(-(ratio)*input2[input2.length-1]+9*height/10) ,paintInput2);
				}

			}else if(sequenceData.stage==4){
				canvas.drawLine(xAxis[input1.length+sequenceData.xCoord[0]],9*height/10,xAxis[input1.length+sequenceData.xCoord[0]],(float)(-(ratio)*sequenceData.inputArray[0]+9*height/10) ,paintInput1);
				
				for(int j=0;j<sequenceData.xCoord.length-1;j++){
					 if(input1Type.equalsIgnoreCase("dr")){
					if((sequenceData.inputArray[j]==0)&&(sequenceData.inputArray[j+1]!=0)&&(j!=0)){
						canvas.drawLine(xAxis[input1.length+sequenceData.xCoord[j]],9*height/10,xAxis[input1.length+1+sequenceData.xCoord[j]],9*height/10 ,paintInput1);
						canvas.drawLine(xAxis[input1.length+1+sequenceData.xCoord[j]],9*height/10,xAxis[input1.length+1+sequenceData.xCoord[j]],(float)(-(ratio)*sequenceData.inputArray[j+1]+9*height/10) ,paintInput1);
					}else if((sequenceData.inputArray[j]!=0)&&(sequenceData.inputArray[j+1]==0)&&(j!=sequenceData.inputArray.length-2)){
						canvas.drawLine(xAxis[input1.length+sequenceData.xCoord[j]],(float)(-(ratio)*sequenceData.inputArray[j]+9*height/10),xAxis[input1.length+sequenceData.xCoord[j]],9*height/10 ,paintInput1);
						canvas.drawLine(xAxis[input1.length+sequenceData.xCoord[j]],9*height/10,xAxis[input1.length+1+sequenceData.xCoord[j]],9*height/10 ,paintInput1);
					}else
						canvas.drawLine(xAxis[input1.length+sequenceData.xCoord[j]],(float)(-(ratio)*sequenceData.inputArray[j]+9*height/10),xAxis[input1.length+1+sequenceData.xCoord[j]],(float)(-(ratio)*sequenceData.inputArray[j+1]+9*height/10) ,paintInput1);
				} else
					canvas.drawLine(xAxis[input1.length+sequenceData.xCoord[j]],(float)(-(ratio)*sequenceData.inputArray[j]+9*height/10),xAxis[input1.length+1+sequenceData.xCoord[j]],(float)(-(ratio)*sequenceData.inputArray[j+1]+9*height/10) ,paintInput1);
					if(j==sequenceData.xCoord.length-2)
						canvas.drawLine(xAxis[input1.length+sequenceData.xCoord.length+sequenceData.xCoord[0]-1],9*height/10,xAxis[input1.length+sequenceData.xCoord.length+sequenceData.xCoord[0]-1],(float)(-(ratio)*sequenceData.inputArray[sequenceData.inputArray.length-1]+9*height/10) ,paintInput1);
				}

				canvas.drawLine(xAxis[input2.length],9*height/10,xAxis[input2.length],(float)(-(ratio)*input2[0]+9*height/10) ,paintInput2);

				for(int j=0;j<input2.length-1;j++){
					 if(input2Type.equalsIgnoreCase("dr")){
					if((input2[j]==0)&&(input2[j+1]!=0)&&(j!=0)){
						
						canvas.drawLine(xAxis[j+input2.length],9*height/10,xAxis[j+input2.length+1],9*height/10 ,paintInput2);
						canvas.drawLine(xAxis[j+input2.length+1],9*height/10,xAxis[j+input2.length+1],(float)(-(ratio)*input2[j+1]+9*height/10) ,paintInput2);
					}else if((input2[j]!=0)&&(input2[j+1]==0)&&(j!=input2.length-2)){
						canvas.drawLine(xAxis[j+input2.length],(float)(-(ratio)*input2[j]+9*height/10),xAxis[j+input2.length],9*height/10 ,paintInput2);
						canvas.drawLine(xAxis[j+input2.length],9*height/10,xAxis[j+input2.length+1],9*height/10 ,paintInput2);
					}else
						canvas.drawLine(xAxis[j+input2.length],(float)(-(ratio)*input2[j]+9*height/10),xAxis[j+input2.length+1],(float)(-(ratio)*input2[j+1]+9*height/10) ,paintInput2);
					 } else 
						 canvas.drawLine(xAxis[j+input2.length],(float)(-(ratio)*input2[j]+9*height/10),xAxis[j+input2.length+1],(float)(-(ratio)*input2[j+1]+9*height/10) ,paintInput2);
					if(j==input2.length-2)
						canvas.drawLine(xAxis[input2.length+input2.length-1],9*height/10,xAxis[input2.length+input2.length-1],(float)(-(ratio)*input2[input2.length-1]+9*height/10) ,paintInput2);
				}

				canvas.drawLine(xAxis[(input1.length-1)/2+1],9*height/10,xAxis[(input1.length-1)/2+1],(float)(-(ratio)*output[0]+9*height/10) ,paintOutput);
				for(int j=0;j<sequenceData.output-1;j++){
					if(outputType.equalsIgnoreCase("d")){
						if((output[j]==0)&&(output[j+1]!=0)&&(j!=0)){
							canvas.drawLine(xAxis[j+(input1.length-1)/2+1],9*height/10,xAxis[j+(input1.length-1)/2+1+1],9*height/10 ,paintOutput);
							canvas.drawLine(xAxis[j+(input1.length-1)/2+1+1],9*height/10,xAxis[j+(input1.length-1)/2+1+1],(float)(-(ratio)*output[j+1]+9*height/10) ,paintOutput);
						}else if((output[j]!=0)&&(output[j+1]==0)&&(j!=output.length-2)){
							canvas.drawLine(xAxis[j+(input1.length-1)/2+1],(float)(-(ratio)*output[j]+9*height/10),xAxis[j+(input1.length-1)/2+1],9*height/10 ,paintOutput);
							canvas.drawLine(xAxis[j+(input1.length-1)/2+1],9*height/10,xAxis[j+(input1.length-1)/2+1+1],9*height/10 ,paintOutput);
						}else
							canvas.drawLine(xAxis[j+(input1.length-1)/2+1],(float)(-(ratio)*output[j]+9*height/10),xAxis[j+(input1.length-1)/2+1+1],(float)(-(ratio)*output[j+1]+9*height/10) ,paintOutput);
						}else{
							canvas.drawLine(xAxis[j+(input1.length-1)/2+1],(float)(-(ratio)*output[j]+9*height/10),xAxis[j+(input1.length-1)/2+1+1],(float)(-(ratio)*output[j+1]+9*height/10) ,paintOutput);
							
						}

					if(j==output.length-2)
						canvas.drawLine(xAxis[(input1.length-1)/2+output.length],9*height/10,xAxis[(input1.length-1)/2+1+output.length-1],(float)(-(ratio)*output[output.length-1]+9*height/10) ,paintOutput);
				}
				
			} 
			
			}
		}

		public MyContSurfaceView(Context context){
			super(context);
			init();
		}

		private void init(){
			getHolder().addCallback(this);
		
			paintAxes.setColor(Color.BLACK);
			paintInput1.setColor(Color.GREEN);
		//	paintInput1.setStyle(Style.STROKE);
			paintInput1.setStrokeWidth(2);
			paintInput2.setColor(Color.BLUE);
			paintInput2.setStrokeWidth(2);
			paintOutput.setColor(Color.RED);
			paintOutput.setStrokeWidth(2);
			width=PartsController.width;
			
			
			
			step=2;
			for(int i=-(input1.length*3-1);i<=(input1.length*5-1);i=i+2){
				xAxis[(i+(input1.length*3-1))/2]=width/2+i;
			}
		}
		public void getSequenceData(SequenceData sequence){
			sequenceData =sequence;
		}

		@Override
		public void surfaceChanged(SurfaceHolder arg0, int arg1, int arg2, int arg3) {
		}
		@Override
		public void surfaceCreated(SurfaceHolder holder){
			thread = new MyContSurfaceThread(getHolder(), this);
			if (!thread.myThreadRun) {
			
	    		  thread.setRunning(true);
	              thread.start();
	    	    } else {
	    	       thread.onResume();
	    	    }
		}
		@Override
		public void surfaceDestroyed(SurfaceHolder holder){
			//boolean retry = true;
			thread.setRunning(false);
			thread.onPause();
		/*while (retry){
				try{
					thread.join();
					retry = false;
				}
				catch (InterruptedException e){}
			}*/
		}
	}

}
