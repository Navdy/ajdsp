package com.prototype.ajdsp;
/* *****************************************************************************
Copyright (c) 1999 Andreas Spanias, Arizona State University
JDSP Concept by Andreas Spanias

Development Team:
Director: Andreas Spanias
Research Assistants/Associates: Argyris Constantinou, Axel Clausen, Maya Tampi

For questions regarding use of this software write or
call Prof. Andreas Spanias  (spanias@asu.edu,  (480) 965 1837)) 
****************************************************************************** 

DESCRIPTION: Matrix manipulation matrices for IIR filter design
*/    

import java.util.*;
import java.io.*;


public class UtilityFunctions
{
	
	public double[][] invM(double[][] at,int n)
	{
		// Performs matrix inversion
		// a - iput matrix (square) n - size of matrix
	double[][] a = new double[n+1][n+1];
	for (int i=0;i<=n;i++)
		for (int j=0;j<=n;j++)
		a[i][j]=0;
	for (int i=0;i<n;i++)
		for (int j=0;j<n;j++)
		a[i+1][j+1]=at[i][j];
	double[][] bb = new double[n][n];
	int[] indxc = new int[n+1];
	int[] indxr = new int[n+1];
	int[] ipiv = new int[n+1];
	int i,icol=0,irow=0,j,k,l,ll;
	double big,dum,pivinv,temp,ta;
	for (j=1;j<=n;j++)
		ipiv[j]=0;
    for (i=1;i<=n;i++)
	{
		big=0;
		for (j=1;j<=n;j++)
		{
			if (ipiv[j]!=1)
			{
				for (k=1;k<=n;k++)
				{
					if (ipiv[k]==0)
					{
						if (Math.abs(a[j][k])>=big)
						{
							big=Math.abs(a[j][k]);
							irow=j;
							icol=k;
						}
					}
				}
			}
		}
		ipiv[icol]+=1;
		if (irow!=icol)
		{
			for (l=1;l<=n;l++) 
			{ta=a[irow][l];a[irow][l]=a[icol][l];a[icol][l]=ta;}
		}
		indxr[i]=irow;
		indxc[i]=icol;
		pivinv=1/a[icol][icol];
		a[icol][icol]=1;
		for (l=1;l<=n;l++)
			a[icol][l]*=pivinv;
		for (ll=1;ll<=n;ll++)
		{
			if(ll!=icol)
			{
				dum=a[ll][icol];
				a[ll][icol]=0;
				for (l=1;l<=n;l++)
					a[ll][l]-=a[icol][l]*dum;
			}
		}
	}
	for ( l=n;l>=1;l--)
	{
		if (indxr[l]!=indxc[l])
		{
			for (k=1;k<=n;k++)
			{ta=a[k][indxr[l]];a[k][indxr[l]]=a[k][indxc[l]];a[k][indxc[l]]=ta;}
		}
	}


	for (i=1;i<=n;i++)
		for (j=1;j<=n;j++)
		bb[i-1][j-1]=a[i][j];
	return bb;
	}

	///////////////////////////////////////////////////////////////////////////////

	
	public double[][] MxM(double a[][],double b[][], int h1, int lh, int l2)
	{
		// Matrix multiplication 
		double[][] c = new double[h1][l2];
		
		for (int i=0;i<h1;i++)
		{
			for (int j=0;j<l2;j++)
			{
				c[i][j]=0;
				for (int k=0;k<lh;k++)
				c[i][j]+=a[i][k]*b[k][j];
			}
		}
		return c;
	}

	public double[][] sxM(double a[][], double sc, int h, int l)
	{
		for (int i=0;i<h;i++)
			for (int j=0;j<l;j++)
			a[i][j]*=sc;
		return a;
	}

	public double[][] MpM(double a[][], double b[][], int h, int l)
	{
		double[][] c = new double[h][l];
		for (int i=0;i<h;i++)
			for (int j=0;j<l;j++)
			c[i][j]=a[i][j]+b[i][j];
		return c;
	}

	public double[][] MmM(double a[][], double b[][], int h, int l)
	{
		double[][] c = new double[h][l];
		for (int i=0;i<h;i++)
			for (int j=0;j<l;j++)
			c[i][j]=a[i][j]-b[i][j];
		return c;
	}

	private double[][] balance(double[][] a, int n)
	{
		double RADIX=2;
		int last,j,i;
		double s,r,g,f,c,sqrdx;
		sqrdx=RADIX*RADIX;
		last=0;
		while (last == 0) 
		{
			last=1;
			for (i=1;i<=n;i++) 
			{ 
				r=c=0.0;
				for (j=1;j<=n;j++)
				{
					if (j != i) 
					{
						c += Math.abs(a[j][i]);
						r += Math.abs(a[i][j]);
					}
				}
				if (c!=0 && r!=0) 
				{ 
					g=r/RADIX;
					f=1.0;
					s=c+r;
					while (c<g) 
					{
						f *= RADIX;
						c *= sqrdx;
					}
					g=r*RADIX;
					while (c>g)
					{
						f /= RADIX;
						c /= sqrdx;
					}
					if ((c+r)/f < 0.95*s) 
					{
						last=0;
						g=1.0/f;
						for (j=1;j<=n;j++) a[i][j] *= g; 
						for (j=1;j<=n;j++) a[j][i] *= f;
					}
				}
			}
		}
		return a;
	}
	private double[][] elmhes(double[][] a, int n)
	{
		// reduces a general matrix into Hessenberg form
		int m,j,i;
		double y,x,temp;
		for (m=2;m<n;m++) 
		{
			x=0.0;
			i=m;
			for (j=m;j<=n;j++) 
			{
				if (Math.abs(a[j][m-1]) > Math.abs(x))
				{
					x=a[j][m-1];
					i=j;
				}
			}
			if (i != m)
			{
				for (j=m-1;j<=n;j++) {temp=a[i][j];a[i][j]=a[m][j];a[m][j]=temp;}
				for (j=1;j<=n;j++) {temp=a[j][i];a[j][i]=a[j][m];a[j][m]=temp;}
			}
			if (x!=0)
			{
				for (i=m+1;i<=n;i++)
				{
					if ((y=a[i][m-1]) != 0.0)
					{
						y /= x;
						a[i][m-1]=y;
						for (j=m;j<=n;j++)
						a[i][j] -= y*a[m][j];
						for (j=1;j<=n;j++)
						a[j][m] += y*a[j][i];
					}
				}
			}
		}
		return a;
	}
	
	
	private double[][] hqr(double[][] a, int n)
	{
		// finds eigenvalues of a Hessenberg matrix
		double[][] ww = new double[2][n+1];
		a=elmhes(a,n);
		double[] wi= new double[n+1];
		double[] wr= new double[n+1];
		int nn,m,l,k,j,its,i,mmin;
        double z,y,x,w,v,u,t,s,r=1,q=1,p=1,anorm;
        anorm=0.0;
		for (i=1;i<=n;i++)
			for (j=IMAX(i-1,1);j<=n;j++)
				anorm += Math.abs(a[i][j]);
		nn=n;
		t=0.0;
		while (nn >= 1)
		{ 
			its=0;
			do
			{
				for (l=nn;l>=2;l--)
				{
					s=Math.abs(a[l-1][l-1])+Math.abs(a[l][l]);
					if (s == 0.0) s=anorm;
					if ((Math.abs(a[l][l-1]) + s) == s) break;
				}
				x=a[nn][nn];
				if (l == nn)
				{
					wr[nn]=x+t;
					wi[nn--]=0.0;
				}
				else
				{
					y=a[nn-1][nn-1];
					w=a[nn][nn-1]*a[nn-1][nn];
					if (l == (nn-1)) 
					{ 
						p=0.5*(y-x);
						q=p*p+w;
						z=Math.sqrt(Math.abs(q));
						x += t;
						if (q >= 0.0)
						{ 
							z=p+SIGN(z,p);
							wr[nn-1]=wr[nn]=x+z;
							if (z!=0) wr[nn]=x-w/z;
							wi[nn-1]=wi[nn]=0.0;
						} 
						else 
						{ 
							wr[nn-1]=wr[nn]=x+p;
							wi[nn-1]= -(wi[nn]=z);
						}
						nn -= 2;
					} 
					else 
					{
						if (its == 10 || its == 20)
						{ 
							for (i=1;i<=nn;i++) a[i][i] -= x;
							s=Math.abs(a[nn][nn-1])+Math.abs(a[nn-1][nn-2]);
							y=x=0.75*s;
							w = -0.4375*s*s;
						}
						++its;
						for (m=(nn-2);m>=l;m--) 
						{
							z=a[m][m];
							r=x-z;
							s=y-z;
							p=(r*s-w)/a[m+1][m]+a[m][m+1];
							q=a[m+1][m+1]-z-r-s;
							r=a[m+2][m+1];
							s=Math.abs(p)+Math.abs(q)+Math.abs(r); 
							p /= s;
							q /= s;
							r /= s;
							if (m == l) break;
							u=Math.abs(a[m][m-1])*(Math.abs(q)+Math.abs(r));
							v=Math.abs(p)*(Math.abs(a[m-1][m-1])+Math.abs(z)+Math.abs(a[m+1][m+1]));	
							if ((u+v) == v) break; 
						}
						for (i=m+2;i<=nn;i++)
						{
							a[i][i-2]=0.0;
							if (i != (m+2)) a[i][i-3]=0.0;
						}
						for (k=m;k<=nn-1;k++)
						{
							if (k != m)
							{
								p=a[k][k-1];
								q=a[k+1][k-1];
								r=0.0;
								if (k != (nn-1)) r=a[k+2][k-1];
								if ((x=Math.abs(p)+Math.abs(q)+Math.abs(r)) != 0.0)
								{
									p /=x;
									q /=x;
									r /=x;
								}
							}
							if ((s=SIGN(Math.sqrt(p*p+q*q+r*r),p)) != 0.0) 
							{
								if (k == m) 
								{
									if (l != m)
										a[k][k-1] = -a[k][k-1];
								} 
								else
									a[k][k-1] = -s*x;
								p += s;
								x=p/s;
								y=q/s;
								z=r/s;
								q /= p;
								r /= p;
								for (j=k;j<=nn;j++) 
								{
									p=a[k][j]+q*a[k+1][j];
									if (k != (nn-1)) 
									{
										p += r*a[k+2][j];
										a[k+2][j] -= p*z;
									}
									a[k+1][j] -= p*y;
									a[k][j] -= p*x;
								}
								mmin = nn<k+3 ? nn : k+3;
								for (i=l;i<=mmin;i++) 
								{ 
									p=x*a[i][k]+y*a[i][k+1];
									if (k != (nn-1))
									{
										p += z*a[i][k+2];
										a[i][k+2] -= p*r;
									}
									a[i][k+1] -= p*q;
									a[i][k] -= p;
								}
							}
						}
					}
				}
			} while (l < nn-1);
		}
		for (int k1=1;k1<=n;k1++)
		{
			ww[0][k1]=wr[k1];
			ww[1][k1]=wi[k1];
		}
		return ww;
	}

	private double SIGN(double a1, double a2)
	{
		if (a2>=0)
			return Math.abs(a1);
		else
			return -Math.abs(a1);
	}
	private int IMAX(int a1, int a2)
	{
		if (a1<a2)
			return a1;
		else 
			return a2;
	}


	public double[] polyMat (double[][] at, int n)
	{
		// calculates roots of a matrix (asymmetric-real)
		double[][] a = new double[n+1][n+1];
		for (int i=0;i<=n;i++)
			for (int j=0;j<=n;j++)
				a[i][j]=0;
		for (int i=0;i<n;i++)
			for (int j=0;j<n;j++)
				a[i+1][j+1]=at[i][j];
		double[][] abal = new double[n+1][n+1];
		double[][] ee = new double[2][n+1];
		double[] er = new double[n+1];
		double[] ei = new double[n+1];
		double[] cr = new double[n+1];
		double[] ci = new double[n+1];
		double[] ctr = new double[n+1];
		double[] cti = new double[n+1];
		abal=balance(a,n);
		ee=hqr(abal,n);
		for (int i=1;i<=n;i++)
		{er[i]=ee[0][i];ei[i]=ee[1][i];}
		cr[0]=1;ci[0]=0;ctr[1]=0;cti[0]=0;
		for (int i=1;i<=n;i++)
		{cr[i]=0;ci[i]=0;ctr[i]=0;cti[i]=0;}
		for (int j=0;j<=n-1;j++)
		{
			for (int k=0;k<=n;k++)
			{
				ctr[k]=cr[k];cti[k]=ci[k];
			}
			for (int k=1;k<=j+1;k++)
			{
				cr[k]=cr[k]-(er[j+1]*ctr[k-1]-ei[j+1]*cti[k-1]);
				ci[k]=ci[k]-(er[j+1]*cti[k-1]+ei[j+1]*ctr[k-1]);
			}
		}
        return cr;
	}

}
