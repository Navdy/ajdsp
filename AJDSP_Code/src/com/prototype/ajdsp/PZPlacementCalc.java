package com.prototype.ajdsp;

public class PZPlacementCalc {
	  int maxOrder=64;
	 public double [] CalcNumCoeff(double[] m_dZeroX, double[] m_dZeroY, int zero_no)
		{
			double tempZ2, tempZ1;
			double [] b=new double[maxOrder+1];
			int zCount = 0;
			b[0] = 1.0;
			for (int i = 1; i < 11;i++)
				b[i] = 0.00;
			for(int m = 0;m < 2*zero_no;m++)
			{
				if(m_dZeroY[m] == 0.00)
				{
					zCount++;
					for(int i = zCount;i>=1;i--)
						b[i] += b[i-1]*(-m_dZeroX[m]);
				}
				else
				{
					zCount += 2;
					tempZ2 = m_dZeroX[m]*m_dZeroX[m] + m_dZeroY[m]*m_dZeroY[m];
					tempZ1 = -2*m_dZeroX[m];
					b[zCount] = b[zCount-2]*tempZ2;
					for(int i = zCount-1;i>=2;i--)
					{
						b[i] += b[i-1]*tempZ1 + b[i-2]*tempZ2;
					}
					b[1] += tempZ1;
				}
			}
			for (int i=zCount+1;i<=10; i++)
			{
				b[i] = 0.00;
			}
			return b;
		}
		public double [] CalcDenCoeff(double[] m_dPoleX, double[] m_dPoleY, int pole_no)
		{
			double [] a=new double[maxOrder+1];
			int pCount=0;
			double tempP2, tempP1;
			a[0] = 1.0;
			for (int i = 1; i < 11;i++)
				a[i] = 0.00;
			for(int m = 0;m < 2*pole_no;m++)
			{
				if(m_dPoleY[m] == 0.00)
				{
					pCount++;
					for(int i = pCount;i>=1;i--)
						a[i] += a[i-1]*(-m_dPoleX[m]);
				}
				else
				{
					pCount += 2;
					tempP2 = m_dPoleX[m]*m_dPoleX[m] + m_dPoleY[m]*m_dPoleY[m];
					tempP1 = -2*m_dPoleX[m];
					a[pCount] = a[pCount-2]*tempP2;
					for(int i = pCount-1;i>=2;i--)
					{a[i] += a[i-1]*tempP1 + a[i-2]*tempP2;}
					a[1] += tempP1;
				}
			}
			for (int i=pCount+1;i<=10; i++)
			{a[i] = 0.00;}
			 return a;
		}
		
      
}
