package com.prototype.ajdsp;

import com.prototype.ajdsp.PartObject.PinSignalTypes;
import com.prototype.ajdsp.PartObject.PinTypes;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.view.View.OnClickListener;
import android.widget.ArrayAdapter;
import android.widget.Button;

import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;
public class IFFTMenu extends Activity implements OnClickListener {
	RelativeLayout layout;
	RelativeLayout topbar;
	Button addParts;
	Spinner ifftSpinner;
	Bundle inputValues=new Bundle();
	

	Button partList;
	public static int ifftNumber=0;
	Boolean create=true;
	double [] inputMag;
	double [] inputPhase;


	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		//requestWindowFeature(Window.FEATURE_CUSTOM_TITLE);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
		setContentView(R.layout.fft_menu);
		//getWindow().setFeatureInt(Window.FEATURE_CUSTOM_TITLE, R.layout.row);
		layout=(RelativeLayout)findViewById(R.id.fftMenu);
		layout.setBackgroundColor(Color.WHITE);
		partList=(Button) findViewById(R.id.back_select_parts);
		partList.setOnClickListener(this);
		
	    addParts=(Button) findViewById(R.id.add_part);
		addParts.setOnClickListener(this);
		ifftSpinner = (Spinner) findViewById(R.id.fft_length);
		ArrayAdapter<CharSequence> ifftTypeAdapter = ArrayAdapter.createFromResource(
				this, R.array.fft_length, android.R.layout.simple_spinner_item);
		ifftTypeAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
	//	filterTypeSpinner.setBackgroundColor(Color.WHITE);
		ifftSpinner.setAdapter(ifftTypeAdapter);
		


		Bundle extras = getIntent().getExtras();

		if(extras !=null)
		{
			inputValues = extras.getBundle("blockValues");
		
		    ifftSpinner.setSelection((int) inputValues.getLong("itemSelected"));
			inputMag=inputValues.getBundle("leftPin").getDoubleArray("signalMag");
			inputPhase=inputValues.getBundle("leftPin").getDoubleArray("signalPhase");
			create=false;
			addParts.setText("Update");
			partList.setText("Back");

		}else{
			
			 ifftSpinner.setSelection(0);
		}
	}
	@Override
	public void onBackPressed() {
		/*if(create){
			Intent i = new Intent(this, PartList.class);
			startActivity(i);}
		else {
			Intent i = new Intent(this, StartView.class);
			startActivity(i);
		}*/
	    }
	@Override
	 public void onStop() {
		        super.onStop();	
				finish();
				
	 }

@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		if (v == addParts){
			
		
			Bundle ifftData=new Bundle();
			double [] ifftArray = new double[256];
			Bundle ifftOutputBundle=new Bundle();
			ifftOutputBundle.putDoubleArray("signalMag",ifftArray);
			ifftOutputBundle.putDoubleArray("signalPhase",ifftArray);
			ifftData.putBundle("rightPin", ifftOutputBundle);
			ifftData.putInt("ifftLength", Integer.parseInt(ifftSpinner.getSelectedItem().toString()));
			ifftData.putLong("itemSelected", ifftSpinner.getSelectedItemId());
			if(create){
				ifftNumber++;
				Bundle ifftInputBundle=new Bundle();
				ifftInputBundle.putDoubleArray("signalMag",ifftArray);
				ifftInputBundle.putDoubleArray("signalPhase",ifftArray);
				ifftData.putBundle("leftPin", ifftInputBundle);
				PinSignalTypes ifftSigType=new PinSignalTypes();
				ifftData.putSerializable("pinSignalType", ifftSigType);
				ifftData.putString("title","IFFT"+" "+ifftNumber); 
				PinTypes ifftPinTypes=new PinTypes();
				ifftPinTypes.leftPin=true;
				ifftPinTypes.rightPin=true;
				ifftData.putSerializable("PinTypes",ifftPinTypes);			    
				PartsController.createBlock(ifftData);
			}
			else{
				Bundle ifftInputBundle=new Bundle();
				ifftInputBundle.putDoubleArray("signalMag",inputMag);
				ifftInputBundle.putDoubleArray("signalPhase",inputPhase);
				ifftData.putBundle("leftPin", ifftInputBundle);
				ifftData.putString("title", inputValues.getString("title"));
				PartsController.updateBlock(ifftData);
			}
			Intent i = new Intent(this, StartView.class);
			startActivity(i);
			
		}

		if(v==partList){
			if(create){
				Intent i = new Intent(this, PartList.class);
				startActivity(i);}
			else {
				Intent i = new Intent(this, StartView.class);
				startActivity(i);
			}
		}

	}
}
