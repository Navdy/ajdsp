package com.prototype.ajdsp;

import java.util.ArrayList;
import java.util.List;

import com.prototype.ajdsp.PartObject.PinSignalTypes;
import com.prototype.ajdsp.PartObject.PinTypes;

import android.app.Activity;
import android.app.ListActivity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup.LayoutParams;
import android.view.Window;
import android.view.WindowManager;
import android.view.inputmethod.EditorInfo;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.AdapterView.OnItemClickListener;

public class PartList extends ListActivity implements OnClickListener {
	public static int plotNumber=0;
	public static boolean partConfirm=false;
	public static int convolutionNumber=0;
	public static int filterNumber=0;
	public static int freqRespNumber=0;
	public static int pzPlotNumber=0;
	public static int snrNumber=0;

	public static int pzPlacementNumber=0;
	public static int pz2CoeffNumber=0;
	public static int junctionNumber=0;

	RelativeLayout layout;
	RelativeLayout topLayout;
	Button back;
	Button done;
	EditText search;
	private List<String> array_sort= new ArrayList<String>();
	private List<Integer> array_index= new ArrayList<Integer>();
	int textlength=0;
	String[] parts;
	ListView lv;


	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
		parts = getResources().getStringArray(R.array.parts_list_array);
		//requestWindowFeature(Window.FEATURE_CUSTOM_TITLE);


		setContentView(R.layout.part_list_layout);
		//getWindow().setFeatureInt(Window.FEATURE_CUSTOM_TITLE, R.layout.row);
		layout= (RelativeLayout) findViewById(R.id.part_list_view);
		layout.setBackgroundColor(Color.WHITE);
		back=(Button) findViewById(R.id.back_select_parts);
		back.setText("Back");
		back.setOnClickListener(this);
		TextView title= (TextView)findViewById(R.id.Title);
		title.setText("Function List");
		done=(Button) findViewById(R.id.add_part);
		done.setVisibility(View.GONE);



		lv  = (ListView) findViewById(android.R.id.list);
		lv.setAdapter(new ArrayAdapter<String>(this, R.layout.part_list, parts));

		lv.setTextFilterEnabled(true);
		lv.setBackgroundColor(Color.WHITE);
		search=(EditText)findViewById(R.id.list_search);
		//search.setImeOptions(EditorInfo.IME_ACTION_DONE|EditorInfo.IME_FLAG_NO_EXTRACT_UI);
		
		search.addTextChangedListener(new TextWatcher()
		{
			public void afterTextChanged(Editable s)
			{
				// Abstract Method of TextWatcher Interface.
			}
			public void beforeTextChanged(CharSequence s,
					int start, int count, int after)
			{
				// Abstract Method of TextWatcher Interface.
			}
			public void onTextChanged(CharSequence s,
					int start, int before, int count)
			{
				textlength = search.getText().length();
				array_sort.clear();
				array_index.clear();
				for (int i = 0; i < parts.length; i++)
				{
					if (textlength <= parts[i].length())
					{
						if(search.getText().toString().equalsIgnoreCase(
								(String)
								parts[i].subSequence(0,
										textlength)))
						{
							array_sort.add(parts[i]);
							array_index.add(i);
							
						}
					}
				}
				lv.setAdapter(new ArrayAdapter<String>
				(PartList.this,
						R.layout.part_list, array_sort));
				lv.setOnItemClickListener(new OnItemClickListener() {

					public void onItemClick(AdapterView<?> parent, View view,
							int position, long id) {
						passId(array_index.get((int)id));
					}
				});
				
			}
		});
		lv.setOnItemClickListener(new OnItemClickListener() {

			public void onItemClick(AdapterView<?> parent, View view,
					int position, long id) {
				passId(id);
			}
		});
		

	}
	@Override
	public void onBackPressed() {
		
			/*Intent i = new Intent(this, StartView.class);
			startActivity(i);
		*/
	}

	public void passId(long id){
		ConfirmationDialog confirmDialog = new ConfirmationDialog(this);
		WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
		switch((int)id){
		case 0:
			Intent adderIntent = new Intent(this, AdderMenu.class);
			startActivity(adderIntent);


			break;
		case 1:
			Intent bwIntent = new Intent(this, BWMenu.class);
			startActivity(bwIntent);
			break;
		case 2:
			Bundle convDemoData=new Bundle();
			convDemoData.putString("title","Conv Demo"); 
			PinTypes convDemoTypes=new PinTypes();
			convDemoData.putSerializable("PinTypes",convDemoTypes);
			confirmDialog.setTitle("Add Conv Demo?");
			confirmDialog.setPartCheckText("Conv Demo");
			confirmDialog.setInputBundle(convDemoData);
			confirmDialog.show();
			lp.copyFrom(confirmDialog.getWindow().getAttributes());
			
			
			confirmDialog.getWindow().setAttributes(lp);



			break;
		case 3:

			convolutionNumber++;
			Bundle convData=new Bundle();
			convData.putString("title","Conv"+" "+convolutionNumber); 
			PinTypes convTypes=new PinTypes();
			convTypes.leftTopPin=true;
			convTypes.leftBottomPin=true;
			convTypes.rightPin=true;
			convData.putSerializable("PinTypes",convTypes);

			PinSignalTypes convSigType=new PinSignalTypes();
			convData.putSerializable("pinSignalType", convSigType);

			Bundle convInputTopBundle=new Bundle();
			double [] convArray = new double[256];
			convInputTopBundle.putDoubleArray("signalMag", convArray);
			// convInputTopBundle.putDouble("pulseWidth", 0);
			Bundle convInputBottomBundle=new Bundle();
			convInputBottomBundle.putDoubleArray("signalMag", convArray);
			// convInputBottomBundle.putInt("pulseWidth",0);
			Bundle convOutputBundle=new Bundle();
			convOutputBundle.putDoubleArray("signalMag", convArray);
			//convOutputBundle.putInt("pulseWidth",0);
			convData.putBundle("leftTopPin",  convInputTopBundle);
			convData.putBundle("leftBottomPin",  convInputBottomBundle);
			convData.putBundle("rightPin", convOutputBundle);


			confirmDialog.setTitle("Add Convolution?");
			confirmDialog.setPartCheckText("Convolution");
			confirmDialog.setInputBundle(convData);
			confirmDialog.show();
			lp.copyFrom(confirmDialog.getWindow().getAttributes());
			
			confirmDialog.getWindow().setAttributes(lp);



			break;
		case 4:
			Intent downSamplerIntent = new Intent(this, DownSamplerMenu.class);
			startActivity(downSamplerIntent);

			break;

		case 5:
			Intent dtmfIntent = new Intent(this, DTMFTone.class);
			startActivity(dtmfIntent);


			break;


		case 6:
			Intent fftIntent = new Intent(this, FFTMenu.class);
			startActivity(fftIntent);
			break;

		case 7:
			filterNumber++;
			Bundle filterData=new Bundle();
			filterData.putString("title","Filter"+" "+filterNumber); 
			PinTypes filterTypes=new PinTypes();
			filterTypes.topPin=true;
			filterTypes.bottomPin=true;
			filterTypes.rightPin=true;
			filterTypes.leftPin=true;

			PinSignalTypes filterSigType=new PinSignalTypes();
			filterSigType.topPin="frequency";
			filterSigType.bottomPin="frequency";

			filterData.putSerializable("pinSignalType", filterSigType);

			filterData.putSerializable("PinTypes",filterTypes);
			Bundle filterInputLeftBundle=new Bundle();
			double [] filterArray = new double[256];
			double []filterCoeffcientArray=new double[11];



			filterInputLeftBundle.putDoubleArray("signalMag", filterArray);
			filterInputLeftBundle.putDoubleArray("signalPhase", filterArray);
			Bundle filterInputBottomBundle=new Bundle();
			filterInputBottomBundle.putDoubleArray("filterCoeffB", filterCoeffcientArray);
			filterInputBottomBundle.putDoubleArray("filterCoeffA",filterCoeffcientArray );
			Bundle filterOutputTopBundle=new Bundle();
			filterOutputTopBundle.putDoubleArray("filterCoeffB", filterCoeffcientArray);
			filterOutputTopBundle.putDoubleArray("filterCoeffA", filterCoeffcientArray);
			Bundle filterOutputRightBundle=new Bundle();
			filterOutputRightBundle.putDoubleArray("signalMag", filterArray);
			filterOutputRightBundle.putDoubleArray("signalPhase", filterArray);
			filterData.putBundle("topPin",  filterOutputTopBundle);
			filterData.putBundle("bottomPin",  filterInputBottomBundle);
			filterData.putBundle("rightPin", filterOutputRightBundle);
			filterData.putBundle("leftPin",  filterInputLeftBundle);


			confirmDialog.setTitle("Add Filter?");
			confirmDialog.setPartCheckText("Filter");

			confirmDialog.setInputBundle(filterData);
			confirmDialog.show();
			lp.copyFrom(confirmDialog.getWindow().getAttributes());

			confirmDialog.getWindow().setAttributes(lp);

			break;
		case 8:
			Intent filterCoeffIntent = new Intent(this, FilterCoeffMenu.class);
			startActivity(filterCoeffIntent);
			break;
		case 9:
			Intent firDesignIntent = new Intent(this, FIRDesignMenu.class);
			startActivity(firDesignIntent);
			break;
		case 10:
			freqRespNumber++;
			Bundle freqRespData=new Bundle();
			freqRespData.putString("title","Freq Resp"+" "+freqRespNumber); 
			PinTypes freqRespTypes=new PinTypes();
			freqRespTypes.topPin=true;
			freqRespTypes.bottomPin=true;
			freqRespTypes.rightPin=true;
			freqRespData.putSerializable("PinTypes",freqRespTypes);

			PinSignalTypes freqRespSigType=new PinSignalTypes();
			freqRespSigType.bottomPin="frequency";
			freqRespSigType.topPin="frequency";
			freqRespData.putSerializable("pinSignalType", freqRespSigType);

			double [] freqRespArray = new double[256];
			double []coeffcientArray=new double[11];


			Bundle freqRespInputBottomBundle=new Bundle();
			freqRespInputBottomBundle.putDoubleArray("filterCoeffB",coeffcientArray );
			freqRespInputBottomBundle.putDoubleArray("filterCoeffA",coeffcientArray);
			Bundle freqRespOutputTopBundle=new Bundle();
			freqRespOutputTopBundle.putDoubleArray("filterCoeffB", coeffcientArray);
			freqRespOutputTopBundle.putDoubleArray("filterCoeffA", coeffcientArray);
			Bundle freqRespOutputRightBundle=new Bundle();
			freqRespOutputRightBundle.putDoubleArray("signalMag", freqRespArray);
			freqRespOutputRightBundle.putDoubleArray("signalPhase", freqRespArray);
			freqRespData.putBundle("topPin",  freqRespOutputTopBundle);
			freqRespData.putBundle("bottomPin",  freqRespInputBottomBundle);
			freqRespData.putBundle("rightPin", freqRespOutputRightBundle);
			confirmDialog.setTitle("Add Freq Resp?");
			confirmDialog.setPartCheckText("Freq Resp");
			confirmDialog.setInputBundle(freqRespData);
			confirmDialog.show();

			lp.copyFrom(confirmDialog.getWindow().getAttributes());

			confirmDialog.getWindow().setAttributes(lp);



			break;
		/*case 11:
			Intent freqSamplerIntent = new Intent(this, FreqSamplerMenu.class);
			startActivity(freqSamplerIntent);


			break;*/
		case 11:
			Intent ifftIntent = new Intent(this, IFFTMenu.class);
			startActivity(ifftIntent);
			break;

			
		case 12:
			Intent iirDesignIntent = new Intent(this, IIRDesignMenu.class);
			startActivity(iirDesignIntent);


			break;
		case 13:
			junctionNumber++;
			Bundle junctionData=new Bundle();
			junctionData.putString("title","Junction"+" "+junctionNumber); 
			PinTypes junctionPinTypes=new PinTypes();
			junctionPinTypes.leftPin=true;
			junctionPinTypes.rightTopPin=true;
			junctionPinTypes.rightBottomPin=true;
			junctionData.putSerializable("PinTypes",junctionPinTypes);
			double [] junctionInputArray = new double[256];
			Bundle junctionInputBundle=new Bundle();
			junctionInputBundle.putDoubleArray("signalMag", junctionInputArray);
			Bundle junctionOutputTopBundle=new Bundle();
			junctionOutputTopBundle.putDoubleArray("signalMag", junctionInputArray);
			Bundle junctionOutputBottomBundle=new Bundle();
			junctionOutputBottomBundle.putDoubleArray("signalMag", junctionInputArray);
			junctionData.putBundle("leftPin", junctionInputBundle);
			junctionData.putBundle("rightTopPin", junctionOutputTopBundle);
			junctionData.putBundle("rightBottomPin", junctionOutputBottomBundle);
			junctionData.putDouble("arrayLength", 256.0);

			PinSignalTypes junctionSigType=new PinSignalTypes();

			junctionData.putSerializable("pinSignalType", junctionSigType);
			confirmDialog.setTitle("Add Junction?");
			confirmDialog.setPartCheckText("Junction");
			confirmDialog.setInputBundle(junctionData);
			confirmDialog.show();
			lp.copyFrom(confirmDialog.getWindow().getAttributes());

			confirmDialog.getWindow().setAttributes(lp);

			break;
		case 14:
			Intent kaiserIntent = new Intent(this, KaiserMenu.class);
			startActivity(kaiserIntent);
			break;
		case 15:
			Intent midiIntent = new Intent(this, MIDIDisplay.class);
			startActivity(midiIntent);

			break;
		case 16:
			Intent pmlIntent = new Intent(this, PmlMenu.class);
			startActivity(pmlIntent);
			break;
		case 17:
			Intent pkPickingIntent = new Intent(this, PkPickingMenu.class);
			startActivity(pkPickingIntent);
			break;

		case 18:
			plotNumber++;
			Bundle plotData=new Bundle();
			plotData.putString("title","Plot"+" "+PartList.plotNumber); 
			PinTypes plotPinTypes=new PinTypes();
			plotPinTypes.leftPin=true;
			plotPinTypes.rightPin=true;
			plotData.putSerializable("PinTypes",plotPinTypes);
			double [] plotInputArray = new double[256];
			Bundle plotInputBundle=new Bundle();
			plotInputBundle.putDoubleArray("signalMag", plotInputArray);
			Bundle plotOutputBundle=new Bundle();
			plotOutputBundle.putDoubleArray("signalMag", plotInputArray);
			plotData.putBundle("leftPin", plotInputBundle);
			plotData.putBundle("rightPin", plotOutputBundle);
			plotData.putDouble("arrayLength", 256.0);
			PinSignalTypes plotSigType=new PinSignalTypes();
			plotData.putSerializable("pinSignalType",plotSigType );

			confirmDialog.setTitle("Add Plot?");
			confirmDialog.setPartCheckText("Plot");
			confirmDialog.setInputBundle(plotData);
			confirmDialog.show();
			lp.copyFrom(confirmDialog.getWindow().getAttributes());

			confirmDialog.getWindow().setAttributes(lp);
			break;
		case 19:
			pz2CoeffNumber++;
			Bundle pz2CoeffData=new Bundle();
			pz2CoeffData.putString("title","PZ2Coeff"+" "+pz2CoeffNumber); 
			PinTypes  pz2CoeffTypes=new PinTypes();
			pz2CoeffTypes.topPin=true;
			pz2CoeffData.putSerializable("PinTypes",pz2CoeffTypes);	   
			PinSignalTypes pz2CoeffSigType=new PinSignalTypes();
			pz2CoeffSigType.topPin="frequency";
			pz2CoeffData.putSerializable("pinSignalType", pz2CoeffSigType);
			double [] pz2CoeffInputArray = new double[11];

			Bundle pz2CoeffTopBundle=new Bundle();
			pz2CoeffTopBundle.putDoubleArray("filterCoeffB",pz2CoeffInputArray );
			pz2CoeffTopBundle.putDoubleArray("filterCoeffA",pz2CoeffInputArray);
			pz2CoeffData.putBundle("topPin",pz2CoeffTopBundle);
			
			confirmDialog.setTitle("Add PZ2Coeff?");
			confirmDialog.setPartCheckText("PZ2Coeff");
			confirmDialog.setInputBundle( pz2CoeffData);
			confirmDialog.show();

			lp.copyFrom(confirmDialog.getWindow().getAttributes());

			confirmDialog.getWindow().setAttributes(lp);
			break;
			
		case 20:
			pzPlacementNumber++;
			Bundle pzPlacementData=new Bundle();
			pzPlacementData.putString("title","PZ Placement"+" "+pzPlacementNumber); 
			PinTypes  pzPlacementTypes=new PinTypes();
			pzPlacementData.putSerializable("PinTypes",pzPlacementTypes);	    
			confirmDialog.setTitle("Add PZ Placement?");
			confirmDialog.setPartCheckText("PZ Placement");
			confirmDialog.setInputBundle( pzPlacementData);
			confirmDialog.show();

			lp.copyFrom(confirmDialog.getWindow().getAttributes());

			confirmDialog.getWindow().setAttributes(lp);
			break;

		case 21:
			pzPlotNumber++;
			Bundle pzPlotData=new Bundle();
			pzPlotData.putString("title","PZ Plot"+" "+PartList.pzPlotNumber); 
			PinTypes pzPlotPinTypes=new PinTypes();
			pzPlotPinTypes.bottomPin=true;
			pzPlotData.putSerializable("PinTypes",pzPlotPinTypes);

			PinSignalTypes pzPlotSigType=new PinSignalTypes();
			pzPlotSigType.bottomPin="frequency";
			pzPlotData.putSerializable("pinSignalType", pzPlotSigType);
			double [] pzPlotInputArray = new double[10];

			Bundle pzPlotInputBottomBundle=new Bundle();
			pzPlotInputBottomBundle.putDoubleArray("filterCoeffB",pzPlotInputArray );
			pzPlotInputBottomBundle.putDoubleArray("filterCoeffA",pzPlotInputArray);
			pzPlotData.putBundle("bottomPin",pzPlotInputBottomBundle);


			confirmDialog.setTitle("Add PZ Plot?");
			confirmDialog.setPartCheckText("PZ Plot");
			confirmDialog.setInputBundle(pzPlotData);
			confirmDialog.show();


			lp.copyFrom(confirmDialog.getWindow().getAttributes());

			confirmDialog.getWindow().setAttributes(lp);



			break;
		case 22:
			Intent sigGenIntent = new Intent(this, SigGenMenu.class);
			startActivity(sigGenIntent);

			break;
		case 23:
			snrNumber++;
			Bundle snrData=new Bundle();
			snrData.putString("title","SNR"+" "+PartList.snrNumber); 
			PinTypes snrPinTypes=new PinTypes();
			snrPinTypes.leftTopPin=true;
			snrPinTypes.leftBottomPin=true;
			snrData.putSerializable("PinTypes",snrPinTypes);

			PinSignalTypes snrSigType=new PinSignalTypes();
			
			snrData.putSerializable("pinSignalType", snrSigType);
			double [] snrInputArray = new double[256];

			Bundle snrInputBottomBundle=new Bundle();
			snrInputBottomBundle.putDoubleArray("signalMag",snrInputArray );
			snrInputBottomBundle.putDoubleArray("signalPhase",snrInputArray);
			snrData.putBundle("leftBottomPin",snrInputBottomBundle);
			Bundle snrInputTopBundle=new Bundle();
			snrInputTopBundle.putDoubleArray("signalMag",snrInputArray );
			snrInputTopBundle.putDoubleArray("signalPhase",snrInputArray);
			snrData.putBundle("leftTopPin",snrInputTopBundle);


			confirmDialog.setTitle("Add SNR?");
			confirmDialog.setPartCheckText("SNR");
			confirmDialog.setInputBundle(snrData);
			confirmDialog.show();


			lp.copyFrom(confirmDialog.getWindow().getAttributes());

			confirmDialog.getWindow().setAttributes(lp);



			break;
		case 24:
			Intent upSamplerIntent = new Intent(this, UpSamplerMenu.class);
			startActivity(upSamplerIntent);
			break;
		case 25:
			Intent windowIntent = new Intent(this, WindowMenu.class);
			startActivity(windowIntent);

			break;

		default:
			break;
		}
	}
	@Override
	 public void onStop() {		
			 super.onStop();				
				finish();
				
	 }
	@Override
	public void onClick(View v) {
		if(v==back){
			Intent i = new Intent(this, StartView.class);
			startActivity(i);
		}

	}
}
