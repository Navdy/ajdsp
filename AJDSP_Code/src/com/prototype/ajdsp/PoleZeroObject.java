package com.prototype.ajdsp;

import java.util.Comparator;

import android.graphics.Bitmap;



public class PoleZeroObject  {

		  /**
		 * 
		 */
		

		/**
	   * Contains the PoleZeroCoordinates of the graphic.
	   */
		
	  public class PoleZeroCoordinates {
	      private int _x ;
	      private int _y ;
	      
	     public int getX(){
	    	 return _x;
	     }
	     public int getY(){
	    	 return _y;
	     }
	      
	        public int getBottomY() {
	            return _y + _bitmap.getHeight() / 2;
	        }
	        public int getRightX() {
	            return _x + _bitmap.getWidth() / 2;
	        }
	        
	        public int getLeftX() {
	            return _x - _bitmap.getWidth() / 2;
	        }

	        public void setX(int value) {
	            _x = value;
	        }

	        public int getTopY() {
	            return _y - _bitmap.getHeight() / 2;
	        }

	        public void setY(int value) {
	            _y = value;
	        }

	    
	  }
	 
	  private Bitmap _bitmap;
	  private PoleZeroCoordinates _PoleZeroCoordinates;

	  public PoleZeroObject(Bitmap bitmap) {	  	
	  	  _bitmap=bitmap;     
	      _PoleZeroCoordinates = new PoleZeroCoordinates();	    
	    } 
	  
	 public PoleZeroCoordinates getPoleZeroCoordinates() {
	      return _PoleZeroCoordinates;
	  } 


	  public Bitmap getGraphic() {
	      return _bitmap;
	  }  

	  public void setGraphic(Bitmap bitmap) {
	      _bitmap=bitmap;
	  }  
	  public void setPoleZeroCoordinates(PoleZeroCoordinates coord) {
		  _PoleZeroCoordinates=coord ;
	  } 

	}


