package com.prototype.ajdsp;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.achartengine.GraphicalView;

import com.prototype.ajdsp.PZPlacementView.ConjugatePoleZero;
import com.prototype.ajdsp.PZPlacementView.ZeroPolePositionListener;



import android.app.Activity;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

public class PZPlacementDemo extends Activity implements OnClickListener {
	RelativeLayout layout;
	Button back;
	RelativeLayout topLayout;
	Bundle inputValues;
	double[] inputArrayLinear;

	double[] inputArrayDb;
	Button linear;
	Button db;
	Button addZero;
	Button addPole;
	Button reset;
	Button undo;
	

	static GraphView magnPlot;
	static GraphView phasePlot;
	TextView xCoord;
	TextView yCoord;

	double [] signalMag;double []signalPhase;
	double x[] = new double[128];
	double phi[] = new double[128];
	double amp[] = new double[128];
	double ang[] = new double[128];
	double ampdB[] = new double[128];

	int maxOrder=64;
	double [] coeffB=new double[maxOrder+1];
	double [] coeffA=new double[maxOrder+1];
	private boolean dBSelected=false;
	String ampTitleLinear="Amplitude(Linear)";
	String phaseTitle="Phase";
	String ampTitledB="Amplitude(dB)";
	List<String> poleZeroSequence=Collections.synchronizedList(new ArrayList<String>());

	static String[] verLabels;
	String[] horLabels;
	PZPlacementView pzPlacementView;

int poleCount=0;
int zeroCount=0;
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
		//requestWindowFeature(Window.FEATURE_CUSTOM_TITLE);
		setContentView(R.layout.pz_placement_demo);
		//getWindow().setFeatureInt(Window.FEATURE_CUSTOM_TITLE, R.layout.row);

		layout= (RelativeLayout) findViewById(R.id.pzplacement_view);
		layout.setBackgroundColor(Color.WHITE);
	
		 
        back=(Button) findViewById(R.id.back_to_startView_pzplacement);
		back.setOnClickListener(this);
		

		addZero=(Button)findViewById(R.id.pzPlacement_addZero);
		addZero.setOnClickListener(this);
		addPole=(Button)findViewById(R.id.pzPlacement_addPole);
		addPole.setOnClickListener(this);
		reset=(Button)findViewById(R.id.pzPlacement_reset);
		reset.setOnClickListener(this);
		linear=(Button)findViewById(R.id.select_linear_pzplacement);
		linear.setOnClickListener(this);
		db=(Button)findViewById(R.id.select_db_pzplacement);
		db.setOnClickListener(this);
		//undo=(Button)findViewById(R.id.pzPlacement_undo);
		//undo.setOnClickListener(this);
		xCoord=(TextView) findViewById(R.id.pzPlacement_xcoordinates);
		yCoord=(TextView) findViewById(R.id.pzPlacement_ycoordinates);


		pzPlacementView=(PZPlacementView)findViewById(R.id.pzplacement_view_window);   
		magnPlot=(GraphView) findViewById(R.id.pzplacement_magnPlot);
		magnPlot.setLength(128);
		phasePlot=(GraphView) findViewById(R.id.pzplacement_phasePlot);
		phasePlot.setLength(128);
		verLabels = new String[] { "great", "ok", "bad" };
		horLabels = new String[] { "0", "1/4pi", "1/2pi", "3/pi","1" };

		magnPlot.SetParameters(ampTitleLinear,horLabels, verLabels);
		phasePlot.SetParameters(phaseTitle,horLabels, verLabels);





		pzPlacementView.setPositionListener(new ZeroPolePositionListener(){
			public void onZeroPoleListChange( double[] zeroX,double [] zeroY,double[] poleX,double[]poleY) {
				coeffB=CalcNumCoeff(zeroX, zeroY, PZPlacementView.zeroNo);
				coeffA=CalcDenCoeff(poleX, poleY, PZPlacementView.poleNo);             
				getAmpPhi();    

				magnPlot.setValues(amp); 
				verLabels= magnPlot.getYLabels();
				magnPlot.setVerLabels(verLabels);
				phasePlot.setValues(ang);
				verLabels= phasePlot.getYLabels();
				phasePlot.setVerLabels(verLabels);
				magnPlot.invalidate();
				phasePlot.invalidate();
				String strX = String.format("x: %3.2f",
						PZPlacementView.xCoord);
				xCoord.setText(strX);
				String strY = String.format("y: %3.2f",
						PZPlacementView.yCoord);
				yCoord.setText(strY);
			}
		});

	}
	@Override
	 public void onStop() {
		 super.onStop();	
				finish();
				
	 }
	@Override
	public void onBackPressed() {
		/*reset();
			Intent i = new Intent(this, StartView.class);
			startActivity(i);*/
		
	}
	@Override
	public void onClick(View v) {
		if(v==addZero){
			PZPlacementThread.stopDraw=true;
			zeroCount++;
			if(zeroCount<10){
			
			PZPlacementView.addZero();
			poleZeroSequence.add("zero");
			
			}else{
				Toast.makeText(getApplicationContext(), "Limit of zeros excedded", Toast.LENGTH_SHORT).show();
			}
			PZPlacementThread.stopDraw=false;
		} else if(v==addPole){
			
			PZPlacementThread.stopDraw=true;
			poleCount++;
			if(poleCount<10){
			PZPlacementView.addPole();
			poleZeroSequence.add("pole");
			}
			else{
				Toast.makeText(getApplicationContext(), "Limit of poles excedded", Toast.LENGTH_SHORT).show();
			}
			PZPlacementThread.stopDraw=false;
		} else if(v==reset){
			PZPlacementThread.stopDraw=true;
			reset();
			PZPlacementThread.stopDraw=false;
			
		} else if(v==linear){
			if(dBSelected==true){
				magnPlot.setValues(amp);
				magnPlot.SetParameters(ampTitleLinear,horLabels, verLabels);
				phasePlot.SetParameters(phaseTitle,horLabels, verLabels);
				magnPlot.invalidate();	
				phasePlot.invalidate();		

				dBSelected=false;
			}

		}else if(v==db){
			for (int k=0;k<amp.length;k++)
			{
				ampdB[k]=20*Math.log10(amp[k]);				
				magnPlot.setValues(ampdB);
				magnPlot.SetParameters(ampTitledB,horLabels, verLabels);
				phasePlot.SetParameters(phaseTitle,horLabels, verLabels);

				magnPlot.invalidate();	
				phasePlot.invalidate();
				dBSelected=true;

			}
		} else if(v==back){
			reset();
			Intent i = new Intent(this, StartView.class);
			startActivity(i);

		}/*else if(v==undo){
			if(!poleZeroSequence.isEmpty()){
				String poleZero=poleZeroSequence.get(poleZeroSequence.size()-1);
			if(poleZero.equalsIgnoreCase("zero")){
				PZPlacementView.zeroList.remove(PZPlacementView.zeroList.size()-1);	
				PZPlacementView.zeroArrayX[2*(PZPlacementView.zeroNo-1)]=0.0;
				PZPlacementView.zeroArrayY[2*(PZPlacementView.zeroNo-1)]=0.0;

				PZPlacementView.zeroArrayX[2*PZPlacementView.zeroNo]=  0.0;
				PZPlacementView.zeroArrayY[2*PZPlacementView.zeroNo]=0.0;
				PZPlacementView.zeroNo--;
				coeffB=CalcNumCoeff(PZPlacementView.zeroArrayX, PZPlacementView.zeroArrayY, PZPlacementView.zeroNo);
				coeffA=CalcDenCoeff(PZPlacementView.poleArrayX, PZPlacementView.poleArrayX, PZPlacementView.poleNo);             
				getAmpPhi();    

				magnPlot.setValues(amp); 
				verLabels= magnPlot.getYLabels();
				magnPlot.setVerLabels(verLabels);
				phasePlot.setValues(ang);
				verLabels= phasePlot.getYLabels();
				phasePlot.setVerLabels(verLabels);
				magnPlot.invalidate();
				phasePlot.invalidate();
				poleZeroSequence.remove(poleZeroSequence.size()-1);
			}
			else if(poleZero.equalsIgnoreCase("pole")) {
				PZPlacementView.poleList.remove(PZPlacementView.poleList.size()-1);
				PZPlacementView.poleArrayX[2*(PZPlacementView.poleNo-1)]=0.0;
				PZPlacementView.poleArrayY[2*(PZPlacementView.poleNo-1)]=0.0;

				PZPlacementView.poleArrayX[2*PZPlacementView.poleNo]=  0.0;
				PZPlacementView.poleArrayY[2*PZPlacementView.poleNo]=0.0;
				PZPlacementView.poleNo--;
				coeffB=CalcNumCoeff(PZPlacementView.zeroArrayX, PZPlacementView.zeroArrayY, PZPlacementView.zeroNo);
				coeffA=CalcDenCoeff(PZPlacementView.poleArrayX, PZPlacementView.poleArrayX, PZPlacementView.poleNo);             
				getAmpPhi();    

				magnPlot.setValues(amp); 
				verLabels= magnPlot.getYLabels();
				magnPlot.setVerLabels(verLabels);
				phasePlot.setValues(ang);
				verLabels= phasePlot.getYLabels();
				phasePlot.setVerLabels(verLabels);
				magnPlot.invalidate();
				phasePlot.invalidate();
				poleZeroSequence.remove(poleZeroSequence.size()-1);
			}
		}
		}*/

	}

	public double [] CalcNumCoeff(double[] m_dZeroX, double[] m_dZeroY, int zero_no)
	{
		double tempZ2, tempZ1;
		double [] b=new double[maxOrder+1];
		int zCount = 0;
		b[0] = 1.0;
		for (int i = 1; i < 11;i++)
			b[i] = 0.00;
		for(int m = 0;m < 2*zero_no;m++)
		{
			if(m_dZeroY[m] == 0.00)
			{
				zCount++;
				for(int i = zCount;i>=1;i--)
					b[i] += b[i-1]*(-m_dZeroX[m]);
			}
			else
			{
				zCount += 2;
				tempZ2 = m_dZeroX[m]*m_dZeroX[m] + m_dZeroY[m]*m_dZeroY[m];
				tempZ1 = -2*m_dZeroX[m];
				b[zCount] = b[zCount-2]*tempZ2;
				for(int i = zCount-1;i>=2;i--)
				{
					b[i] += b[i-1]*tempZ1 + b[i-2]*tempZ2;
				}
				b[1] += tempZ1;
			}
		}
		for (int i=zCount+1;i<=10; i++)
		{
			b[i] = 0.00;
		}
		return b;
	}
	public double [] CalcDenCoeff(double[] m_dPoleX, double[] m_dPoleY, int pole_no)
	{
		double [] a=new double[maxOrder+1];
		int pCount=0;
		double tempP2, tempP1;
		a[0] = 1.0;
		for (int i = 1; i < 11;i++)
			a[i] = 0.00;
		for(int m = 0;m < 2*pole_no;m++)
		{
			if(m_dPoleY[m] == 0.00)
			{
				pCount++;
				for(int i = pCount;i>=1;i--)
					a[i] += a[i-1]*(-m_dPoleX[m]);
			}
			else
			{
				pCount += 2;
				tempP2 = m_dPoleX[m]*m_dPoleX[m] + m_dPoleY[m]*m_dPoleY[m];
				tempP1 = -2*m_dPoleX[m];
				a[pCount] = a[pCount-2]*tempP2;
				for(int i = pCount-1;i>=2;i--)
				{a[i] += a[i-1]*tempP1 + a[i-2]*tempP2;}
				a[1] += tempP1;
			}
		}
		for (int i=pCount+1;i<=10; i++)
		{a[i] = 0.00;}
		return a;
	}
	public void getAmpPhi(){

		double  nr =0;double ni=0;double dr=0;double di=0;

		for (int i=0;i<=127;i++)
		{
			double omega= Math.PI/((double)128)*i;
			nr =0;ni=0;dr=0;di=0;

			for (int k=0;k<coeffA.length;k++)
			{
				nr=nr+coeffB[k]*Math.cos((double)(64-k)*omega);
				ni=ni+coeffB[k]*Math.sin((double)(64-k)*omega);
				dr=dr+coeffA[k]*Math.cos((double)(64-k)*omega);
				di=di+coeffA[k]*Math.sin((double)(64-k)*omega);

			}
			if((dr*dr+di*di)!=0){

				double  re= (nr*dr+ni*di)/(dr*dr+di*di);
				double im= (ni*dr-nr*di)/(dr*dr+di*di);

				x[i] = Math.sqrt(re*re+im*im);
				phi[i]=Math.atan2(im,re);


				amp[i] = x[i];
				ang[i] = phi[i]*180/Math.PI;


				if (x[i] >=100000)			               
					x[i]=100000;			               

				if (x[i] <=-100000)		               

					x[i]=-100000;


			}
		}

	}
	public void convertTodB(){

	}
	public static void setPlot(double []amp,double []ang){
		magnPlot.setValues(amp); 
		verLabels= magnPlot.getYLabels();
		magnPlot.setVerLabels(verLabels);
		phasePlot.setValues(ang);
		verLabels= phasePlot.getYLabels();
		phasePlot.setVerLabels(verLabels);
		magnPlot.invalidate();
		phasePlot.invalidate();

	}
	/* @Override
     protected void onStop() {
    System.exit(1);
    super.onStop();

}*/
	public void reset(){
		PZPlacementView.zeroList.clear();
		PZPlacementView.poleList.clear();
		zeroCount=0;
		poleCount=0;
		PZPlacementView.zeroNo=0;
		PZPlacementView.poleNo=0;
		PZPlacementView.poleArrayX= new double[20];
		PZPlacementView.poleArrayY= new double[20];
		PZPlacementView.zeroArrayX= new double[20];
		PZPlacementView.zeroArrayY= new double[20];
		String strX = String.format("x: %3.2f",
				0.0);
		xCoord.setText(strX);
		String strY = String.format("y: %3.2f",
				0.0);
		yCoord.setText(strY);
		magnPlot.setValues(new double[128]); 		
		phasePlot.setValues(new double[128]);
		magnPlot.invalidate();
		phasePlot.invalidate();
	}
}

