package com.prototype.ajdsp;

import com.prototype.ajdsp.PartObject.PinSignalTypes;
import com.prototype.ajdsp.PartObject.PinTypes;


import android.app.Activity;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.AdapterView.OnItemSelectedListener;

public class WindowMenu extends Activity implements OnClickListener {
	RelativeLayout layout;
	RelativeLayout topbar;
	Button addParts;
	EditText windowLength;
	EditText beta;
	TextView betaText;
	
	Spinner windowTypeSpinner;
	EditText timeshift;
    Spinner periodicTypeSpinner;
    Button partList;
    double [] input=new double[256];
    public static int windowNumber=0;
    Bundle inputValues;
    Boolean create =true;
  

	@Override
	  public void onCreate(Bundle savedInstanceState) {
      super.onCreate(savedInstanceState);
      //requestWindowFeature(Window.FEATURE_CUSTOM_TITLE);
      requestWindowFeature(Window.FEATURE_NO_TITLE);
     setContentView(R.layout.window_layout);
     this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
     //getWindow().setFeatureInt(Window.FEATURE_CUSTOM_TITLE, R.layout.row);

     layout=(RelativeLayout)findViewById(R.id.windowMenu);
     layout.setBackgroundColor(Color.WHITE);
     
     windowTypeSpinner = (Spinner) findViewById(R.id.windowType);
     ArrayAdapter<CharSequence> windowTypeAdapter = ArrayAdapter.createFromResource(
             this, R.array.window_type_array, android.R.layout.simple_spinner_item);
     windowTypeAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
    // windowTypeSpinner.setBackgroundColor(Color.WHITE);
     windowTypeSpinner.setAdapter(windowTypeAdapter);
     windowTypeSpinner.setOnItemSelectedListener(new MyOnItemSelectedListener());
     windowLength=(EditText)findViewById(R.id.windowLength);
     
     beta=(EditText)findViewById(R.id.kaiserWindowBeta);
     betaText=(TextView)findViewById(R.id.kaiserWindowBetaText);
     
	 
     addParts=(Button)findViewById(R.id.add_part);
    // addParts.setVisibility(View.GONE); 
     addParts.setOnClickListener(this);
     partList=(Button) findViewById(R.id.back_select_parts);
		partList.setOnClickListener(this);
		TextView title= (TextView)findViewById(R.id.Title);
		title.setText("Window");
    
     Bundle extras=getIntent().getExtras();
     if(extras!=null){
    	 
     inputValues = extras.getBundle("blockValues");
     input=inputValues.getBundle("leftPin").getDoubleArray("signalMag");
     windowTypeSpinner.setSelection((int) inputValues.getLong("windowType"));
     Integer lengthValue=inputValues.getInt("windowLength");
	 windowLength.setText(lengthValue.toString());
	 Double betaValue=inputValues.getDouble("beta");
	 beta.setText(betaValue.toString());	
	 input=inputValues.getBundle("leftPin").getDoubleArray("signalMag");
	 create=false;
	 addParts.setText("Update");
	 partList.setText("Back");
     } else{
     Integer lengthValue=40;
	 windowLength.setText(lengthValue.toString());
	 Double betaValue=3.0;
	 beta.setText(betaValue.toString());	
     }
	

}
	@Override
	public void onBackPressed() {
		/*if(create){
			Intent i = new Intent(this, PartList.class);
			startActivity(i);}
		else {
			Intent i = new Intent(this, StartView.class);
			startActivity(i);
		}*/
	}
	@Override
	 public void onStop() {
		 super.onStop();	
				finish();
				
	 }
	 public class MyOnItemSelectedListener implements OnItemSelectedListener {

	        public void onItemSelected(AdapterView<?> parent,
	            View view, int pos, long id) {
	        	beta.setVisibility(View.GONE);
	        	betaText.setVisibility(View.GONE);
	        	
	        	
	            switch((int)id){
	        	case 5:
	        		beta.setVisibility(View.VISIBLE);
	        		betaText.setVisibility(View.VISIBLE);
	        		break;
	        	default:
	           	    break;
	           }
	      }

	        public void onNothingSelected(AdapterView parent) {
	          // Do nothing.
	        }
	    }
	   
	 private Double value(String input){
		  if (input.equalsIgnoreCase("")||input.equalsIgnoreCase("-")){
			  return 0.0;
		  }
		  else return Double.parseDouble(input);
	  }
	  
	 
	  private Integer valueInt(String input){
		  if (input.equalsIgnoreCase("")){
			  return 0;
		  }
		  else return Integer.parseInt(input);
	  }
	@Override
	public void onClick(View v) {
		if (v == addParts){
		Bundle windowInput=new Bundle();
			
			
			String lengthString=windowLength.getText().toString();
	        String betaString=beta.getText().toString();
			
		
			windowInput.putLong("windowType", windowTypeSpinner.getSelectedItemId());
			if(lengthString.length()>3){
	  			Toast.makeText(getApplicationContext(), "Window Length should not be larger than 256", Toast.LENGTH_LONG).show();
	  			
	  		} else if((valueInt(lengthString)>256)){
	  			Toast.makeText(getApplicationContext(), "Window Length should not be larger than 256", Toast.LENGTH_LONG).show();
	  		} else {
			windowInput.putInt("windowLength",valueInt(lengthString));
		    windowInput.putDouble("beta",value(betaString));
		
		
	    Bundle outPutArray=new Bundle();
	    
	    
	    outPutArray.putDoubleArray("signalMag", input);
	 
		windowInput.putBundle("leftPin", outPutArray);
		windowInput.putBundle("rightPin", outPutArray);
		if(create){
		windowNumber++;
		PinSignalTypes windowSigType=new PinSignalTypes();
		windowInput.putSerializable("pinSignalType", windowSigType);
		
		windowInput.putString("title", "Window"+" "+windowNumber);
		PinTypes types=new PinTypes();
		types.rightPin=true;
		types.leftPin=true;
		windowInput.putSerializable("PinTypes",types);
		windowInput.putString("signalType","time");
	
		PartsController.createBlock(windowInput);
		}
		else {
			windowInput.putString("title", inputValues.getString("title"));
			
			
			PartsController.updateBlock(windowInput);
		}
		Intent i = new Intent(this, StartView.class);
		startActivity(i);
	  		}
		}
		
		if(v==partList){
			if(create){
				Intent i = new Intent(this, PartList.class);
				startActivity(i);}
				 else {
					 Intent i = new Intent(this, StartView.class);
						startActivity(i);
				 }

			}
	   }
		
	}

