package com.prototype.ajdsp;

import android.os.Bundle;

public class AdderCalc {
	int MAX_SIZE=256;
	public double y[] = new double[MAX_SIZE];
	public double phase[] = new double[MAX_SIZE];
	public int signU=1,signL=1,topPinType,bottomPinType;
	public double inputTopMag[];
	public double inputBottomMag[];
	public double inputTopPhase[];
	public double inputBottomPhase[];
	public double inputTopPulseWidth;
	public double inputBottomPulseWidth;
	public Bundle Adding(Bundle inputBundle){
		topPinType=(int)inputBundle.getLong("topPinType");
		bottomPinType=(int)inputBundle.getLong("bottomPinType");
		if(topPinType==0)
			signU=1;
		else if(topPinType==1)
			signU=-1;
		if(bottomPinType==0)
			signL=1;
		else 
			if(bottomPinType==1)
				signL=-1;
		inputTopMag=inputBundle.getBundle("leftTopPin").getDoubleArray("signalMag");
		inputTopPhase=inputBundle.getBundle("leftTopPin").getDoubleArray("signalPhase");
		inputTopPulseWidth=inputBundle.getBundle("leftTopPin").getDouble("pulseWidth");
		
		inputBottomMag=inputBundle.getBundle("leftBottomPin").getDoubleArray("signalMag");
		inputBottomPhase=inputBundle.getBundle("leftBottomPin").getDoubleArray("signalPhase");
		inputBottomPulseWidth=inputBundle.getBundle("leftBottomPin").getDouble("pulseWidth");

		for (int i=0;i<MAX_SIZE;i++){
			y[i]=signU*inputTopMag[i]+signL*inputBottomMag[i];
			phase[i]=signU*inputTopPhase[i]+signL*inputBottomPhase[i];
		}
		double  maxPulseWidth=Math.max(inputTopPulseWidth, inputBottomPulseWidth);
		inputBundle.getBundle("rightPin").putDoubleArray("signalMag", y);
		inputBundle.getBundle("rightPin").putDoubleArray("signalPhase", phase);
		inputBundle.getBundle("rightPin").putDouble("pulseWidth", maxPulseWidth);
		return inputBundle;
	}
}
