package com.prototype.ajdsp;





import com.prototype.ajdsp.PZPlacementView.ConjugatePoleZero;

import android.R;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Rect;
import android.graphics.Paint.Style;
import android.media.MediaPlayer;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.view.View;
import android.view.View.MeasureSpec;
import android.view.View.OnLongClickListener;

public class MIDIView  extends SurfaceView  implements SurfaceHolder.Callback {
	private MIDIThread _thread;  
	int numberOfButtons = 13;
	MediaPlayer wave;
	private static ButtonPressedListener mPressedListener;
	float mm = 15; //rectangle margin from window

	int cntrX;
	int cntrY;
	
	Paint touchPaint=new Paint();
	Paint smallTouchPaint=new Paint();	
	Paint smallButtonPaint=new Paint();
	Paint rectPaint=new Paint();
	public static double frequency;
	

	float lengthOfButtons ;
	float lengthOfSmallButtons;
	float halfWidthOfSmallButtons;
	float widthOfButtons;
	float x,y;
	int fs=8000;
	float height,width;
	 public static double []magn=new double[256];
	double []phase=new double[256];
	public static double[] timePlot;

	public static int buttonPressed = -1; //from 0 to 99 for big buttons. the respective small button is 100+big button number

	float topLeftCornerX[] = new float[numberOfButtons];

	boolean hasSmallButtonRight[]= new boolean[numberOfButtons];
	boolean hasSmallButtonLeft[]= new boolean[numberOfButtons];
	boolean aButtonIsPressed;
	interface ButtonPressedListener {
		void onButtonPressed(int buttonPressed);
	}
	public MIDIView(final Context context) {

		super(context,null,0);   

		getHolder().addCallback(this);

		
		setFocusable(true);
		mPressedListener = null;

	}
	public MIDIView(final Context context, final AttributeSet attrs) {
		this(context, attrs, 0);
		
	}
	public MIDIView(final Context context, final AttributeSet attrs,
			final int defStyle) {
		super(context, attrs, defStyle);   
		getHolder().addCallback(this);

	//	_thread = new MIDIThread(getHolder(), this);
		
		setFocusable(true);
		mPressedListener = null;
		
	}	
	
	public void setPositionListener(ButtonPressedListener listener) {
		mPressedListener = listener;
	}
	@Override
	public void onDraw(Canvas canvas) {
		if(canvas!=null){
			
		super.onDraw(canvas); 
		height=getHeight();
	    width=getWidth();
		canvas.drawColor(Color.WHITE);	
	
		lengthOfButtons = 9*height/10;
		lengthOfSmallButtons = 90*lengthOfButtons/150;
		halfWidthOfSmallButtons = width/39;
		widthOfButtons = width/13;			
		smallTouchPaint.setColor(Color.RED);
		smallButtonPaint.setColor(Color.BLACK);
		rectPaint.setStyle(Style.STROKE);
		rectPaint.setColor(Color.BLACK);
		for (int i=0; i<numberOfButtons; i++)
		{
			topLeftCornerX[i] = i*widthOfButtons;
			canvas.drawRect(topLeftCornerX[i],0,topLeftCornerX[i]+widthOfButtons,lengthOfButtons,rectPaint);
			touchPaint.setColor(Color.BLUE);
			//paint the selected button
			if (aButtonIsPressed == true && i == buttonPressed){
				
				canvas.drawRect(topLeftCornerX[i],0,topLeftCornerX[i]+widthOfButtons,lengthOfButtons,touchPaint);
				
				
			}
		}

		//draw the small buttons
		for (int i=0; i<numberOfButtons; i++)
		{

			switch(i)
			{
			case 1: break ; //where NOT to place small buttons
			case 5: break;
			case 8: break;
			case 12: break;
			default: {
				canvas.drawRect(i*widthOfButtons+widthOfButtons-halfWidthOfSmallButtons,0,i*widthOfButtons+widthOfButtons+halfWidthOfSmallButtons,lengthOfSmallButtons,smallButtonPaint);
				hasSmallButtonRight[i]=true;
				if (i<numberOfButtons) hasSmallButtonLeft[i+1]=true;
				//paint the selected button
				if (aButtonIsPressed == true && (100+i) == buttonPressed){

					canvas.drawRect(i*widthOfButtons+widthOfButtons-halfWidthOfSmallButtons,0,i*widthOfButtons+widthOfButtons+halfWidthOfSmallButtons,lengthOfSmallButtons,smallTouchPaint);

				}

			}//place a small button
			}

		}
		}
	}
	/*
	@Override
	public boolean onTouchEvent(MotionEvent event) {

		synchronized (_thread.getSurfaceHolder()) {
			if (event.getAction() == MotionEvent.ACTION_DOWN) {
				x=(int) event.getX();
				y=(int) event.getY();
				for (int i=0; i<numberOfButtons; i++){//1
					if ( x >=topLeftCornerX[i]  & x < topLeftCornerX[i]+widthOfButtons){//1.1
						  aButtonIsPressed = true;
						buttonPressed = i;
						
						if ((hasSmallButtonLeft[i]==true || hasSmallButtonRight[i]==true) && y<lengthOfSmallButtons )
						{//1.1.1
							//System.out.println("small button might have been pressed");
							if ( hasSmallButtonLeft[i]==true & x >= topLeftCornerX[i] & x <= topLeftCornerX[i] + halfWidthOfSmallButtons)
								buttonPressed= 100+(i-1);//System.out.println("small left button pressed");
							else if (hasSmallButtonRight[i]==true & x >= topLeftCornerX[i]+widthOfButtons-halfWidthOfSmallButtons & x <= topLeftCornerX[i] + widthOfButtons)
								buttonPressed= 100+i;//System.out.println("small right button pressed");
						}
					}
					
					if (mPressedListener != null) {
						

						mPressedListener.onButtonPressed(buttonPressed);  
						int l=0;
						l=l+1;
									}
							
				}
			} 
			else if(event.getAction() == MotionEvent.ACTION_MOVE){
				
				aButtonIsPressed=false;
					
				}
			else if(event.getAction() == MotionEvent.ACTION_UP){
					
				aButtonIsPressed=false;
					
				}
		}
		return super.onTouchEvent(event);
	} */
	@Override
	public boolean onTouchEvent(MotionEvent event) {

		synchronized (_thread.getSurfaceHolder()) {
			if (event.getAction() == MotionEvent.ACTION_DOWN) {
				x=(int) event.getX();
				y=(int) event.getY();
				for (int i=0; i<numberOfButtons; i++){//1
					if ( x >=topLeftCornerX[i]  & x < topLeftCornerX[i]+widthOfButtons){//1.1
						  aButtonIsPressed = true;
						buttonPressed = i;
						
						if ((hasSmallButtonLeft[i]==true || hasSmallButtonRight[i]==true) && y<lengthOfSmallButtons )
						{//1.1.1
							//System.out.println("small button might have been pressed");
							if ( hasSmallButtonLeft[i]==true & x >= topLeftCornerX[i] & x <= topLeftCornerX[i] + halfWidthOfSmallButtons)
								buttonPressed= 100+(i-1);//System.out.println("small left button pressed");
							else if (hasSmallButtonRight[i]==true & x >= topLeftCornerX[i]+widthOfButtons-halfWidthOfSmallButtons & x <= topLeftCornerX[i] + widthOfButtons)
								buttonPressed= 100+i;//System.out.println("small right button pressed");
						}
					}
					
					if (mPressedListener != null) {
						

						mPressedListener.onButtonPressed(buttonPressed);  
						int l=0;
						l=l+1;
									}
							
				}
			} 
			
			else if (event.getAction() == MotionEvent.ACTION_MOVE) {
				
			}


			else if(event.getAction() == MotionEvent.ACTION_UP){
				aButtonIsPressed=false;
			}   
		}
		return true;
	}

	 private double getFrequency(int keyNo)
    {
      switch(keyNo){
        case 0: return 146.83;
          case 100: return 138.59;
        case 1: return 164.81;
          case 101: return -1;
        case 2: return 174.61;
          case 102: return 185.00;
        case 3: return 196.00;
          case 103: return 207.65;
        case 4: return 220.00;
          case 104: return 233.08;
        case 5: return 246.94;
          case 105: return -1;
        case 6: return 261.63;
          case 106: return 277.18;
        case 7: return 293.67;
          case 107: return 311.13;
        case 8: return 329.63;
          case 108: return -1;
        case 9: return 349.23;
          case 109: return 369.99;
        case 10: return 392.00;
          case 110: return 415.30;
        case 11: return 440.00;
          case 111: return 466.16;
        case 12: return 493.88;
          case 112: return -1;
        case -200: return -200;
        default: return -1;
      }
    }
	 private double[] discretize(double frequency, int framesNo)
	    {
	    int sampleLength = framesNo*256;
	    double dt = 1/fs;
	    double cosine[] = new double[sampleLength];
	    
	    if (frequency >=0) //for the case of a space
	      for (int i = 0; i < sampleLength; i++)
	      cosine[i] = Math.cos(2*Math.PI*frequency*i*dt);

	    return cosine;
	    }
	
	protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
		super.onMeasure(widthMeasureSpec, heightMeasureSpec);

		int parentWidth = MeasureSpec.getSize(widthMeasureSpec);
		int parentHeight = MeasureSpec.getSize(heightMeasureSpec);
		this.setMeasuredDimension(
				parentWidth, 6*parentHeight/10);
	}
	  
	@Override
	public void surfaceChanged(SurfaceHolder arg0, int arg1, int arg2, int arg3) {
		// TODO Auto-generated method stub
		
		

	}

	@Override
	public void surfaceCreated(SurfaceHolder arg0) {
		// TODO Auto-generated method stub
		_thread = new MIDIThread(getHolder(), this);
		_thread.setRunning(true);
		_thread.start();

	}

	@Override
	public void surfaceDestroyed(SurfaceHolder holder) {
		// TODO Auto-generated method stub
		_thread.setRunning(false);
		_thread.onPause();
		/*boolean retry = true;
		_thread.setRunning(false);
		while (retry) {
			try {
				_thread.join();
				retry = false;
			} catch (InterruptedException e) {

			}
		}*/

	}
}
