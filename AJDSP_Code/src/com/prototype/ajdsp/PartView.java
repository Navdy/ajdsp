package com.prototype.ajdsp;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.ListIterator;
import java.util.Set;

import com.prototype.ajdsp.PartObject.Coordinates;
import com.prototype.ajdsp.PartObject.PinCoordinates;
import com.prototype.ajdsp.PartObject.PinTypes;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.Rect;
import android.os.Bundle;
import android.os.Parcel;
import android.os.Parcelable;
import android.util.AttributeSet;
//import android.view.GestureDetector.OnDoubleTapListener;
import android.view.GestureDetector.OnDoubleTapListener;
import android.view.View.MeasureSpec;
import android.view.Gravity;
import android.view.MotionEvent;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.Toast;

public class PartView extends SurfaceView implements SurfaceHolder.Callback {
	/**
	 * 
	 */

	private StartViewThread _thread;
	private List<PartObject> _graphics = Collections
			.synchronizedList(new ArrayList<PartObject>());
	private static boolean selectPin = false;
	private List<ConnectionData> connectionDataList = Collections
			.synchronizedList(new ArrayList<ConnectionData>());
	private int blockIndex = 0;
	private float touchPositionDownX = 0, touchPositionDownY = 0;
	private int selectedBlock = -1;
	private long lastTouchTime = -1;
	private boolean isPinSelected = false;
	private static PartObject selectedPart = null;
	private static String selectedPin = null;
	private static String inputOutput = null;
	int previousPositionX = -1;
	int previousPositionY = -1;
	public int width = 0;
	public int height = 0;
	boolean partDeleteShown = false;
	boolean connectionDeleteShown = false;
	DeleteDialog partDelete;
	DeleteDialog connectionDelete;
	FreqRespDialog freqRespDialog;
	Paint linePaint = new Paint();
	private boolean move = true;
	private boolean freqRespShown = false;
	public static int buttonHeight;
	static ConnectionData startConnection;

	private boolean ifMove = false;

	public PartView(Context context) {
		super(context);
		buttonHeight = BitmapFactory.decodeResource(getResources(),
				R.drawable.blue_button).getHeight();
		getHolder().addCallback(this);

		setFocusable(true);
		setOnLongClickListener(new OnLongClickListener() {
			@Override
			public boolean onLongClick(View v) {
				return false;
			}
		});
		setOnLongClickListener(new OnLongClickListener() {
			@Override
			public boolean onLongClick(View v) {
				if (ifMove)
					return false;
			

				partDelete = new DeleteDialog(getContext());
				connectionDelete = new DeleteDialog(getContext());

				if (selectedBlock != -1) {
					
					DeleteDialog.stopDraw = true;
					partDeleteShown = true;
					partDelete.setPartNumber(selectedBlock);
					partDelete.setType("Part");
					partDelete.getWindow().setFlags(
							WindowManager.LayoutParams.FLAG_NOT_TOUCH_MODAL,
							WindowManager.LayoutParams.FLAG_NOT_TOUCH_MODAL);

					partDelete.setCancelable(true);
					partDelete.show();
					
					WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
					lp.copyFrom(partDelete.getWindow().getAttributes());

					lp.x = (int) (touchPositionDownX - width / 2);
					lp.y = (int) touchPositionDownY + lp.height;
					lp.gravity = Gravity.TOP;
					lp.flags &= ~WindowManager.LayoutParams.FLAG_DIM_BEHIND;
					// lp.flags &=
					// WindowManager.LayoutParams.FLAG_NOT_TOUCH_MODAL;
					partDelete.getWindow().setAttributes(lp);
					return true;
				} else if ((selectedBlock == -1)
						&& (connectionDataList.size() != -1)
						&& (!partDeleteShown)) {

					for (int i = 0; i < connectionDataList.size(); i = i + 2) {
						float x1 = connectionDataList.get(i).pinData.xCoordinate;
						
						float y1 = connectionDataList.get(i).pinData.yCoordinate;
						float x2 = connectionDataList.get(i + 1).pinData.xCoordinate;
						float y2 = connectionDataList.get(i + 1).pinData.yCoordinate;

						List<Rect> rectList = new ArrayList<Rect>();
						double yMin = 0;
						double xMin = Math.min(x1, x2);
						if (xMin == x1)
							yMin = y1;
						else
							yMin = y2;

						double slope = (y2 - y1) / (x2 - x1);
						double length = Math
								.pow(Math.pow((y2 - y1), 2)
										+ Math.pow((x2 - x1), 2), .5);

						for (int j = 0; j < 10; j++) {

							Rect rect = new Rect();
							int left = (int) (x1 + j * (x2 - x1) / 10);
							int right = (int) (x1 + (j + 1) * (x2 - x1) / 10);
							int top = (int) (y1 + j * (y2 - y1) / 10);
							int bottom = (int) (y1 + (j + 1) * (y2 - y1) / 10);

							int offSet = 20;
							if ((left < right) && (top < bottom)) {
								rect.set(left - offSet, top - offSet, right
										+ offSet, bottom + offSet);
								rectList.add(rect);
							}
							if ((left > right) && (top < bottom)) {
								rect.set(right - offSet, top - offSet, left
										+ offSet, bottom + offSet);
								rectList.add(rect);
							}
							if ((left < right) && (top > bottom)) {
								rect.set(left - offSet, bottom - offSet, right
										+ offSet, top + offSet);
								rectList.add(rect);
							}
							if ((left > right) && (top > bottom)) {
								rect.set(right - offSet, bottom - offSet, left
										+ offSet, top + offSet);
								rectList.add(rect);
							}

						}

						for (Rect selectConn : rectList) {
							int touchPositionX = (int) touchPositionDownX;
							int touchPositionY = (int) touchPositionDownY;

							if (selectConn.contains(touchPositionX,
									touchPositionY)) {
								DeleteDialog.stopDraw = true;
								connectionDeleteShown = true;
								connectionDelete.setType("Connection");
								connectionDelete.setConnectionNumber(i);
								connectionDelete.setButtonText("Delete Conn");

								connectionDelete
										.getWindow()
										.setFlags(
												WindowManager.LayoutParams.FLAG_NOT_TOUCH_MODAL,
												WindowManager.LayoutParams.FLAG_NOT_TOUCH_MODAL);

								connectionDelete.setCancelable(true);
								connectionDelete.show();

								WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
								lp.copyFrom(connectionDelete.getWindow()
										.getAttributes());

								lp.x = (int) (touchPositionDownX - width / 2);
								lp.y = (int) touchPositionDownY + lp.height;
								lp.gravity = Gravity.TOP;
								lp.flags &= ~WindowManager.LayoutParams.FLAG_DIM_BEHIND;

								connectionDelete.getWindow().setAttributes(lp);
								break;

							} else {
								// partDelete.dismiss();
								// connectionDelete.dismiss();

							}
						}

					}
					return true;
				}

				return true;
			}

		});
	}

	public boolean ifDescendant(PartObject parent, PartObject child,
			String parentPin, String childPin) {
		boolean isChild = false;
		List<PartObject> childList = parent.getChildList();
		if (childList.contains(child) && !seeIfOutputPin(parentPin)
				&& seeIfOutputPin(childPin))
			return true;

		for (PartObject partObject : childList) {
			if (ifDescendant(partObject, child, parentPin, childPin)) {
				isChild = true;
				break;
			}

		}
		return isChild;

	}

	@Override
	protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
		super.onMeasure(widthMeasureSpec, heightMeasureSpec);
		// String parentClass= getContext().getClass().getName();
		// int height=StartView.button_height;
		int parentWidth = MeasureSpec.getSize(widthMeasureSpec);
		int parentHeight = MeasureSpec.getSize(heightMeasureSpec);
		this.setMeasuredDimension(parentWidth, parentHeight - buttonHeight);
		int a = 0;
	}

	/*
	 * public void showAlerts(){ AlertDialog.Builder builder = new
	 * AlertDialog.Builder(this.getContext());
	 * builder.setMessage("Are you sure you want to exit?")
	 * .setCancelable(false) .setPositiveButton("Yes", new
	 * DialogInterface.OnClickListener() { public void onClick(DialogInterface
	 * dialog, int id) { // MyActivity.this.finish(); } })
	 * .setNegativeButton("No", new DialogInterface.OnClickListener() { public
	 * void onClick(DialogInterface dialog, int id) { dialog.cancel(); } });
	 * AlertDialog alert = builder.create(); alert.show(); }
	 */

	public void setParameters(List<PartObject> partList,
			List<ConnectionData> connectionList) {
		_graphics = partList;
		connectionDataList = connectionList;
	}

	public boolean seeIfOutputPin(String pinSelected) {
		// TODO Auto-generated method stub

		if (pinSelected.equalsIgnoreCase("topPin")
				|| pinSelected.equalsIgnoreCase("rightPin")
				|| pinSelected.equalsIgnoreCase("rightTopPin")
				|| pinSelected.equalsIgnoreCase("rightBottomPin")) {

			return true;
		}

		else
			return false;
	}

	@Override
	public boolean onTouchEvent(MotionEvent event) {
		synchronized (_thread.getSurfaceHolder()) {
			if (event.getAction() == MotionEvent.ACTION_DOWN) {
				if (partDeleteShown) {
					partDelete.dismiss();
					partDeleteShown = false;
					DeleteDialog.stopDraw = false;

				}
				if (connectionDeleteShown) {
					connectionDelete.dismiss();
					connectionDeleteShown = false;
					DeleteDialog.stopDraw = false;
				}
				if (freqRespShown) {
					freqRespDialog.dismiss();
					freqRespShown = false;
				}

				float touchPositionX = event.getX();
				float touchPositionY = event.getY();

				if ((previousPositionX == -1) && (previousPositionY == -1)) {
					previousPositionX = (int) touchPositionX;
					previousPositionY = (int) touchPositionY;
				}

				touchPositionDownX = touchPositionX;
				touchPositionDownY = touchPositionY;
				Rect partSelector = new Rect();
				long thisTime = System.currentTimeMillis();

				if (thisTime - lastTouchTime < 250) {
					for (PartObject graphic : Reversed.reversed(_graphics)) {
						partSelector.set(graphic.getCoordinates().getLeftX(),
								graphic.getCoordinates().getTopY(), graphic
										.getCoordinates().getRightX(), graphic
										.getCoordinates().getBottomY());
						if (partSelector.contains((int) touchPositionX,
								(int) touchPositionY)) {

							if (graphic._partName.contains("Sig Gen")) {
								Intent i = new Intent(this.getContext(),
										SigGenMenu.class);

								i.putExtra("blockValues",
										graphic.getBlockData());
								getContext().startActivity(i);
							} else if ((graphic._partName.contains("Plot"))
									&& (!graphic._partName.contains("PZ Plot"))) {

								PlotDialog plotDialog = new PlotDialog(
										getContext());
								double[] signalMag = graphic.getBlockData()
										.getBundle("leftPin")
										.getDoubleArray("signalMag");
								plotDialog.setInputBundle(graphic
										.getBlockData().getBundle("leftPin"));
								plotDialog.setTitle("Plot Options");
								plotDialog.show();

								WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
								lp.copyFrom(plotDialog.getWindow()
										.getAttributes());
								lp.width = WindowManager.LayoutParams.FILL_PARENT;
								lp.flags &= ~WindowManager.LayoutParams.FLAG_DIM_BEHIND;
								plotDialog.getWindow().setAttributes(lp);
							} else if (graphic._partName
									.equalsIgnoreCase("Conv Demo")) {
								Intent i = new Intent(this.getContext(),
										Mode.class);
								getContext().startActivity(i);
							} else if ((graphic._partName.contains("FFT"))&& (!graphic._partName.contains("IFFT"))) {
								Intent i = new Intent(this.getContext(),
										FFTMenu.class);
								Bundle fftDisplayData = graphic.getBlockData();
								i.putExtra("blockValues", fftDisplayData);
								getContext().startActivity(i);
							} else if (graphic._partName.contains("IFFT")) {
								Intent i = new Intent(this.getContext(),
										IFFTMenu.class);
								Bundle ifftDisplayData = graphic.getBlockData();
								i.putExtra("blockValues", ifftDisplayData);
								getContext().startActivity(i);
							} else if (graphic._partName
									.contains("Filter Coeff")) {
								Intent i = new Intent(this.getContext(),
										FilterCoeffMenu.class);
								Bundle filterCoeffData = graphic.getBlockData();
								i.putExtra("blockValues", filterCoeffData);
								getContext().startActivity(i);
							} else if (graphic._partName.contains("Freq Resp")) {
								freqRespDialog = new FreqRespDialog(
										getContext());
								freqRespDialog
										.setTitle("Plot Magnitude or Phase");
								freqRespDialog.setInputBundle(graphic
										.getBlockData());
								freqRespDialog
										.getWindow()
										.setFlags(
												WindowManager.LayoutParams.FLAG_NOT_TOUCH_MODAL,
												WindowManager.LayoutParams.FLAG_NOT_TOUCH_MODAL);
								freqRespDialog.setTitle("Freq Resp Plot");
								freqRespDialog.setCancelable(true);
								freqRespDialog.show();
								freqRespShown = true;

								WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
								lp.copyFrom(freqRespDialog.getWindow()
										.getAttributes());
								lp.width = WindowManager.LayoutParams.FILL_PARENT;
								lp.flags &= ~WindowManager.LayoutParams.FLAG_DIM_BEHIND;

								freqRespDialog.getWindow().setAttributes(lp);

							} else if (graphic._partName.contains("PZ Plot")) {
								Intent i = new Intent(this.getContext(),
										PZPlotPart.class);
								Bundle pzPlotData = graphic.getBlockData()
										.getBundle("pzCoord");
								i.putExtra("blockValues", pzPlotData);
								getContext().startActivity(i);
							} else if (graphic._partName.contains("Kaiser")) {
								Intent i = new Intent(this.getContext(),
										KaiserMenu.class);
								i.putExtra("blockValues",
										graphic.getBlockData());
								getContext().startActivity(i);
							} else if (graphic._partName.contains("Parks")) {
								Intent i = new Intent(this.getContext(),
										PmlMenu.class);
								i.putExtra("blockValues",
										graphic.getBlockData());
								getContext().startActivity(i);
							} else if (graphic._partName.contains("Window")) {
								Intent i = new Intent(this.getContext(),
										WindowMenu.class);
								i.putExtra("blockValues",
										graphic.getBlockData());
								getContext().startActivity(i);
							} else if (graphic._partName.contains("FIR Design")) {
								Intent i = new Intent(this.getContext(),
										FIRDesignMenu.class);
								i.putExtra("blockValues",
										graphic.getBlockData());
								getContext().startActivity(i);
							} else if (graphic._partName.contains("IIR Design")) {
								Intent i = new Intent(this.getContext(),
										IIRDesignMenu.class);
								i.putExtra("blockValues",
										graphic.getBlockData());
								getContext().startActivity(i);
							} else if (graphic._partName.contains("Mixer")) {
								Intent i = new Intent(this.getContext(),
										AdderMenu.class);
								i.putExtra("blockValues",
										graphic.getBlockData());
								getContext().startActivity(i);
							} else if (graphic._partName.contains("Up Sampler")) {
								Intent i = new Intent(this.getContext(),
										UpSamplerMenu.class);
								i.putExtra("blockValues",
										graphic.getBlockData());
								getContext().startActivity(i);
							} else if (graphic._partName
									.contains("Down Sampler")) {
								Intent i = new Intent(this.getContext(),
										DownSamplerMenu.class);
								i.putExtra("blockValues",
										graphic.getBlockData());
								getContext().startActivity(i);
							} else if (graphic._partName
									.contains("PZ Placement")) {
								Intent i = new Intent(this.getContext(),
										PZPlacementDemo.class);
								i.putExtra("blockValues",
										graphic.getBlockData());
								getContext().startActivity(i);
							} else if (graphic._partName.contains("Freq Samp")) {
								Intent i = new Intent(this.getContext(),
										FreqSamplerMenu.class);
								i.putExtra("blockValues",
										graphic.getBlockData());
								getContext().startActivity(i);
							} else if (graphic._partName.contains("MIDI")) {
								Intent i = new Intent(this.getContext(),
										MIDIDisplay.class);
								i.putExtra("blockValues",
										graphic.getBlockData());
								getContext().startActivity(i);
							} else if (graphic._partName.contains("DTMF")) {
								Intent i = new Intent(this.getContext(),
										DTMFTone.class);
								i.putExtra("blockValues",
										graphic.getBlockData());
								getContext().startActivity(i);
							} else if (graphic._partName.contains("BW Exp")) {
								Intent i = new Intent(this.getContext(),
										BWMenu.class);
								i.putExtra("blockValues",
										graphic.getBlockData());
								getContext().startActivity(i);
							} else if (graphic._partName.contains("PZ2Coeff")) {
								Intent i = new Intent(this.getContext(),
										PZ2CoeffWindow.class);
								i.putExtra("blockValues",
										graphic.getBlockData());
								getContext().startActivity(i);
							}
							else if (graphic._partName.contains("Peak Picking")) {
								Intent i = new Intent(this.getContext(),
										PkPickingMenu.class);
								i.putExtra("blockValues",
										graphic.getBlockData());
								getContext().startActivity(i);
							}else if (graphic._partName.contains("SNR")) {
								Intent i = new Intent(this.getContext(),
										SNRMenu.class);
								i.putExtra("blockValues",
										graphic.getBlockData());
								getContext().startActivity(i);
							}

							break;
						}
					}
					lastTouchTime = -1;
				} else {
					// too slow
					lastTouchTime = thisTime;
				}

				blockIndex = _graphics.size();
				selectedBlock = -1;
				for (PartObject graphic : Reversed.reversed(_graphics)) {

					partSelector.set(graphic.getCoordinates().getLeftX(),
							graphic.getCoordinates().getTopY(), graphic
									.getCoordinates().getRightX(), graphic
									.getCoordinates().getBottomY());
					blockIndex = blockIndex - 1;
					if (partSelector.contains((int) touchPositionX,
							(int) touchPositionY)) {
						selectedBlock = blockIndex;

						if (graphic.isPinSelected((int) touchPositionX,
								(int) touchPositionY)) {
							String pinSelected = graphic.getPinSelected();

							isPinSelected = true;
							graphic.highlightPin(graphic.getPinTypes(),
									pinSelected);
							boolean isOutputPin = seeIfOutputPin(pinSelected);

							if (!selectPin && isOutputPin
									&& !graphic.getIsPinConnected(pinSelected)) {

								selectPin = true;
								graphic.setHasConnections();
								selectedPart = graphic;
								selectedPin = pinSelected;
								startConnection = new ConnectionData();
								startConnection.part = graphic;
								startConnection.pinData = new PinCoordinates();
								startConnection.pinType = pinSelected;
								inputOutput = "output";

							} else if (!selectPin && !isOutputPin
									&& !graphic.getIsPinConnected(pinSelected)) {
								selectPin = true;
								// graphic._isLeftPinConnected=true;
								graphic.setHasConnections();

								selectedPart = graphic;
								selectedPin = pinSelected;
								startConnection = new ConnectionData();

								startConnection.part = graphic;
								startConnection.pinData = new PinCoordinates();
								startConnection.pinType = pinSelected;
								// connectionDataList.add(connection);
								inputOutput = "input";

							} else if (selectPin
									&& (selectedPart.getPartName()
											.equalsIgnoreCase(
													graphic.getPartName())
											|| graphic
													.getIsPinConnected(pinSelected)
											|| selectedPart._isdeleted
											||signalMatch(graphic,pinSelected,selectedPart,selectedPin) 
											|| ifDescendant(selectedPart,
													graphic, selectedPin,
													pinSelected) || ifDescendant(
												graphic, selectedPart,
												pinSelected, selectedPin))) {
								selectedPart.setIsPinConnected(selectedPin,
										false);
								selectPin = false;
								inputOutput = null;

								selectedPart = null;
								selectedPin = null;
								startConnection = new ConnectionData();

								// connectionDataList.remove(connectionDataList.size()-1);
							}

							else if (selectPin && !isOutputPin
									&& inputOutput.equalsIgnoreCase("output")) {
								graphic.setHasConnections();
								Bundle data = selectedPart
										.getPinData(selectedPin);
								graphic.setPinData(pinSelected, data);
								selectedPart.addChild(graphic, pinSelected,
										selectedPin);
								PartsController.updateBlock(graphic
										.getBlockData());
								PinCoordinates pinCenter = graphic
										.getSelectedPinCoordinates(pinSelected);

								ConnectionData connection = new ConnectionData();
								connection.part = graphic;
								connection.pinData = pinCenter;
								connection.pinType = pinSelected;
								connectionDataList.add(connection);
								startConnection.pinData = startConnection.part
										.getSelectedPinCoordinates(startConnection.pinType);
								connectionDataList.add(startConnection);
								startConnection = new ConnectionData();

								PartsController.connectionList = connectionDataList;
								selectPin = false;
								inputOutput = null;

								selectedPart = null;
								selectedPin = null;
							}

							else if (selectPin && isOutputPin
									&& inputOutput.equalsIgnoreCase("input")) {

								graphic.setHasConnections();
								Bundle data = graphic.getPinData(pinSelected);

								selectedPart.setPinData(selectedPin, data);
								graphic.addChild(selectedPart, selectedPin,
										pinSelected);

								PartsController.updateBlock(selectedPart
										.getBlockData());
								selectedPart = null;
								selectedPin = null;
								PinCoordinates pinCenter = graphic
										.getSelectedPinCoordinates(pinSelected);

								ConnectionData connection = new ConnectionData();
								connection.part = graphic;
								connection.pinData = pinCenter;
								connection.pinType = pinSelected;

								connectionDataList.add(connection);
								startConnection.pinData = startConnection.part
										.getSelectedPinCoordinates(startConnection.pinType);
								connectionDataList.add(startConnection);
								startConnection = new ConnectionData();
								inputOutput = null;
								selectPin = false;
							} else {
								selectedPart = null;
								selectedPin = null;
								inputOutput = null;
								selectPin = false;
								startConnection = new ConnectionData();
								// connectionDataList.remove(connectionDataList.size()-1);
							}

						}

						break;
					}

				}

			} else if (event.getAction() == MotionEvent.ACTION_MOVE) {

				int touchPositionX = (int) event.getX();
				int touchPositionY = (int) event.getY();
				if ((touchPositionX < width) && (touchPositionY < height)
						&& (touchPositionX > 0) && (touchPositionY > 0)) {
					Rect touchPosition = new Rect();
					touchPosition.set(previousPositionX - 20,
							previousPositionY - 20, previousPositionX + 20,
							previousPositionY + 20);
					if (!touchPosition.contains(touchPositionX, touchPositionY)) {
						ifMove = true;
						if (selectedBlock != -1) {
							Rect partCoord = new Rect();
							partCoord.set(_graphics.get(selectedBlock)
									.getCoordinates().getLeftX(), _graphics
									.get(selectedBlock).getCoordinates()
									.getTopY(), _graphics.get(selectedBlock)
									.getCoordinates().getRightX(), _graphics
									.get(selectedBlock).getCoordinates()
									.getBottomY());

							if (partCoord.contains(touchPositionX,
									touchPositionY)) {
								_graphics.get(selectedBlock).getCoordinates()
										.setX((int) event.getX());
								_graphics.get(selectedBlock).getCoordinates()
										.setY((int) event.getY());
								PartObject part = _graphics.get(selectedBlock);

								if (part.hasConnections()) {
									if (part._isRightPinConnected) {
										PinCoordinates pinCenter = part
												.getRightPinCoordinates();
										for (ConnectionData connection : connectionDataList) {
											if ((connection.part.equals(part))
													&& (connection.pinType
															.equalsIgnoreCase("rightPin"))) {
												connection.pinData = pinCenter;
											}
										}
									}
									if (part._isLeftPinConnected) {
										PinCoordinates pinCenter = part
												.getLeftPinCoordinates();
										for (ConnectionData connection : connectionDataList) {
											if ((connection.part.equals(part))
													&& (connection.pinType
															.equalsIgnoreCase("leftPin"))) {
												connection.pinData = pinCenter;
											}
										}
									}
									if (part._isLeftTopPinConnected) {
										PinCoordinates pinCenter = part
												.getLeftTopPinCoordinates();
										for (ConnectionData connection : connectionDataList) {
											if ((connection.part.equals(part))
													&& (connection.pinType
															.equalsIgnoreCase("leftTopPin"))) {
												connection.pinData = pinCenter;
											}
										}
									}
									if (part._isLeftBottomPinConnected) {
										PinCoordinates pinCenter = part
												.getLeftBottomPinCoordinates();
										for (ConnectionData connection : connectionDataList) {
											if ((connection.part.equals(part))
													&& (connection.pinType
															.equalsIgnoreCase("leftBottomPin"))) {
												connection.pinData = pinCenter;
											}

										}
									}
									if (part._isTopPinConnected) {
										PinCoordinates pinCenter = part
												.getTopPinCoordinates();
										for (ConnectionData connection : connectionDataList) {
											if ((connection.part.equals(part))
													&& (connection.pinType
															.equalsIgnoreCase("topPin"))) {
												connection.pinData = pinCenter;
											}

										}
									}
									if (part._isBottomPinConnected) {
										PinCoordinates pinCenter = part
												.getBottomPinCoordinates();
										for (ConnectionData connection : connectionDataList) {
											if ((connection.part.equals(part))
													&& (connection.pinType
															.equalsIgnoreCase("bottomPin"))) {
												connection.pinData = pinCenter;
											}

										}
									}
									if (part._isRightTopPinConnected) {
										PinCoordinates pinCenter = part
												.getRightTopPinCoordinates();
										for (ConnectionData connection : connectionDataList) {
											if ((connection.part.equals(part))
													&& (connection.pinType
															.equalsIgnoreCase("rightTopPin"))) {
												connection.pinData = pinCenter;

											}

										}
									}
									if (part._isRightBottomPinConnected) {
										PinCoordinates pinCenter = part
												.getRightBottomPinCoordinates();
										for (ConnectionData connection : connectionDataList) {
											if ((connection.part.equals(part))
													&& (connection.pinType
															.equalsIgnoreCase("rightBottomPin"))) {
												connection.pinData = pinCenter;
											}

										}
									}
								}
								PartsController.partList = _graphics;
							}
						}
					}
				}
			} else if (event.getAction() == MotionEvent.ACTION_UP) {
				previousPositionX = -1;
				previousPositionY = -1;
				ifMove = false;
				blockIndex = -1;
				if (isPinSelected) {
					_graphics.get(selectedBlock).backtoSizePin(
							_graphics.get(selectedBlock).getPinTypes());
					isPinSelected = false;
				}

				/*
				 * if(selectedBlock!=-1){ long
				 * uptime=System.currentTimeMillis();
				 * 
				 * 
				 * if((uptime-downTime)>3000){ PartDeleteDialog partDelete = new
				 * PartDeleteDialog(this.getContext());
				 * partDelete.setPartNumber(selectedBlock); partDelete.show();
				 * WindowManager.LayoutParams lp = new
				 * WindowManager.LayoutParams();
				 * lp.copyFrom(partDelete.getWindow().getAttributes());
				 * lp.x=(int) event.getX(); lp.y=(int) event.getY();
				 * 
				 * partDelete.getWindow().setAttributes(lp); }
				 * 
				 * 
				 * }
				 */

			}

		}
		return super.onTouchEvent(event);
		// return true;
	}

	public static class ConnectionData {
		PartObject part;
		PinCoordinates pinData;

		String pinType;
	}

	@Override
	public void onDraw(Canvas canvas) {
		if (canvas != null) {
			width = canvas.getWidth();
			height = canvas.getHeight();
			canvas.drawColor(Color.WHITE);

			linePaint.setColor(Color.BLACK);

			linePaint.setStrokeWidth(2f);

			Bitmap bitmap;
			PartObject.Coordinates coords;

			for (PartObject graphic : _graphics) {
				bitmap = graphic.getGraphic();
				coords = graphic.getCoordinates();
				canvas.drawBitmap(bitmap, coords.getLeftX(), coords.getTopY(),
						null);
			}

			if (connectionDataList.size() != 0) {
				int connectionArraySize = connectionDataList.size() * 2;
				float[] connectionArray = new float[connectionArraySize];
				int arrayIndex = 0;
				for (ConnectionData connection : connectionDataList) {
					connectionArray[arrayIndex] = connection.pinData.xCoordinate;
					arrayIndex++;
					connectionArray[arrayIndex] = connection.pinData.yCoordinate;
					arrayIndex++;
				}

				canvas.drawLines(connectionArray, linePaint);
			}
		}
	}

	@Override
	public void surfaceChanged(SurfaceHolder holder, int format, int width,
			int height) {
		// TODO Auto-generated method stub
	}

	@Override
	public void surfaceCreated(SurfaceHolder holder) {
		_thread = new StartViewThread(getHolder(), this);
		if (!_thread._run) {

			_thread.setRunning(true);
			_thread.start();
		} else {
			_thread.onResume();
		}

	}

	@Override
	public void surfaceDestroyed(SurfaceHolder holder) {

		// boolean retry = true;

		_thread.setRunning(false);
		_thread.onPause();
		/*
		 * boolean retry = true; while (retry) { try { _thread.join(); retry =
		 * false; } catch (InterruptedException e) { // we will try it again and
		 * again... } }
		 */
	}

	public static class Reversed<T> implements Iterable<T> {
		private final List<T> original;

		public Reversed(List<T> original) {
			this.original = original;
		}

		public Iterator<T> iterator() {
			final ListIterator<T> i = original.listIterator(original.size());

			return new Iterator<T>() {
				public boolean hasNext() {
					return i.hasPrevious();
				}

				public T next() {
					return i.previous();
				}

				public void remove() {
					i.remove();
				}
			};
		}

		public static <T> Reversed<T> reversed(List<T> original) {
			return new Reversed<T>(original);
		}
	}
	public boolean signalMatch(PartObject part1,String pin1,PartObject part2,String pin2){
		String pinType1=part1.getSignalType(pin1);
		String pinType2=part2.getSignalType(pin2);
		if(pinType1.contains("|")&&pinType2.contains("|")){
			String []pinArray1=pinType1.split("|");
			String []pinArray2=pinType2.split("|");
			for(int i=0;i<pinArray2.length;i++){
				if(Arrays.asList(pinArray1).contains(pinArray2[i])){
					return false;
				}
			}
		}else if(!pinType1.contains("|")&&pinType2.contains("|")){
			if(pinType2.contains(pinType1))
				return false;
		}else if(pinType1.contains("|")&&!pinType2.contains("|")){
			if(pinType1.contains(pinType2))
				return false;
		}
		else if(!pinType1.contains("|")&&!pinType2.contains("|")){
			if(pinType1.equalsIgnoreCase(pinType2)){
				return false;
			}
		}
		//.equalsIgnoreCase(
			//	part2.getSignalType(pin2));
		
		return true;
	}

}
