package com.prototype.ajdsp;


import android.os.Bundle;

public class KaiserCalculator {
	  int Order, ftype;
	    double W1,W2,Wp1,Wp2,Ws1,Ws2,Rp,Rs,beta;
	    double[] aCoeff;  
	    double[] bCoeff;

	public Bundle getFilterCoeff(Bundle inputBundle){
		Wp1=inputBundle.getDouble("Wp1");
		Ws1=inputBundle.getDouble("Ws1");
		Wp2=inputBundle.getDouble("Wp2");
		Ws2=inputBundle.getDouble("Ws2");
		Rp=inputBundle.getDouble("PB");
		Rs=inputBundle.getDouble("SB");
		ftype=(int)inputBundle.getLong("filterType");
	
		 Kaiser Kp = new Kaiser(ftype, Wp1, Ws1, Wp2, Ws2, Rp, Rs);
		   Order=Kp.getOrder();
	        
	           aCoeff = new double[Order+1];
	           bCoeff = new double[Order+1];

		   if (Order>0)
		   {
			   W1=Kp.getW1();
	                   beta=Kp.getBeta();
			   W2=Kp.getW2();
			   Bundle bundle=new Bundle();
			   bundle.putLong("windowType", 5);
			   bundle.putInt("order", Order);
			   bundle.putDouble("beta", beta);
			   bundle.putDouble("W1", W1);
			   bundle.putDouble("W2", W2);
			   bundle.putInt("filterType",ftype);
			   
			   FIRDesignCalc Kfilter = new FIRDesignCalc();
			   Bundle filterCoeff=Kfilter.getFilterCoeff(bundle);
			  
			   bCoeff=filterCoeff.getDoubleArray("filterCoeffB");
			   aCoeff=filterCoeff.getDoubleArray("filterCoeffA");
		   }
		   Bundle filterCoeff=new Bundle();
		   filterCoeff.putDoubleArray("filterCoeffB", bCoeff);
		   filterCoeff.putDoubleArray("filterCoeffA", aCoeff);
		   filterCoeff.putInt("order",Order);
		   filterCoeff.putDouble("beta",beta);
		   
		   
		   return filterCoeff;
		   
		
	}

}
