package com.prototype.ajdsp;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;

import org.achartengine.ChartFactory;
import org.achartengine.GraphicalView;
import org.achartengine.chart.LineChart;
import org.achartengine.chart.PointStyle;
import org.achartengine.chart.XYChart;
import org.achartengine.chart.BarChart.Type;
import org.achartengine.model.CategorySeries;
import org.achartengine.model.RangeCategorySeries;
import org.achartengine.model.XYMultipleSeriesDataset;
import org.achartengine.model.XYSeries;
import org.achartengine.renderer.DefaultRenderer;
import org.achartengine.renderer.SimpleSeriesRenderer;
import org.achartengine.renderer.XYMultipleSeriesRenderer;
import org.achartengine.renderer.XYSeriesRenderer;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Paint.Align;
import android.graphics.Typeface;
import android.util.AttributeSet;
import android.view.View;
import android.view.View.MeasureSpec;
import android.widget.RelativeLayout;

public class PlotView extends View {
	int index=0;

	GraphicalView graph;
	private double[] inputArray;
	private String [] xAxisValues;
	GraphicalView view;
	DecimalFormat twoDForm = new DecimalFormat("#.###");
	public PlotView(final Context context) {
		super(context);	
		inputArray=new double[256];
		//graph=execute(context,);
	}




	public PlotView(final Context context, final AttributeSet attrs) {
		super(context, attrs);
		inputArray=new double[256];
		// graph=execute(context);
	}

	public PlotView(final Context context, final AttributeSet attrs,
			final int defStyle) {
		super(context, attrs, defStyle);
		inputArray=new double[256];
		// graph=execute(context);


	}

	@Override
	public void onDraw(Canvas canvas) {
		if(canvas!=null){
		graph.draw(canvas);
		postInvalidate();
		}
	}


	@Override
	protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
		super.onMeasure(widthMeasureSpec, heightMeasureSpec);
		int parentWidth = MeasureSpec.getSize(widthMeasureSpec);
		int parentHeight = MeasureSpec.getSize(heightMeasureSpec);
		this.setMeasuredDimension(
				parentWidth/2, parentHeight/2);
	}
	public GraphicalView execute(Context context,long sigType) { 
		String[] titles = new String[] {""};
		List<double[]> x = new ArrayList<double[]>();
		double[] valueArray=inputArray;
		double [] xArray=new double[valueArray.length];
		//if(xAxisValues==null){
			for(int i=0;i<valueArray.length;i++){
				xArray[i]=i;
			}
		
		
			
		for (int i = 0; i < titles.length; i++) {
			x.add(xArray);
		}

		List<double[]> values = new ArrayList<double[]>();
		values.add(valueArray);
		int[] colors = new int[] { Color.BLUE};
		PointStyle[] styles = new PointStyle[] { PointStyle.POINT};
		XYMultipleSeriesRenderer renderer = buildRenderer(colors, styles);

		double max=valueArray[0],min=0;

		for (int i = 0; i < valueArray.length; i++) 
		{
			if(valueArray[i]>max)
			{ 
				max=valueArray[i];

			}
		} 
		for (int i = 0; i < valueArray.length; i++) 
		{
			if(valueArray[i]<min)
			{ 
				min=valueArray[i];
			}
		} 

		int length = renderer.getSeriesRendererCount();

		for (int i = 0; i < 1; i++) {
			((XYSeriesRenderer) renderer.getSeriesRendererAt(0)).setFillPoints(true);
		}
		setChartSettings(renderer,"", 0, valueArray.length, min,max,
				Color.BLACK, Color.BLACK);
		

		renderer.setApplyBackgroundColor(true);
		renderer.setBackgroundColor(Color.WHITE);
		renderer.setMarginsColor(Color.WHITE);

		renderer.setShowLegend(false);

		renderer.setShowGrid(true);
		 if(xAxisValues!=null){
				for(int i=0;i<xAxisValues.length;i++){
					
					renderer.addXTextLabel(i*valueArray.length/(4),xAxisValues[i]);
				}
				renderer.setXLabels(0);
		 }
		 
		
		renderer.setXLabelsAlign(Align.RIGHT);
		renderer.setYLabelsAlign(Align.RIGHT);
		renderer.setTextTypeface("sans", Typeface.BOLD);
		renderer.setZoomButtonsVisible(true);
		//renderer.setZoomEnabled(true, true);
		//renderer.setZoomRate(3);
		renderer.setMargins(new int[] { 0, 25, -15, 0 });
	
		renderer.setPanLimits(new double[] { 0, valueArray.length+10, min-10,max+10 });
	
		renderer.setZoomLimits(new double[] { 0, valueArray.length+10, min-10, max+10 });
		if(sigType==0){
			view = ChartFactory.getLineChartView(context, buildDataset(titles, x, values),
					renderer);}
		else if(sigType==1){
			double yAxisMin=renderer.getYAxisMin();
			if(valueArray.length<50)
			renderer.setBarSpacing(20.0);
			else if(valueArray.length>=50)
				renderer.setBarSpacing(0.5);
     double[] valueBarArray =  values.get(0);
     XYMultipleSeriesDataset dataset = new XYMultipleSeriesDataset();
     RangeCategorySeries series = new RangeCategorySeries("Temperature");
    
     for (int k = 0; k < valueBarArray.length; k++) {
       series.add(Math.min(0, valueBarArray[k]), Math.max(0,valueBarArray[k]));
     }
     dataset.addSeries(series.toXYSeries());
			view=    ChartFactory.getRangeBarChartView(context, dataset, renderer,
					Type.STACKED);
		}
		return view;


	}


	protected void setChartSettings(XYMultipleSeriesRenderer renderer, String yTitle, double xMin, double xMax, double yMin, double yMax, int axesColor,
			int labelsColor) {

		renderer.setYTitle(yTitle);

		renderer.setXAxisMin(xMin);
		renderer.setXAxisMax(xMax);
		renderer.setYAxisMin(yMin);
		renderer.setYAxisMax(yMax);
		renderer.setAxesColor(axesColor);
		renderer.setLabelsColor(labelsColor);
		
	
	}
	protected XYMultipleSeriesRenderer buildRenderer(int[] colors, PointStyle[] styles) {
		XYMultipleSeriesRenderer renderer = new XYMultipleSeriesRenderer();
		setRenderer(renderer, colors, styles);
		return renderer;
	}
	protected void setRenderer(XYMultipleSeriesRenderer renderer, int[] colors, PointStyle[] styles) {
		renderer.setAxisTitleTextSize(25);
		renderer.setChartTitleTextSize(30);
		renderer.setLabelsTextSize(15);
		renderer.setLegendTextSize(15);
		renderer.setPointSize(5f);
        
		int length = colors.length;
		for (int i = 0; i < length; i++) {
			XYSeriesRenderer r = new XYSeriesRenderer();
			r.setColor(colors[i]);
			r.setLineWidth(r.getLineWidth()*3);

			r.setPointStyle(styles[i]);

			renderer.addSeriesRenderer(r);
		}


	}
	protected XYMultipleSeriesDataset buildDataset(String[] titles, List<double[]> xValues,
			List<double[]> yValues) {
		XYMultipleSeriesDataset dataset = new XYMultipleSeriesDataset();
	
		addXYSeries(dataset, titles, xValues, yValues, 0);
		return dataset;
	}

	public void addXYSeries(XYMultipleSeriesDataset dataset, String[] titles, List<double[]> xValues,
			List<double[]> yValues, int scale) {
		int length = titles.length;
		for (int i = 0; i < length; i++) {
			XYSeries series = new XYSeries(titles[0], scale);
			
			double[] xV = xValues.get(0);
			double[] yV = yValues.get(0);
			int seriesLength = xV.length;
			for (int k = 0; k < seriesLength; k++) {
				series.add(xV[k], yV[k]);
			}
			dataset.addSeries(series);
		}
	}

	public void setInputArray(double[]signalArray,double length){
		
		double []input=new double[(int) length];
		for(int i=0;i<length;i++){
			input[i]=Double.valueOf((signalArray[i]));
		}	   

		inputArray=input;
		//graph=execute(this.getContext());

	}
	public void setXAxisValues(String[] xAxis){
		xAxisValues=xAxis;
		//graph=execute(this.getContext());

	}


}
