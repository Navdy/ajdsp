package com.prototype.ajdsp;


import android.os.Bundle;

public class FreqSampCalculator {
	  public int number_seg, nolineseg, numsamp;
	    public double xgridsafe = 6.25; //xgrid line
	    public int ygridsafe = 5;       //ygrid line
	    int N,lengthX=250;
	    double ydiff, xdiff, alpha, scalefactor;
	    double Fs = 2*Math.PI;

	    public int num_len1,num_len2,den_len,num2_len,den2_len,num3_len,num4_len;
	    double a2_in[];
	    double b2_in[];
	    public int pn = 1;
	    public int pd = 2;
	    public double r = 0.985; ///<---------------------------------------------------stability
	    double a0_in[];
	    double b0_in[];
	    double a1_in[];
	    double b1_in[];
	    double a[];
	    double b[];
	    double c[];
	   

	    double aCoeff[] = new double[65];
	    double bCoeff[] = new double[65];
	    double magnK[];
	    //------------------->>>>>>>>>>>
	    public float datapoints[];	   
	    public float xpoints[] = new float[5];
	    public float ypoints[] = new float[5];
	    double [] y_mag = new double[5];
	    //------------------>>>>>>>>>>>>
	    public float xpoints2[] = new float[5];
	    public float ypoints2[] = new float[5];
	    public int currspots;
	    double x_seg[] = new double[65];
	    double x_seg2[] = new double[65];
	    //------------------->>>>>>>>>>>
	    double [] x = new double [65];
	    double [] y = new double [65];
	    double magn[];
	    double[] z = new double [65];
	    int[] m = new int [4];
	 //--------------------------------------5/15/03
	  public int fs_type = 0;
	  public int filt_typ=0;
	  public int index_ls = 0;
	public Bundle freqSamp(Bundle inputData){
		Bundle bundle=new Bundle();
		numsamp=inputData.getInt("numSamp");
		number_seg=inputData.getInt("numLinSeg");
		filt_typ=inputData.getInt("filterType");
		fs_type=inputData.getInt("mode");
		datapoints=inputData.getFloatArray("freqSampData");
		
		 for (int i=0;i<=4;i++)
	      {xpoints[i]=0;ypoints[i]=0;y_mag[i]=0;}

	      for (int i=0;i<=N-1;i++)
	      {
	        aCoeff[i]=0.0;
	        bCoeff[i]=0.0;
	      }
	      int j=0;
	      for(int i=0;i<datapoints.length;i=i+4){
	    	  xpoints[i/4]=datapoints[i];
	    	  j=i+1;
	          ypoints[i/4]=datapoints[j];
			}
	      FrequencyDomainSampling(); 
	      bundle.putDoubleArray("filterCoeffA", aCoeff);
	      bundle.putDoubleArray("filterCoeffB", bCoeff);	      
	      return bundle;
	}
	 public void FrequencyDomainSampling()
	  {
	      getSize();
	      alpha = (double)(N-1)/2;
	      magn = new double [N];
	      magnK = new double [N];

	      if (filt_typ == 1)
	      {
	        aCoeff = new double[N];
	        bCoeff = new double[N];
	        z = new double [N];
	      }
	      if (filt_typ == 0)
	      {
	        aCoeff = new double[64];
	        bCoeff = new double[64];
	      }

	      nolineseg = number_seg + 1;
	      scalefactor = (double)((double)(lengthX)/(double)(N-1));

	      for (int i=0;i<=N-1;i++)
	      {x_seg[i] = scalefactor*i;}

	      for (int i=0;i<=4;i++)
	      {
	          xpoints2[i] = xpoints[i]-33;
	          ypoints2[i] = ypoints[i]-33;
	      }

	      // Optimum Grid -- fixed point calculations

	      for (int i=0; i<=4; i++)
	      {
	        for (int j=1; j<=20; j++)
	        {
	          for(int p=0; p<=19; p++)
	          {
	            if(xpoints2[i] <= (int)(j*xgridsafe) && ypoints2[i] >= p*ygridsafe && ypoints2[i] <= (p+1)*ygridsafe)
	            {
	              xpoints2[i] = (int)((double)((double)Math.round((double)xpoints2[i]/xgridsafe))*xgridsafe);

	              if(ypoints2[i] >=95)ypoints2[i]=100;
	              else ypoints2[i] = Math.round(ypoints2[i]/ygridsafe)*ygridsafe;

	              y_mag[i] = 1-((double)ypoints2[i]/(double)100);
	            }

	          }
	        }
	      }
	      //-------------------------------------------------<<<<<<<<<<<<<<<<<<<<<<

	      for (int i=0;i<=3;i++)
	      {
	         if (y_mag[i] > y_mag[i+1])m[i]=-1;
	         else m[i]= 1;
	      }

	      for (int j=0; j<=(N/2)-1; j++) // Discretizing the desired frequency response
	      {
	         for (int i=1;i<=4;i++) // setting magnitude zero for outside the drawing area
	         {
	            if (nolineseg == i)if(x_seg[j]< xpoints2[0] || x_seg[j]>xpoints2[i])y[j]=0;
	         }

	          for (int k=0; k<=3;k++)
	          {
	            if(x_seg[j]>=xpoints2[k] && x_seg[j]<=xpoints2[k+1]+3)
	            {
	                if(m[k]==1) // positive slope line
	                {
	                  xdiff = (double)(xpoints2[k+1] - xpoints2[k]);
	                  ydiff = y_mag[k+1] - y_mag[k];
	                  x_seg2[j] = (x_seg[j]-xpoints2[k])/xdiff;
	                  magn[j] = x_seg2[j]*ydiff+y_mag[k];
	                }
	                if(m[k]==-1) // negative slope line
	                {
	                  xdiff = (double)(xpoints2[k+1] - xpoints2[k]);
	                  ydiff = y_mag[k] - y_mag[k+1];
	                  x_seg2[j] = (x_seg[j]-xpoints2[k])/xdiff;
	                  magn[j] = y_mag[k]-x_seg2[j]*ydiff;
	                }
	            }
	          }
	      }
	      int u = 1;
	      for (int j=N/2; j<=N-1; j++)
	      {
	        magn[j] = magn[j-u];              // Mirror image of the frequency response.
	        u=u+2;
	      }

	      magnK = magn;        // assigning a new variable to use it in the frequency sampling algorithm |H(k)|=magnK
	/*
	        System.out.println("------------------------------");
		    for (int k=0; k < N; k++)
	        {System.out.println("|H(k)|= "+magnK[k]);}*/


	      if (filt_typ ==1)
	      {    // Frequency Sampling Algorithm
	          if (fs_type == 0)
	          {
	              for (int n=0; n < N; n++)
	              {
	                for (int k=1; k < N/2;k++)
	                {
	                  z[n] = z[n]+(2*Math.abs(magnK[k])*Math.cos(Math.abs((Fs*k*(n-alpha))/N)));
	                }
	                bCoeff[n] = (z[n]+magnK[0])/N;
	              }
	       /*        System.out.println("------------------------------");
				   for (int i=0; i < N; i++)
	          	  {System.out.println("bCoeffN= "+bCoeff[i]);}*/
	          }
	          if (fs_type == 1) //Least Squares using half the sampling points
	          {
	             int L = N/2;
	             alpha = (double)(L-1)/2;
	             bCoeff = new double[L];

	             for (int n=0; n < L; n++)
	              {
	                for (int k=1; k < N/2;k++)
	                {
	                  z[n] = z[n]+(2*Math.abs(magnK[k])*Math.cos((Fs*k*(n-alpha))/N));
	                }
	                bCoeff[n] = (z[n]+magnK[0])/N;
	              }

				 /* System.out.println("------------------------------");
	              for (int i=0; i < L; i++)
	          	  {System.out.println("bCoeffLS= "+bCoeff[i]);}*/

	          }
	          aCoeff[0] = 1;

	      }
	      if (filt_typ == 0)
	      { // Frequency Sampling Algorithm

	          // Stability problems (for Recursive) can be avoided by sampling H(z)
	          // at a radius r (say 0.95) slightly less than unity
	          double[] b3_in = new double[2];
	          a = new double[N/2];
	          b = new double[N/2];
	          c = new double[N/2];
	          den_len = 0; num_len1 = 0; num_len2 =0;num2_len =0; den2_len = 0;

	         if (fs_type == 0)
	         {
	             for (int k=1;k < N/2; k++)
	             {
	                a[k] = magnK[k]*2*Math.cos(2*Math.PI*k*alpha/N);
	                b[k] = magnK[k]*(-2*r*Math.cos(2*Math.PI*k*(1+alpha)/N));
	                c[k] = -2*r*Math.cos(2*Math.PI*k/N);

	               if (k == 1)
	               {
	                    a2_in = new double[3];
	                    b2_in = new double[2];
	                    a2_in[0] = 1;    a2_in[1] = c[k];  a2_in[2]=r*r;
	                    b2_in[0] = a[k]; b2_in[1] = b[k];
	               }
	               if (k == 2)
	               {
	                     a0_in = new double[3];
	                     b0_in = new double[2];
	                     a1_in = new double[3];
	                     b1_in = new double[2];

	                    if (a[k-1] !=0 && b[k-1] !=0)
	                    {
	                      a0_in[0] = 1;      a0_in[1] = c[k-1];  a0_in[2]=r*r;
	                      b0_in[0] = a[k-1]; b0_in[1] = b[k-1];

	                      a1_in[0] = 1;    a1_in[1] = c[k];  a1_in[2]=r*r;
	                      b1_in[0] = a[k]; b1_in[1] = b[k];

	                        double num1_ref[] = mult_polyz(pn,pd,b0_in,a1_in);
	                        double num2_ref[] = mult_polyz(pd,pn,a0_in,b1_in);
	                        double den_ref[]  = mult_polyz(pd,pd,a0_in,a1_in);

	                        a2_in = new double[den_ref.length];
	                        a2_in  = den_ref;

	                        num_len1 = num1_ref.length;
	                        num_len2 = num2_ref.length;
	                        den_len  = (den_ref.length)-1;

	                        b2_in = new double[num1_ref.length];

	                        for (int ii=0; ii < Math.max(num_len1,num_len2); ii++)
	                        {   for (int hh=0; hh < num_len2; hh++)
	                            {
	                                if (ii == hh) b2_in[ii] = num1_ref[ii] + num2_ref[hh];
	                            }
	                        }
	                    }
	                    else//k=1
	                    {
	                        a2_in = new double[3];
	                        b2_in = new double[2];
	                        a2_in[0] = 1;    a2_in[1] = c[k];  a2_in[2]=r*r;
	                        b2_in[0] = a[k]; b2_in[1] = b[k];

	                        int tralz1 =0;
	                        for (int uu = 0; uu < magnK.length; uu++)
	                        {
	                            if(magnK[uu] == 0)
	                               tralz1 = tralz1 + 1;
	                            else
	                                break;
	                        }
	                        if (tralz1 >= 2)
	                        {
	                            num_len1 = 0;
	                            den_len = 0;
	                        }
	                        else
	                        {
	                            num_len1 = 2;
	                            den_len = 2;
	                        }

	                    }
	                }
	                else
	                {
	                    if (a[k] !=0 && b[k] !=0)
	                    {
	                        num_len1 = num_len1-1;

	                        a1_in = new double[3];
	                        b1_in = new double[2];

	                        a1_in[0] = 1;    a1_in[1] = c[k];  a1_in[2]=r*r;
	                        b1_in[0] = a[k]; b1_in[1] = b[k];

	                        double num1_ref[] = mult_polyz(pn,den_len,b1_in,a2_in);
	                        double num2_ref[] = mult_polyz(pd,num_len1,a1_in,b2_in);
	                        double den_ref[]  = mult_polyz(pd,den_len,a1_in,a2_in);

	                        a2_in = new double[den_ref.length];
	                        a2_in  = den_ref;

	                        num_len1 = num1_ref.length;
	                        num_len2 = num2_ref.length;
	                        num_len1 = Math.max(num_len1,num_len2);
	                        den_len  = den_ref.length - 1;

	                        b2_in = new double[num_len1];

	                        for (int ii = 0; ii < num_len1; ii++)
	                        {
	                            for (int hh = 0; hh < num_len2; hh++)
	                            {
	                                if (ii == hh)b2_in[ii] = num1_ref[ii] + num2_ref[hh];
	                            }
	                        }
	                    }

	                }
	            } // for loop k--> N/2-1
	            b3_in[0] = 1; b3_in[1] = -1;
	            if (magnK[0] != 0)
	            {
	                num2_len = b2_in.length -1;
	                den2_len = a2_in.length -1;

	                double[] num2_ref = new double[a2_in.length];

	                double num1_ref[] = mult_polyz(1,num2_len,b3_in,b2_in);

	                for (int ppp = 0; ppp < a2_in.length; ppp++)num2_ref[ppp] = magnK[0]*a2_in[ppp];

	                double den_ref[] =  mult_polyz(1,den2_len,b3_in,a2_in);

	                a2_in  = new double[den_ref.length];
	                a2_in = den_ref;  //

	                num_len1 = num1_ref.length;
	                num_len2 = num2_ref.length;
	                den_len  = den_ref.length-1;

	                b2_in = new double[num_len1];

	                for (int ii = 0; ii < num_len1; ii++)
	                {
	                    for (int hh = 0; hh < num_len2; hh++)
	                    {
	                        if (ii == hh)b2_in[ii] = num1_ref[ii] + num2_ref[hh];
	                    }
	                }
	            }
	              double b4_in[] = new double[N+1];
	              b4_in[0] = 1/((double)N);
	              for (int qq = 1; qq < N-1; qq++)b4_in[qq] = 0;
	              b4_in[N]= -1/((double)N)*Math.pow(r,(double)N);

	              num3_len = b2_in.length -1;
	              num4_len = b4_in.length -1;
	              double num1_ref[] = mult_polyz(num3_len,num4_len,b2_in,b4_in);

	              aCoeff = new double[a2_in.length];
	              aCoeff = a2_in;  //

	              bCoeff  = new double[num1_ref.length];
	              bCoeff = num1_ref; //
	            } //Normal if
	           if(fs_type == 1)//Least Squares using half the sampling points
	            {
	              int L = N/2;

	              for (int n=0; n<=L-1; n++)
	              {
	                     for (int k=1; k<=(N/2)-1;k++)
	                     {
	                       z[n] = z[n]+(2*Math.abs(magnK[k])*Math.cos(Math.abs((Fs*k*(n-alpha))/N)));
	                     }
	                     bCoeff[n] = (z[n]+magnK[0])/L;
	              }

	            } // LS if*/
	     } // Recursive if
	 }// freqsampling if

	public double[] mult_polyz(int p1,int p2,double[] a1, double[] a2)
	{
	  int pp;
	  pp= p1 + p2;
	  double[] r_s = new double[pp+1];

	  for (int n=0; n < pp+1; n++)
	  {
	    for (int k2=0; k2 < p1+1; k2++)
	    {
	        for (int m=0; m < p2+1; m++)
	        {
	            if ((k2+m) == n) r_s[n]= r_s[n] + a1[k2]*a2[m];
	        }
	    }
	  }
	  return r_s;
	}
	 public int getSize()
	  {
	      if (numsamp == 0)N =8;
	      if (numsamp == 1)N =16;
	      if (numsamp == 2)N =32;
	      if (numsamp == 3)N =64;
	      return N;
	  }

}
