package com.prototype.ajdsp;

import java.util.ArrayList;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.TextView;

import com.prototype.ajdsp.PartView.ConnectionData;

public class AllPartDeleteDialog extends Dialog implements  OnClickListener{
	Button deleteButton;
	Button cancelButton;
	TextView pinDialogText;
	PartObject graphic;
	Bitmap pinBitmap;
	Bundle _inputBundle;
	String partCheckText;
	int partNumber;
	PartView partView;
	TextView textView;


	public AllPartDeleteDialog(Context context,PartView partview) {
		super(context);
		//requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.all_part_delete_dialog);
		partView=partview;
		deleteButton = (Button) findViewById(R.id.all_part_confirm);
		deleteButton.setOnClickListener(this);
		cancelButton = (Button) findViewById(R.id.all_part_cancel);
		cancelButton.setOnClickListener(this);
	}
	@Override
	public void onBackPressed() {
		dismiss();
	}
	@Override
	public void onClick(View v) {
		if (v == deleteButton){
			PartsController.partList=new ArrayList<PartObject>();
			PartsController.connectionList=new ArrayList<ConnectionData>();
			partView.setParameters(PartsController.partList,PartsController.connectionList);
			SigGenMenu.sigGenNumber=0;
			FFTMenu.fftNumber=0;
			IFFTMenu.ifftNumber=0;
			PartList.convolutionNumber=0;
			PartList.plotNumber=0;
			PartList.filterNumber=0;
			FilterCoeffMenu.filterCoeffNumber=0;
			AdderMenu.adderNumber=0;
			BWMenu.bwexpNumber=0;
			DownSamplerMenu.downSamplerNumber=0;
		    DTMFTone.dtmfNumber=0;
		    PartList.freqRespNumber=0;
		    PartList.junctionNumber=0;
		    KaiserMenu.kaiserNumber=0;
		    MIDIDisplay.midiNumber=0;
		    PmlMenu.pmlNumber=0;
		    PartList.pz2CoeffNumber=0;
		    PartList.pzPlacementNumber=0;
		    PartList.pzPlotNumber=0;
		    UpSamplerMenu.upSamplerNumber=0;
		    WindowMenu.windowNumber=0;
			dismiss();
		}
		else if(v==cancelButton)
			dismiss();
	}
}



