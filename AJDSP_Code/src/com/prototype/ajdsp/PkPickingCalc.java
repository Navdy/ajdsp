package com.prototype.ajdsp;

import android.os.Bundle;

public class PkPickingCalc {
	 double[] phz = new double[256];
		double[] magn = new double[256];
		boolean symmetric=true;
		public int ppstate=1;
		public int peakno=64;
		int l1=0;
	public  Bundle pkPick(Bundle dataBundle){
		
		peakno=dataBundle.getInt("noPeaks");
		ppstate=(int)dataBundle.getLong("itemSelected");
		Bundle inputData=dataBundle.getBundle("leftPin");
		 l1= (int)inputData.getDouble("pulseWidth");
		if (peakno>l1/2)
			peakno=l1/2;
		
		double[] inputMag=inputData.getDoubleArray("signalMag");
		double[] inputPhase=new double[256];
		if(inputData.getDoubleArray("signalPhase")!=null)
			inputPhase=inputData.getDoubleArray("signalPhase");
		calc(inputMag,inputPhase);
		Bundle outputBundle=new Bundle();
		outputBundle.putDoubleArray("signalMag", magn);
		outputBundle.putDoubleArray("signalPhase", phz);
		outputBundle.putDouble("pulseWidth",(double)(l1/2) );
		dataBundle.putBundle("rightPin", outputBundle);
		return dataBundle;
	}
	public void calc(double[]inputMag,double[]inputPhase)
    {
		for (int i=0;i<256;i++)
		{
			phz[i]=0;magn[i]=0;
		}
		double[] mag = new double[l1];
		double[] ph  = new double[l1];
		
		for (int k=0;k<l1;k++)
		{
			mag[k]=inputMag[k];ph[k]=inputPhase[k];
		}

/*	 PkPking pkpking = new PkPking(mag,ph,peakno,ppstate,l1);

	 for (int k=0;k<l1;k++)
	 {
		 magn[k]=pkpking.getMagnitude(k);
		 phz[k]=pkpking.getPhase(k);
	 }
	}
*/
//XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
	  int i,j,k;
	  int m=0;
	  double maxX=0; int fft=l1;
	  double xt[] = new double[fft];
	  if (peakno>l1 && !symmetric)
	  peakno=l1;
	  else if (peakno>l1/2 && symmetric)
	  peakno=l1/2;

	  if (ppstate == 0)  // select 1st values
	  {
		  if (peakno!=0) magn[0]=mag[0];
		  for (i=1;i<peakno;i++)
		  {
			  magn[i]=mag[i];phz[i]=ph[i];
			  if (symmetric){magn[fft-i]=mag[fft-i];phz[fft-i]=ph[fft-i];}
		  }
	  }

	  if (ppstate == 1) // select highest value peaks
	  {
		  for (i=0;i<fft;i++)
			  xt[i]=mag[i];

		  maxX=-1E10;
		  for (i=0;i<peakno;i++)
		  {
			  int fft2;
			  if (symmetric) fft2=fft/2;
			  else fft2=fft;
			  for (j=0;j<fft2;j++)
			  {
				  if (xt[j]>=maxX)
				  {
				      maxX=xt[j];
				      m=j;
				  }
			  }
			  magn[m]=xt[m];phz[m]=ph[m];
			  if (m!=0 && symmetric) {magn[fft-m]=xt[fft-m];phz[fft-m]=ph[fft-m];}
			  maxX=-1E10;
		      xt[m]=-1E10;
		  }

  }
 }


}
