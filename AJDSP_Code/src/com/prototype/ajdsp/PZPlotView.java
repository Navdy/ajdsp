
package com.prototype.ajdsp;



import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.os.Bundle;
import android.util.AttributeSet;
import android.view.View;

public class PZPlotView  extends View {
	Paint axesPaint=new Paint();
	Paint boxPaint=new Paint();
	Paint circlePaint=new Paint();
	Paint zeroPaint=new Paint();
	Bundle pzCoords;
	public int buttonHeight;
	int MAX_ORD_10 = 40;
	double[] poleXCoord=new double[MAX_ORD_10+1];
	double[] poleYCoord=new double[MAX_ORD_10+1];
	double[] zeroYCoord=new double[MAX_ORD_10+1];
	double[] zeroXCoord=new double[MAX_ORD_10+1];
	int offsetHeight,width,height;

	public PZPlotView(Context context) {
		super(context);
		axesPaint.setColor(Color.BLACK);
		boxPaint.setColor(Color.RED);
		boxPaint.setStyle(Paint.Style.STROKE);
		boxPaint.setStrokeWidth(2);
		circlePaint.setColor(Color.BLUE);
		circlePaint.setStrokeWidth(2);
		circlePaint.setStyle(Paint.Style.STROKE);
		 width=getWidth();
		 setPadding(0,0,0,0);
		    height=getHeight();
		    buttonHeight=PartView.buttonHeight;
	}
	public PZPlotView(final Context context, final AttributeSet attrs) {
		this(context, attrs, 0);
	}
	public PZPlotView(final Context context, final AttributeSet attrs,
			final int defStyle) {
	
		super(context,attrs, defStyle);
		axesPaint.setColor(Color.BLACK);
		boxPaint.setColor(Color.RED);
		boxPaint.setStyle(Paint.Style.STROKE);
		boxPaint.setStrokeWidth(2);
		circlePaint.setColor(Color.BLUE);
		circlePaint.setStrokeWidth(2);
		circlePaint.setStyle(Paint.Style.STROKE);
		zeroPaint.setColor(Color.RED);
		
		buttonHeight=PartView.buttonHeight;
		 setPadding(0,0,0,0);
	    
	}

	  @Override
	    public void onDraw(Canvas canvas) {
		  width=getWidth();
		    height=getHeight();
	    	//int height=canvasHeight;
	    	int dim=Math.min(width, height);
	    	canvas.drawLine(width/2, 0, width/2, height, axesPaint);
	        canvas.drawLine(0, height/2, width, height/2, axesPaint);
	        canvas.drawCircle(width/2, height/2,3*dim/10,circlePaint);
	        canvas.drawText("1.5",width/2+9*dim/20-axesPaint.measureText("1.5"),height/2+9*dim/20 , axesPaint);
			canvas.drawText("1.0",width/2+9*dim/20-axesPaint.measureText("1.0"),height/2+3*dim/10 , axesPaint);
			canvas.drawText("0.0",width/2+9*dim/20-axesPaint.measureText("0.0"),height/2, axesPaint);
			canvas.drawText("-1.0",width/2+9*dim/20-axesPaint.measureText("-1.0"),height/2-3*dim/10 , axesPaint);
			canvas.drawText("-1.5",width/2+9*dim/20-axesPaint.measureText("-1.5"),height/2-9*dim/20, axesPaint);
			canvas.drawText("Real", width/2-9*dim/20, height/2, axesPaint);
			canvas.drawText("Imaginary",width/2-axesPaint.measureText("Imaginary"), height/2-9*dim/20, axesPaint);
	        canvas.drawRect(width/2-9*dim/20, height/2-9*dim/20, width/2+9*dim/20,height/2+9*dim/20,circlePaint);
	        if(pzCoords!=null){
	        	 poleXCoord= pzCoords.getDoubleArray("PoleX");
	        	 poleYCoord= pzCoords.getDoubleArray("PoleY");
	        	 zeroXCoord= pzCoords.getDoubleArray("ZeroX");
	        	 zeroYCoord= pzCoords.getDoubleArray("ZeroY");
	        	}
	        for(int i=0;i<poleXCoord.length;i++){
	        	 if( (poleXCoord[i]==0)&&( poleYCoord[i]==0)&&(zeroXCoord[i]==0)&&(zeroYCoord[i]==0))
	        		 continue;
	        	float poleXCenter=(float) (width/2+poleXCoord[i]*3*dim/10);
	        	float poleYCenter=(float) ((height/2)+poleYCoord[i]*3*dim/10);
	        	float zeroXCenter=(float) (width/2+zeroXCoord[i]*3*dim/10);
	        	float zeroYCenter=(float) ((height/2)+zeroYCoord[i]*3*dim/10);
	        	canvas.drawLine(poleXCenter-dim/50, poleYCenter-dim/50, poleXCenter+dim/50, poleYCenter+dim/50, boxPaint);
	        	canvas.drawLine(poleXCenter+dim/50, poleYCenter-dim/50, poleXCenter-dim/50, poleYCenter+dim/50, boxPaint);
	        	canvas.drawCircle(zeroXCenter, zeroYCenter, dim/50, zeroPaint);
	        }
		 	  }
	  public void setPzCoord(Bundle inputValues){
		  pzCoords=inputValues;
	  }
	  
	  
}
