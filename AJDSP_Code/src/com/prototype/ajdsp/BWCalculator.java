package com.prototype.ajdsp;

import android.os.Bundle;

public class BWCalculator {
	public int MAXSIZE=256;
	public double bw_exp;
	double[] filterCoeffAexp = new double[65];
	double[] filterCoeffBexp= new double[65];
	double[] filterCoeffA= new double[65];
	double[] filterCoeffB= new double[65];
	

	public  Bundle bwCalc(Bundle dataBundle){
		
		bw_exp=dataBundle.getDouble("bwExp");
		if (bw_exp< 0)
			bw_exp= 0;
		else if (bw_exp> 1)
			bw_exp= 1;
        
		Bundle inputData=dataBundle.getBundle("bottomPin");
		filterCoeffA=inputData.getDoubleArray("filterCoeffA");
		filterCoeffB=inputData.getDoubleArray("filterCoeffB");
		BWExp(bw_exp);
		Bundle outputBundle=new Bundle();
		outputBundle.putDoubleArray("filterCoeffA", filterCoeffAexp);
		outputBundle.putDoubleArray("filterCoeffB", filterCoeffBexp);
		
		dataBundle.putBundle("topPin", outputBundle);
		return dataBundle;
	}
	public void BWExp(double coef)
    {
		for (int i = 0; i < filterCoeffA.length; i++)
		{
		
			filterCoeffAexp[i] = filterCoeffA[i]*Math.pow(coef,i);
			filterCoeffBexp[i] = filterCoeffB[i]*Math.pow(coef,i);
		
		}
	}


}

