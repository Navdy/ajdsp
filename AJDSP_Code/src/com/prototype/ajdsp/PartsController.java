package com.prototype.ajdsp;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import com.prototype.ajdsp.PartObject.PinSignalTypes;
import com.prototype.ajdsp.PartObject.PinTypes;
import com.prototype.ajdsp.PartView.ConnectionData;

import android.app.Activity;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Matrix;
import android.os.Bundle;
import android.view.View;
import android.view.WindowManager;

public class PartsController extends View{
	public static Bitmap partbitmap;
	public static Bitmap pinbitmap;
	private static Bitmap largepinbitmap;
	//public static Canvas comboImage;
	public static List<PartObject> partList =Collections.synchronizedList( new ArrayList<PartObject>());
	public static List<ConnectionData> connectionList=Collections.synchronizedList(new ArrayList<ConnectionData>());
     public static int width;
     public static int height;
	public PartsController(Context context,int screenWidth,int screenHeight) {
		super(context);
		partbitmap=BitmapFactory.decodeResource(getResources(), R.drawable.part);
		pinbitmap =BitmapFactory.decodeResource(getResources(), R.drawable.pin);
		largepinbitmap=resizePin();
		width=screenWidth;
		height=screenHeight;
		/*Bitmap part= Bitmap.createBitmap(partbitmap.getWidth()+largepinbitmap.getWidth(), partbitmap.getHeight()+largepinbitmap.getHeight(), Bitmap.Config.ARGB_8888); 
	    comboImage = new Canvas(part);
		comboImage.drawBitmap(partbitmap,largepinbitmap.getWidth()/2,largepinbitmap.getHeight()/2,null); */
	    
	}

	private Bitmap resizePin(){  

		int width = pinbitmap.getWidth();    		
		int height = pinbitmap.getHeight();    	
		float scaleWidth = 2;    		
		float scaleHeight = 2;    		
		Matrix matrix = new Matrix();    		
		matrix.postScale(scaleWidth, scaleHeight);    		
		return Bitmap.createBitmap(pinbitmap, 0, 0, width, height, matrix, false);
	}

	public static void createBlock(Bundle inputBundle){
		
		String partName=inputBundle.getString("title");
		PinTypes pinTypes=(PinTypes)inputBundle.getSerializable("PinTypes");
		PinSignalTypes pinSignalTypes=(PinSignalTypes)inputBundle.getSerializable("pinSignalType");
		PartObject graphic = new PartObject(partbitmap,pinbitmap,partName,pinTypes,pinSignalTypes);
		graphic.setBlockData(inputBundle);
		graphic.getCoordinates().setX(width/2);
		graphic.getCoordinates().setY(9*height/20);
		partList.add(graphic);
	}

	public static void updateBlock(Bundle inputBundle){
		String partName=inputBundle.getString("title");
		for(PartObject part:partList){
			
			if(part.getPartName().equalsIgnoreCase(partName)){
				if(partName.contains("Sig Gen")){
					part.setBlockData(inputBundle);
					part.updateChildern();
				}else 
					if((partName.contains("Plot"))&&(!partName.contains("PZ Plot"))){
						Bundle leftPinData=inputBundle.getBundle("leftPin");

						inputBundle.putBundle("rightPin", leftPinData);
						part.setBlockData(inputBundle);
						part.updateChildern();
					}else
						if((partName.contains("FFT"))&&(!partName.contains("IFFT"))){
							FFTCalculator fftCalc= new FFTCalculator();
							Bundle updatedBundle=fftCalc.FFT(inputBundle);
							part.setBlockData(updatedBundle);
							part.updateChildern();
						}else
						if(partName.contains("IFFT")){
							IFFTCalculator ifftCalc= new IFFTCalculator();
							Bundle updatedBundle=ifftCalc.IFFT(inputBundle);
							part.setBlockData(updatedBundle);
							part.updateChildern();
						}
							
							if(partName.contains("BW Exp")){
								BWCalculator bwCalc= new BWCalculator();
								Bundle updatedBundle=bwCalc.bwCalc(inputBundle);
								part.setBlockData(updatedBundle);
								part.updateChildern();
							}else
							if(partName.contains("Conv")){
								ConvCalculator convCalc= new ConvCalculator();
								Bundle updatedBundle=convCalc.contConv(inputBundle);
                                part.setBlockData(updatedBundle);
								part.updateChildern();
							}else
								if(partName.contains("Filter")&&!partName.contains("Filter Coeff")){
									FilterCalculator filCalc= new FilterCalculator();
									Bundle updatedBundle=filCalc.filterCompute(inputBundle);
									part.setBlockData(updatedBundle);
									part.updateChildern();
								}else
									if(partName.contains("Filter Coeff")){

										Bundle updatedBundle=inputBundle;
										part.setBlockData(updatedBundle);
										part.updateChildern();
									}else 
										if(partName.contains("Freq Resp")){
											FreqRespCalculator frCalc= new FreqRespCalculator();
											Bundle updatedBundle= frCalc.freqRespCalc(inputBundle);
											part.setBlockData(updatedBundle);
											part.updateChildern();
										}else 
											if(partName.contains("PZ Plot")){
												PZPlotCalc pzCalc= new  PZPlotCalc();
												Bundle updatedBundle= pzCalc.pzPlotCalc(inputBundle);
												part.setBlockData(updatedBundle);
												part.updateChildern();
											}
											else 
												if(partName.contains("Kaiser")){
													part.setBlockData(inputBundle);
													part.updateChildern();
												}else
													if(partName.contains("Parks")){
														part.setBlockData(inputBundle);
														part.updateChildern();
													}
													else if(partName.contains("Window")){
														WindowCalc windowCalc= new  WindowCalc();
														Bundle updatedBundle= windowCalc.Windowing(inputBundle);
														part.setBlockData(updatedBundle);
														part.updateChildern();

													}else
														if(partName.contains("FIR Design")){
															part.setBlockData(inputBundle);
															part.updateChildern();
														}
														else
															if(partName.contains("IIR Design")){
																part.setBlockData(inputBundle);
																part.updateChildern();
															}

															else 
																if(partName.contains("Mixer")){
																	AdderCalc adderCalc= new  AdderCalc();
																	Bundle updatedBundle= adderCalc.Adding(inputBundle);
																	part.setBlockData(updatedBundle);
																	part.updateChildern();

																}
																else 
																	if(partName.contains("Junction")){

																		Bundle leftPinData=inputBundle.getBundle("leftPin");
																		inputBundle.putBundle("rightTopPin", leftPinData);
																		inputBundle.putBundle("rightBottomPin", leftPinData);
																		part.setBlockData(inputBundle);
																		part.updateChildern();

																	}
																	else 
																		if(partName.contains("Up Sampler")){
																			UpSamplerCalc upSamplerCalc= new  UpSamplerCalc();
																			Bundle updatedBundle= upSamplerCalc.upSampler(inputBundle);
																			part.setBlockData(updatedBundle);
																			part.updateChildern();

																		}
																		else 
																			if(partName.contains("Down Sampler")){
																				DownSamplerCalc downSamplerCalc= new  DownSamplerCalc();
																				Bundle updatedBundle= downSamplerCalc.downSampler(inputBundle);
																				part.setBlockData(updatedBundle);
																				part.updateChildern();

																			}
																			else
																				if(partName.contains("Freq Samp")){
																					part.setBlockData(inputBundle);
																					part.updateChildern();
																				}
																				else
																					if(partName.contains("MIDI")){
																						part.setBlockData(inputBundle);
																						part.updateChildern();
																					} else
																						if(partName.contains("DTMF")){
																							part.setBlockData(inputBundle);
																							part.updateChildern();
																						}else
																							if(partName.contains("PZ2Coeff")){
																								part.setBlockData(inputBundle);
																								part.updateChildern();
																							}
																							else
																								if(partName.contains("Peak Picking")){
																									PkPickingCalc pkPickingCalc= new  PkPickingCalc();
																									Bundle updatedBundle= pkPickingCalc.pkPick(inputBundle);
																									part.setBlockData(updatedBundle);
																									part.updateChildern();

																								}


			}
		}
	}
}
