package com.prototype.ajdsp;

import android.os.Bundle;

public class FreqRespCalculator {
	
	 
	 double x[] = new double[128];
	 double phi[] = new double[128];
	   double amp[] = new double[128];
	   double ang[] = new double[128];
	   double ampOutput[] = new double[128];
	   double angOutput[] = new double[128];
	   
	   boolean initalflag=false;
	   boolean stable = true;
	   
	   
	   public Bundle freqRespCalc(Bundle inputBundle){
		   Bundle coeffBundle=inputBundle.getBundle("bottomPin");
		 double[] filterCoeffa=  coeffBundle.getDoubleArray("filterCoeffA");
		 double[] filterCoeffb=  coeffBundle.getDoubleArray("filterCoeffB");
		 for (int i = 0; i <= 127; i++)
	            { amp[i]= 0; ang[i]= 0;}
		 
		 inputBundle.putBundle("topPin", coeffBundle);
		     getAmpPhi(filterCoeffa,filterCoeffb);
            inputBundle.getBundle("rightPin").putDoubleArray("signalMag", amp);
            inputBundle.getBundle("rightPin").putDouble("pulseWidth", 128.0);
            inputBundle.getBundle("rightPin").putDoubleArray("signalPhase", ang);
            for(int i=0;i<amp.length;i++){
            	ampOutput[i]=amp[i];
            	angOutput[i]=ang[i];
            }
            inputBundle.putDoubleArray("signalMag", ampOutput);
            inputBundle.putDouble("pulseWidth",128.0);
            inputBundle.putDoubleArray("signalPhase", angOutput);
            return inputBundle;
	   }
	          
	   public void getAmpPhi(double[] coeffA,double[] coeffB){
    double min=0;
	double max=0;

		 double  nr =0;double ni=0;double dr=0;double di=0;
			
		 for (int i=0;i<=127;i++)
         {
          double omega= Math.PI/((double)128)*i;
          nr =0;ni=0;dr=0;di=0;
			
			for (int k=0;k<coeffA.length;k++)
			{
			 nr=nr+coeffB[k]*Math.cos((double)(64-k)*omega);
			 ni=ni+coeffB[k]*Math.sin((double)(64-k)*omega);
			 dr=dr+coeffA[k]*Math.cos((double)(64-k)*omega);
			 di=di+coeffA[k]*Math.sin((double)(64-k)*omega);

			}
			if((dr*dr+di*di)!=0){
					
        double  re= (nr*dr+ni*di)/(dr*dr+di*di);
		double im= (ni*dr-nr*di)/(dr*dr+di*di);
		
          x[i] = Math.sqrt(re*re+im*im);
		  phi[i]=Math.atan2(im,re);
		

			    amp[i] = x[i];
                ang[i] = phi[i];
			

          if (x[i] >=100000)
               {
                x[i]=100000;
                stable = false;
               }
          if (x[i] <=-100000)
               {
                x[i]=-100000;
                stable = false;
               }
          if (x[i]> max) max=x[i];
          if (x[i] < min) min= x[i];
         }
         }
       if (Math.abs(min) > max) max = Math.abs(min);
      }
  

		   
	   }
	 

