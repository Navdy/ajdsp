package com.prototype.ajdsp;


import android.os.Bundle;

public class PZPlotCalc {
	 int MAX_ORD_10 = 40;

		//public double a[]  = new double[MAX_ORD_10+1];
		//public double b[]  = new double[MAX_ORD_10+1];
		public double PX[] = new double[MAX_ORD_10+1];
		public double PY[] = new double[MAX_ORD_10+1];
		public double ZX[] = new double[MAX_ORD_10+1];
		public double ZY[] = new double[MAX_ORD_10+1];
		public int no_zeros;
		public int no_poles;
	
	public Bundle pzPlotCalc(Bundle inputBundle){
		Bundle filterCoeff=inputBundle.getBundle("bottomPin");
		double[] filterCoeffB=filterCoeff.getDoubleArray("filterCoeffB");
		double[] filterCoeffA=filterCoeff.getDoubleArray("filterCoeffA");
		int numN,denN;
		if (filterCoeffB.length>MAX_ORD_10+1)
			numN=MAX_ORD_10+1;
		else
			numN=filterCoeffB.length;
		if (filterCoeffA.length>MAX_ORD_10+1)
			denN=MAX_ORD_10+1;
		else
			denN=filterCoeffA.length;
		int coefN = Math.max(denN,numN);
		double a[] = new double[coefN];
		double b[] = new double[coefN];
		int k=0;boolean flag=true;
		for (int i=0;i<coefN;i++)
		{
			if (i<numN)
				b[i]=filterCoeffB[i];
			else
				b[i]=0;
			if (b[i]==0 && flag)
				k++;else flag=false;
		}
		double newb[] = new double[coefN-k];

		//Jayaraman - 12 December 2008
		int ref_index = coefN-k;

		for (int i=0;i<coefN-k;i++)
			newb[i]=b[i+k];
		flag=true;k=0;
		for (int i=0;i<coefN;i++)
		{
			if (i<denN)
				a[i]=filterCoeffA[i];
			else
				a[i]=0;
			if (a[i]==0 && flag)k++;else flag=false;
		}
		//Jayaraman - 12 December 2008
		//double newa[] = new double[coefN-k];
		//for (int i=0;i<coefN-k;i++)
		//	newa[i]=a[i+k];

		//Jayaraman - 12 December 2008
		int temp_index;
		if ((coefN-k)==0)
			temp_index = Math.max(ref_index,1);
		else
			temp_index = coefN-k;
		
		double newa[] = new double[temp_index];

		if ((coefN-k)==0)
		{
		newa[0] = 1;
		for (int i=1;i<ref_index;i++)
		newa[i]=0;

		}
		else
		{
		for (int i=0;i<coefN-k;i++)
		newa[i]=a[i+k];
		}

	 /*
		for (int i=0;i<coefN;i++)
		if (i<denN)
			a[i]=data2[0][i];
		else a[i]=0;
	*/

		RootFinder RFpoles = new RootFinder(newa, this, 1);
		RootFinder RFzeros = new RootFinder(newb, this, 0);
		
		Bundle poleZeroData=new Bundle();
		poleZeroData.putDoubleArray("PoleX", PX);
		poleZeroData.putDoubleArray("ZeroX", ZX);
		poleZeroData.putDoubleArray("PoleY", PY);
		poleZeroData.putDoubleArray("ZeroY", ZY);
		
		inputBundle.putBundle("pzCoord", poleZeroData);
		return inputBundle;
		

}
}
