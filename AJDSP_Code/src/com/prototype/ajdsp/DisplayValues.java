package com.prototype.ajdsp;



import java.util.ArrayList;
import java.util.HashMap;

import android.app.ListActivity;
import android.content.Intent;

import android.graphics.Color;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.view.View.OnClickListener;

import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.SimpleAdapter;
import android.widget.TextView;


public class DisplayValues extends ListActivity implements OnClickListener {

	Bundle inputValues;
	RelativeLayout layout;
	RelativeLayout topLayout;
	Button back;
	String[] values;
           
	String returnClass;
	Button startView;
    EditText search;
	Integer numLinSeg=0;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		//requestWindowFeature(Window.FEATURE_CUSTOM_TITLE);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
		Bundle extras = getIntent().getExtras(); 
		setContentView(R.layout.part_list_layout);
		//getWindow().setFeatureInt(Window.FEATURE_CUSTOM_TITLE, R.layout.row);
		layout= (RelativeLayout) findViewById(R.id.part_list_view);
		layout.setBackgroundColor(Color.WHITE);
		
		back=(Button) findViewById(R.id.back_select_parts);
		back.setOnClickListener(this);
		TextView title= (TextView)findViewById(R.id.Title);
		title.setText("Values");
		startView=(Button) findViewById(R.id.add_part);
		startView.setOnClickListener(this);
		startView.setText("Done");
		search=(EditText)findViewById(R.id.list_search);
		search.setVisibility(View.GONE);
		

		if(extras !=null)
		{			
			inputValues = extras.getBundle("blockValues");
			values=inputValues.getStringArray("plotValues");
			numLinSeg=inputValues.getInt("lineSegmentsId");
			returnClass=inputValues.getString("returnClass");
		}
		setListAdapter(new ArrayAdapter<String>(this, R.layout.part_list, values));
		
		ListView lv  = (ListView) findViewById(android.R.id.list);
		lv.setTextFilterEnabled(true);
		lv.setBackgroundColor(Color.WHITE);
	}
	@Override
	 public void onStop() {
		super.onStop();
				finish();
				
	 }
	@Override
	public void onBackPressed() {
	    // do nothing.
	}
	@Override
	public void onClick(View v) {

		if(v==back){

			if(returnClass.equalsIgnoreCase("PlotPart")){
				Intent i = new Intent(this, PlotPart.class);					   
				startActivity(i);
			}	
			else if(returnClass.equalsIgnoreCase("FreqSamplerDraw")){
				Intent i = new Intent(this, FreqSamplerDraw.class);
				Bundle bundle=new Bundle();
				bundle.putInt("lineSegmentsId",numLinSeg);
				i.putExtra("displayValues", bundle);
				startActivity(i);
			}

			else if(returnClass.equalsIgnoreCase("PZPlot")){
				Intent i = new Intent(this, PZPlotPart.class);
				
				startActivity(i);
			}else if(returnClass.equalsIgnoreCase("Kaiser")){
				Intent i = new Intent(this, KaiserMenu.class);
				i.putExtra("blockValues", inputValues);
				startActivity(i);
			}
			else if(returnClass.equalsIgnoreCase("Parks")){
				Intent i = new Intent(this, PmlMenu.class);
				i.putExtra("blockValues", inputValues);
				startActivity(i);
			}
			else if(returnClass.equalsIgnoreCase("FIRDesignMenu")){
				Intent i = new Intent(this, FIRDesignMenu.class);
				i.putExtra("blockValues", inputValues);
				startActivity(i);
			}


		}
		else if(v==startView){
			if(returnClass.equalsIgnoreCase("PlotPart")){
				PlotPart.sigType=0;
				PlotPart.isLinear=true;
				PlotPart.xAxisValues=null;
			}	
			Intent i = new Intent(this, StartView.class);			
			startActivity(i);
		}
	}
}
