package com.prototype.ajdsp;

public class SNRCalc {
	public double snr(double [] data1,double[] data2){
		int max =0;
        double[] y1= new double[1];
        double[] y2= new double[1];
      
        
        double snr=0.0;
       
        


        if (data1.length > data2.length)
        {
           y2 = new double[data1.length];
           System.arraycopy(data2,0,y2,0,data2.length);
           y1 = data1;
           max = y1.length;
        }
        else if (data1.length <= data2.length)
        {
           y1 = new double[data2.length];
           System.arraycopy(data1,0,y1,0,data1.length);
           y2 = data2;
           max = y2.length;
        }




             double snr1=0, snr2=0;
           for (int i=0;i<max;i++)
            {
                      snr1=y1[i]*y1[i]+snr1;
                      snr2=(y1[i]-y2[i])*(y1[i]-y2[i])+snr2;
             }
             snr=10*Math.log(snr1/snr2)/Math.log(10);

           


        return snr;  
	}
	

}
