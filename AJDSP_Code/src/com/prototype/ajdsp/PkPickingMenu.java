package com.prototype.ajdsp;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.view.View.OnClickListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.Toast;

import com.prototype.ajdsp.PartObject.PinSignalTypes;
import com.prototype.ajdsp.PartObject.PinTypes;

public class PkPickingMenu extends Activity implements OnClickListener{
	RelativeLayout layout;
	RelativeLayout topbar;
	Button addParts;
	Spinner pkpickSpinner;
	Bundle inputValues=new Bundle();
	EditText noPeaks;
	

	Button partList;
	public static int pkpickNumber=0;
	Boolean create=true;
	double [] inputMag;
	double [] inputPhase;
	double pulseWidth;


	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		//requestWindowFeature(Window.FEATURE_CUSTOM_TITLE);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
		setContentView(R.layout.pkpicking_menu);
		//getWindow().setFeatureInt(Window.FEATURE_CUSTOM_TITLE, R.layout.row);
		layout=(RelativeLayout)findViewById(R.id.pkpickMenu);
		layout.setBackgroundColor(Color.WHITE);
		partList=(Button) findViewById(R.id.back_select_parts);
		partList.setOnClickListener(this);
		
		
	    addParts=(Button) findViewById(R.id.add_part);
		addParts.setOnClickListener(this);
		pkpickSpinner = (Spinner) findViewById(R.id.pkpickType);
		ArrayAdapter<CharSequence> pkpickTypeAdapter = ArrayAdapter.createFromResource(
				this, R.array.pkpick_type, android.R.layout.simple_spinner_item);
		pkpickTypeAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
	
		pkpickSpinner.setAdapter(pkpickTypeAdapter);
		noPeaks=(EditText)findViewById(R.id.pkpickNumber);


		Bundle extras = getIntent().getExtras();

		if(extras !=null)
		{
			inputValues = extras.getBundle("blockValues");
			Integer peaks=inputValues.getInt("noPeaks");
	        noPeaks.setText(peaks.toString());
		    pkpickSpinner.setSelection((int) inputValues.getLong("itemSelected"));
			inputMag=inputValues.getBundle("leftPin").getDoubleArray("signalMag");
			inputPhase=inputValues.getBundle("leftPin").getDoubleArray("signalPhase");
			pulseWidth=inputValues.getBundle("leftPin").getDouble("pulseWidth");
			create=false;
			addParts.setText("Update");
			partList.setText("Back");

		}else{
			 noPeaks.setText("64");
			 pkpickSpinner.setSelection(0);
		}
	}
	@Override
	public void onBackPressed() {
		/*if(create){
			Intent i = new Intent(this, PartList.class);
			startActivity(i);}
		else {
			Intent i = new Intent(this, StartView.class);
			startActivity(i);
		}*/
	    }
	@Override
	 public void onStop() {
		        super.onStop();	
				finish();
				
	 }
	private Integer valueInt(String input){
		  if (input.equalsIgnoreCase("")){
			  return 0;
		  }
		  else return Integer.parseInt(input);
	  }

@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		if (v == addParts){
			
		
			Bundle pkpickData=new Bundle();
			double [] pkpickArray = new double[256];
			Bundle pkpickOutputBundle=new Bundle();
			String peakNo=noPeaks.getText().toString();
			 if(peakNo.length()>3){
		  			Toast.makeText(getApplicationContext(), "Period should not be larger than 256", Toast.LENGTH_LONG).show();
		  			
		  		} else if(valueInt(peakNo)>256){
		  			Toast.makeText(getApplicationContext(), "Period should not be larger than 256", Toast.LENGTH_LONG).show();
		  		} else{
			pkpickData.putInt("noPeaks",valueInt(peakNo));
			
			pkpickOutputBundle.putDoubleArray("signalMag",pkpickArray);
			pkpickOutputBundle.putDoubleArray("signalPhase",pkpickArray);
			pkpickData.putBundle("rightPin", pkpickOutputBundle);
			
			pkpickData.putLong("itemSelected", pkpickSpinner.getSelectedItemId());
			if(create){
				pkpickNumber++;
				Bundle pkpickInputBundle=new Bundle();
				pkpickInputBundle.putDoubleArray("signalMag",pkpickArray);
				pkpickInputBundle.putDoubleArray("signalPhase",pkpickArray);
				pkpickData.putBundle("leftPin", pkpickInputBundle);
				PinSignalTypes pkpickSigType=new PinSignalTypes();
				pkpickSigType.leftPin="fft";
				
				pkpickData.putSerializable("pinSignalType", pkpickSigType);
				pkpickData.putString("title","Peak Picking"+" "+pkpickNumber); 
				PinTypes pkpickPinTypes=new PinTypes();
				pkpickPinTypes.leftPin=true;
				pkpickPinTypes.rightPin=true;
				pkpickData.putSerializable("PinTypes",pkpickPinTypes);			    
				PartsController.createBlock(pkpickData);
			}
			else{
				Bundle pkpickInputBundle=new Bundle();
				pkpickInputBundle.putDoubleArray("signalMag",inputMag);
				pkpickInputBundle.putDoubleArray("signalPhase",inputPhase);
				pkpickInputBundle.putDouble("pulseWidth",pulseWidth);
				pkpickData.putBundle("leftPin", pkpickInputBundle);
				pkpickData.putString("title", inputValues.getString("title"));
				PartsController.updateBlock(pkpickData);
			}
			Intent i = new Intent(this, StartView.class);
			startActivity(i);
		  		}
		}

		if(v==partList){
			if(create){
				Intent i = new Intent(this, PartList.class);
				startActivity(i);}
			else {
				Intent i = new Intent(this, StartView.class);
				startActivity(i);
			}
		}

	}

}
