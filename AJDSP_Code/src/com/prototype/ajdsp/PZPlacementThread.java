package com.prototype.ajdsp;

import android.graphics.Canvas;
import android.view.SurfaceHolder;

public class PZPlacementThread extends Thread{
	private SurfaceHolder _surfaceHolder;
    private PZPlacementView _pzPlacementView;
    public boolean _run = false;
    private Object mPauseLock = new Object();  
    private boolean mPaused;
    public static boolean stopDraw=false;

    public PZPlacementThread(SurfaceHolder surfaceHolder, PZPlacementView panel) {
        _surfaceHolder = surfaceHolder;
        _pzPlacementView = panel;
    }

    public void setRunning(boolean run) {
        _run = run;
    }

    public SurfaceHolder getSurfaceHolder() {
        return _surfaceHolder;
    }

    @Override
    public void run() {
        Canvas c;
        while (_run) {
            c = null;
            try {
                c = _surfaceHolder.lockCanvas(null);
                synchronized (_surfaceHolder) {
                	if(!stopDraw)
                  _pzPlacementView.onDraw(c);
                }
                synchronized (mPauseLock) {
                    while (mPaused) {                       
                            mPauseLock.wait();
                        
                    }
                }
            }catch (InterruptedException e) {
            } finally {
                // do this in a finally so that if an exception is thrown
                // during the above, we don't leave the Surface in an
                // inconsistent state
                if (c != null) {
                    _surfaceHolder.unlockCanvasAndPost(c);
                }
            }
        }
        synchronized (mPauseLock) {
            while (mPaused) {
                try {
                    mPauseLock.wait();
                } catch (InterruptedException e) {
                }
            }
        }
    }
    public void onPause() {
        synchronized (mPauseLock) {
            mPaused = true;
        }
    }
    public void onResume() {
        synchronized (mPauseLock) {
            mPaused = false;
            mPauseLock.notifyAll();
        }
    }
}
