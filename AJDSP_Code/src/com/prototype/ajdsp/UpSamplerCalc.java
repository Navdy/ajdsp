package com.prototype.ajdsp;

import android.os.Bundle;

public class UpSamplerCalc {
	public int L=1,MAXSIZE=256;	//sampling rate
	   private double[] y = new double[MAXSIZE];
	   int usSize=256;
	   
	   public Bundle upSampler(Bundle inputBundle){
		  L= inputBundle.getInt("upSamplerRate");
		  Bundle input1=inputBundle.getBundle("leftPin");
		  if(input1!=null){
		  int l1= (int)input1.getDouble("pulseWidth");
		  double [] input=inputBundle.getBundle("leftPin").getDoubleArray("signalMag");
		   int i;
			
			 for (i=0;i<MAXSIZE;i++)
			 {
				 if ((i*L>=MAXSIZE)) break;
				 y[i*L]=input[i];
			 }
			 int pulseWidth=l1*L;
			 if(pulseWidth>256)
				 pulseWidth=256;
			 inputBundle.getBundle("rightPin").putDoubleArray("signalMag", y);
			 inputBundle.getBundle("rightPin").putDouble("pulseWidth", (double)pulseWidth);
		  }
			 return  inputBundle;
		  
			 
	   }
}
