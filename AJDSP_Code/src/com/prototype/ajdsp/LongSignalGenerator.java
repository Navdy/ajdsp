package com.prototype.ajdsp;

import java.io.BufferedInputStream;
import java.io.DataInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

import android.os.Bundle;
import android.os.Environment;
	 
	 
	
	
public class LongSignalGenerator {

	 long longSignalType;
	 // long periodType;
	  double amplitude;
	  int pinValue;
	  long overlapValue;
	  int frameSize;
//	  public static int frameMax; 
	  double overlapsamples;
	
	  double ys[], xs[];
	
	  double[] current_y;
      double y_till_now[][];
	  double freq1 = 0.1925, freq2 = 0.334;
	  int pinChoice = 0;
	  int sig_length_time, sig_length = 8192;
	  double amp1 = 1;
	  double amp2 = 1;
	  boolean currFrame = true, normAll = false;
	  private ArrayList<Double> values = new ArrayList<Double>();
	  File file;
	
	  public LongSignalGenerator(){

		}

	  
	  
	public double[] setSignals(Bundle longSigGenInput) {
		// TODO Auto-generated method stub
		
		  longSignalType=longSigGenInput.getLong("longSigType");
		  amplitude=longSigGenInput.getDouble("gain");
		  frameSize=longSigGenInput.getInt("pulseWidth");
		 // pinValue=(int) longSigGenInput.getLong("pinSelection");
		  overlapValue=longSigGenInput.getLong("overlapValue");
		 // frameMax = longSigGenInput.getInt("frameMaxNum");
		  ys = new double[sig_length];
		   
		
		  
		  if (longSignalType == 1){
			 WavreadMale1a inputSignals = new WavreadMale1a();
			 sig_length = inputSignals.speech1a.length;
             System.arraycopy(inputSignals.speech1a, 0, ys, 0, sig_length);
       //      frameMax = (int)(sig_length/frameSize);
			 for(int i =0; i< sig_length; i++)
             ys[i] = amplitude*ys[i];
		  }

			
		  
		  
		  
		  
		return ys;
	
		
	
	
	
	}
	
	
	
	
	public double[] getFrame(int frame_no, double[]signal){
		
		
		current_y = new double[frameSize];
		overlapsamples = (overlapValue*frameSize/100);
		
		if(overlapValue == 0){
			for(int i =0; i< frameSize; i++)
				current_y[i] = ys[(frame_no-1)*frameSize+i];
		}
		/*else if(frame_no == 1){
			for(int i =0; i< frameSize; i++)
				current_y[i] = ys[(frame_no-1)*frameSize+i];
		}*/else
		{
			for(int i =0; i< frameSize; i++)
				current_y[i] = signal[(int) ((frame_no-1)*(frameSize-overlapsamples)+i)];
		}
		
		return current_y;
		
	}
	
	

public double[] generateSinusoid(double freq, double amp)
	{	double[] sine = new double[8192];
		for (int i = 0; i < 8192; i++)  sine[i] = amp*Math.sin(i*freq*Math.PI);

		return sine;
	}

	
public double[] getdoubleValue(List<Double> values) {
    int length = values.size();
    double[] result = new double[length];
    for (int i = 0; i < length; i++) {
      result[i] = values.get(i).doubleValue();
    }
    return result;
  }	
	
public String[] setXAxisValues(){
	String []xval = {"done", ""};
	//xval = 
	return xval;
}
	
}


