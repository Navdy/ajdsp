package com.prototype.ajdsp;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.TextView;

public class FreqRespDialog  extends Dialog implements OnClickListener{
	Button magButton;
	Button phaseButton;
	TextView pinDialogText;
	PartObject graphic;
	Bitmap pinBitmap;
	Bundle _inputBundle;
	String partCheckText;
	PlotView plotView;

	public FreqRespDialog(Context context) {
		super(context);
		//requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.freq_resp);
		
		pinDialogText = (TextView) findViewById(R.id.confirmation_message);
		magButton = (Button) findViewById(R.id.magSelect);

		magButton.setOnClickListener(this);
		phaseButton = (Button) findViewById(R.id.phaseSelect);
		phaseButton.setOnClickListener(this);
	}

	
	public void setInputBundle(Bundle inputBundle){
		_inputBundle=inputBundle;
	}
	@Override
	public void onBackPressed() {
	    dismiss();
	}
	@Override
	public void onClick(View v) {
		if (v == magButton){
			double[]signalMag=  _inputBundle.getDoubleArray("signalMag");
			Bundle signalMagBundle=new Bundle();
			if(signalMag==null)
				signalMag=new double[128];
			signalMagBundle.putDoubleArray("signal",signalMag);
			signalMagBundle.putDouble("pulseWidth", 128);
			String pi="\u03C0";
			String [] xAxisValues={"0",pi+"/4",pi+"/2","3"+pi+"/4",pi};
			signalMagBundle.putStringArray("xAxis",xAxisValues);
			Intent i = new Intent(this.getContext(), PlotPart.class);

			i.putExtra("blockValues",signalMagBundle);
			getContext().startActivity(i);


			dismiss();
		}
		if (v == phaseButton){
			double[]signalPhase=  _inputBundle.getDoubleArray("signalPhase");
			Bundle signalPhaseBundle=new Bundle();
			if(signalPhase==null)
				signalPhase=new double[128];
			signalPhaseBundle.putDoubleArray("signal",signalPhase);
			signalPhaseBundle.putDouble("pulseWidth", 128);
			String pi="\u03C0";
			String [] xAxisValues={"0",pi+"/4",pi+"/2","3"+pi+"/4",pi};
			signalPhaseBundle.putStringArray("xAxis",xAxisValues);
			Intent i = new Intent(this.getContext(), PlotPart.class);
			i.putExtra("blockValues",signalPhaseBundle);
			getContext().startActivity(i);
			dismiss();
		}
	}
}

