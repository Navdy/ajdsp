package com.prototype.ajdsp;

import android.content.Context;
import android.graphics.BitmapFactory;
import android.view.View;
import android.view.View.MeasureSpec;

public class SplashImage extends View {
int buttonHeight;
	public SplashImage(Context context) {
		super(context);
		buttonHeight=BitmapFactory.decodeResource(getResources(), R.drawable.blue_button).getHeight();
		setBackgroundResource(R.drawable.splashscreen);
		
	}
	@Override
	protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
		super.onMeasure(widthMeasureSpec, heightMeasureSpec);
       //String parentClass=  getContext().getClass().getName();
       //int height=StartView.button_height;
		int parentWidth = MeasureSpec.getSize(widthMeasureSpec);
		int parentHeight = MeasureSpec.getSize(heightMeasureSpec);
		this.setMeasuredDimension(
			parentWidth, parentHeight-buttonHeight);
		int a=0;
	}
	

}
