package com.prototype.ajdsp;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.TextView;

public class PlotDialog extends Dialog implements OnClickListener{
	Button magButton;
	Button phaseButton;
	Button realButton;
	Button imagButton;
	Button cancelButton;
	TextView pinDialogText;
	PartObject graphic;
	Bitmap pinBitmap;
	Bundle _inputBundle;
	String partCheckText;
	PlotView plotView;

	public PlotDialog(Context context) {
		super(context);
		//requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.plot_dialog);		
		magButton = (Button) findViewById(R.id.mag_plot);
		magButton.setOnClickListener(this);
		phaseButton = (Button) findViewById(R.id.phase_plot);
		phaseButton.setOnClickListener(this);
		realButton = (Button) findViewById(R.id.real_plot);
		realButton.setOnClickListener(this);
		imagButton = (Button) findViewById(R.id.imag_plot);
		imagButton.setOnClickListener(this);
		cancelButton = (Button) findViewById(R.id.cancel_plot);
		cancelButton.setOnClickListener(this);
		cancelButton.setBackgroundColor(Color.BLACK);
		cancelButton.setTextColor(Color.WHITE);
		
		
	}
	@Override
	public void onBackPressed() {
	    dismiss();
	}

	public void setText(String buttonText){
		pinDialogText.setText(buttonText);
		partCheckText=buttonText;


	}
	public void setInputBundle(Bundle inputBundle){
		_inputBundle=inputBundle;
	}

	@Override
	public void onClick(View v) {
		if (v == magButton){
			double[]signalMag=  _inputBundle.getDoubleArray("signalMag");
			if(signalMag==null)
				signalMag=new double[256];
			double[] signalMagLocal=new double[256];
			for(int i=0;i<signalMag.length;i++){
				signalMagLocal[i]=Math.sqrt(Math.pow(signalMag[i], 2));
			}
			Bundle signalMagBundle=new Bundle();
			
			signalMagBundle.putDoubleArray("signal",signalMagLocal);
			signalMagBundle.putDouble("pulseWidth", _inputBundle.getDouble("pulseWidth"));
		
			Intent i = new Intent(this.getContext(), PlotPart.class);
			i.putExtra("blockValues",signalMagBundle);
			getContext().startActivity(i);
			dismiss();
		}
		else if (v == phaseButton){
			double[]signalPhase=  _inputBundle.getDoubleArray("signalPhase");
			Bundle signalPhaseBundle=new Bundle();
			if(signalPhase==null)
				signalPhase=new double[256];
			signalPhaseBundle.putDoubleArray("signal",signalPhase);
			signalPhaseBundle.putDouble("pulseWidth", _inputBundle.getDouble("pulseWidth"));
			Intent i = new Intent(this.getContext(), PlotPart.class);
			i.putExtra("blockValues",signalPhaseBundle);
			getContext().startActivity(i);
			dismiss();
		}
		else if (v == realButton){
			double[]signalPhase=  _inputBundle.getDoubleArray("signalPhase");
			double[]signalMag=  _inputBundle.getDoubleArray("signalMag");
			double[] signalReal=new double[256];
			Bundle signalRealBundle=new Bundle();
			if((signalPhase==null))
				signalPhase=new double[256];
			
				if((signalMag==null))
				signalMag=new double[256];
			
				for(int i=0;i<signalMag.length;i++)
					signalReal[i]=signalMag[i]*Math.cos(signalPhase[i]*Math.PI/180);
							
				
			
			signalRealBundle.putDoubleArray("signal",signalReal);
			signalRealBundle.putDouble("pulseWidth", _inputBundle.getDouble("pulseWidth"));
			Intent i = new Intent(this.getContext(), PlotPart.class);
			i.putExtra("blockValues",signalRealBundle);
			getContext().startActivity(i);
			dismiss();
		}
		else if (v == imagButton){
			double[]signalPhase=  _inputBundle.getDoubleArray("signalPhase");
			double[]signalMag=  _inputBundle.getDoubleArray("signalMag");
			double[] signalImag=new double[256];
			Bundle signalImagBundle=new Bundle();
			
			if((signalPhase==null))
				signalPhase=new double[256];
			
				if((signalMag==null))
				signalMag=new double[256];
			
				for(int i=0;i<signalMag.length;i++)
					signalImag[i]=signalMag[i]*Math.sin(signalPhase[i]*Math.PI/180);
							
				
			
			signalImagBundle.putDoubleArray("signal",signalImag);
			signalImagBundle.putDouble("pulseWidth", _inputBundle.getDouble("pulseWidth"));
			Intent i = new Intent(this.getContext(), PlotPart.class);
			i.putExtra("blockValues",signalImagBundle);
			getContext().startActivity(i);
			dismiss();
		}
		else if(v==cancelButton)
			dismiss();
		
	}
}
