package com.prototype.ajdsp;

import android.os.Bundle;

public class PmlCalculator {
	int LP=0,HP=1,BP=2,SB=3;
	double pi = Math.PI;
	public int ftype = LP;
	public int order, filt_len, filt_cutoff;
	public int grid_length, num_bands, find_odd;
	public double wp1=0.1, ws1=0.25, wp2, ws2, rp=3, rs=20;
	public double wp_1, wp_2, ws_1, ws_2;
	public int lgrid = 16;
	public int Iter_max = 200;
	double[] freq = new double[6];
	double[] wts = new double[3];
	public double grid_v[];
	public double spread_weights[];
	public double desired[];

	//----------------------->>>>>>>> INITS FOR MINIMAX FUNCTION
	public int iext[];
	public double x[];
	public double ad[];
	public double res_y[];
	public double error[];
	public double taps[];
	public double bCoeff[];
	public double aCoeff[];
	boolean flag_bound = false;



	public Bundle getFilterCoeff(Bundle inputBundle){
		wp1=inputBundle.getDouble("Wp1");
		ws1=inputBundle.getDouble("Ws1");

		wp2=inputBundle.getDouble("Wp2");
		ws2=inputBundle.getDouble("Ws2");
		rp=inputBundle.getDouble("PB");
		rs=inputBundle.getDouble("SB");
		ftype=(int)inputBundle.getLong("filterType");

		calc_order();        // To compute the order

		parks_grid();        // To place the grid


		
		Bundle filterCoeff=new Bundle();
		if (order < 65)
		{  
			bCoeff = new double[order+1];
			aCoeff = new double[order+1];
			bCoeff = MINIMAX();
			aCoeff[0]=1;
			filterCoeff.putDoubleArray("filterCoeffB", bCoeff);
			filterCoeff.putDoubleArray("filterCoeffA", aCoeff);
			filterCoeff.putInt("order",order);
		}
		else if (order > 64)
		{
			bCoeff = new double[64];
			aCoeff = new double[64];
			filterCoeff.putDoubleArray("filterCoeffB", bCoeff);
			filterCoeff.putDoubleArray("filterCoeffA", aCoeff);
			filterCoeff.putInt("order",order);

		}
		else
		{System.out.println("This is bad");


		}
		return filterCoeff;


	}
	public void calc_order()
	{
		double d1, d2;
		double D, fk, df, length;
		double df1, df2;
		double dev_rp, dev_rs, maxim_dev;

		//--------------------------------------------->>>>>>>
		wp_1=wp1/2;    ws_1=ws1/2;
		wp_2=wp2/2;    ws_2=ws2/2;
		d1 = -rp/10;
		d2 = -rs/10;

		if (ftype == HP || ftype == BP)
		{	double  p = d2;
		d2 = d1;
		d1 = p;
		}

		D = (-0.428 - 0.594*d1 - 0.00266*d1*d1) + (d2*(-0.476 + d1*0.0711 + d1*d1*0.00531));
		fk = 11.01217 + 0.51244*(d1-d2);

		if (ftype == LP || ftype == HP)
		{	df = Math.abs(ws_1-wp_1);
		length = D/df - fk*df + 1;
		order = (int)Math.ceil(length) - 1;
		num_bands = 2;
		}
		else if (ftype == BP || ftype == SB)
		{	df1 = Math.abs(ws_1-wp_1);
		df2 = Math.abs(ws_2-wp_2);
		length = Math.max((D/df1 - fk*df1 + 1), (D/df2 - fk*df2 + 1));
		order = (int)Math.ceil(length) - 1;
		num_bands = 3;
		}
		dev_rp = Math.pow(10, (-rp/10));
		dev_rs = Math.pow(10, (-rs/10));
		maxim_dev = Math.max(dev_rp, dev_rs);

		// Organinizing the frequency bands and weights
		if (ftype == LP)
		{	freq[0] = 0.0;	freq[1] = wp_1;	freq[2] = ws_1;	freq[3] = 0.5;
		wts[0] = maxim_dev/dev_rp;	wts[1] = maxim_dev/dev_rs;
		}
		if (ftype == HP)
		{	freq[0] = 0.0;	freq[1] = ws_1;	freq[2] = wp_1;	freq[3] = 0.5;
		wts[0] = maxim_dev/dev_rs;	wts[1] = maxim_dev/dev_rp;
		}
		if (ftype == BP)
		{	freq[0] = 0.0;	freq[1] = ws_1;	freq[2] = wp_1;
		freq[3] = wp_2;	freq[4] = ws_2;	freq[5] = 0.5;
		wts[0] = maxim_dev/dev_rs;	wts[1] = maxim_dev/dev_rp;	wts[2] = maxim_dev/dev_rs;
		}
		if (ftype == SB)
		{	freq[0] = 0.0;	freq[1] = wp_1;	freq[2] = ws_1;
		freq[3] = ws_2;	freq[4] = wp_2;	freq[5] = 0.5;
		wts[0] = maxim_dev/dev_rp;	wts[1] = maxim_dev/dev_rs;	wts[2] = maxim_dev/dev_rp;
		}

	}		//calc_order ends here....


	//--------------------------- TO FIND THE GRID POINTS IN THE BANDS------------------->>>>>>>>>>>>>>>>> STEP - 2

	public void parks_grid()
	{
		int band1_len, band2_len, band3_len;
		double delf, dum_test;


		filt_len = order + 1;
		filt_cutoff = (int)Math.floor(filt_len/2);
		find_odd = filt_len % 2;
		if (find_odd == 1)    {filt_cutoff++;}
		delf = 1/(double)(lgrid * filt_cutoff);

		if (ftype == LP || ftype == HP)	// for LP and HP
		{
			band1_len = (int)Math.floor((2*Math.min(wp_1,ws_1))/delf);
			band2_len = 0;
			dum_test = 2*Math.max(wp_1,ws_1);
			while ( dum_test <= 1)
			{    dum_test = dum_test + delf;
			band2_len++;
			}

			grid_length = band1_len + 1 + band2_len;



			grid_v = new double[grid_length];
			spread_weights = new double[grid_length];
			desired = new double[grid_length];



			grid_v[0] = 0;
			for (int i = 1; i<= band1_len; i++)
			{	grid_v[i] = grid_v[i-1] + delf;
			}

			grid_v[band1_len] = freq[1]*2;
			grid_v[band1_len+1] = freq[2]*2;

			for (int i = 0; i< (band2_len-1); i++)
			{ grid_v[i+band1_len+2] = grid_v[i+band1_len+1] + delf;
			}
			grid_v[grid_length-1] = 1.0;



			// fixing spread weights and desired freq resp.
			if (ftype == LP)
			{	for (int i = 0; i <= band1_len; i++)
			{	spread_weights[i] = wts[0];
			desired[i] = 1.0;
			}
			for (int i = band1_len+1; i < grid_length; i++)
			{	spread_weights[i] = wts[1];
			desired[i] = 0.0;
			}
			}
			if (ftype == HP)
			{	for (int i = 0; i <= band1_len; i++)
			{	spread_weights[i] = wts[0];
			desired[i] = 0.0;
			}
			for (int i = band1_len+1; i < grid_length; i++)
			{	spread_weights[i] = wts[1];
			desired[i] = 1.0;
			}
			}
			for (int i = 0; i < grid_length; i++) {   grid_v[i] = grid_v[i] / 2;}
		}

		if(ftype == BP || ftype == SB)	// for BP and BS
		{
			band1_len = (int)Math.floor((2*Math.min(wp_1,ws_1))/delf);

			band2_len = 0;
			dum_test = 2*Math.max(wp_1,ws_1);
			while ( dum_test <= 2*Math.min(wp_2,ws_2) )
			{    dum_test = dum_test + delf;
			band2_len++;
			}

			band3_len = 0;
			dum_test = 2*Math.max(wp_2,ws_2);
			while ( dum_test <= 1)
			{    dum_test = dum_test + delf;
			band3_len++;
			}

			grid_length = band1_len + 1 + band2_len + 1 + band3_len;

			//   System.out.println("Band_1 length ="+band1_len);
			//   System.out.println("Band_2 length ="+band2_len);
			//   System.out.println("Band_3 length ="+band3_len);
			//   System.out.println("Grid length ="+grid_length);


			grid_v = new double[grid_length];
			spread_weights = new double[grid_length];
			desired = new double[grid_length];

			grid_v[0] = 0.0;
			for (int i = 1; i<= band1_len; i++)
			{ grid_v[i] = grid_v[i-1] + delf;
			}
			grid_v[band1_len] = freq[1]*2;
			grid_v[band1_len+1] = freq[2]*2;

			for (int i = 0; i< band2_len-1; i++)
			{	grid_v[i+band1_len+2] = grid_v[i+band1_len+1] + delf;
			}
			grid_v[band1_len + band2_len + 1]= freq[3] * 2;
			grid_v[band1_len + band2_len + 2]= freq[4] * 2;

			for( int i =0; i< band3_len-1; i++)
			{	grid_v[i + band1_len + band2_len + 2] = grid_v[i + band1_len + band2_len + 1] + delf;
			}
			grid_v[grid_length-1] = 1.0;

			// fixing spread weights and desired freq resp.

			if (ftype == BP)
			{	for (int i = 0; i <= band1_len; i++)
			{	spread_weights[i] = wts[0];
			desired[i] = 0.0;
			}
			for (int i = 0; i <= band2_len; i++)
			{	spread_weights[i + band1_len+1] = wts[1];
			desired[i + band1_len+1] = 1.0;
			}
			for (int i = 0; i <= band3_len ; i++)
			{	spread_weights[i + band1_len + band2_len + 1] = wts[2];
			desired[i+ band1_len + band2_len + 1] = 0.0;
			}
			}
			if (ftype == SB)
			{	for (int i = 0; i <= band1_len; i++)
			{	spread_weights[i] = wts[0];
			desired[i] = 1.0;
			}
			for (int i = 0; i <= band2_len; i++)
			{	spread_weights[i + band1_len+1] = wts[1];
			desired[i + band1_len+1] = 0.0;
			}
			for (int i = 0; i <= band3_len ; i++)
			{	spread_weights[i + band1_len + band2_len + 1] = wts[2];
			desired[i+ band1_len + band2_len + 1] = 1.0;
			}
			}

			//    System.out.println("I am done with assigning ===>"+grid_v.length+"  "+""+spread_weights.length);

			for (int i = 0; i < grid_length; i++){   grid_v[i] = grid_v[i] / 2; }

		}
	}
	public double[] MINIMAX()
	{

		/*System.out.println(" ");
             System.out.println(" ");
System.out.println("******************************************************************************* ");
System.out.println("			Parks-McClellan FILTER DESIGN - PARAMETERS VALUES					");
System.out.println("******************************************************************************* ");
System.out.println("delf = " + 1/(lgrid*filt_cutoff));
System.out.println("Order = " +order);
System.out.println("grid length = " + grid_length);
System.out.println("Num of bands = " +num_bands );
System.out.println("Filter length = " +filt_len );
System.out.println("NFCNS = " +filt_cutoff );
System.out.println("odd or not = " +find_odd );

		 */


		if (find_odd == 0)
		{    for (int i = 0; i < grid_length; i++)
		{    desired[i] = (double)desired[i] / (double)Math.cos(Math.PI * grid_v[i]);
		spread_weights[i] = (double)spread_weights[i] * (double)Math.cos(Math.PI * grid_v[i]);
		}
		}

		double c, temp;
		int symmetry, iter;

		iext = new int[filt_cutoff +2];
		ad = new double[filt_cutoff +1];
		x = new double[filt_cutoff +2];
		res_y = new double[filt_cutoff +1];
		error = new double[grid_length];
		taps = new double[filt_cutoff +1];


		temp = (double)(grid_length - 1) / filt_cutoff;
		symmetry = 1;

		for ( int i = 0; i <= filt_cutoff; i++)
		{	iext[i] = (int)(temp * i);
		}

		iext[filt_cutoff+1] = grid_length;


		/*System.out.println(" ");
System.out.println("******************************************************************************** ");
System.out.println("	<============	Initial iext values ===========>			");
System.out.println("******************************************************************************** ");
for (int i = 0; i < filt_cutoff +2; i = i + 1)
{    System.out.print("  [" +i);
	System.out.print("] =  " +iext[i]);
	if(i%5 ==0)
	{	System.out.println(" ");
	}
}
System.out.println(" ");

		 */



		//----------------------------------------------------------------------->>>>>>>>>>>>>>>>>>>>>>>
		//			TO PERFORM THE REMEZ EXCHANGE ALGORITHM
		//----------------------------------------------------------------------->>>>>>>>>>>>>>>>>>>>>>>

		for (iter=0; iter < Iter_max; iter++)
		{
			// System.out.println("No. of iterations = " +iter);

			CALC_RES_Y();
			COMP_ERROR();
			ERROR_TRACK();

			if (isDone(filt_cutoff, iext, error)) break;
		}

		if (iter == Iter_max) {     System.out.println("Reached maximum iteration count. Results may be bad");
		flag_bound = true;                           
		}

		CALC_RES_Y();


		/*System.out.println(" ");
System.out.println("******************************************************************************** ");
System.out.println("	<============	Final iext values ===========>			");
System.out.println("******************************************************************************** ");
for (int i = 0; i < filt_cutoff +2; i = i + 1)
{    System.out.print("  [" +i);
	System.out.print("] =  " +iext[i]);
	if(i%5 ==0)
	{	System.out.println(" ");
	}
}
System.out.println(" ");

		 */


		//----------------------------------------------------------------------->>>>>>>>>>>>>>>>>>>>>>>
		// Find the 'taps' of the filter for use with Frequency sampling.
		// If odd or Negative symmetry, fix the taps according to Parks McClellan
		//----------------------------------------------------------------------->>>>>>>>>>>>>>>>>>>>>>>

		for (int i=0; i<=filt_len/2; i++)
		{
			// System.out.println("TAPS of the filter===> "+i);
			if (symmetry == 1)
			{
				if (filt_len%2 != 0) c = 1;
				else c = Math.cos(pi * (double)i/filt_len);
			}
			else
			{
				if (filt_len%2 != 0) c = Math.sin(2 * pi * (double)i/filt_len);
				else c = Math.sin(pi * (double)i/filt_len);
			}
			taps[i] = compute_bs((double)i/filt_len, filt_cutoff, ad, x, res_y)*c;
		}
		// Frequency sampling design with calculated taps
		return freqSample(taps, symmetry);

	}


	//----------------------------------------------------------------------->>>>>>>>>>>>>>>>>>>>>>>
	//				TO COMPUTE THE PARAMETERS ad, x and y
	//----------------------------------------------------------------------->>>>>>>>>>>>>>>>>>>>>>>

	void CALC_RES_Y()
	{
		double sign, xi, delta, denom, numer;

		// Find x[]
		for (int i=0; i <= filt_cutoff; i++) x[i] = Math.cos(2 * pi * grid_v[iext[i]]);

		// Calculate ad[]  - Oppenheim & Schafer eq 7.132
		int ld = (filt_cutoff-1)/15 + 1;         // Skips around to avoid round errors

		for (int i=0; i<=filt_cutoff; i++)
		{
			denom = 1.0;
			xi = x[i];
			for (int j=0; j<ld; j++)
			{
				for (int k=j; k<=filt_cutoff; k+=ld)
					if (k != i) denom *= 2.0*(xi - x[k]);
			}
			if (Math.abs(denom)<0.00001) denom = 0.00001;
			ad[i] = 1.0/denom;
		}

		// Calculate delta  - Oppenheim & Schafer eq 7.131
		numer = denom = 0;
		sign = 1;
		for (int i=0; i<=filt_cutoff; i++) {
			numer += ad[i] * desired[iext[i]];
			denom += sign * ad[i]/spread_weights[iext[i]];
			sign = -sign;
		}
		delta = numer/denom;
		sign = 1;

		// Calculate y[]  - Oppenheim & Schafer eq 7.133b
		for (int i=0; i<=filt_cutoff; i++) {
			res_y[i] = desired[iext[i]] - sign * delta/spread_weights[iext[i]];
			sign = -sign;
		}
	}


	//----------------------------------------------------------------------->>>>>>>>>>>>>>>>>>>>>>>
	//				TO COMPUTE THE ERROR PARAMETERS
	//----------------------------------------------------------------------->>>>>>>>>>>>>>>>>>>>>>>

	void COMP_ERROR()
	{
		double A;
		for (int i = 0; i < grid_length; i++)
		{
			A = compute_bs(grid_v[i], filt_cutoff, ad, x, res_y);

			//  System.out.println("A_value = "+A);

			error[i] = spread_weights[i] * ( desired[i] - A );
		}
	}

	//----------------------------------------------------------------------->>>>>>>>>>>>>>>>>>>>>>>
	//				TO SEARCH FOR THE MAXIMA AND MINIMA OF THE ERROR CURVE
	//----------------------------------------------------------------------->>>>>>>>>>>>>>>>>>>>>>>
	//search(filt_cutoff, iext, grid_length, error);

	void ERROR_TRACK()
	{
		boolean up, alt;
		int[] foundExt = new int[grid_length];  /* Array of found extremals */
		int k = 0;
		// Check for extremum at 0.
		if (((error[0]>0.0) && (error[0]>error[1])) || ((error[0]<0.0) && (error[0]<error[1])))
			foundExt[k++] = 0;

		// Check for extrema inside dense grid
		for (int i=1; i<grid_length-1; i++) {
			if (((error[i]>=error[i-1]) && (error[i]>error[i+1]) && (error[i]>0.0)) ||
					((error[i]<=error[i-1]) && (error[i]<error[i+1]) && (error[i]<0.0)))
				foundExt[k++] = i;
		}

		// Check for extremum at 0.5
		int j = grid_length-1;
		if (((error[j]>0.0) && (error[j]>error[j-1])) || ((error[j]<0.0) && (error[j]<error[j-1])))
			foundExt[k++] = j;

		// Remove extra extremals
		int extra = k - (filt_cutoff+1);
		int l;
		while (extra > 0)
		{
			up = error[foundExt[0]] > 0.0;
			// up = true -->  first one is a maximum
			// up = false --> first one is a minimum
			l=0;
			alt = true;
			for (j=1; j<k; j++) {
				if (Math.abs(error[foundExt[j]]) < Math.abs(error[foundExt[l]]))
					l = j;               // new smallest error.
				if (up && (error[foundExt[j]] < 0.0))
					up = false;             // switch to a minima
				else if (!up && (error[foundExt[j]] > 0.0))
					up = true;             // switch to a maxima
				else {
					alt = false;
					break;              // Ooops, found two non-alternating
				}                     // extrema.  Delete smallest of them
			}  // if the loop finishes, all extrema are alternating

			// If there's only one extremal and all are alternating,
			// delete the smallest of the first/last extremals.
			if (alt && (extra == 1)) {
				if (Math.abs(error[foundExt[k-1]]) < Math.abs(error[foundExt[0]]))
					l = foundExt[k-1];   // Delete last extremal
				else
					l = foundExt[0];     // Delete first extremal
			}

			for (j=l; j<k; j++)      // Loop that does the deletion
				foundExt[j] = foundExt[j+1];
			k--;
			extra--;
		}
		//  Copy found extremals to ext[]
		for (int i=0; i<=filt_cutoff; i++) iext[i] = foundExt[i];
	}

	//----------------------------------------------------------------------->>>>>>>>>>>>>>>>>>>>>>>
	//				TO PERFORM FREQUENCY SAMPLING---->>> TO COMPUTE THE IMPULSE RESPONSE
	//----------------------------------------------------------------------->>>>>>>>>>>>>>>>>>>>>>>

	double compute_bs(double freq, int r, double ad[], double x[], double y[])
	{
		double c;
		double numer = 0;
		double denom = 0;
		double xc = Math.cos(2 * pi * freq);
		for (int i=0; i<=r; i++) {
			c = xc - x[i];
			if (Math.abs(c) < 1.0e-7) {
				numer = y[i];
				denom = 1;
				break;
			}
			c = ad[i]/c;
			denom += c;
			numer += c*y[i];
		}
		return numer/denom;
	}


	//----------------------------------------------------------------------->>>>>>>>>>>>>>>>>>>>>>>
	//				TO PERFORM FREQUENCY SAMPLING---->>> TO COMPUTE THE IMPULSE RESPONSE
	//----------------------------------------------------------------------->>>>>>>>>>>>>>>>>>>>>>>

	double[] freqSample(double A[], int symm)
	{
		double x, val;
		int N = filt_len;
		double M = (N-1.0)/2.0;
		double[] h = new double[N];
		if (symm == 1) {
			if (N%2 != 0) {
				for (int n=0; n<N; n++) {
					val = A[0];
					x = 2 * pi * (n - M)/N;
					for (int k=1; k<=M; k++) val += 2.0 * A[k] * Math.cos(x*k);
					h[n] = val/N;
				}
			}
			else {
				for (int n=0; n<N; n++) {
					val = A[0];
					x = 2 * pi * (n - M)/N;
					for (int k=1; k<=(N/2-1); k++) val += 2.0 * A[k] * Math.cos(x*k);
					h[n] = val/N;
				}
			}
		}
		else {
			if (N%2 != 0) {
				for (int n=0; n<N; n++) {
					val = 0;
					x = 2 * pi * (n - M)/N;
					for (int k=1; k<=M; k++) val += 2.0 * A[k] * Math.sin(x*k);
					h[n] = val/N;
				}
			}
			else {
				for (int n=0; n<N; n++) {
					val = A[N/2] * Math.sin(pi * (n - M));
					x = 2 * pi * (n - M)/N;
					for (int k=1; k<=(N/2-1); k++) val += 2.0 * A[k] * Math.sin(x*k);
					h[n] = val/N;
				}
			}
		}

		/*System.out.println(" ");
System.out.println("******************************************************************************** ");
System.out.println("	<============	FILTER COEFFICIENTS ===========>			");
System.out.println("******************************************************************************** ");
for (int i = 0; i < N; i = i + 1)
{
	System.out.print(" P(" +(i+1)+") =  " +h[i]+";");
  			System.out.println(" ");

}
System.out.println(" ");
		 */

		return h;
	}


	//----------------------------------------------------------------------->>>>>>>>>>>>>>>>>>>>>>>
	//				TO COMPUTE THE PARAMETERS ad, x and y
	//----------------------------------------------------------------------->>>>>>>>>>>>>>>>>>>>>>>

	boolean isDone(int r, int ext[], double e[])
	{

		double min, max, current;
		min = max = Math.abs(e[ext[0]]);

		for (int i=1; i<=r; i++)
		{
			current = Math.abs(e[ext[i]]);
			if (current < min) min = current;
			if (current > max) max = current;
		}
		if (((max-min)/max) < 0.0001) return true;
		return false;
	}

}