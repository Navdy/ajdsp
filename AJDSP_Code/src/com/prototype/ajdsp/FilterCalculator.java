package com.prototype.ajdsp;

import android.os.Bundle;

public class FilterCalculator {
	public double a[], b[], xI[], xQ[], yI[],yQ[];
	public int coefno;
	public int L;	

	public Bundle filterCompute(Bundle inputBundle){

		Bundle filterCoeffBundle=inputBundle.getBundle("bottomPin");
		double[] coeffB=filterCoeffBundle.getDoubleArray("filterCoeffB");
		double[] coeffA=filterCoeffBundle.getDoubleArray("filterCoeffA");

		Bundle inputSignalBundle=inputBundle.getBundle("leftPin");
		double[] inputSignalMag=inputSignalBundle.getDoubleArray("signalMag");
		double[] inputSignalPhase=inputSignalBundle.getDoubleArray("signalPhase");
		
		double length=inputSignalBundle.getDouble("pulseWidth");
		double [] inputSignalReal=new double[inputSignalMag.length];
		double [] inputSignalImag=new double[inputSignalMag.length];


		if ((filterCoeffBundle!=null) && (inputSignalBundle != null))
		{
			for(int i=0;i<inputSignalMag.length;i++){
				inputSignalReal[i]= inputSignalMag[i]*Math.cos(inputSignalPhase[i]*Math.PI/180);
				inputSignalImag[i]= inputSignalMag[i]*Math.sin(inputSignalPhase[i]*Math.PI/180);
			}
			initialize_data(coeffB,coeffA,inputSignalReal,inputSignalImag);
			filter();
		}

		double[] yMag=new double[yI.length];
		double[]yPhase=new double[yI.length];
		for(int i=0;i<yI.length;i++){
			//yMag[i]=Math.pow(Math.pow(+yI[i],2)+Math.pow(yQ[i],2),.5);
			//yPhase[i]=Math.atan(yQ[i]/yI[i]);
			yMag[i]=yI[i];
		}

		Bundle rightPinBundle=new Bundle();
		rightPinBundle.putDoubleArray("signalMag",yMag);
		rightPinBundle.putDoubleArray("signalPhase",yPhase);
		
		rightPinBundle.putDouble("pulseWidth",length);
       
		inputBundle.putBundle("rightPin",rightPinBundle);
		inputBundle.putBundle("topPin",filterCoeffBundle);
		
		return inputBundle;
	}

	public void initialize_data(double[] coeffB,double[] coeffA,double[] inputSignalReal,double [] inputSignalImag)
	{
		int length=coeffB.length;
		if (coeffB.length>64+1)	{	length=64+1;	}
		coefno=length;
		L=inputSignalReal.length;

		a = new double[coefno];
		b = new double[coefno];

		yI = new double[L];
		yQ = new double[L];
		xI = new double[L];
		xQ = new double[L];
		for (int i=0;i<L;i++)	{	
			yI[i]=0;
			yQ[i]=0; 
			xI[i]=inputSignalReal[i];
			xQ[i]=inputSignalImag[i];	
		}
		for (int i=0;i<coefno;i++)	{	
			b[i]=coeffB[i];	
			a[i]=coeffA[i];	
		}
	}

	public void filter()
	{

		double sumI=0,sumQ=0;
		for (int i=0;i<L;i++)
		{	sumI=0;sumQ=0;
		for (int j=0;j<coefno;j++)	{
			if (i>=j)	{
				sumI=sumI+b[j]*xI[i-j];
				sumQ=sumQ+b[j]*xQ[i-j];
			}
		}
		yI[i]=sumI;
		yQ[i]=sumQ;
		for (int j=1;j<coefno;j++)	{
			if (i>=j)	{
				yI[i]=yI[i]-a[j]*yI[i-j];
				yQ[i]=yQ[i]-a[j]*yQ[i-j];
			}
		}
		}
	}

}
