package com.prototype.ajdsp;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.net.MalformedURLException;

import android.os.Bundle;

import com.facebook.android.AsyncFacebookRunner;
import com.facebook.android.AsyncFacebookRunner.RequestListener;
import com.facebook.android.DialogError;
import com.facebook.android.Facebook;
import com.facebook.android.Facebook.DialogListener;
import com.facebook.android.FacebookError;

import android.app.Activity;

import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;

import android.view.View;
import android.view.Window;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;



public class FBLogin extends Activity implements OnClickListener {
	RelativeLayout layout;
	
	Facebook facebook = new Facebook("419180978147389");
	AsyncFacebookRunner mAsyncRunner = new AsyncFacebookRunner(facebook);
	private SharedPreferences mPrefs;
	Button fblogout;
	Button fbpost;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.fb_home);
		layout= (RelativeLayout) findViewById(R.id.fb_start_layout);
		layout.setBackgroundColor(Color.WHITE);
		  mPrefs = getPreferences(MODE_PRIVATE);
	        String access_token = mPrefs.getString("access_token", null);
	        long expires = mPrefs.getLong("access_expires", 0);
	        if(access_token != null) {
	            facebook.setAccessToken(access_token);
	        }
	        if(expires != 0) {
	            facebook.setAccessExpires(expires);
	        }
	        fblogout=(Button)findViewById(R.id.fblogout);
	        fblogout.setOnClickListener(this);
	        fbpost=(Button)findViewById(R.id.fbpost);
	        fbpost.setOnClickListener(this);
	        
		
		
		
	        if(!facebook.isSessionValid()) {
		 facebook.authorize(this,  new String[] { "email", "publish_checkins" },new DialogListener() {
	            @Override
	            public void onComplete(Bundle values) {
	            SharedPreferences.Editor editor = mPrefs.edit();
                editor.putString("access_token", facebook.getAccessToken());
                editor.putLong("access_expires", facebook.getAccessExpires());
                editor.commit();
                }

	            @Override
	            public void onFacebookError(FacebookError error) {}

	            @Override
	            public void onError(DialogError e) {}

	            @Override
	            public void onCancel() {}
	        });
	        }
	}
	@Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        facebook.authorizeCallback(requestCode, resultCode, data);
    }
	/*@Override
	 public void onStop() {
		        super.onStop();	
				finish();

	 }*/
	 public void onResume() {    
	        super.onResume();
	        facebook.extendAccessTokenIfNeeded(this, null);
	    }

	
	
	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		if(v==fblogout){
			mAsyncRunner.logout(this, new RequestListener() {
				  @Override
				  public void onComplete(String response, Object state) {}
				  
				  @Override
				  public void onIOException(IOException e, Object state) {}
				  
				  @Override
				  public void onFileNotFoundException(FileNotFoundException e,
				        Object state) {}
				  
				  @Override
				  public void onMalformedURLException(MalformedURLException e,
				        Object state) {}
				  
				  @Override
				  public void onFacebookError(FacebookError e, Object state) {}
				});
			Intent i = new Intent(this, StartView.class);
			startActivity(i);
			
		}else if(v==fbpost){
			 Bundle params = new Bundle();
			    params.putString("to", "");
			    
			    facebook.dialog(this, "feed", params, new DialogListener(){
			    		@Override
			    		public void onComplete(Bundle values) {
			    			// TODO Auto-generated method stub

			    		}

			    		@Override
			    		public void onFacebookError(FacebookError e) {
			    			// TODO Auto-generated method stub

			    		}

			    		@Override
			    		public void onError(DialogError e) {
			    			// TODO Auto-generated method stub

			    		}

			    		@Override
			    		public void onCancel() {
			    			// TODO Auto-generated method stub

			    		}});
			   
			  
		}
	}

	
	}


