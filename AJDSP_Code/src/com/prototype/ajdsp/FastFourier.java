package com.prototype.ajdsp;

public class FastFourier {
	  public double data[] = new double[513];
	  private double[] x;
	  private double[] y;
	  private double[] phase;
	  private double[] phase_o;
	  double max, min;
	  int size,sign;
	  double pi=Math.PI;

	  public FastFourier(double[] xin, double[] ph, int fft_length,int ff_flag)
	    {
		  this.size=fft_length;
		  phase = new double[size];
		  x = new double[size];
		  phase_o = new double[size];
		  y = new double[size];

		 for (int k=0;k<size;k++)
		 {
			 this.x[k] =xin[k];
			 this.phase[k]=ph[k];
		 }
		 this.sign=ff_flag;
	     Calculate();
	    }


	  void Calculate()
	    {
	     int i;
	     double frac;

		 for (i=0;i<=512;i++)
			 data[i]=0;

		 if (sign == 1)
		 { for (i=0;i<size;i++)
	             {
	              data[2*i+1] = x[i]*Math.cos(phase[i]*pi/(double)180);
	              data[2*i+2] = x[i]*Math.sin(phase[i]*pi/(double)180);
	             }
		 }
		 else
		 {  for (i=0;i<size;i++)
	           {
	              data[2*i+1] = x[i]*Math.cos(phase[i]*pi/(double)180);
	              data[2*i+2] = x[i]*Math.sin(phase[i]*pi/(double)180);
	            }

		 }
		 fft(size,sign);

	     for (i=0;i<size;i++)
	       {
			 if (sign==1)
			 {
				 y[i] = Math.sqrt(data[2*i+1]*data[2*i+1]+data[2*i+2]*data[2*i+2]);
				 phase_o[i]=Math.atan2(data[2*i+2],data[2*i+1])*180/pi;

			 }
			 if (sign==-1)
			 {
				 y[i] = data[2*i+1];
		         phase_o[i]=0;
			 }
		 }
		 //for (i=size;i<256;i++)
		 //{
		//	 y[i]=0;
		//	 phase_o[i]=0;
		// }
	  }
	/*
	*//* one dimensional fft routine                                             */
	/*                                                                           */
	/*                                                                           */
	/* void fft(double *data, int nn, int isign)                                 */
	/*                                                                           */
	/*                                                                           */
	/* data  one dimensional array containing 2*nn real input values             */
	/*                                                                           */
	/* nn    number of complex data points for the fft, MUST be a power of 2.    */
	/*       This is checked for only if DEBUG is defined.                       */
	/*                                                                           */
	/* isign "direction" of the fft. "+1" -> fft, "-1" -> ifft. The 1/nn scaling */
	/*       required to maintain constant power of the signal IS done by the    */
	/*       routine.                                                            */
	/*                                                                           */
	/*                                                                           */
	/* data organization, time domain                                            */
	/*                                                                           */
	/* - each complex values occupies two sequential locations, the real part is */
	/*   followed by the imagniary part                                          */
	/*                                                                           */
	/* - the subscript ranges from 1 to nn rather than from 0 to nn-1            */
	/*                                                                           */
	/*                                                                           */
	/* data organization, frequency domain                                       */
	/*                                                                           */
	/* - each complex values occupies two sequential locations, the real part is */
	/*   followed by the imagniary part                                          */
	/*                                                                           */
	/* - complex sample 1         f =    (0.0)                                   */
	/*   complex sample 2         f = +  (1.0)/(nn*dt)                           */
	/*   .                                                                       */
	/*   complex sample nn/2      f = +  (0.5*nn-1)/(nn*dt)                      */
	/*   complex sample nn/2+1    f = +- (1.0)/(2*dt)        (combination)       */
	/*   complex sample nn/2+2    f = -  (0.5*nn-1)/(nn*dt)                      */
	/*   .                                                                       */
	/*   complex sample nn        f = -  (1.0)/(nn*dt)                           */
	/*                                                                           */
	/*****************************************************************************/

	void fft(int nn, int isign)
	{
	  int n,mmax,m,j,istep,i;
	  double wtemp,wr,wpr,wpi,wi,theta;
	  double tempr,tempi;

	  n=nn<<1;
	  j=1;
	  for(i=1;i<n;i+=2) {
	    if(j>i) {
	      tempr=data[j];
	      data[j]=data[i];
	      data[i]=tempr;
	      tempr=data[j+1];
	      data[j+1]=data[i+1];
	      data[i+1]=tempr;
	    }
	    m=n>>1;
	    while((m>=2)&&(j>m)) {
	      j-=m;
	      m>>=1;
	    }
	    j+=m;
	  }
	  mmax=2;
	  while(n>mmax) {
	    istep=2*mmax;
	    theta=-6.28318530717959/((double)isign*(double)mmax);
	    wtemp=Math.sin(0.5*theta);
	    wpr=-2.0*wtemp*wtemp;
	    wpi=Math.sin(theta);
	    wr=1.0;
	    wi=0.0;
	    for(m=1;m<mmax;m+=2) {
	      for(i=m;i<=n;i+=istep) {
		j=i+mmax;
		tempr=wr*data[j]-wi*data[j+1];
		tempi=wr*data[j+1]+wi*data[j];
		data[j]=data[i]-tempr;
		data[j+1]=data[i+1]-tempi;
		data[i]+=tempr;
		data[i+1]+=tempi;
	      }
	      wr=(wtemp=wr)*wpr-wi*wpi+wr;
	      wi=wi*wpr+wtemp*wpi+wi;
	    }
	    mmax=istep;
	  }
	  if(isign == -1) {
	    tempi = 1.0/nn;
	    for(i=1;i<=2*nn;i++) data[i] *= tempi;
	  }
	}

	 public double getMagnitude(int indx)
	 {
		 return y[indx];
	 }
	 public double getPhase(int indx)
	 {
		 return phase_o[indx];
	 }

}
