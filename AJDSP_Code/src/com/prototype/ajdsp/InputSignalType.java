
package com.prototype.ajdsp;






import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import com.prototype.ajdsp.ConvolutionPart.SignalArrays;


import android.app.Activity;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.AdapterView.OnItemSelectedListener;

public class InputSignalType extends Activity implements OnClickListener{
	Button okButton;
	Button back;
	String signal1Type;
	public static String signal1Causality;
	String signal2Type;
	public static String signal2Causality;
	Button add;
	TextView title;
	public static List<SequenceData> contSequence=new ArrayList<SequenceData>();
	
	LinearLayout layout;
	RelativeLayout topBar;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		//requestWindowFeature(Window.FEATURE_CUSTOM_TITLE);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
		setContentView(R.layout.input);
		//getWindow().setFeatureInt(Window.FEATURE_CUSTOM_TITLE, R.layout.row);
		layout=(LinearLayout)findViewById(R.id.inputLayout);
		layout.setBackgroundColor(Color.WHITE);
		title=(TextView)findViewById(R.id.Title);
		String signalType=getIntent().getStringExtra("signalType");
		if(signalType.equalsIgnoreCase("discrete")){
			title.setText("Discrete Signal");
		} else if (signalType.equalsIgnoreCase("continuous")){
			title.setText("Continuous Signal");
		}
			Bundle extras=getIntent().getExtras();
			if(extras!=null){
				Bundle spinnerValue= extras.getBundle("spinnerData");
				if(spinnerValue!=null){
		setSpinner(R.id.signalType1,R.array.signal_type,spinnerValue.getInt("signal1Type"));
		setSpinner(R.id.causalityType1,R.array.causality_type,spinnerValue.getInt("signal1Causality"));
		setSpinner(R.id.signalType2,R.array.signal_type,spinnerValue.getInt("signal2Type"));
		setSpinner(R.id.causalityType2,R.array.causality_type,spinnerValue.getInt("signal2Causality"));
			} else{
				setSpinner(R.id.signalType1,R.array.signal_type,0);
				setSpinner(R.id.causalityType1,R.array.causality_type,0);
				setSpinner(R.id.signalType2,R.array.signal_type,0);
				setSpinner(R.id.causalityType2,R.array.causality_type,0);
			}
			}
		back=(Button) findViewById(R.id.back_select_parts);
		back.setOnClickListener(this);
		back.setText("Back");
		
		okButton= (Button)findViewById(R.id.add_part);       
        okButton.setText("Update");
        okButton.setOnClickListener(this);
        	}
	@Override
	public void onClick(View v) {
		/** When OK Button is clicked, dismiss the dialog */
		if (v == okButton){

			Bundle inputData=new Bundle();
			InputSignal input1=new InputSignal();
			InputSignal input2=new InputSignal();
			input1.type=signal1Type;
			input2.type=signal2Type;
			Bundle spinnerData= new Bundle();
			spinnerData.putInt("signal1Type",getSelItem(R.id.signalType1));
			spinnerData.putInt("signal2Type",getSelItem(R.id.signalType2));
			spinnerData.putInt("signal1Causality",getSelItem(R.id.causalityType1));
			spinnerData.putInt("signal2Causality",getSelItem(R.id.causalityType2));	
			inputData.putBundle("spinnerData", spinnerData);
			input1.causality=signal1Causality;
			input2.causality=signal2Causality;
			ConvolutionPart part=new ConvolutionPart();
			
			
			String extras=getIntent().getStringExtra("signalType");
			if(extras!=null){
				if(extras.equalsIgnoreCase("discrete")){
					SignalArrays inputOutput=part.obtainSignals(input1, input2,15,"disc");
				
					inputData.putDoubleArray("input1", inputOutput.inputArray1);
					inputData.putDoubleArray("input2", inputOutput.inputArray2);
					inputData.putDoubleArray("output", inputOutput.outputArray);
					SequenceDataList sequence=new SequenceDataList();
					sequence. sequenceList=createDiscreteSequence(inputOutput.inputArray1,inputOutput.inputArray1.length,inputOutput.inputArray2.length,inputOutput.outputArray.length);
					inputData.putSerializable("sequenceData",sequence);
					Intent i = new Intent(this, ConvDemoDisplay.class);
					i.putExtra("inputValues", inputData);
					startActivity(i);
				}

				else if(extras.equalsIgnoreCase("continuous")){
				int length=0;
					if(PartsController.width>640)
					 length=64*2+1;
				else 
					length=64+1;
					SignalArrays inputOutput=part.obtainSignals(input1, input2,length,"cont");
				
					inputData.putDoubleArray("input1", inputOutput.inputArray1);
					inputData.putDoubleArray("input2", inputOutput.inputArray2);
					
				if(input1.type.equalsIgnoreCase("Delta")||input2.type.equalsIgnoreCase("Delta")||input1.type.equalsIgnoreCase("Shifted Delta")||input2.type.equalsIgnoreCase("Shifted Delta"))
					inputData.putString("outputType", "d");
				else 
					inputData.putString("outputType", "nd");
				if(input1.type.equalsIgnoreCase("Delta")||input1.type.equalsIgnoreCase("Shifted Delta")||input1.type.equalsIgnoreCase("Rectangular"))
					inputData.putString("input1Type", "dr");
				else 
					inputData.putString("input1Type", "nd");
				if(input2.type.equalsIgnoreCase("Delta")||input2.type.equalsIgnoreCase("Shifted Delta")||input2.type.equalsIgnoreCase("Rectangular"))
					inputData.putString("input2Type", "dr");
				else 
					inputData.putString("input2Type", "nd");
			
					
					inputData.putDoubleArray("output", inputOutput.outputArray);
					SequenceDataList sequence=new SequenceDataList();
					double []inputArray1=new double[inputOutput.inputArray1.length];
				
					
					contSequence=createContinouousSequence(inputOutput.inputArray1,inputOutput.inputArray1.length,inputOutput.inputArray2.length,inputOutput.outputArray.length);
					
					Intent i = new Intent(this, ConvDemoContDisplay.class);
					i.putExtra("inputValues", inputData);
					startActivity(i);
				}
			}



		}
		else if(v==back){
			Intent i = new Intent(this, Mode.class);	
			startActivity(i);

		}
	}
	@Override
	 public void onStop() {
		        super.onStop();	
				finish();
				
	 }
	@Override
	public void onBackPressed() {
	    // do nothing.
	}
	public static class SequenceDataList implements Serializable{

		/**
		 * 
		 */
		private static final long serialVersionUID = 1L;
		List<SequenceData> sequenceList;
	}
	public static class SequenceData  implements Serializable{
		/**
		 * 
		 */
		private static final long serialVersionUID = 1L;
		int input1=0;
		int input2=0;
		int output=0;
		int stage=0;
		int[] xCoord;
		double[] inputArray;
		List<double[]> circleArrayList= new ArrayList<double[]>() ;
		List<double[]> inputArrayList= new ArrayList<double[]>() ;
	}
	public List<SequenceData> createDiscreteSequence(double[] input1,int inputLength1,int inputLength2,int outputLength){
		List<SequenceData> sequenceList=new ArrayList<SequenceData>();


		for(int i=1;i<=inputLength1;i++){
			SequenceData sequenceData=new SequenceData();
			sequenceData.input1=i;
			sequenceList.add(sequenceData);
		}
		for(int i=0;i<5;i++)
		{
			SequenceData sequenceData=new SequenceData();
			sequenceData.stage++;
			if(i==0){
				sequenceData.xCoord=new int[7];
				sequenceData.inputArray= new double[7];
				for(int j=2;j<9;j++){
					sequenceData.xCoord[j-2]=j;	
					if(j==2){
						//sequenceData.inputArray[j-2]=Math.max(input1[j-1],input1[j-2]);
						double[]circleArray=new double[2];
						double[]inputArray=new double[2];
						for(int circleCount=0;circleCount<2;circleCount++){
							circleArray[circleCount]=input1[circleCount];
						    inputArray[circleCount]=input1[circleCount];
						}
						
						for(int inputCount=0;inputCount<2;inputCount++)
							
						//sequenceData.inputArray[j-2]=maxArray(circleArray);
						sequenceData.inputArrayList.add(j-2, inputArray);
						sequenceData.circleArrayList.add(j-2, circleArray);
					}else if (j==8){
						sequenceData.inputArray[j-2]=Math.max(input1[j+1],input1[j+2]);
						double[]circleArray=new double[2];
						double[]inputArray=new double[2];
						for(int circleCount=0;circleCount<2;circleCount++){
							circleArray[circleCount]=input1[circleCount+j+1];
						    inputArray[circleCount]=input1[circleCount+j+1];
						}
						//sequenceData.inputArray[j-2]=maxArray(circleArray);
						sequenceData.circleArrayList.add(j-2, circleArray);
						sequenceData.inputArrayList.add(j-2, inputArray);
					}  

					else if(j==7){
						sequenceData.inputArray[j-2]=Math.max(input1[j+1],input1[j]);
						double[]circleArray=new double[2];
						double[]inputArray=new double[2];
						for(int circleCount=0;circleCount<2;circleCount++){
							circleArray[circleCount]=input1[circleCount+j];
							inputArray[circleCount]=input1[circleCount+j];
						}
						//sequenceData.inputArray[j-2]=maxArray(circleArray);
						sequenceData.inputArrayList.add(j-2, inputArray);
						sequenceData.circleArrayList.add(j-2, circleArray);
					}else if(j==3){
						sequenceData.inputArray[j-2]=Math.max(input1[j],input1[j-1]);
						double[]circleArray=new double[2];
						double[]inputArray=new double[2];
						for(int circleCount=0;circleCount<2;circleCount++){
							circleArray[circleCount]=input1[circleCount+j-1];
							inputArray[circleCount]=input1[circleCount+j-1];
							
						}
						//sequenceData.inputArray[j-2]=maxArray(circleArray);
						sequenceData.inputArrayList.add(j-2, inputArray);
						sequenceData.circleArrayList.add(j-2, circleArray);
					}
					else {
						sequenceData.inputArray[j-2]=input1[j];
						double[]circleArray=new double[1];
						double[]inputArray=new double[1];
						circleArray[0]=input1[j];
						inputArray[0]=input1[j];
						//sequenceData.inputArray[j-2]=maxArray(circleArray);
						sequenceData.inputArrayList.add(j-2, inputArray);
						sequenceData.circleArrayList.add(j-2,circleArray);
					}

				}
			}else if(i==1) {
				sequenceData.xCoord=new int[3];
				sequenceData.inputArray= new double[3];
				for(int j=((inputLength1-1)/2-1);j<(inputLength1-1)/2+2;j++){
					sequenceData.xCoord[j-(((inputLength1-1)/2-1))]=j;
					double[]inputArray=new double[(inputLength1-1)/2];
					if(j==((inputLength1-1)/2-1)){
						
                       double[]circleArray=new double[(inputLength1-1)/2];
						for(int circleCount=0;circleCount<(inputLength1-1)/2;circleCount++){
							circleArray[circleCount]=input1[circleCount];
							inputArray[circleCount]=input1[circleCount];
						}
						//sequenceData.inputArray[j-(((inputLength1-1)/2-1))]=maxArray(circleArray);
						sequenceData.circleArrayList.add(j-((inputLength1-1)/2-1),circleArray);}
					else if(j==(inputLength1-1)/2+1){
						double[]circleArray=new double[(inputLength1-1)/2];
						for(int circleCount=0;circleCount<(inputLength1-1)/2;circleCount++){
							circleArray[circleCount]=input1[circleCount+j];
							inputArray[circleCount]=input1[circleCount+j];
						}
						sequenceData.circleArrayList.add(j-((inputLength1-1)/2-1),circleArray);
						//sequenceData.inputArray[j-(((inputLength1-1)/2-1))]=maxArray(circleArray);
					} else{
						double[]circleArray=new double[1];
						circleArray[0]=input1[(inputLength1-1)/2];
						inputArray[0]=input1[(inputLength1-1)/2];
						//sequenceData.inputArray[j-(((inputLength1-1)/2-1))]=maxArray(circleArray);
						
						sequenceData.circleArrayList.add(j-((inputLength1-1)/2-1),circleArray);
					}
					sequenceData.inputArrayList.add(j-((inputLength1-1)/2-1),inputArray);
				}
			}else if(i==2) {
				sequenceData.xCoord=new int[1];
				sequenceData.inputArray= new double[1];
				sequenceData.xCoord[0]=(inputLength1-1)/2;
				sequenceData.inputArray[0]=input1[(inputLength1-1)/2];
				double[]circleArray=new double[inputLength1];
				double[]inputArray=new double[inputLength1];
				for(int circleCount=0;circleCount<inputLength1;circleCount++){
					circleArray[circleCount]=input1[circleCount];
					inputArray[circleCount]=input1[circleCount];
				}
				sequenceData.inputArrayList.add(0,circleArray);
				sequenceData.circleArrayList.add(0,circleArray);
			}else if(i==3) {
				sequenceData.xCoord=new int[3];
				sequenceData.inputArray= new double[3];
				
				for(int j=((inputLength1-1)/2-1);j<(inputLength1-1)/2+2;j++){
					double[]inputArray=new double[((inputLength1-1)/2)];
					sequenceData.xCoord[j-((inputLength1-1)/2-1)]=j;
					sequenceData.inputArray[j-((inputLength1-1)/2-1)]=input1[inputLength1-1-j];
					if(j==((inputLength1-1)/2-1)){
						double[]circleArray=new double[((inputLength1-1)/2)];
						for(int circleCount=0;circleCount<(inputLength1-1)/2;circleCount++){
							circleArray[circleCount]=input1[inputLength1-1-circleCount];
							inputArray[circleCount]=input1[inputLength1-1-circleCount];
						}
						//sequenceData.inputArray[j-((inputLength1-1)/2-1)]=maxArray(circleArray);
						sequenceData.circleArrayList.add(j-((inputLength1-1)/2-1),circleArray);
					}
					else if(j==(inputLength1-1)/2+1){
						double[]circleArray=new double[(inputLength1-1)/2];
						for(int circleCount=0;circleCount<(inputLength1-1)/2;circleCount++){
							circleArray[circleCount]=input1[circleCount];
							inputArray[circleCount]=input1[circleCount];
						}
						//sequenceData.inputArray[j-(((inputLength1-1)/2-1))]=maxArray(circleArray);
						sequenceData.circleArrayList.add(j-((inputLength1-1)/2-1),circleArray);
					} else{
						double[]circleArray=new double[1];
						circleArray[0]=input1[(inputLength1-1)/2];
						inputArray[0]=input1[(inputLength1-1)/2];
						//sequenceData.inputArray[j-(((inputLength1-1)/2-1))]=maxArray(circleArray);
						sequenceData.circleArrayList.add(j-((inputLength1-1)/2-1),circleArray);
					}
					sequenceData.inputArrayList.add(j-((inputLength1-1)/2-1),inputArray);
					}
			}else if(i==4) {
				sequenceData.xCoord=new int[7];
				sequenceData.inputArray= new double[7];
				
				for(int j=2;j<9;j++){
					sequenceData.xCoord[j-2]=j;
					double[]inputArray=new double[inputLength1];
					if(j==2){
						//sequenceData.inputArray[j-2]=input1[inputLength1-1-(j-1)];
						double[]circleArray=new double[2];
						for(int circleCount=0;circleCount<2;circleCount++){
							circleArray[circleCount]=input1[inputLength1-1-circleCount];
							inputArray[circleCount]=input1[inputLength1-1-circleCount];
						}
						//sequenceData.inputArray[j-2]=maxArray(circleArray);
						//sequenceData.inputArray[j-(((inputLength1-1)/2-1))]=maxArray(circleArray);
						sequenceData.circleArrayList.add(j-2, circleArray);
					}else if (j==8){
						//sequenceData.inputArray[j-2]=input1[inputLength1-1-(j+1)];
						double[]circleArray=new double[2];
						for(int circleCount=0;circleCount<2;circleCount++){
							circleArray[circleCount]=input1[inputLength1-1-(circleCount+j+1)];
							inputArray[circleCount]=input1[inputLength1-1-(circleCount+j+1)];
						}
						//sequenceData.inputArray[j-2]=maxArray(circleArray);
						sequenceData.circleArrayList.add(j-2,circleArray);
					} else if(j==7){
						//sequenceData.inputArray[j-2]=input1[inputLength1-1-j];
						double[]circleArray=new double[2];
						for(int circleCount=0;circleCount<2;circleCount++){
							circleArray[circleCount]=input1[inputLength1-1-(circleCount+j)];
							inputArray[circleCount]=input1[inputLength1-1-(circleCount+j)];
						}
						//sequenceData.inputArray[j-2]=maxArray(circleArray);
						sequenceData.circleArrayList.add(j-2, circleArray);
					}else if(j==3){
						//sequenceData.inputArray[j-2]=input1[inputLength1-1-j];
						double[]circleArray=new double[2];
						for(int circleCount=0;circleCount<2;circleCount++){
							circleArray[circleCount]=input1[inputLength1-1-(circleCount+j-1)];
							inputArray[circleCount]=input1[inputLength1-1-(circleCount+j-1)];
						}
						//sequenceData.inputArray[j-2]=maxArray(circleArray);
						sequenceData.circleArrayList.add(j-2, circleArray);
					}else {
						sequenceData.inputArray[j-2]=input1[inputLength1-1-j];
						double[]circleArray=new double[1];
						circleArray[0]=input1[inputLength1-1-j];
						inputArray[0]=input1[inputLength1-1-j];						
						//sequenceData.inputArray[j-2]=maxArray(circleArray);
						sequenceData.circleArrayList.add(j-2,circleArray);

					}
					sequenceData.inputArrayList.add(j-2, inputArray);
				}
			} 
			sequenceList.add(sequenceData);
		}
		for(int i=0;i<inputLength1;i++){
			SequenceData sequenceData=new SequenceData();
			sequenceData.inputArray=new double[inputLength1];
			sequenceData.xCoord=new int[inputLength1];
			sequenceData.stage=2;
			for(int k=0;k<inputLength1;k++){
				sequenceData.inputArray[k]=input1[inputLength1-1-k];
				sequenceData.xCoord[k]=k-i;
			}
			sequenceList.add(sequenceData);
		}
		for(int i=1;i<=inputLength2;i++){
			SequenceData sequenceData=new SequenceData();
			sequenceData.input2=i;
			sequenceData.stage=3;
			sequenceData.inputArray=new double[inputLength1];
			sequenceData.xCoord=new int[inputLength1];
			for(int k=0;k<inputLength1;k++){
				sequenceData.inputArray[k]=input1[inputLength1-1-k];
				sequenceData.xCoord[k]=k-inputLength1;
			}
			sequenceList.add(sequenceData);
		}
		for(int i=1;i<=outputLength;i++){
			SequenceData sequenceData=new SequenceData();
			sequenceData.output=i;
			sequenceData.stage=4;
			sequenceData.inputArray=new double[inputLength1];
			sequenceData.xCoord=new int[inputLength1];
			for(int k=0;k<inputLength1;k++){
				sequenceData.inputArray[k]=input1[inputLength1-1-k];
				sequenceData.xCoord[k]=k-inputLength1+i;
			}

			sequenceList.add(sequenceData);
		}


		return sequenceList;

	}


	public List<SequenceData> createContinouousSequence(double[] input1,int inputLength1,int inputLength2,int outputLength){
		  for(int i=0;i<input1.length;i++){
          	if((i==0||i==(input1.length-1)/2||i==(input1.length-1))){
          		if((input1[i]==.01))
          			input1[i]=1;
          		
          	}
          		
          }
		List<SequenceData> sequenceList=new ArrayList<SequenceData>();
		for(int i=1;i<=inputLength1;i++){
			SequenceData sequenceData=new SequenceData();
			sequenceData.input1=i;
			sequenceList.add(sequenceData);
		}


		int startPoint=0;
		for(int j=0;j<2*(Math.log(inputLength1-1)/Math.log(2)-1)+1;j++){
			SequenceData sequenceData=new SequenceData();
			sequenceData.stage=1;

			if(j<Math.log(inputLength1-1)/Math.log(2)-1){
				int increment=(int) Math.pow(2, Math.log(inputLength1-1)/Math.log(2)-2-j);
				startPoint=startPoint+increment;
				int [] xCoord=new int[2*increment+1];                
				for(int x=startPoint;x<=inputLength1/2+(xCoord.length-1)/2;x++)
					xCoord[x-startPoint]=x;

				double []downSampledInput=new double[(int) ((inputLength1-1)/(Math.pow(2,j+1)))+1];
				for(int k=0;k<downSampledInput.length;k++)
					downSampledInput[k]=input1[(int) (k*Math.pow(2,j+1))];
				sequenceData.xCoord= xCoord;
				sequenceData.inputArray=downSampledInput;
				sequenceList.add(sequenceData);
			}else if(j==Math.log(inputLength1-1)/Math.log(2)-1){
				double [] downSampledInput= new double[1];
				int [] xCoord=new int[1];
				xCoord[0]=(inputLength1-1)/2;
				downSampledInput[0]=input1[(inputLength1-1)/2];
				sequenceData.xCoord= xCoord;
				sequenceData.inputArray=downSampledInput;
				sequenceList.add(sequenceData);
			} else if(j>Math.log(inputLength1-1)/Math.log(2)-1){
				int i=inputLength1+(int) (2*(Math.log(inputLength1-1)/Math.log(2)-1)-j);
				double [] prevArray=sequenceList.get(i).inputArray;
				int [] prevxCoord=sequenceList.get(i).xCoord;
				double []downSampledInput=new double[prevArray.length];			

				for(int k=0;k<downSampledInput.length;k++)
					downSampledInput[k]=prevArray[prevArray.length-1-k];
				sequenceData.xCoord=prevxCoord ;
				sequenceData.inputArray=downSampledInput;
				sequenceList.add(sequenceData);
			}

		}
		for(int i=0;i<inputLength1;i++){
			SequenceData sequenceData=new SequenceData();
			sequenceData.inputArray=new double[inputLength1];
			sequenceData.xCoord=new int[inputLength1];
			sequenceData.stage=2;
			for(int k=0;k<inputLength1;k++){
				sequenceData.inputArray[k]=input1[inputLength1-1-k];
				sequenceData.xCoord[k]=k-i;
			}
			sequenceList.add(sequenceData);
		}
		for(int i=1;i<=inputLength2;i++){
			SequenceData sequenceData=new SequenceData();
			sequenceData.input2=i;
			sequenceData.stage=3;
			sequenceData.inputArray=new double[inputLength1];
			sequenceData.xCoord=new int[inputLength1];
			for(int k=0;k<inputLength1;k++){
				sequenceData.inputArray[k]=input1[inputLength1-1-k];
				sequenceData.xCoord[k]=k-inputLength1;
			}
			sequenceList.add(sequenceData);
		}
		for(int i=1;i<=outputLength;i++){
			SequenceData sequenceData=new SequenceData();
			sequenceData.output=i;
			sequenceData.stage=4;
			sequenceData.inputArray=new double[inputLength1];
			sequenceData.xCoord=new int[inputLength1];
			for(int k=0;k<inputLength1;k++){
				sequenceData.inputArray[k]=input1[inputLength1-1-k];
				sequenceData.xCoord[k]=k-inputLength1+i;
			}

			sequenceList.add(sequenceData);
		}
		
		return sequenceList;
	}

	public static class InputSignal{
		String type;
		String causality;
	}

	private void setSpinner(int viewId,int textArrayResId,int position){
		Spinner spinner = (Spinner) findViewById(viewId);
		ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(
				this,textArrayResId , android.R.layout.simple_spinner_item);
		adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		spinner.setAdapter(adapter);
		spinner.setOnItemSelectedListener(new MyOnItemSelectedListener());
		spinner.setSelection(position);	

	}
	private int getSelItem(int viewId){
		Spinner spinner = (Spinner) findViewById(viewId);
		return spinner.getSelectedItemPosition();		
     }
	public class MyOnItemSelectedListener implements OnItemSelectedListener {

		public void onItemSelected(AdapterView<?> parent,
				View view, int pos, long id) {

			switch(parent.getId()){
			case R.id.signalType1:
				signal1Type=parent.getItemAtPosition(pos).toString();
				break;

			case R.id.causalityType1:
				signal1Causality=parent.getItemAtPosition(pos).toString();
				break;

			case R.id.signalType2:
				signal2Type= parent.getItemAtPosition(pos).toString();
				break;

			case R.id.causalityType2:
				signal2Causality=parent.getItemAtPosition(pos).toString();
				break;

			default:
				break;
			}


		}

		public void onNothingSelected(AdapterView parent) {
			// Do nothing.
		}
	}

	public double maxArray(double [] valueArray){
		double max=Double.NEGATIVE_INFINITY;
		for (int i = 0; i < valueArray.length; i++) 
		{
			if(valueArray[i]>max)
			{ 
				max=valueArray[i];

			}
		} 
		return max;
	}


}

