package com.prototype.ajdsp;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Paint.Style;
import android.os.Bundle;
import android.view.Display;
import android.view.View;
import android.view.Window;
import android.view.View.OnClickListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;
import com.prototype.ajdsp.FreqSampleView.CursorPositionListener;
import com.prototype.ajdsp.PartObject.PinSignalTypes;
import com.prototype.ajdsp.PartObject.PinTypes;

public class FreqSamplerDraw extends Activity implements OnClickListener {
	LinearLayout layout;
	RelativeLayout topbar;
	Button addParts;
	Button back;
	Button coefficients;
	Button clear;
	FreqSampleView freqSampleView;
	public int numSamp,filterType,mode,numLinSeg;
	double[]filterCoeffA=new double[128];
	double[]filterCoeffB=new double[128];
	Bundle inputValues;
	TextView coordinates;
	int width = 0;
	int height = 0;
	int pass = 0;
	int xpos = 0;
	int ypos = 0;
	public static int freqSamplerNumber=0;
 
	@Override
	public void onBackPressed() {
	    // do nothing.
	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		//requestWindowFeature(Window.FEATURE_CUSTOM_TITLE);
		setContentView(R.layout.freq_sampler_draw);
		//getWindow().setFeatureInt(Window.FEATURE_CUSTOM_TITLE, R.layout.row);
		layout=(LinearLayout)findViewById(R.id.freqSampler_draw_layout);
		layout.setBackgroundColor(Color.WHITE);
				 
		coordinates=(TextView)findViewById(R.id.freqSampler_coordinates);	
		back=(Button) findViewById(R.id.back_to_menu_freqSamplerDraw);
		back.setOnClickListener(this);
		
	addParts=(Button) findViewById(R.id.freqSamplerDrawAdd);
		addParts.setOnClickListener(this);
		

		back.setOnClickListener(this);
		clear=(Button)findViewById(R.id.clear_freqSamplerDraw);
		clear.setOnClickListener(this);
		
		coefficients=(Button)findViewById(R.id.freqSamplerDrawCoeff);
		coefficients.setOnClickListener(this);
		Bundle extras = getIntent().getExtras(); 

		if(extras !=null)
		{
			inputValues= extras.getBundle("parameterValues");	
			if(inputValues!=null){
				numSamp=inputValues.getInt("samplesId") ;
				filterType=inputValues.getInt("filterTypeId") ;
				mode= inputValues.getInt("modeId");
				numLinSeg=inputValues.getInt("lineSegmentsId");

			}
			else
			{
				Bundle dispValues= extras.getBundle("displayValues");
				numLinSeg=dispValues.getInt("lineSegmentsId");
			}
		}
		freqSampleView=new FreqSampleView(this);
		freqSampleView.setBackgroundColor(Color.WHITE);
		freqSampleView.setNoLineSegments(numLinSeg);
		layout.addView(freqSampleView);
		freqSampleView.setPositionListener(new CursorPositionListener () {
			public void onPositionChange( double newPosition) {
				displayValues();
			}
		});
		displayValues();  

	}
	
	@Override
	 public void onStop() {
		        super.onStop();	
				finish();
				
	 }
	@Override
	public void onClick(View v) { 
		// TODO Auto-generated method stub
		if(v== back){
			FreqSampleView.pointList=new ArrayList<Float>(); 
			Intent i = new Intent(this, FreqSamplerMenu.class);
			i.putExtra("backValues", inputValues);
			startActivity(i);
			FreqSampleView.pointList=new ArrayList<Float>(); 
			FreqSampleView.pointCount=0;

		}
		else if(v==addParts){
			freqSamplerNumber++;

			FreqSampCalculator freqSampCalc=new FreqSampCalculator();
			Bundle freqSampData=new Bundle();
			List<Float> coordList=freqSampleView.getValues();
			float[] coordArray=new float[coordList.size()];
			for(int i=0;i<coordArray.length;i++){
				coordArray[i]=(float)coordList.get(i);
			}
			freqSampData.putFloatArray("freqSampData", coordArray);


			freqSampData.putInt("numSamp",numSamp );
			freqSampData.putInt("filterType",filterType );
			freqSampData.putInt("mode", mode);
			freqSampData.putInt("numLinSeg",numLinSeg );


			Bundle blockData=freqSampCalc.freqSamp(freqSampData);

			freqSampData.putBundle("topPin", blockData);

			if(FreqSamplerMenu.create){
				PinTypes types=new PinTypes();
				types.topPin=true;
				freqSampData.putSerializable("PinTypes",types);
				freqSampData.putString("title", "Freq Samp"+freqSamplerNumber);
				freqSampData.putString("signalType","frequency");
				PinSignalTypes freqSampSigType=new PinSignalTypes();
				freqSampSigType.topPin="frequency";						
				freqSampData.putSerializable("pinSignalType", freqSampSigType);
				PartsController.createBlock(freqSampData);
			}
			else {
				freqSampData.putString("title",  "Freq Samp"+FreqSamplerDraw.freqSamplerNumber);
				freqSampData.putString("signalType","frequency");

				PartsController.updateBlock(freqSampData);
			}

			Intent i = new Intent(this, StartView.class);
			startActivity(i);



		}
		else if(v== clear){
			FreqSampleView.pointList=new ArrayList<Float>(); 
			FreqSampleView.pointCount=0;


		}

		else if(v== coefficients){
			Intent i = new Intent(this, DisplayValues.class);
			Bundle plotValues=new Bundle();
			Bundle freqSampData=new Bundle();
			List<Float> coordList=freqSampleView.getValues();
			float[] coordArray=new float[coordList.size()];
			for(int k=0;k<coordArray.length;k++){
				coordArray[k]=(float)coordList.get(k);
			}
			freqSampData.putFloatArray("freqSampData", coordArray);


			freqSampData.putInt("numSamp",numSamp );
			freqSampData.putInt("filterType",filterType );
			freqSampData.putInt("mode", mode);
			freqSampData.putInt("lineSegmentsId",numLinSeg );
			FreqSampCalculator freqSampCalc=new FreqSampCalculator();
			Bundle blockData=freqSampCalc.freqSamp(freqSampData);
			filterCoeffA=blockData.getDoubleArray("filterCoeffA");
			filterCoeffB=blockData.getDoubleArray("filterCoeffB");
			plotValues.putString("returnClass","FreqSamplerDraw");
			plotValues.putStringArray("plotValues",getValues(filterCoeffA,filterCoeffB));
			//plotValues.putString("returnClass", "FreqSampleDraw");
			plotValues.putInt("lineSegmentsId",numLinSeg );



			i.putExtra("blockValues", plotValues);
			startActivity(i);


		}
	}
	void displayValues() {
		final String str = "Amp: "+String.format("%.2g%n", freqSampleView.getAmplitude());
		coordinates.setText(str);
	}
	private String [] getValues(double[] filterCoeffA,double[] filterCoeffB){
		String [] values= new String[(int)filterCoeffA.length];
		DecimalFormat twoDForm = new DecimalFormat("#.#");
		for(int i=0;i<filterCoeffA.length;i++){
			values[i]="x="+twoDForm.format(filterCoeffA[i])+","+"y="+twoDForm.format(filterCoeffB[i]);

		}
		return values;
	}
	

}
