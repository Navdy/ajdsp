package com.prototype.ajdsp;


import com.prototype.ajdsp.PartObject.PinSignalTypes;
import com.prototype.ajdsp.PartObject.PinTypes;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Color;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.media.ToneGenerator;
import android.os.Bundle;
import android.view.Gravity;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

public class DTMFTone extends Activity implements OnClickListener {
	RelativeLayout layout;
	RelativeLayout topBar;
	EditText dtmfDisplay;
	Button one;
	Button two;
	Button three;
	Button four;
	Button five;
	Button six;
	Button seven;
	Button eight;
	Button nine;
	Button zero;
	Button star;
	Button pound;
	Button back;
	TextView top;
	TextView bottom;
	Boolean create=true;

	ToneGenerator toneGenerator;
	double fs = 8000;
	double [] y=new double[256];
	int rowFrequency[] = {697,770,852,941};
	int colFrequency[] = {1209,1336,1477};
	Button update;
	Bundle inputValues;
	public static int dtmfNumber=0;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		//requestWindowFeature(Window.FEATURE_CUSTOM_TITLE);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
		setContentView(R.layout.dtmf);
		//getWindow().setFeatureInt(Window.FEATURE_CUSTOM_TITLE, R.layout.row);
		layout=(RelativeLayout)findViewById(R.id.dtmf_layout);
		layout.setBackgroundColor(Color.WHITE);
		back=(Button) findViewById(R.id.back_select_parts);
		back.setOnClickListener(this);
		TextView title= (TextView)findViewById(R.id.Title);
		title.setText("DTMF");
		
		dtmfDisplay=(EditText)findViewById(R.id.dtmfText);
		
		dtmfDisplay.setGravity(Gravity.CENTER_HORIZONTAL);
		dtmfDisplay.setTextColor(Color.BLACK);
		top=(TextView)findViewById(R.id.top);
		top.setBackgroundColor(Color.LTGRAY);
		bottom=(TextView)findViewById(R.id.bottom);
		bottom.setBackgroundColor(Color.LTGRAY);


		one=(Button)findViewById(R.id.Button01);
		one.setOnClickListener(this);
		two=(Button)findViewById(R.id.Button02);
		two.setOnClickListener(this);
		three=(Button)findViewById(R.id.Button03);
		three.setOnClickListener(this);
		four=(Button)findViewById(R.id.Button04);
		four.setOnClickListener(this);
		five=(Button)findViewById(R.id.Button05);
		five.setOnClickListener(this);
		six=(Button)findViewById(R.id.Button06);
		six.setOnClickListener(this);
		seven=(Button)findViewById(R.id.Button07);
		seven.setOnClickListener(this);
		eight=(Button)findViewById(R.id.Button08);
		eight.setOnClickListener(this);
		nine=(Button)findViewById(R.id.Button09);
		nine.setOnClickListener(this);
		zero=(Button)findViewById(R.id.Button00);
		zero.setOnClickListener(this);
		star=(Button)findViewById(R.id.ButtonStar);
		star.setOnClickListener(this);
		pound=(Button)findViewById(R.id.ButtonPound);
		pound.setOnClickListener(this);
		
		update=(Button) findViewById(R.id.add_part);
		update.setOnClickListener(this);
		
		toneGenerator= new   ToneGenerator(AudioManager.STREAM_DTMF,ToneGenerator.MAX_VOLUME);
		Bundle extras = getIntent().getExtras(); 
		if(extras !=null)
		{
			inputValues = extras.getBundle("blockValues"); 
			dtmfDisplay.setText(inputValues.getString("numberPressed"));
			update.setText("Update");
			back.setText("Back");
			create=false;


		}
		else {

			update.setText("Add");
		}


	}
	@Override
	 public void onStop() {
		        super.onStop();	
				finish();
				
	 }
	@Override
	public void onBackPressed() {
		/*if(create){
			Intent i = new Intent(this, PartList.class);
			startActivity(i);}
		else {
			Intent i = new Intent(this, StartView.class);
			startActivity(i);
		}*/
	}
	public void PlayDtfmTone(int toneID){


		//this will play tone for 2 seconds.
		toneGenerator.startTone(toneID, 200);



	}
	@Override
	public void onClick(View v) {
		if(v==one){
			PlayDtfmTone(ToneGenerator.TONE_DTMF_1);
			dtmfDisplay.setText("1");
			y=addSignals(discretize(rowFrequency[0],1),discretize(colFrequency[0],1),1);
		}
		else if (v==two){
			PlayDtfmTone(ToneGenerator.TONE_DTMF_2);
			dtmfDisplay.setText("2");
			y=addSignals(discretize(rowFrequency[0],1),discretize(colFrequency[1],1),1);
		}
		else if(v==three){
			PlayDtfmTone(ToneGenerator.TONE_DTMF_3);
			dtmfDisplay.setText("3");
			y=addSignals(discretize(rowFrequency[0],1),discretize(colFrequency[2],1),1);
		}
		else if(v==four){
			PlayDtfmTone(ToneGenerator.TONE_DTMF_4);
			dtmfDisplay.setText("4");
			y=addSignals(discretize(rowFrequency[1],1),discretize(colFrequency[0],1),1);
		}
		else if(v==five){
			PlayDtfmTone(ToneGenerator.TONE_DTMF_5);
			dtmfDisplay.setText("5");
			y=addSignals(discretize(rowFrequency[1],1),discretize(colFrequency[1],1),1);
		}
		else if(v==six){
			PlayDtfmTone(ToneGenerator.TONE_DTMF_6);
			dtmfDisplay.setText("6");
			y=addSignals(discretize(rowFrequency[1],1),discretize(colFrequency[2],1),1);
		}
		else if(v==seven){
			PlayDtfmTone(ToneGenerator.TONE_DTMF_7);
			dtmfDisplay.setText("7");
			y=addSignals(discretize(rowFrequency[2],1),discretize(colFrequency[0],1),1);
		}
		else if(v==eight){
			PlayDtfmTone(ToneGenerator.TONE_DTMF_8);
			dtmfDisplay.setText("8");
			y=addSignals(discretize(rowFrequency[2],1),discretize(colFrequency[1],1),1);
		}
		else if(v==nine){
			PlayDtfmTone(ToneGenerator.TONE_DTMF_9);
			dtmfDisplay.setText("9");
			y=addSignals(discretize(rowFrequency[2],1),discretize(colFrequency[2],1),1);
		}
		else if(v==zero){
			PlayDtfmTone(ToneGenerator.TONE_DTMF_0);
			dtmfDisplay.setText("0");
			y=addSignals(discretize(rowFrequency[3],1),discretize(colFrequency[0],1),1);
		}
		else if(v==star){
			PlayDtfmTone(ToneGenerator.TONE_DTMF_S);
			dtmfDisplay.setText("*");
			y=addSignals(discretize(rowFrequency[3],1),discretize(colFrequency[1],1),1);
		}
		else if(v==pound){
			PlayDtfmTone(ToneGenerator.TONE_DTMF_P);
			dtmfDisplay.setText("#");
			y=addSignals(discretize(rowFrequency[3],1),discretize(colFrequency[2],1),1);
		}
		else if(v==back){
			if(create){
				Intent i = new Intent(this, PartList.class);
				startActivity(i);}
			else {
				Intent i = new Intent(this, StartView.class);
				startActivity(i);
			}

		}else if (v==update){
			Bundle dtmfInput=new Bundle();


			double [] dtmfArray = new double[256];

			Bundle dtmfOutputBundle=new Bundle();
			dtmfOutputBundle.putDoubleArray("signalMag",y);
			dtmfOutputBundle.putDoubleArray("signalPhase",dtmfArray);
			dtmfInput.putBundle("rightPin",dtmfOutputBundle);
			dtmfInput.putString("numberPressed",dtmfDisplay.getText().toString() );
			if(create){
				dtmfNumber++;
				dtmfInput.putString("title","DTMF"+" "+dtmfNumber); 
				PinTypes DTMFTypes=new PinTypes();
				DTMFTypes.rightPin=true;
				dtmfInput.putSerializable("PinTypes",DTMFTypes);
				PinSignalTypes DTMFSigType=new PinSignalTypes();
				DTMFSigType.rightPin="time";
				dtmfInput.putSerializable("pinSignalType", DTMFSigType);
				double [] DTMFInputArray = new double[256];

				/*Bundle DTMFRightInputBundle=new Bundle();
				DTMFRightInputBundle.putDoubleArray("signalMag",DTMFInputArray );
				DTMFRightInputBundle.putDoubleArray("signalPhase",DTMFInputArray);
				dtmfInput.putBundle("rightPin",DTMFRightInputBundle);*/
				PartsController.createBlock(dtmfInput);

			}
			else{

				dtmfInput.putString("title",inputValues.getString("title"));
				PartsController.updateBlock(dtmfInput);
			}
			Intent i = new Intent(this, StartView.class);
			startActivity(i);

		}


	}  
	private double[] addSignals(double[] signal1,double[] signal2, int frames)
	{
		int samples = frames *256;
		double yTemp[] = new double[samples];

		for (int i = 0; i < samples; i++)
			yTemp[i] = signal1[i]+signal2[i];

		return yTemp;
	}



	private double[] discretize(double frequency, int framesNo)
	{
		int sampleLength = framesNo*256;
		double dt = 1/fs;
		double cosine[] = new double[sampleLength];
		if (frequency >=0) //for the case of a space
			for (int i = 0; i < sampleLength; i++)
				cosine[i] = Math.cos(2*Math.PI*frequency*i*dt);

		return cosine;
	}

}
