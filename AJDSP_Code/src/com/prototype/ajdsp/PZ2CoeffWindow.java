package com.prototype.ajdsp;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import com.prototype.ajdsp.PZ2CoeffView.ConjugatePoleZero;
import com.prototype.ajdsp.PZ2CoeffView.ZeroPolePositionListener;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

public class PZ2CoeffWindow extends Activity implements OnClickListener{
	RelativeLayout layout;
	Button back;
	RelativeLayout topLayout;
	Bundle inputValues;
	double[] inputArrayLinear;

	double[] inputArrayDb;
	Button linear;
	Button db;
	Button addZero;
	Button addPole;
	Button reset;
	Button undo;
	Button viewValues;
	
	int maxOrder=64;
	PZPlacementCalc calc=new PZPlacementCalc();
	double [] coeffB=new double[maxOrder+1];
 double [] coeffA=new double[maxOrder+1];
	
	TextView xCoord;
	TextView yCoord;
	PZ2CoeffView _pz2CoeffView;
	private static int zeroCount=0;
	private static int poleCount=0;
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		//requestWindowFeature(Window.FEATURE_CUSTOM_TITLE);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
		setContentView(R.layout.pz2coeff_layout);
		//getWindow().setFeatureInt(Window.FEATURE_CUSTOM_TITLE, R.layout.row);

		layout= (RelativeLayout) findViewById(R.id.pz2coefflayout);
		layout.setBackgroundColor(Color.WHITE);
	
		 
        back=(Button) findViewById(R.id.back_select_parts);
		back.setOnClickListener(this);
		back.setText("Back");
		
		viewValues=(Button) findViewById(R.id.add_part);
		//viewValues.setOnClickListener(this);
		viewValues.setVisibility(View.GONE);
		addZero=(Button)findViewById(R.id.pz2Coeff_addZero);
		addZero.setOnClickListener(this);
		addPole=(Button)findViewById(R.id.pz2Coeff_addPole);
		addPole.setOnClickListener(this);
		reset=(Button)findViewById(R.id.pz2Coeff_reset);
		reset.setOnClickListener(this);
		
		xCoord=(TextView) findViewById(R.id.pz2Coeff_xcoordinates);
		yCoord=(TextView) findViewById(R.id.pz2Coeff_ycoordinates);


		_pz2CoeffView=(PZ2CoeffView)findViewById(R.id.pz2Coeff_view_window); 
	
		Bundle extras = getIntent().getExtras(); 
		if(extras !=null)	{	
			inputValues = extras.getBundle("blockValues");
			_pz2CoeffView.zeroNo=inputValues.getInt("zeroNo");
			_pz2CoeffView.poleNo=inputValues.getInt("poleNo");
			_pz2CoeffView.invalidate();
			
		}
		
		if(inputValues.getSerializable("poleZeroArrays")!=null){
			  List<ConjugatePoleZero> zeroList=new ArrayList<ConjugatePoleZero>(); 
			 List<ConjugatePoleZero> poleList=new ArrayList<ConjugatePoleZero>();
			PoleZeroArrays poleZeroArrays=(PoleZeroArrays) inputValues.getSerializable("poleZeroArrays");
			int width=(PZ2CoeffView.width-PZ2CoeffView.width/90)/2;
			int height=PZ2CoeffView.height;
			for(int i=0;i<2*_pz2CoeffView.zeroNo;i=i+2){
				//if((poleZeroArrays.zeroArrayX[i]!=0)||(poleZeroArrays.zeroArrayY[i]!=0)){
				ConjugatePoleZero zero=new ConjugatePoleZero();
				PoleZeroObject bottomZeroObject=new PoleZeroObject(PZ2CoeffView.zeroBitmap);
				PoleZeroObject topZeroObject=new PoleZeroObject(PZ2CoeffView.zeroBitmap);
				
				topZeroObject.getPoleZeroCoordinates().setX((int) (height*poleZeroArrays.zeroArrayX[i]/3+width));
				topZeroObject.getPoleZeroCoordinates().setY((int) (-height*poleZeroArrays.zeroArrayY[i]/3+height/2));
				
				bottomZeroObject.getPoleZeroCoordinates().setX((int) (height*poleZeroArrays.zeroArrayX[i+1]/3+width));
			    bottomZeroObject.getPoleZeroCoordinates().setY((int) (-height*poleZeroArrays.zeroArrayY[i+1]/3+height/2));
				
			    _pz2CoeffView.zeroArrayX[i]=  poleZeroArrays.zeroArrayX[i];
			    _pz2CoeffView.zeroArrayY[i]=  poleZeroArrays.zeroArrayY[i];
			    _pz2CoeffView.zeroArrayX[i+1]=  poleZeroArrays.zeroArrayX[i+1];
			    _pz2CoeffView.zeroArrayY[i+1]=  poleZeroArrays.zeroArrayY[i+1];
			    zero.topObject=topZeroObject;
			    zero.bottomObject=bottomZeroObject;
			    zeroList.add(zero);
			}
				//}
				//if((poleZeroArrays.poleArrayX[i]!=0)||(poleZeroArrays.poleArrayY[i]!=0)){
			for(int i=0;i<2*_pz2CoeffView.poleNo;i=i+2){   
			ConjugatePoleZero pole=new ConjugatePoleZero();
			    PoleZeroObject topPoleObject=new PoleZeroObject(PZ2CoeffView.poleBitmap);
			    PoleZeroObject bottomPoleObject=new PoleZeroObject(PZ2CoeffView.poleBitmap);
				
			    topPoleObject.getPoleZeroCoordinates().setX((int) (height*poleZeroArrays.poleArrayX[i]/3+width));
				topPoleObject.getPoleZeroCoordinates().setY((int) (-height*poleZeroArrays.poleArrayY[i]/3+height/2));
				
				bottomPoleObject.getPoleZeroCoordinates().setX((int) (height*poleZeroArrays.poleArrayX[i+1]/3+width));
			    bottomPoleObject.getPoleZeroCoordinates().setY((int) (-height*poleZeroArrays.poleArrayY[i+1]/3+height/2));
			    _pz2CoeffView.poleArrayX[i]=  poleZeroArrays.poleArrayX[i];
			    _pz2CoeffView.poleArrayY[i]=  poleZeroArrays.poleArrayY[i];
			    _pz2CoeffView.poleArrayX[i+1]=  poleZeroArrays.poleArrayX[i+1];
			    _pz2CoeffView.poleArrayY[i+1]=  poleZeroArrays.poleArrayY[i+1];
			    pole.topObject=topPoleObject;
			    pole.bottomObject=bottomPoleObject;
			    poleList.add(pole);
				}
			
			
			
			_pz2CoeffView.poleList=poleList;
			_pz2CoeffView.zeroList=zeroList;
		}
		
		_pz2CoeffView.setPositionListener(new ZeroPolePositionListener(){
			public void onZeropoleListChange( double[] zeroX,double [] zeroY,double[] poleX,double[]poleY) {
				
				String strX = String.format("x: %3.2f",
						PZ2CoeffView.xCoord);
				xCoord.setText(strX);
				String strY = String.format("y: %3.2f",
						PZ2CoeffView.yCoord);
				yCoord.setText(strY);
			}
		});

	}
	@Override
	 public void onStop() {
		 super.onStop();	
				finish();
				
	 }
	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		if(v==back){
			Intent i = new Intent(this, StartView.class);
			Bundle topPinData=new Bundle();
			topPinData.putDoubleArray("filterCoeffB", calc.CalcNumCoeff(_pz2CoeffView.zeroArrayX, _pz2CoeffView.zeroArrayY, _pz2CoeffView.zeroNo));
			topPinData.putDoubleArray("filterCoeffA", calc.CalcDenCoeff(_pz2CoeffView.poleArrayX, _pz2CoeffView.poleArrayY,_pz2CoeffView.poleNo));
			PoleZeroArrays poleZeroArrays=new PoleZeroArrays();
			poleZeroArrays.zeroArrayX=_pz2CoeffView.zeroArrayX;
			poleZeroArrays.zeroArrayY=_pz2CoeffView.zeroArrayY;
			poleZeroArrays.poleArrayX=_pz2CoeffView.poleArrayX;
			poleZeroArrays.poleArrayY=_pz2CoeffView.poleArrayY;
			inputValues.putBundle("topPin", topPinData);
			inputValues.putInt("zeroNo", _pz2CoeffView.zeroNo);
			inputValues.putInt("poleNo", _pz2CoeffView.poleNo);
			inputValues.putSerializable("poleZeroArrays",  poleZeroArrays);

			PartsController.updateBlock(inputValues);
			//i.putExtra("blockValues", inputValues);
			
			startActivity(i);
		} else if(v==addZero){
			PZ2CoeffThread.stopDraw=true;
			zeroCount++;
			if(zeroCount<10){
			
			_pz2CoeffView.addZero();
			
			
			}else{
				Toast.makeText(getApplicationContext(), "Limit of zeros excedded", Toast.LENGTH_SHORT).show();
			}
			PZ2CoeffThread.stopDraw=false;
			
		} else if(v==addPole){
			PZ2CoeffThread.stopDraw=true;
			poleCount++;
			if(poleCount<10){
			
			_pz2CoeffView.addPole();
			
			
			}else{
				Toast.makeText(getApplicationContext(), "Limit of Poles excedded", Toast.LENGTH_SHORT).show();
			}
			PZ2CoeffThread.stopDraw=false;
		} else if(v==reset){
			_pz2CoeffView.poleList.clear();
			_pz2CoeffView.zeroList.clear();
			_pz2CoeffView.zeroNo=0;
			_pz2CoeffView.poleNo=0;
			zeroCount=0;
			poleCount=0;
			_pz2CoeffView.poleArrayX= new double[20];
			_pz2CoeffView.poleArrayY= new double[20];
			_pz2CoeffView.zeroArrayX= new double[20];
			_pz2CoeffView.zeroArrayY= new double[20];
			String strX = String.format("x: %3.2f",
					0.0);
			xCoord.setText(strX);
			String strY = String.format("y: %3.2f",
					0.0);
			yCoord.setText(strY);
		
		} 
	}
	public static class PoleZeroArrays implements Serializable{
		/**
		 * 
		 */
		private static final long serialVersionUID = -2153924975928463922L;
		double [] zeroArrayX=new double[20];
		double [] zeroArrayY=new double[20];
		double [] poleArrayX=new double[20];
		double [] poleArrayY=new double[20];
	}

}
