package com.prototype.ajdsp;

import java.io.Serializable;

import android.graphics.Canvas;
import android.view.SurfaceHolder;

public class StartViewThread extends Thread {
	/**
	 * 
	 */
	
	private SurfaceHolder _surfaceHolder;
    private PartView _partView;
    public boolean _run = false;
    private Object mPauseLock = new Object();  
    private boolean mPaused;
    public StartViewThread(SurfaceHolder surfaceHolder, PartView panel) {
        _surfaceHolder = surfaceHolder;
        _partView = panel;
    }

    public void setRunning(boolean run) {
        _run = run;
    }

    public SurfaceHolder getSurfaceHolder() {
        return _surfaceHolder;
    }

    @Override
    public void run() {
        Canvas c;
        while (_run) {
            c = null;
            try {
                c = _surfaceHolder.lockCanvas(null);
                synchronized (_surfaceHolder) {
                	if(!DeleteDialog.stopDraw)
                  _partView.onDraw(c);
                }
               
            } finally {
                // do this in a finally so that if an exception is thrown
                // during the above, we don't leave the Surface in an
                // inconsistent state
                if (c != null) {
                    _surfaceHolder.unlockCanvasAndPost(c);
                }
             }
          
            
        }
        
    }
   public void onPause() {
        synchronized (mPauseLock) {
            mPaused = true;
        }
    }
    public void onResume() {
        synchronized (mPauseLock) {
            mPaused = false;
            mPauseLock.notifyAll();
        }
    }
    

}


