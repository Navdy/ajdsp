	package com.prototype.ajdsp;
import android.os.Bundle;


public class FIRDesignCalc {
	int MaxOrd=64; // Maximum allowable order
	double[] aCoeff = new double[65];
	double[] bCoeff = new double[65];
	double W1,W2,beta;
	int winType;
	int Order, nbands;
	double[] F = new double[6];
	double[] M = new double[6];
	int LP=0,HP=1,BP=2,BS=3,ftype;

	public Bundle getFilterCoeff(Bundle inputBundle){
     Order=inputBundle.getInt("order");
     beta=inputBundle.getDouble("beta");
     W1=inputBundle.getDouble("W1");
     W2=inputBundle.getDouble("W2");
     ftype=inputBundle.getInt("filterType");
     winType=(int)inputBundle.getLong("windowType");
     for (int i=0;i<6;i++)
	  {F[i]=0;M[i]=0;}
	  if (ftype==0)
	  {F[0]=W1/2;F[1]=W2/2;F[2]=W2/2;F[3]=0.5;nbands=2;
	   M[0]=1;M[1]=1;M[2]=0;M[3]=0;}
	  else if (ftype==1)
	  {F[0]=0;F[1]=W1/2;F[2]=W1/2;F[3]=W2/2;nbands=2;
	   M[0]=0;M[1]=0;M[2]=1;M[3]=1;}
	  else if (ftype==2)
	  {F[0]=0;F[1]=W1/2;F[2]=W1/2;F[3]=W2/2;F[4]=W2/2;F[5]=0.5;nbands=3;
	   M[0]=0;M[1]=0;M[2]=1;M[3]=1;M[4]=0;M[5]=0;}
	  else if (ftype==3)
	  {F[0]=0;F[1]=W1/2;F[2]=W1/2;F[3]=W2/2;F[4]=W2/2;F[5]=0.5;nbands=3;
	   M[0]=1;M[1]=1;M[2]=0;M[3]=0;M[4]=1;M[5]=1;}

	  Design();
	  aCoeff[0]=1;
	  Bundle filterCoeff=new Bundle();
	  filterCoeff.putDoubleArray("filterCoeffB", bCoeff);

      filterCoeff.putDoubleArray("filterCoeffA", aCoeff);
      return filterCoeff;
     
	}
	 private void Design()
	  {
		  double[] b = new double[65];
		  double[] h = new double[65];

		  double m,b1,k;
		    int N=Order;


		  //for HP and BS must be even, increase by 1 if not
		  //if ((N%2)!=0 && W2==1)
		  //	  N=N+1;
		  N=N+1; // number of coefficients
		  double[] wind=setWinType(winType,N,beta); //get selected window of length Order+1

		  aCoeff[0]=1;
		  for (int i=1;i<=64;i++)
			 aCoeff[i]=0.0;

		 int L=(N-1)/2;

		 for (int i=0;i<=L;i++)
			 b[i]=0.0;

		 for (int s=0;s<(2*nbands-1);s+=2)
		 {
			 m=(M[s+1]-M[s])/(F[s+1]-F[s]);    //finding the slope
			 b1=M[s]-m*F[s];
			 if ((N%2)!=0)
			 {
				 k=1;
				 b[0]=b[0]+(b1*(F[s+1]-F[s])+ m/2*(F[s+1]*F[s+1]-F[s]*F[s]));
				 for (int i=1;i<=L;i++)
				 {
					 //b[i]=b[i]+(m/(4*Math.PI*Math.PI)*(Math.cos(2*Math.PI*k*F[s+1])-Math.cos(2*Math.PI*k*F[s]))/(k*k));
					 //b[i] = b[i] + (F[s+1]*(m*F[s+1]+b1)*sinc(2*k*F[s+1])-F[s]*(m*F[s]+b1)*sinc(2*k*F[s]));

					 b[i] = b[i] + F[s+1]*(b1)*sinc(2*k*F[s+1])-F[s]*(b1)*sinc(2*k*F[s]);
					 k=k+1;
				 }
			 }
			 else
			 {
				 k=0.5;
				 for (int i=0;i<=L;i++)
				 {
					 //b[i]=b[i]+(m/(4*Math.PI*Math.PI)*(Math.cos(2*Math.PI*k*F[s+1])-Math.cos(2*Math.PI*k*F[s]))/(k*k));
					 //b[i] = b[i] + (F[s+1]*(m*F[s+1]+b1)*sinc(2*k*F[s+1])-F[s]*(m*F[s]+b1)*sinc(2*k*F[s]));
					 b[i] = b[i] + F[s+1]*(b1)*sinc(2*k*F[s+1])-F[s]*(b1)*sinc(2*k*F[s]);
					 k=k+1;
				 }
			 }
		 }

		if ((N%2)!=0)
		{
			h[L]=2*b[0];
			for (int i=1;i<=L;i++)
			{h[L+i]=2*b[i];h[L-i]=h[L+i];}
		}
		else
		{
			for (int i=0;i<=L;i++)
			{h[L+i+1]=2*b[i];h[L-i]=h[L+i+1];}
		}

		double sumb=0.0;
		double sumb2=0.0;
		for (int i=0;i<N;i++)
		{
			bCoeff[i]=h[i]*wind[i];
			sumb=sumb+bCoeff[i];
		}


		// Scale
		if (ftype==LP || ftype==BS) //LP & BS
		{
			for (int i=0;i<N;i++)
				bCoeff[i]=bCoeff[i]/sumb;

		}
		else
		{
			double f0=0;
			if (ftype==HP) //HP
				f0=1;
			else        //BP
				f0=(F[2]+F[3]);
			double sumb2r=0;
			double sumb2i=0;
			for (int i=0;i<N;i++)
			{
				sumb2r=Math.cos(-Math.PI*i*f0)*bCoeff[i]+sumb2r;
				sumb2i=Math.sin(-Math.PI*i*f0)*bCoeff[i]+sumb2i;
			}
			double scale=Math.sqrt(Math.pow(sumb2r,2)+Math.pow(sumb2i,2));
			for (int i=0;i<N;i++)
				bCoeff[i]=bCoeff[i]/scale;
		}
	}
	 private double sinc(double x)
	  {
		  if (x!=0)
			  return Math.sin(Math.PI*x)/(Math.PI*x);
		  else
			  return 1;
	  }
	 private double[] setWinType(long wintype,int winlength, double winbeta)
     {
	  // Calculates selected window typw
      double pi = 3.1415927;
	   double win[] = new double[winlength+1];

      if (winlength>255) winlength=255;

      if (wintype == 0)
        {
         for (int i = 0;i<winlength;i++)
            win[i]=1;
        }
      //Updated by Jayaraman
      else if (wintype ==1)
        {
         //If window length is even
         if(winlength%2==0)
         {
			  int L = winlength;
			  int N = L-1;

			  for (int i = 0;i<=L/2-1;i++)
				 win[i]=2*i/(double)(N);
			  for (int i = L/2;i<=N;i++)
				 win[i]=2-2*i/(double)(N);
		  }
		  else//If window length is odd
		  {
			  int L = winlength;
			  int N = L-1;

			  for (int i = 0;i<=N/2;i++)
				 win[i]=2*i/(double)(N);
			  for (int i = N/2;i<=N;i++)
			  	 win[i]=2-2*i/(double)(N);
		  }
        }
      //Updated by Jayaraman
      else if (wintype ==2)
        {
         int L=winlength;
         int N=L-1;
         for (int i = 0;i<=N;i++)
            win[i]=0.54-0.46*Math.cos(2*pi*i/(N));
        }
      //Updated by Jayaraman
      else if (wintype ==3)
        {
         int L=winlength;
         int N=L-1;
         for (int i = 0;i<=N;i++)
            win[i]=0.5-0.5*Math.cos(2*pi*i/N);
        }
      //Updated by Jayaraman
      else if (wintype ==4)
        {
         int L=winlength;
         int N=L-1;
         for (int i = 0;i<=N;i++)
            win[i]=0.42-0.5*Math.cos(2*pi*i/N)+0.08*Math.cos(4*pi*i/N);
        }
     //Updated by Jayaraman
     else if (wintype ==5)
        {
         int N=winlength-1;
         for (int i = 0;i<=N;i++)
           win[i]=bessel(winbeta*Math.sqrt(1-Math.pow(((2*i-N)/(double) N),2)))/bessel(winbeta);
        }
     return win;
     }
	 private double bessel( double x)
     {
      double ax,ans,y3;

      ax=Math.abs(x);
      if (ax <3.75)
        {
         y3=x/3.75;
         y3=y3*y3;
         ans= 1.0+y3*(3.5156229+y3*(3.0899424+y3*(1.2067492+y3*(0.2659732+y3*(0.360768e-1+y3*0.45813e-2)))));
        }
      else
        {
         y3=3.75/ax;
         ans=(Math.exp(ax)/Math.sqrt(ax))*(0.39894228+y3*(0.1328592e-1+y3*(0.225319e-2+y3*(-0.157565e-2+y3*(0.916281e-2+y3*(-0.2057706e-1+y3*(0.2635537e-1+y3*(-0.1647633e-1+y3*0.392377e-2))))))));
        }
      return ans;
     }


}
