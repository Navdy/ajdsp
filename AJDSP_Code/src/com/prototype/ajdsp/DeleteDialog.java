package com.prototype.ajdsp;

import java.util.ArrayList;

import android.app.Dialog;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.TextView;

import com.prototype.ajdsp.PartView.ConnectionData;

public class DeleteDialog extends Dialog implements  OnClickListener{
	Button deleteButton;
	Button cancelButton;
	TextView pinDialogText;
	PartObject graphic;
	Bitmap pinBitmap;
	Bundle _inputBundle;
	String partCheckText;
	int partNumber;
	int connectionNumber;
	String type;
	public static boolean stopDraw=false;

	public DeleteDialog(Context context) {
		super(context);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.part_delete_dialog);

		deleteButton = (Button) findViewById(R.id.partDelete);
		deleteButton.setBackgroundColor(Color.BLUE);
		deleteButton.setOnClickListener(this);
		deleteButton.setTextColor(Color.WHITE);
	}

	public void setConnectionNumber(Integer connectionNo){
		connectionNumber=connectionNo;
	}
	public void setButtonText(String text){
		deleteButton.setText(text);
	}

	public void setPartNumber(Integer partNo){
		partNumber=partNo;	

	}
	public void setType(String deleteType){
		type=deleteType;

	}
	public void setInputBundle(Bundle inputBundle){
		_inputBundle=inputBundle;
	}
	@Override
	public void onBackPressed() {
	    dismiss();
	}

	@Override
	public void onClick(View v) {
		if (v == deleteButton){
			
			if(type.equalsIgnoreCase("Part")){
				int a=0;
				String partName=  PartsController.partList.get(partNumber).getPartName();

				ArrayList<ConnectionData> removedConn=new ArrayList<ConnectionData>();
				for(ConnectionData connection:PartsController.connectionList){
					if(connection.part.getPartName().equalsIgnoreCase(partName)){
						removedConn.add(connection);
						if(((a%2)==0)){
							ConnectionData beforeConnnection=PartsController.connectionList.get(a+1);
							beforeConnnection.part.setIsPinConnected(beforeConnnection.pinType,false);
							removedConn.add(beforeConnnection); 
						}
						else{
							ConnectionData afterConnnection=PartsController.connectionList.get(a-1);
							afterConnnection.part.setIsPinConnected(afterConnnection.pinType,false);
							removedConn.add(afterConnnection); 
							
						}
					}
					a++;
				}
				PartsController.connectionList.removeAll(removedConn);
				PartsController.partList.get(partNumber)._isdeleted=true;
				PartsController.partList.remove(partNumber);
			
			}
			else if(type.equalsIgnoreCase("Connection")){
				ConnectionData oneCon=PartsController.connectionList.get(connectionNumber);
				ConnectionData twoCon=PartsController.connectionList.get(connectionNumber+1);
				Bundle bundle =new Bundle();
				double [] signalMag= new double[256];
				bundle.putDoubleArray("signalMag", signalMag);
				bundle.putDoubleArray("signalPhase", signalMag);
				bundle.putDouble("pulseWidth", 0);
				if(seeIfOutputPin(oneCon.pinType)){
					PartObject oneObj=oneCon.part;	
					oneObj.removeChild(twoCon.part, twoCon.pinType, oneCon.pinType);
					twoCon.part.setPinData(twoCon.pinType, bundle);
					PartsController.updateBlock(twoCon.part.getBlockData());				
				
					
				} else{
					PartObject twoObj=twoCon.part;
					twoObj.removeChild(oneCon.part, oneCon.pinType, twoCon.pinType);
					oneCon.part.setPinData(oneCon.pinType, bundle);
					PartsController.updateBlock(oneCon.part.getBlockData());
					
				}
				deleteConnection();
				deleteConnection();
				
			}
			
			
			dismiss();
			stopDraw=false;
		}
		
		
	}
	public boolean seeIfOutputPin(String pinSelected) {
		// TODO Auto-generated method stub

		if (pinSelected.equalsIgnoreCase("topPin")
				|| pinSelected.equalsIgnoreCase("rightPin")
				|| pinSelected.equalsIgnoreCase("rightTopPin")
				|| pinSelected.equalsIgnoreCase("rightBottomPin")) {

			return true;
		}

		else
			return false;
	}
	public void deleteConnection(){
		ConnectionData deletedConnection=PartsController.connectionList.get(connectionNumber);
		deletedConnection.part.setIsPinConnected(deletedConnection.pinType,false);
		if((deletedConnection.part._partName.contains("Plot"))&(!deletedConnection.part._partName.contains("PZ Plot")))
			deletedConnection.part.getBlockData().putBundle("leftPin", new Bundle());
		   PartsController.connectionList.remove(connectionNumber);
	
		
	}
}
