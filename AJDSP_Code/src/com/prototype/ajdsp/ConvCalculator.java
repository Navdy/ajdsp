package com.prototype.ajdsp;

import android.os.Bundle;

public class ConvCalculator {


	public Bundle contConv(Bundle inputBundle){
		double[] y = new double[256];
		int L ;
		for (int i=0;i<256;i++)
			y[i]=0;
		Bundle input1=inputBundle.getBundle("leftTopPin");

		double [] input1Array=input1.getDoubleArray("signalMag");
		int l1= (int)input1.getDouble("pulseWidth");
		Bundle input2=inputBundle.getBundle("leftBottomPin");
		double [] input2Array=input2.getDoubleArray("signalMag");
		int l2= (int)input2.getDouble("pulseWidth");
		L=l1+l2-1;
		if(L>256)
			L=256;

		for (int i=0;i<L;i++)
		{
			y[i]=0;

			for(int k=0;k<L;k++)
			{
				if(i-k>=0)
					y[i]+=input1Array[k]*input2Array[i-k];
			}
		}
		Bundle output=new Bundle();
		output.putDoubleArray("signalMag",y);
		double []phaseArray=new double[256];
		output.putDoubleArray("signalPhase",phaseArray);
		output.putDouble("pulseWidth", (double)L);
		inputBundle.putBundle("rightPin", output);
		return inputBundle;
	}

}
