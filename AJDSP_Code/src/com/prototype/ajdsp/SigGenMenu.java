package com.prototype.ajdsp;


import java.util.ArrayList;
import java.util.List;

import com.prototype.ajdsp.PartObject.PinSignalTypes;
import com.prototype.ajdsp.PartObject.PinTypes;



import android.app.Activity;
import android.app.ListActivity;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.text.InputType;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.AdapterView.OnItemSelectedListener;

public class SigGenMenu extends Activity implements OnClickListener{
	RelativeLayout layout;
	RelativeLayout topbar;
	Button addParts;
	EditText gain;
	EditText pulseWidth;
	EditText period;
	EditText expBase;
	EditText freq;
	EditText mean;
	TextView freqText;
	TextView expbaseText;
	TextView periodText;
	TextView periodicText;
	TextView gainText;
	TextView meanText;
	TextView timeShiftText;
	TextView pulseWidthText;
	Spinner sigTypeSpinner;
	
	EditText timeshift;
    Spinner periodicTypeSpinner;
    Button partList;
    public static int sigGenNumber=0;
    Boolean create=true;
    public boolean takingFromBundle=false;
  
    
    Bundle inputValues;
    
	
	@Override
	  public void onCreate(Bundle savedInstanceState) {
      super.onCreate(savedInstanceState);
      //requestWindowFeature(Window.FEATURE_CUSTOM_TITLE);
      requestWindowFeature(Window.FEATURE_NO_TITLE);
      this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
     setContentView(R.layout.siggen_menu);
     //getWindow().setFeatureInt(Window.FEATURE_CUSTOM_TITLE, R.layout.row);
     layout=(RelativeLayout)findViewById(R.id.signalMenu);
     layout.setBackgroundColor(Color.WHITE);
    
     
     sigTypeSpinner = (Spinner) findViewById(R.id.signalType);
     ArrayAdapter<CharSequence> sigTypeAdapter = ArrayAdapter.createFromResource(
             this, R.array.sig_type_array, android.R.layout.simple_spinner_item);
     sigTypeAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
    // sigTypeSpinner.setBackgroundColor(Color.WHITE);
     sigTypeSpinner.setAdapter(sigTypeAdapter);
     sigTypeSpinner.setOnItemSelectedListener(new MyOnItemSelectedListener());
    
     periodicTypeSpinner = (Spinner) findViewById(R.id.periodic);
     ArrayAdapter<CharSequence> periodicTypeAdapter = ArrayAdapter.createFromResource(
             this, R.array.period_type, android.R.layout.simple_spinner_item);
     periodicTypeAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
    // periodicTypeSpinner.setBackgroundColor(Color.WHITE);
     periodicTypeSpinner.setAdapter(periodicTypeAdapter);
     //periodicTypeSpinner.setOnItemSelectedListener(new PeriodicTypeOnItemSelectedListener());
     expBase=(EditText)findViewById(R.id.expbase);
     expbaseText=(TextView)findViewById(R.id.expbaseText);
     
     meanText=(TextView)findViewById(R.id.meanText);
     mean=(EditText)findViewById(R.id.mean);
     
     freq=(EditText)findViewById(R.id.freq);
     freqText=(TextView)findViewById(R.id.freqText);
     timeShiftText=(TextView)findViewById(R.id.timeShiftText);
     gainText=(TextView)findViewById(R.id.gainText);
     pulseWidthText=(TextView)findViewById(R.id.pulseWidthText);
     
     periodText=(TextView)findViewById(R.id.periodText);
     periodicText=(TextView)findViewById(R.id.periodicText);
    
     gain=(EditText) findViewById(R.id.gain);
     pulseWidth=(EditText) findViewById(R.id.pulseWidth);
	 period=(EditText) findViewById(R.id.period);
	 timeshift=(EditText) findViewById(R.id.timeshift);
	 addParts=(Button)findViewById(R.id.add_part);
     addParts.setOnClickListener(this);  
     partList=(Button)findViewById(R.id.back_select_parts);
     partList.setOnClickListener(this);  
  
     
		TextView title= (TextView)findViewById(R.id.Title);
		title.setText("Sig Gen");
   
	
     Bundle extras = getIntent().getExtras(); 
     if(extras !=null)
	 {
	 inputValues = extras.getBundle("blockValues");
	 sigTypeSpinner.setSelection((int) inputValues.getLong("sigType"));
	 periodicTypeSpinner.setSelection((int) inputValues.getLong("periodicType"));
	 Double gainValue=inputValues.getDouble("gain");
	 Integer pulseWidthValue= inputValues.getInt("pulseWidth");
	 Integer periodValue= inputValues.getInt("period");
	 Integer timeshiftValue=inputValues.getInt("timeShift");
	 Double meanValue=inputValues.getDouble("mean");
	 Double freqValue= inputValues.getDouble("freq");
	 Double expBaseValue=inputValues.getDouble("expbase");
	// takingFromBundle=true;
	 gain.setText(gainValue.toString());
	 pulseWidth.setText(pulseWidthValue.toString());
	 period.setText(periodValue.toString());
	 timeshift.setText(timeshiftValue.toString());
	 mean.setText(meanValue.toString());
	 freq.setText(freqValue.toString());
	 expBase.setText(expBaseValue.toString());
	 create=false;
	 addParts.setText("Update");
	 partList.setText("Back");


	 
	 }else{
		
		 reset();
		 
	 }
	
}
@Override
	public void onBackPressed() {
	/*if(create){
		Intent i = new Intent(this, PartList.class);
		startActivity(i);}
	else {
		Intent i = new Intent(this, StartView.class);
		startActivity(i);
	}*/
	}
	 public class MyOnItemSelectedListener implements OnItemSelectedListener {

	        public void onItemSelected(AdapterView<?> parent,
	            View view, int pos, long id) {
	        	expBase.setVisibility(View.GONE);	        	
	        	expbaseText.setVisibility(View.GONE);
	        	freq.setVisibility(View.GONE);
	        	freqText.setVisibility(View.GONE);
	        	period.setVisibility(View.VISIBLE);
	        	periodText.setVisibility(View.VISIBLE);
	        	meanText.setVisibility(View.GONE);
	        	mean.setVisibility(View.GONE);
	          
	        	gainText.setText("Gain");
	        	 timeshift.setVisibility(View.VISIBLE);
        		 timeShiftText.setVisibility(View.VISIBLE);
        		 pulseWidth.setVisibility(View.VISIBLE);
        		 pulseWidthText.setVisibility(View.VISIBLE);
        		 periodicTypeSpinner.setVisibility(View.VISIBLE);
		        periodicText.setVisibility(View.VISIBLE);
        		 gain.setInputType(InputType.TYPE_CLASS_NUMBER|InputType.TYPE_NUMBER_FLAG_DECIMAL|InputType.TYPE_NUMBER_FLAG_SIGNED);
	        	
	        	
	            switch((int)id){
	            
	        	case 3:
	        		expBase.setVisibility(View.VISIBLE);
		        	expbaseText.setVisibility(View.VISIBLE);
		        	break;
	        	
	        	case 4:
	        		freq.setVisibility(View.VISIBLE);
		        	freqText.setVisibility(View.VISIBLE);
		        	period.setVisibility(View.GONE);
		        	periodText.setVisibility(View.GONE);
		        	periodicTypeSpinner.setVisibility(View.GONE);
		        	periodicText.setVisibility(View.GONE);
		        	break;
	        	
	        	case 5:
	        		freq.setVisibility(View.VISIBLE);
		        	freqText.setVisibility(View.VISIBLE);
		        	break;
	        	
	        	case 6:
	        		gainText.setText("Variance");
	        		gain.setInputType(InputType.TYPE_CLASS_NUMBER|InputType.TYPE_NUMBER_FLAG_DECIMAL);
	        		gain.setText("1.0");
	        		meanText.setVisibility(View.VISIBLE);
		        	mean.setVisibility(View.VISIBLE);
		        	period.setVisibility(View.GONE);
		        	periodText.setVisibility(View.GONE);
		        	periodicTypeSpinner.setVisibility(View.GONE);
		        	periodicText.setVisibility(View.GONE);
		        	break;
	        	
	        	case 7:
	        		gainText.setText("Variance");
	        		gain.setInputType(InputType.TYPE_CLASS_NUMBER|InputType.TYPE_NUMBER_FLAG_DECIMAL);
	        		gain.setText("1.0");
	        		meanText.setVisibility(View.VISIBLE);
		        	mean.setVisibility(View.VISIBLE);
		        	period.setVisibility(View.GONE);
		        	periodText.setVisibility(View.GONE);
		        	periodicTypeSpinner.setVisibility(View.GONE);
		        	periodicText.setVisibility(View.GONE);
		        	break;
	        	
	        	case 8:
	        		gainText.setText("Variance");
	        		gain.setInputType(InputType.TYPE_CLASS_NUMBER|InputType.TYPE_NUMBER_FLAG_DECIMAL);
	        		gain.setText("1.0");
	        		meanText.setVisibility(View.VISIBLE);
		        	mean.setVisibility(View.VISIBLE);
		        	period.setVisibility(View.GONE);
		        	periodText.setVisibility(View.GONE);
		        	periodicTypeSpinner.setVisibility(View.GONE);
		        	periodicText.setVisibility(View.GONE);
		        	break;
	        	case 9:
	        	case 10:
	        	case 11:
	        		period.setVisibility(View.GONE);
		        	periodText.setVisibility(View.GONE);
		        	periodicTypeSpinner.setVisibility(View.GONE);
		        	periodicText.setVisibility(View.GONE);
	        		
	        		pulseWidth.setVisibility(View.GONE);
	        		pulseWidthText.setVisibility(View.GONE);
	        		
	        		 timeshift.setVisibility(View.GONE);
	        		 timeShiftText.setVisibility(View.GONE);
	        		        	
	 	        	
		        	break;
		       
	        	default:
	           	    break;
	           }
	      }

	        public void onNothingSelected(AdapterView<?> parent) {
	          // Do nothing.
	        }
	    }
	/* public class PeriodicTypeOnItemSelectedListener implements OnItemSelectedListener {

	        public void onItemSelected(AdapterView<?> parent,
	            View view, int pos, long id) {
	        	
	        	period.setVisibility(View.GONE);
	        	periodText.setVisibility(View.GONE);
	        	
	        	
	            switch((int)id){
	            
	        	case 0:
	        		period.setVisibility(View.VISIBLE);
		        	periodText.setVisibility(View.VISIBLE);
	        		
		        	break;
	        	
	        	
		       
	        	default:
	           	    break;
	           }
	      }

	        public void onNothingSelected(AdapterView <?>parent) {
	          // Do nothing.
	        }
	    }*/
	   
	 @Override
	  	public void onClick(View v) {
	  		/** When OK Button is clicked, dismiss the dialog */
	  		if (v == addParts){
	  			Bundle sigGenInput=new Bundle();
	  			
	  			
	  			String gainString=gain.getText().toString();
	  	        String pulseWidthString=pulseWidth.getText().toString();
	  			String periodString=period.getText().toString();
	  			String timeshiftString=timeshift.getText().toString();
	  			String freqString=freq.getText().toString();
	  			String expBaseString=expBase.getText().toString();
	  			String meanString=mean.getText().toString();
	  			long ifPeriodic=periodicTypeSpinner.getSelectedItemId();
	  			double pulseWidth;
	  		//	if(ifPeriodic==0)
	  			//	pulseWidth=256;
	  			//else
	  			pulseWidth=value(pulseWidthString)+value(timeshiftString);
	  			
	  		if(pulseWidth>256.0){
	  			Toast.makeText(getApplicationContext(), "Sum of pulse width and timeshift should not be larger than 256", Toast.LENGTH_LONG).show();
	  		}else if(periodString.length()>3){
	  			Toast.makeText(getApplicationContext(), "Period should not be larger than 256", Toast.LENGTH_LONG).show();
	  			
	  		} else if((valueInt(periodString)>256)){
	  			Toast.makeText(getApplicationContext(), "Period should not be larger than 256", Toast.LENGTH_LONG).show();
	  		}
	  			else{
	  		
	  				long sigType=sigTypeSpinner.getSelectedItemId();
	  			sigGenInput.putString("signalType", "time");
	  			sigGenInput.putLong("sigType", sigType);
	  			sigGenInput.putLong("periodicType",ifPeriodic);
	  			sigGenInput.putDouble("gain",value(gainString));
				sigGenInput.putInt("pulseWidth",valueInt(pulseWidthString));
				sigGenInput.putInt("period",valueInt(periodString));
				sigGenInput.putInt("timeShift",valueInt(timeshiftString));
				sigGenInput.putDouble("mean",value(meanString));
				sigGenInput.putDouble("freq",value(freqString));
				sigGenInput.putDouble("expbase",value(expBaseString));
				SignalGenerator inputSignals=new SignalGenerator();
			    double[] output=inputSignals.setSignals(sigGenInput);
			    Bundle outPutArray=new Bundle();
			    double [] phase=new double[256];
			    
			    if((sigType==9)||(sigType==10)||(sigType==11)){
			    	pulseWidth=256.0;
			    }
			    outPutArray.putDoubleArray("signalMag", output);
			    outPutArray.putDoubleArray("signalPhase", phase);
			    outPutArray.putDouble("pulseWidth",pulseWidth);
			    
				sigGenInput.putBundle("rightPin", outPutArray);
				if(create){
				sigGenNumber++;				
				sigGenInput.putString("title", "Sig Gen"+" "+sigGenNumber);
				PinTypes types=new PinTypes();
				types.rightPin=true;				
				sigGenInput.putSerializable("PinTypes",types);
				PinSignalTypes sigGenSigType=new PinSignalTypes();
				sigGenInput.putSerializable("pinSignalType",sigGenSigType );
				PartsController.createBlock(sigGenInput);
				} else{
					sigGenInput.putString("title", inputValues.getString("title"));					
					PartsController.updateBlock(sigGenInput);
				}
			
				
				Intent i = new Intent(this, StartView.class);
				startActivity(i);
	  		}
				}
	  		
	  		if(v==partList){
	  			if(create){
					Intent i = new Intent(this, PartList.class);
					startActivity(i);}
					 else {
						 Intent i = new Intent(this, StartView.class);
							startActivity(i);
					 }

	  			}
	  	   }
	  private Double value(String input){
		  if (input.equalsIgnoreCase("")||input.equalsIgnoreCase("-")){
			  return 0.0;
		  }
		  else return Double.parseDouble(input);
	  }
	  private Integer valueInt(String input){
		  if (input.equalsIgnoreCase("")||input.equalsIgnoreCase("-")){
			  return 0;
		  }
		  else return Integer.parseInt(input);
	  }
	  @Override
		 public void onStop() {
		  super.onStop();	
					finish();
					
		 }
	  private void reset(){
		  sigTypeSpinner.setSelection(0);
			 periodicTypeSpinner.setSelection(1);
			 Double gainValue=1.0;
			 Integer pulseWidthValue=20;
			 Integer periodValue= 10;
			 Integer timeshiftValue=0;
			 Double meanValue=0.0;
			 Double freqValue=0.2;
			 Double expBaseValue=0.9;
			 
			 gain.setText(gainValue.toString());
			 pulseWidth.setText(pulseWidthValue.toString());
			 period.setText(periodValue.toString());
			 timeshift.setText(timeshiftValue.toString());
			 mean.setText(meanValue.toString());
			 freq.setText(freqValue.toString());
			 expBase.setText(expBaseValue.toString());
	  }
}
	

