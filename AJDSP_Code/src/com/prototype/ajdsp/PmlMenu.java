package com.prototype.ajdsp;

import java.text.DecimalFormat;

import com.prototype.ajdsp.PartObject.PinSignalTypes;
import com.prototype.ajdsp.PartObject.PinTypes;


import android.app.Activity;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.AdapterView.OnItemSelectedListener;

public class PmlMenu extends Activity implements OnClickListener{

	RelativeLayout layout;
	RelativeLayout topbar;
	Button addParts;
	Button back;
	EditText Wp1;
	EditText Ws1;
	EditText Wp2;
	EditText Ws2;
	EditText PB;
	EditText SB;
	Bundle inputValues;
	TextView Wp2Text;
	TextView Ws2Text;
	TextView titleText;
	Boolean create=true,update=false;

	Spinner filterTypeSpinner;


	public static int pmlNumber=0;
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
		//requestWindowFeature(Window.FEATURE_CUSTOM_TITLE);
		setContentView(R.layout.kaiser_menu);
		//getWindow().setFeatureInt(Window.FEATURE_CUSTOM_TITLE, R.layout.row);
		layout=(RelativeLayout)findViewById(R.id.kaiserMenu);
		layout.setBackgroundColor(Color.WHITE);
		addParts=(Button)findViewById(R.id.add_part);
        addParts.setOnClickListener(this);  
        back=(Button) findViewById(R.id.back_select_parts);
		back.setOnClickListener(this);
		TextView title= (TextView)findViewById(R.id.Title);
		title.setText("PML");


		filterTypeSpinner = (Spinner) findViewById(R.id.kaiserFilterType);
		ArrayAdapter<CharSequence> filterTypeAdapter = ArrayAdapter.createFromResource(
				this, R.array.filter_type_array_kaiser, android.R.layout.simple_spinner_item);
		filterTypeAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		//filterTypeSpinner.setBackgroundColor(Color.WHITE);
		filterTypeSpinner.setAdapter(filterTypeAdapter);
		filterTypeSpinner.setOnItemSelectedListener(new MyOnItemSelectedListener());


		Wp1=(EditText)findViewById(R.id.Wp1Kaiser);
		Ws1=(EditText)findViewById(R.id.Ws1Kaiser);
		Wp2=(EditText) findViewById(R.id.Wp2Kaiser);
		Ws2=(EditText) findViewById(R.id.Ws2Kaiser);
		PB=(EditText) findViewById(R.id.pbKaiser);
		SB=(EditText) findViewById(R.id.sbKaiser);


		Wp2Text=(TextView)findViewById(R.id.Wp2KaiserText);
		Ws2Text=(TextView)findViewById(R.id.Ws2KaiserText);
		titleText=(TextView)findViewById(R.id.Title);
		titleText.setText("Parks McClellan");
		

		Bundle extras = getIntent().getExtras(); 
		if(extras !=null)
		{
			inputValues = extras.getBundle("blockValues");
			filterTypeSpinner.setSelection((int)inputValues.getLong("filterType"));
			Double Wp1Value=inputValues.getDouble("Wp1");
			Double Ws1Value= inputValues.getDouble("Ws1");
			Double Wp2Value= inputValues.getDouble("Wp2");
			Double Ws2Value=inputValues.getDouble("Ws2");
			Double pbValue=inputValues.getDouble("PB");
			Double sbValue= inputValues.getDouble("SB");


			Wp1.setText(Wp1Value.toString());
			Ws1.setText(Ws1Value.toString());
			Wp2.setText(Wp2Value.toString());
			Ws2.setText(Ws2Value.toString());
			PB.setText(pbValue.toString());
			SB.setText(sbValue.toString());
			create=false;
			update=true;
			addParts.setText("Update");


		}else{


			filterTypeSpinner.setSelection(0);

			/*Double Wp1Value=0.1;
			Double Ws1Value=0.25;
			Double Wp2Value= 0.0;
			Double Ws2Value=0.0;*/
			Double pbValue=3.0;
			Double sbValue=20.0;

			/*Wp1.setText(Wp1Value.toString());
			Ws1.setText(Ws1Value.toString());
			Wp2.setText(Wp2Value.toString());
			Ws2.setText(Ws2Value.toString());*/
			PB.setText(pbValue.toString());
			SB.setText(sbValue.toString());

		}

	


	}
	@Override
	 public void onStop() {
		 super.onStop();	
				finish();
				
	 }
	@Override
	public void onBackPressed() {
	/*	if(create){
			Intent i = new Intent(this, PartList.class);
			startActivity(i);}
		else {
			Intent i = new Intent(this, StartView.class);
			startActivity(i);
		}*/
	}
	public class MyOnItemSelectedListener implements OnItemSelectedListener {

		public void onItemSelected(AdapterView<?> parent,
				View view, int pos, long id) {
			Wp2Text.setVisibility(View.GONE);
			Ws2Text.setVisibility(View.GONE);
			Wp2.setVisibility(View.GONE);
			Ws2.setVisibility(View.GONE);

			switch((int)id){
			case 0:
				if((!update)||(inputValues.getLong("filterType")!=0)){
				Wp1.setText(".2");
				Ws1.setText(".4");
				}else{
					update=false;
				}
				break;
			case 1:
				if((!update)||(inputValues.getLong("filterType")!=1)){
				Wp1.setText(".4");
				Ws1.setText(".2");
				}else{
					update=false;
				}
				break;
			case 2:
				Wp2Text.setVisibility(View.VISIBLE);
				Ws2Text.setVisibility(View.VISIBLE);
				Wp2.setVisibility(View.VISIBLE);
				Ws2.setVisibility(View.VISIBLE);
				if((!update)||(inputValues.getLong("filterType")!=2)){
					Wp1.setText(".4");
					Ws1.setText(".2");
					Wp2.setText(".6");
					Ws2.setText(".8");
					}else{
						update=false;
					}
				break;

			case 3:
				Wp2Text.setVisibility(View.VISIBLE);
				Ws2Text.setVisibility(View.VISIBLE);
				Wp2.setVisibility(View.VISIBLE);
				Ws2.setVisibility(View.VISIBLE);
				if((!update)||(inputValues.getLong("filterType")!=3)){
					Wp1.setText(".2");
					Ws1.setText(".4");
					Wp2.setText(".8");
					Ws2.setText(".6");
					}else{
						update=false;
					}
				break;
			

			default:
				break;
			}
		}

		public void onNothingSelected(AdapterView parent) {
			// Do nothing.
		}
	}
	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		if (v == addParts){
			Bundle pmlInput=new Bundle();


			double wp1=value(Wp1.getText().toString());
			double wp2=value(Wp2.getText().toString());
			double ws1=value(Ws1.getText().toString());
			double ws2=value(Ws2.getText().toString());
			double pb=value(PB.getText().toString());
			double sb=value(SB.getText().toString());
			
			long filterType=filterTypeSpinner.getSelectedItemId();
			pmlInput.putDouble("Wp1",wp1);
			pmlInput.putDouble("Ws1",ws1);
			pmlInput.putDouble("Wp2",wp2);
			pmlInput.putDouble("Ws2",ws2);
			pmlInput.putDouble("PB",pb);
			pmlInput.putDouble("SB",sb);

			
			pmlInput.putLong("filterType",filterType );
			PmlCalculator pmlCalc=new PmlCalculator();
			Bundle filterCoeff =pmlCalc.getFilterCoeff(pmlInput);
			int order=filterCoeff.getInt("order");

			if(order>64){
				Toast.makeText(getApplicationContext(), "Non-Valid filter order.\nMaximum order = 64 \n Please redefine parameters.", Toast.LENGTH_LONG).show();
			}
			else if((filterType==0)&&(wp1>=ws1)){
				Toast.makeText(getApplicationContext(), "Non-Valid inputs.Allowable inputs conform to:\nWp1 < Ws1", Toast.LENGTH_LONG).show();
			}else if((filterType==1)&&(wp1<=ws1)){
				Toast.makeText(getApplicationContext(), "Non-Valid inputs.Allowable inputs conform to:\nWp1 > Ws1", Toast.LENGTH_LONG).show();
			} else if((filterType==2)&&((wp1<=ws1)||(wp2>=ws2)||(wp1>=wp2))){
				Toast.makeText(getApplicationContext(), "Non-Valid inputs.Allowable inputs conform to:\nWp1 > Ws1 \n Wp2 < Ws2 \n Wp1 < Wp2", Toast.LENGTH_LONG).show();
			} else if((filterType==3)&&((wp1>=ws1)||(wp2<=ws2)||(wp1>=wp2))){
				Toast.makeText(getApplicationContext(), "Non-Valid inputs.Allowable inputs conform to:\nWp1 < Ws1 \n Wp2 > Ws2 \n Wp1 < Wp2", Toast.LENGTH_LONG).show();
			} 
	
			 else if(pb>=sb){
				Toast.makeText(getApplicationContext(), "Invalid Tolerace Values. PB < SB", Toast.LENGTH_LONG).show();
			} else if((wp1>1)||(ws1>1)||(wp2>1)||(ws2>1)){
				Toast.makeText(getApplicationContext(), "Cut-off frequencies must be less than 1", Toast.LENGTH_LONG).show();
			}
			else{
			

			pmlInput.putBundle("topPin", filterCoeff);
if(create){
			pmlNumber++;
			PinTypes types=new PinTypes();
			types.topPin=true;
			pmlInput.putSerializable("PinTypes",types);
			pmlInput.putString("title", "Parks-Mc" +" "+pmlNumber);
			PinSignalTypes pmlSigType=new PinSignalTypes();
			pmlSigType.topPin="frequency";			
			pmlInput.putSerializable("pinSignalType", pmlSigType);
			PartsController.createBlock(pmlInput);
} else {
	        pmlInput.putString("title",inputValues.getString("title"));
   	        PartsController.updateBlock(pmlInput);
   	        back.setText("Back");
   	        }
			
			
			pmlInput.putStringArray("plotValues", getValues(filterCoeff));
			pmlInput.putString("returnClass", "Parks");
		

			Intent i = new Intent(this, DisplayValues.class);
			i.putExtra("blockValues", pmlInput);
			startActivity(i);
			}
		}
		else if(v==back){
			if(create){
				Intent i = new Intent(this, PartList.class);
				startActivity(i);}
				 else {
					 Intent i = new Intent(this, StartView.class);
						startActivity(i);
				 }
		}
	}
	public String [] getValues(Bundle bundle){		
		int order=bundle.getInt("order");

		String [] valueArray= new String[order+3];
		valueArray[0]="Order: "+order;
		valueArray[1]="Coeff: ";
		DecimalFormat threeDForm = new DecimalFormat("#.###");
		double [] bCoeff=bundle.getDoubleArray("filterCoeffB");
		for(int i=2;i<valueArray.length;i++){
			valueArray[i]="b"+(i-2)+": "+threeDForm.format(bCoeff[i-2]);
		}
		return valueArray;

	}
	 private Double value(String input){
		  if (input.equalsIgnoreCase("")){
			  return 0.0;
		  }
		  else return Double.parseDouble(input);
	  }
	  
}
