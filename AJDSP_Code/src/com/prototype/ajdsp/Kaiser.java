package com.prototype.ajdsp;

public class Kaiser {
	public static final int LP = 0;
	  public static final int HP = 1;
	  public static final int BP = 2;
	  public static final int BS = 3;

	  private int filtType,Order;


	//  private double[] aCoeff;// = new double[11];
	//  private double[] bCoeff;
	  private double Wp1, Ws1, Wp2, Ws2, Rp, Rs, W1, W2;
	  private double bt, Lt, beta;

	public  Kaiser(int ft, double wp1, double ws1, double wp2, double ws2, double rp, double rs)
	  {
		  Wp1=wp1/2;
		  Ws1=ws1/2;
		  Wp2=wp2/2;
		  Ws2=ws2/2;
		  Rp=Math.pow(10,-0.05*rp);
		  Rs=Math.pow(10,-0.05*rs);
		  filtType=ft;
		  ParamCalc();

	  }
	 private void ParamCalc()
	  {
		  double df,L1,L2,be1,be2,L;
		  if (filtType==0 || filtType==1)
		  {
			  df=Math.abs(Wp1-Ws1);
			  Kaislpord(Wp1,Ws1,Rp,Rs);
			  beta=bt;L=Lt;

		  }
		  else
		  {
			  df=Math.min(Math.abs(Wp1-Ws1),Math.abs(Wp2-Ws2));
			  Kaislpord(Wp1,Ws1,Rp,Rs);
			  be1=bt;L1=Lt;
			  Kaislpord(Wp2,Ws2,Rp,Rs);
			  be2=bt;L2=Lt;
			  L=0;beta=0;
			  if (L1>L)
			  {
				  beta=be1;L=L1;
			  }
			  if (L2>L)
			  {
				  beta=be2;L=L2;
			  }

		  }
		  Order=(int)(Math.ceil(L)-1);
		  if ((filtType==1 || filtType==3) && (Order%2!=0)) //if HP or BS and order odd increase by 1
			  Order++;
		  if (Order<=0 || Order>64) // Max allowable order:10
			  Order=-1;  // indicates non valid order
		  if (filtType==0)
		  {W1=0;W2=Wp1+Ws1;}
		  else if (filtType==1)
		  {W1=Wp1+Ws1;W2=1.0;}
		  else
		  {W1=Wp1+Ws1;W2=Wp2+Ws2;}



	  }


	  public double log10(double x)
	  {
		  return Math.log(x)/Math.log(10);
	  }



	  private void Kaislpord(double f1, double f2, double d1, double d2)
	  {
		  double b1=0,b2=0;
		  double delta=Math.min(d1,d2);
		  double atten = -20*log10(delta);
		  double D=(atten-7.95)/(2*Math.PI*2.285);
		  double df=Math.abs(f1-f2);
		  Lt=D/df+1;
		  //if (atten>50)
			//  b1=0.1102*(atten-8.7);
		  //if (atten>=21)
		//	  b2=0.5842*Math.pow(atten-21,0.4)+0.07886*(atten-21);
		  //bt=b1+b2;
		  if (atten>50)
			  bt=0.1102*(atten-8.7);
		  else if (atten>=21  &&  atten<=50)
			  bt=0.5842*Math.pow(atten-21,0.4)+0.07886*(atten-21);
		  else
			  bt=0;



	  }




	  public int getOrder()
	  {
	    return Order;
	  }


	  public double getW1()
	  {
		  return W1;
	  }

	  public double getW2()
	  {
		  return W2;
	  }
	  public double getBeta()
	  {
		  return beta;
	  }



}
