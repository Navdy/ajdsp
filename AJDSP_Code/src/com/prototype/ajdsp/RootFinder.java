package com.prototype.ajdsp;



public class RootFinder
{
//Part part;

	// Jayaraman - 12 December 2008
	int temp_ord = 40;
	double[] rr= new double[temp_ord+1];
	double[] ri= new double[temp_ord+1];
	int m;
	int MAXM = 100;
	static final double[] frac = {0.0, 0.5, 0.25, 0.75, 0.13, 0.38, 0.62, 0.88, 1.0};


	public  RootFinder(double[] coef, PZPlotCalc PZ, int p_z)

	{
		//CalcCoeff(coef,CalcOrder(coef));
		CalcCoeff(coef,coef.length);
		if (p_z==0)
		{
			PZ.no_zeros = m;
			for (int k=1;k<=m;k++)
			{
				PZ.ZX[k]=rr[k];
				PZ.ZY[k]=ri[k];
			}
		}
		else if (p_z==1)
		{
			PZ.no_poles = m;
			for (int k=1;k<=m;k++)
			{
				PZ.PX[k]=rr[k];
				PZ.PY[k]=ri[k];
			}
		}

	}

	/*public RootFinder(double[] coef, LPCtoLSPPart LSP, int p_z, int no_coef)
	{
		CalcCoeff(coef,no_coef);


		if (p_z == 0)
		{
			LSP.no_zeros = m;
			for (int k = 1;k <= m;k ++)
			{
				LSP.ZX[k]=rr[k];
				LSP.ZY[k]=ri[k];
			}
		}
		else if (p_z == 1)
		{
			LSP.no_poles = m;
			for (int k = 1;k <= m;k ++)
			{
				LSP.PX[k]=rr[k];
				LSP.PY[k]=ri[k];
			}
		}

	}*/
	private int CalcOrder(double[] coef)
	{
		m=0; // calculate number of coefficients

	// Jayaraman - 12 December 2008
		for (int i=0;i<=temp_ord;i++)
		{
			if (coef[i]==0.0)
			{
				for (int j=i;j<=temp_ord;j++)
				{
					if (coef[j]!=0.0)
					{
						m++;
						break;
					}
				}
			}
			else m++;
		}
		return m;
	}

	private void CalcCoeff(double[] coef, int no_coef)
	{
		m = no_coef;
		double EPS = 0.000002;

		double[] temp = new double[2];
		double[] adr= new double[m];
		double[] adi= new double[m];
		double[] ar= new double[m];
		double[] ai= new double[m];


		double xi,xr,cr,ci,br,bi,t1,t2;
		int j,i;


		m=m-1; // number of roots
		j=m;
		for (i=0;i<=m;i++)
		{
			adr[i]=coef[j];
			j--;
			ar[i]=adr[i];
			adi[i]=0.0;
			ai[i]=0.0;
		}
		for (j=m;j>=1;j--)
		{
			xi=0;
			xr=0;
			temp=laguer(adr,adi,j,xr,xi);
			xr=temp[0];
			xi=temp[1];

			if (Math.abs(xi)<=2*EPS*Math.abs(xr))
				xi=0;
			ri[j]=xi;
			rr[j]=xr;
			br=adr[j];
			bi=adi[j];
			for (int jj=j-1;jj>=0;jj--)
			{
				cr=adr[jj];
				ci=adi[jj];
				adr[jj]=br;
				adi[jj]=bi;
				t1=xr*br-xi*bi+cr;
				t2=xr*bi+xi*br+ci;
				br=t1;bi=t2;
			}
		}
		for (j=1;j<=m;j++)
		{
			temp=laguer(ar,ai,m,rr[j],ri[j]);
			rr[j]=temp[0];
			ri[j]=temp[1];
		}
		for (j=1;j<=m;j++)
		{
			xr=rr[j];
			xi=ri[j];
			for (i=j-1;i>=1;i--)
			{
				if (rr[i]<=xr) break;
				rr[i+1]=rr[i];
				ri[i+1]=ri[i];

			}
			rr[i+1]=xr;
			ri[i+1]=xi;
		}
	}

	public double[] laguer(double[] ar,double[] ai,int mm, double xr,double xi)
	{
		double[] result = new double[2];
		double EPSS=0.0000001;

		// Jayaraman - 12 December 2008
		int MAXIT = temp_ord*8;
		int MT = temp_ord;

		int iter,j;
		double abx,abp,abm,err;
		double dxr,x1r,br,dr,fr,gr,hr,sqr,sqtr,gpr,gmr,g2r;
		double dxi,x1i,bi,di,fi,gi,hi,sqi,sqti,gpi,gmi,g2i;
		double temp1,temp2;

		for (iter=1;iter<=MAXIT;iter++)
		{
			br=ar[mm];
			bi=ai[mm];
			err=Math.sqrt(Math.pow(br,2)+Math.pow(bi,2));
			dr=0;di=0;fr=0;fi=0;
			abx=Math.sqrt(Math.pow(xr,2)+Math.pow(xi,2));
			for (j=mm-1;j>=0;j--)
			{
				temp1=xr*fr-xi*fi+dr;
				temp2=xr*fi+xi*fr+di;
				fr=temp1;fi=temp2;
				temp1=xr*dr-xi*di+br;
				temp2=xr*di+xi*dr+bi;
				dr=temp1;di=temp2;
				temp1=xr*br-xi*bi+ar[j];
				temp2=xr*bi+xi*br+ai[j];
				br=temp1;bi=temp2;
				err=Math.sqrt(Math.pow(br,2)+Math.pow(bi,2))+abx*err;
			}
			err*=EPSS;
			if (Math.sqrt(Math.pow(br,2)+Math.pow(bi,2))<=err)
			{
				result[0]=xr;
				result[1]=xi;
				return result;
			}
			gr=(dr*br+di*bi)/(br*br+bi*bi);
			gi=(di*br-dr*bi)/(br*br+bi*bi);
			g2r=gr*gr-gi*gi;
			g2i=2*gr*gi;
			hr=g2r-2*(fr*br+fi*bi)/(br*br+bi*bi);
			hi=g2i-2*(fi*br-fr*bi)/(br*br+bi*bi);
			sqtr=(mm-1)*(mm*hr-g2r);
			sqti=(mm-1)*(mm*hi-g2i);
			sqr=Math.sqrt(Math.sqrt(sqtr*sqtr+sqti*sqti));//*Math.cos(Math.atan(sqti/sqtr)/2);
			sqi=Math.sqrt(Math.sqrt(sqtr*sqtr+sqti*sqti));//*Math.sin(Math.atan(sqti/sqtr)/2);
			gpr=gr+sqr;
			gpi=gi+sqi;
			gmr=gr-sqr;
			gmi=gi-sqi;
			abp=Math.sqrt(Math.pow(gpr,2)+Math.pow(gpi,2));
			abm=Math.sqrt(Math.pow(gmr,2)+Math.pow(gmi,2));
			if (abp<abm)
			{
				gpr=gmr;
				gpi=gmi;
			}
			if (Math.max(abp,abm)>0.0)
			{
				dxr=mm*gpr/(gpr*gpr+gpi*gpi);
				dxi=-mm*gpi/(gpr*gpr+gpi*gpi);
			}
			else
			{
				dxr=(1+abx)*Math.cos((double) iter);
				dxi=(1+abx)*Math.sin((double) iter);
			}
			x1r=xr-dxr;
			x1i=xi-dxi;
			if (xr==x1r && xi==x1i)
			{
				result[0]=xr;
				result[1]=xi;
				return result;
			}
			if (iter % MT >0)
			{
				xr=x1r;
				xi=x1i;
			}
			else
			{
				xr=xr-frac[iter/MT]*dxr;
				xi=xi-frac[iter/MT]*dxi;
			}

		}
		result[0]=xr;
		result[1]=xi;
		return result;


	}


}
