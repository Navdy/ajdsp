package com.prototype.ajdsp;
/*Copyright (c) 1999 Andreas Spanias, Arizona State University
JDSP Concept by Andreas Spanias

Development Team:
Director: Andreas Spanias
Research Assistants/Associates: Argyris Constantinou, Axel Clausen, Maya Tampi

For questions regarding use of this software write or
call Prof. Andreas Spanias  (spanias@asu.edu,  (480) 965 1837))
********************************************************************************

DESCRIPTION:  Implements Chebyshev type I filter design
IIR filter design code based on a Pascal program
listed in "Digital Signal Processing with Computer Applications"
by P. Lynn and W. Fuerst (Wiley)


KEY VARIABLES:
INPUTS:
LP - lowpass filter
HP - highpass filter
BP - bandpass filter
filterType - filter type (LP, HP, BP)
wp1 - passband edge frequency
ws1 - stopband edge frequency
wp2 - second passband edge frequency
ws1 - second stopband edge frequency
rp  - passband ripple
rs  - stopband ripple
OUTPUTS:
order - order of the filter
fp1 - first cut-off frequency
fp2 - second cut-off frequency
*/

class Chebyshev {


  public static final int LP = 0;
  public static final int HP = 1;
  public static final int BP = 2;

  private int order;
  private int  filterType;
  private float fp1, fp2,fN, rate, ripple;
  private double[] pReal;
  private double[] pImag;
  private double[] z;
  private double[] aCoeff;
  private double[] bCoeff;
  private double Wp1, Ws1, Wp2, Ws2, Rp, Rs;

  public  Chebyshev()
  {
    rate = 8000.0f;
    fN = 0.5f*rate;
  }

  public void setFilterParameters(int ft, double wp1, double ws1, double wp2, double ws2, double rp, double rs)
  {
	  double wa,tempord;

	  filterType=ft;
	  Wp1=Math.tan(Math.PI*wp1/2);
	  Ws1=Math.tan(Math.PI*ws1/2);
	  Wp2=Math.tan(Math.PI*wp2/2);
	  Ws2=Math.tan(Math.PI*ws2/2);
	  Rs=rs;
	  Rp=rp;
	  ripple=(float)rp;
	  // calculates band-pass (fp1-fp2) and order
	  switch (filterType){
	  case LP: wa=Ws1/Wp1;break;
	  case HP: wa=Wp1/Ws1;break;
	  case BP: wa=Math.min(Math.abs((Ws1*Ws1-Wp1*Wp2)/(Ws1*(Wp1-Wp2))),Math.abs((Ws2*Ws2-Wp1*Wp2)/(Ws2*(Wp1-Wp2))));break;
	  default: wa=0;
	  }
	  tempord=acosh(Math.sqrt((Math.pow(10,0.1*Math.abs(Rs))-1)/(Math.pow(10,0.1*Math.abs(Rp))-1)))/acosh(wa);
	  order=(int)(tempord+0.999999);

	  if ((filterType == BP) /*&& odd(order)*/)
		  order*=2;

	  switch (filterType){
	  case LP: fp1=0;fp2=(float)((2/Math.PI)*Math.atan(Wp1))*fN;break;
	  case HP: fp1=(float)((2/Math.PI)*Math.atan(Wp1))*fN;fp2=fN;break;
	  case BP: fp1=(float)((2/Math.PI)*Math.atan(Wp1))*fN;
			   fp2=(float)((2/Math.PI)*Math.atan(Wp2))*fN;break;
	  default: fp1=0;fp2=0;
	  }
	  if (order>0 && order<=10)
		  design();
	  else
		  order=-1;

  }
  private double acosh(double x)
  {
	  double v;
	  v=Math.max(1,x);
	  return Math.log(x+v*Math.sqrt(Math.pow(x/v,2)-Math.pow(1/v,2)));
  }

  public int getOrder()
  {
    return order;
  }

  public double getFreq1() {
    return fp1/fN;
  }

  public double getFreq2() {
    return fp2/fN;
  }

  public double getACoeff(int i) {
    // returns IIR filter numerator coefficient with index i
    return aCoeff[i];
  }

  public double getBCoeff(int i) {
    // returns IIR filter denominator coefficient with index i
    return bCoeff[i];
  }

  float sqr(float x) {
    return x * x;
  }

  double sqr(double x) {
    return x * x;
  }

  boolean odd(int n) {
    // returns true if n odd
    return (n % 2) != 0;
  }

private void locatePolesAndZeros() {
    // determines poles and zeros of IIR filter
    // based on bilinear transform method
    pReal  = new double[order + 1];
    pImag  = new double[order + 1];
    z      = new double[order + 1];
    double ln10 = Math.log(10.0);
    for(int k = 1; k <= order; k++) {
      pReal[k] = 0.0;
      pImag[k] = 0.0;
    }
    // Chebyshev parameters
    int n = order;
    if (filterType == BP) n = n/2;
    int ir = n % 2;
    int n1 = n + ir;
    int n2 = (3*n + ir)/2 - 1;
    double f1;
    switch (filterType) {
      case LP: f1 = fp2;       break;
      case HP: f1 = fN - fp1;  break;
      case BP: f1 = fp2 - fp1; break;
      default: f1 = 0.0;
    }
    double tanw1 = Math.tan(0.5*Math.PI*f1/fN);
    double tansqw1 = sqr(tanw1);
    // Real and Imaginary parts of low-pass poles
    double t, a = 1.0, r = 1.0, i = 1.0;
    for (int k = n1; k <= n2; k++)
	{
		t = 0.5*(2*k + 1 - ir)*Math.PI/(double)n;
		double d = 1.0 - Math.exp(-0.05*ripple*ln10);
        double e = 1.0 / Math.sqrt(1.0 / sqr(1.0 - d) - 1.0);
        double x = Math.pow(Math.sqrt(e*e + 1.0) + e, 1.0/(double)n);
        a = 0.5*(x - 1.0/x);
        double b = 0.5*(x + 1.0/x);
        double c3 = a*tanw1*Math.cos(t);
        double c4 = b*tanw1*Math.sin(t);
        double c5 = sqr(1.0 - c3) + sqr(c4);
        r = 2.0*(1.0 - c3)/c5 - 1.0;
        i = 2.0*c4/c5;
		int m = 2*(n2 - k) + 1;
        pReal[m + ir]     = r;
        pImag[m + ir]     = Math.abs(i);
        pReal[m + ir + 1] = r;
        pImag[m + ir + 1] = - Math.abs(i);
    }
    if (odd(n)) {
      r = 2.0/(1.0 + a*tanw1) - 1.0;
      pReal[1] = r;
      pImag[1] = 0.0;
    }
    if (filterType==LP)
	{
        for (int m = 1; m <= n; m++)
          z[m]= -1.0;
    }
	else if (filterType==HP)
    {
        // low-pass to high-pass transformation
        for (int m = 1; m <= n; m++) {
          pReal[m] = -pReal[m];
          z[m]     = 1.0;
        }
	}
    else if (filterType==BP)
	{
        // low-pass to bandpass transformation
		  for (int m = 1; m <= n; m++) {
          z[m]  =  1.0;
          z[m+n]= -1.0;
        }
        double f4 = 0.5*Math.PI*fp1/fN;
        double f5 = 0.5*Math.PI*fp2/fN;

        double aa = Math.cos(f4 + f5)/Math.cos(f5 - f4);
        double aR, aI, h1, h2, p1R, p2R, p1I, p2I;
        for (int m1 = 0; m1 <= (order - 1)/2; m1++) {
          int m = 1 + 2*m1;
          aR = pReal[m];
          aI = pImag[m];
          if (Math.abs(aI) < 0.0001) {
            h1 = 0.5*aa*(1.0 + aR);
            h2 = sqr(h1) - aR;
            if (h2 > 0.0) {
              p1R = h1 + Math.sqrt(h2);
              p2R = h1 - Math.sqrt(h2);
              p1I = 0.0;
              p2I = 0.0;
            }
            else {
              p1R = h1;
              p2R = h1;
              p1I = Math.sqrt(Math.abs(h2));
              p2I = -p1I;
            }
          }
          else {
            double fR = aa*0.5*(1.0 + aR);
            double fI = aa*0.5*aI;
            double gR = sqr(fR) - sqr(fI) - aR;
            double gI = 2*fR*fI - aI;
            double sR = Math.sqrt(0.5*Math.abs(gR + Math.sqrt(sqr(gR) + sqr(gI))));
            double sI = gI/(2.0*sR);
            p1R = fR + sR;
            p1I = fI + sI;
            p2R = fR - sR;
            p2I = fI - sI;
          }
          pReal[m]   = p1R;
          pReal[m+1] = p2R;
          pImag[m]   = p1I;
          pImag[m+1] = p2I;
        } // end of m1 for-loop
        if (odd(n)) {
          pReal[2] = pReal[n+1];
          pImag[2] = pImag[n+1];
        }
        for (int k = n; k >= 1; k--) {
          int m = 2*k - 1;
          pReal[m]   =   pReal[k];
          pReal[m+1] =   pReal[k];
          pImag[m]   =   Math.abs(pImag[k]);
          pImag[m+1] = - Math.abs(pImag[k]);
		  }
    }
  }

  public void design() {
    aCoeff = new double[order + 1];
    bCoeff = new double[order + 1];
    double[] newA = new double[order + 1];
    double[] newB = new double[order + 1];
    locatePolesAndZeros(); // find filter poles and zeros
    // compute filter coefficients from pole/zero values
    aCoeff[0]= 1.0;
    bCoeff[0]= 1.0;
    for (int i = 1; i <= order; i++) {
      aCoeff[i] = 0.0;
      bCoeff[i] = 0.0;
    }
    int k = 0;
    int n = order;
    int pairs = n/2;
    if (odd(order)) {
     // first subfilter is first order
      aCoeff[1] = - z[1];
      bCoeff[1] = - pReal[1];
      k = 1;
    }
    for (int p = 1; p <= pairs; p++) {
      int m = 2*p - 1 + k;
      double alpha1 = - (z[m] + z[m+1]);
      double alpha2 = z[m]*z[m+1];
      double beta1  = - 2.0*pReal[m];
      double beta2  = sqr(pReal[m]) + sqr(pImag[m]);
      newA[1] = aCoeff[1] + alpha1*aCoeff[0];
      newB[1] = bCoeff[1] + beta1 *bCoeff[0];
      for (int i = 2; i <= n; i++) {
        newA[i] = aCoeff[i] + alpha1*aCoeff[i-1] + alpha2*aCoeff[i-2];
        newB[i] = bCoeff[i] + beta1 *bCoeff[i-1] + beta2 *bCoeff[i-2];
      }
      for (int i = 1; i <= n; i++) {
        aCoeff[i] = newA[i];
        bCoeff[i] = newB[i];
      }
    }
	filterGain();

  }
  public void filterGain() {

    // filter gain at uniform frequency intervals
	  int freqPoints=250;
    float[] g = new float[freqPoints+1];
    double theta, s, c, sac, sas, sbc, sbs;
    float gMax = -100.0f;
    float sc = 10.0f/(float)Math.log(10.0f);
    double t = Math.PI / freqPoints;
    for (int i = 0; i <= freqPoints; i++) {
      theta = i*t;
      if (i == 0) theta = Math.PI*0.0001;
      if (i == freqPoints) theta = Math.PI*0.9999;
      sac = 0.0f;
      sas = 0.0f;
      sbc = 0.0f;
      sbs = 0.0f;
      for (int k = 0; k <= order; k++) {
        c = Math.cos(k*theta);
        s = Math.sin(k*theta);
        sac += c*aCoeff[k];
        sas += s*aCoeff[k];
        sbc += c*bCoeff[k];
        sbs += s*bCoeff[k];
      }
      g[i] = sc*(float)Math.log((sqr(sac) + sqr(sas))/(sqr(sbc) + sqr(sbs)));
      gMax = Math.max(gMax, g[i]);
    }
    // normalise to 0 dB maximum gain
    for (int i=0; i<=freqPoints; i++) g[i] -= gMax;
    // normalise numerator (a) coefficients
    float normFactor = (float)Math.pow(10.0, -0.05*gMax);
    for (int i=0; i<=order; i++) aCoeff[i] *= normFactor;
    //return g;
  }


}
