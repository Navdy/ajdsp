package com.prototype.ajdsp;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.text.InputType;
import android.view.View;
import android.view.WindowManager;
import android.view.View.OnClickListener;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.prototype.ajdsp.PartObject.PinSignalTypes;
import com.prototype.ajdsp.PartObject.PinTypes;

public class BWMenu extends Activity implements OnClickListener{
	RelativeLayout layout;
	RelativeLayout topbar;
	Button addParts;
	EditText bwExp;
	Bundle inputValues=new Bundle();



	Button partList;
	public static int bwexpNumber=0;
	Boolean create=true;
	double [] filterCoeffA;
	double [] filterCoeffB;


	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
		setContentView(R.layout.upsampler_menu);
		//getWindow().setFeatureInt(Window.FEATURE_CUSTOM_TITLE, R.layout.row);
		layout=(RelativeLayout)findViewById(R.id.upSamplerMenu);
		layout.setBackgroundColor(Color.WHITE);
		partList=(Button) findViewById(R.id.back_select_parts);
		partList.setOnClickListener(this);
		TextView title= (TextView)findViewById(R.id.Title);
		title.setText("BW Exp");
		TextView bwMenuText= (TextView)findViewById(R.id.upSamplerText);
		bwMenuText.setText("Exp. Coef");

		addParts=(Button) findViewById(R.id.add_part);
		addParts.setOnClickListener(this);
		bwExp=(EditText)findViewById(R.id.upSamplerRate);
		//bwExp.setInputType(InputType.TYPE_CLASS_NUMBER|InputType.TYPE_NUMBER_FLAG_DECIMAL|InputType.TYPE_NUMBER_FLAG_SIGNED);

		Bundle extras = getIntent().getExtras();

		if(extras !=null)
		{
			inputValues = extras.getBundle("blockValues");
			Double coeff=inputValues.getDouble("bwExp");
			bwExp.setText(coeff.toString());
			filterCoeffA=inputValues.getBundle("bottomPin").getDoubleArray("filterCoeffA");
			filterCoeffB=inputValues.getBundle("bottomPin").getDoubleArray("filterCoeffB");
			create=false;
			addParts.setText("Update");
			partList.setText("Back");

		}else{
			Double coeff=.9;
			bwExp.setText(coeff.toString());
		}
	}
	 
	 
	@Override
	public void onStop() {
		super.onStop();
		finish();

	}
	@Override
	public void onBackPressed() {
		/*if(create){
			Intent i = new Intent(this, PartList.class);
			startActivity(i);}
		else {
			Intent i = new Intent(this, StartView.class);
			startActivity(i);
		}*/
	}

	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		if (v == addParts){
			String length=bwExp.getText().toString();
			Bundle bwExpData=new Bundle();
			double [] bwExpArray = new double[11];
			Bundle bwExpOutputBundle=new Bundle();
			bwExpOutputBundle.putDoubleArray("filterCoeffA",bwExpArray);
			bwExpOutputBundle.putDoubleArray("filterCoeffB",bwExpArray);
			bwExpData.putBundle("topPin", bwExpOutputBundle);
			bwExpData.putDouble("bwExp",parseDouble(length));
			if(create){
				bwexpNumber++;
				Bundle bwExpInputBundle=new Bundle();
				bwExpInputBundle.putDoubleArray("filterCoeffA",bwExpArray);
				bwExpInputBundle.putDoubleArray("filterCoeffB",bwExpArray);
				bwExpData.putBundle("bottomPin", bwExpInputBundle);
				PinSignalTypes bwExpSigType=new PinSignalTypes();
				bwExpSigType.topPin="frequency";
				bwExpSigType.bottomPin="frequency";
				bwExpData.putSerializable("pinSignalType", bwExpSigType);
				bwExpData.putString("title","BW Exp"+" "+bwexpNumber); 
				PinTypes bwExpPinTypes=new PinTypes();
				bwExpPinTypes.topPin=true;
				bwExpPinTypes.bottomPin=true;
				bwExpData.putSerializable("PinTypes",bwExpPinTypes);			    
				PartsController.createBlock(bwExpData);
			}
			else{
				Bundle bwExpInputBundle=new Bundle();
				bwExpInputBundle.putDoubleArray("filterCoeffA",filterCoeffA);
				bwExpInputBundle.putDoubleArray("filterCoeffB",filterCoeffB);
				bwExpData.putBundle("bottomPin", bwExpInputBundle);
				bwExpData.putString("title", inputValues.getString("title"));
				PartsController.updateBlock(bwExpData);
			}
			Intent i = new Intent(this, StartView.class);
			startActivity(i);
		}

		if(v==partList){
			if(create){
				Intent i = new Intent(this, PartList.class);
				startActivity(i);}
			else {
				Intent i = new Intent(this, StartView.class);
				startActivity(i);
			}
		}

	}
	public double parseDouble(String valueEntry){
		double value=0;
		if(!valueEntry.equalsIgnoreCase(""))
			value=Double.parseDouble(valueEntry);
		return value;
	}
}
