package com.prototype.ajdsp;

import java.util.Arrays;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.os.Handler;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.prototype.ajdsp.PartObject.PinSignalTypes;
import com.prototype.ajdsp.PartObject.PinTypes;

public class LongSigMenu  {

	RelativeLayout layout;
	Button addParts;
	Button partList;
	Boolean create = true;
	Boolean edited = false, fromUI = true, dataParsed = false; // these flags ensure LongSigGenerator class is not called unnecessarily to set the signals when the
	Spinner sigTypeSpinner;
	// Spinner pinTypeSpinner;
	Spinner overlapSpinner;
	public Bundle inputValues = new Bundle();
	EditText gain;
	EditText frameSize;
	Button frameFirst;
	Button frameLast;
	Button framePlay;
	Button frameForward;
	Button frameBackward;
	Button parse;
	TextView selectpinText;
	TextView inputText;
	TextView frameSizeText;
	TextView overlapText;
	TextView gainText;
	String[] xAxisValues;
	private static Context context;	
	
	Thread start;
	private volatile Thread delayThread; // used in runOnUi method of updating GraphView when 'play' button pressed
	private Handler mHandler = new Handler();
	public boolean threadSuspended = true, isLooping = false;

	String pause = "||";
	String frameNumDisp;

	// LongSigThread thread;

	public static int longSigGenNumber = 0;
	public static int signalSelected; // to indicate to Sound Player block which file to play.
	GraphView magnPlot;

	String phaseTitle = "Phase";
	String ampTitledB = "Amplitude(dB)";
	
	static String[] verLabels;
	String[] horLabels;
	public double[] y, sig;
	public int frame_no = 1;
	Double gainValue = 1.0;
	Integer frameSizeValue = 256;
	int length = 256;
	int maxFrameVal = 32;
	Toast toast1;
	// public static boolean moveForward = true;

	LongSignalGenerator inputSignals = new LongSignalGenerator();


	

	// Update graph when 'Signal Type' is edited
	
	

		

	public  void onClick() {
		// TODO Auto-generated method stub

		
			Bundle longSigGenInput = new Bundle();
			
			double[] empty_frame = new double[256];
	  		Arrays.fill(empty_frame, 0);
			// put all input data values into the input signal bundle
			longSigGenInput.putString("signalType", "time");
			longSigGenInput.putLong("longSigType",1);
			longSigGenInput.putLong("overlapValue",0);
			longSigGenInput.putDouble("gain", 1.0);
			longSigGenInput.putInt("pulseWidth", 256);
			longSigGenInput.putInt("frameNumber", 9);
			longSigGenInput.putInt("TotalNumFrames", 32);
			longSigGenInput.putDoubleArray("signalMag", y);
			longSigGenInput.putDoubleArray("signalPhase", empty_frame);
			

		   double[] output=inputSignals.setSignals(longSigGenInput);
			y = inputSignals.getFrame(9, output);
			System.out.println(y);
			
			
			

}
	 public static void main(String[] args) {
	        LongSigMenu longSigMenu=new LongSigMenu();
	        longSigMenu.onClick();
	    }
}
