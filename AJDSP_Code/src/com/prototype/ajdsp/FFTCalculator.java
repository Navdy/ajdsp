package com.prototype.ajdsp;



import android.os.Bundle;

public class FFTCalculator {
	public int MAXSIZE=256;
	public int fft_size = MAXSIZE;
	double[] phzQ = new double[MAXSIZE];
	double[] magnQ = new double[MAXSIZE];
	double[] phzI = new double[MAXSIZE];
	double[] magnI = new double[MAXSIZE];

	public  Bundle FFT(Bundle dataBundle){
		for (int i=0;i<256;i++)
		{
			phzI[i]=0;magnI[i]=0;
		}
		fft_size=dataBundle.getInt("fftLength");
		Bundle inputData=dataBundle.getBundle("leftPin");
		double[] inputMag=inputData.getDoubleArray("signalMag");
		double[] inputPhase=new double[fft_size];
		if(inputData.getDoubleArray("signalPhase")!=null)
			inputPhase=inputData.getDoubleArray("signalPhase");
		FastFourier fftI= new FastFourier(inputMag, inputPhase, fft_size,1);
		FastFourier fftQ= new FastFourier(inputMag, inputPhase, fft_size,1);
		for (int k=0;k<fft_size;k++)
		{
			magnI[k]=fftI.getMagnitude(k);						
			phzI[k]=fftI.getPhase(k);
			magnQ[k]=fftQ.getMagnitude(k);
			phzQ[k]=fftQ.getPhase(k);
		}
		Bundle outputBundle=new Bundle();
		outputBundle.putDoubleArray("signalMag", magnI);
		outputBundle.putDoubleArray("signalPhase", phzI);
		outputBundle.putDouble("pulseWidth", fft_size);
		dataBundle.putBundle("rightPin", outputBundle);
		return dataBundle;
	}


}
