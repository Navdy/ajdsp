package com.prototype.ajdsp;

import android.os.Bundle;

public class DownSamplerCalc {
	public int M=1,MAXSIZE=256;	//sampling rate
	private double y[] = new double[MAXSIZE];

	public Bundle downSampler(Bundle inputBundle){
		M= inputBundle.getInt("downSamplerRate");
		Bundle input1=inputBundle.getBundle("leftPin");
		if(input1!=null){
			double l1= input1.getDouble("pulseWidth");
			double [] input=input1.getDoubleArray("signalMag");
			for (int k=0;k<MAXSIZE;k++)
				y[k]=0;
			int i;
			for (i=0;i<MAXSIZE;i++)
			{
				if ((i*M>=MAXSIZE)||(i*M>=MAXSIZE*M)) break;
				y[i]=input[i*M];
			}
			inputBundle.getBundle("rightPin").putDoubleArray("signalMag", y);

			inputBundle.getBundle("rightPin").putDouble("pulseWidth", Math.ceil((double)l1/(double)M));
		}
		return  inputBundle;

	}
}
