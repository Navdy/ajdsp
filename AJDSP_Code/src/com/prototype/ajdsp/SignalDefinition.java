package com.prototype.ajdsp;

import com.prototype.ajdsp.InputSignalType.InputSignal;



public class SignalDefinition {
double sinFreq=0.0;
double exponent=0.0;
double amplitude=5.0;
	public InputArrays obtainSignals(InputSignal signal1,InputSignal signal2,int length,String type){
		if(type.equalsIgnoreCase("disc")){
			sinFreq=.3;
			exponent=.8;
		}else if(type.equalsIgnoreCase("cont")){
			sinFreq=.1;
			exponent=.9;
		}
		InputData input1=matchSignal(signal1,length);
		InputData input2=matchSignal(signal2,length);
		InputArrays inputs=new InputArrays();
		inputs.signal1=input1.signal;
		inputs.signal2=input2.signal;
		inputs.minPulseWidth=Math.min(input1.pulseWidth, input2.pulseWidth);
		inputs.d=input1.d||input2.d;
		return inputs;
		
}
	public static class InputArrays{
		public double[] signal1;
		public double[] signal2;
		public int minPulseWidth;
		public boolean d=false;
	}
	public static class InputData{
		public double[] signal;
		int pulseWidth;
		public boolean d=false;
	}
	
private InputData matchSignal(InputSignal signal,int length){
		InputData inputData=new InputData();
		
if(signal.type.equals("Rectangular")&signal.causality.equals("Causal")){
	inputData=RectangularCausal(length);
		}
if(signal.type.equals("Rectangular")&signal.causality.equals("Non Causal")){
	   inputData=RectangularNonCausal(length);
		}
if(signal.type.equals("Triangular")&signal.causality.equals("Causal")){
	inputData=TriangularCausal(length);
}
if(signal.type.equals("Triangular")&signal.causality.equals("Non Causal")){
	inputData=TriangularNonCausal(length);
}
if(signal.type.equals("Delta"))
		{
	inputData=Delta(length);
}

if(signal.type.equals("Shifted Delta")&signal.causality.equals("Causal")){
	inputData=ShiftedDeltaCausal(length);
}
if(signal.type.equals("Shifted Delta")&signal.causality.equals("Non Causal")){
	inputData=ShiftedDeltaNonCausal(length);
}
if(signal.type.equals("Exponential")&signal.causality.equals("Causal")){
	inputData=ExponentialCausal(length);
}
if(signal.type.equals("Exponential")&signal.causality.equals("Non Causal")){
	inputData=ExponentialNonCausal(length);
}
if(signal.type.equals("Sinc")&signal.causality.equals("Causal")){
	inputData=	SincCausal(length);
}
if(signal.type.equals("Sinc")&signal.causality.equals("Non Causal")){
	inputData=SincNonCausal(length);
}
return inputData;
 }

public InputData RectangularCausal(int length){
	InputData input=new InputData();
 double[] output=new double[length];
 for (int i=0;i<=length-1;i++)
		output[i]= 0;
 for (int i=(length-1)/2;i<=(3*(length-1)/4);i++)
		output[i]=amplitude;
 input.signal=output;
 input.pulseWidth=(int) Math.ceil((length-1)/4)+1;
 return input;
}

public InputData RectangularNonCausal(int length){
	InputData input=new InputData();
	double[] output=new double[length];
	double k=((double)length-1)/4;
	double l=Math.ceil(2.5);
	double m=Math.floor(2.5);
	
	 for (int i=0;i<=length-1;i++)
			output[i]=0;
	 for (int i=(int) Math.ceil(((double)length-1)/4);i<=(3*(length-1)/4);i++)
		 output[i]=amplitude;
	 input.signal=output;
	 input.pulseWidth=(length-1)/2+1;
	       return input;
}

public InputData TriangularCausal(int length){
	InputData input=new InputData();
	double[] output=new double[length];
	for (int i=(length-1)/2;i<=3*(length-1)/4-1;i++)
		output[i]= amplitude*(i-(length-1)/2)/(double)((length-1)/4);
	for (int i=3*(length-1)/4;i<(length-1);i++)
		if(i!=3*(length-1)/4)
		output[i]= amplitude*(1 - (1/(double)((length-1)/4))*(i-3*(length-1)/4));
		else output[i]=amplitude;
	input.signal=output;
	input.pulseWidth=(length-1)/2+1;
	return input;
}

public InputData TriangularNonCausal(int length){
	InputData input=new InputData();
	double[] output=new double[length];
	for (int i=0;i<=(length-1)/2-1;i++)
		output[i]= amplitude*i/(double)((length-1)/2);
	for (int i=(length-1)/2;i<=(length-1);i++)
		if(i!=(length-1)/2)
		output[i]= amplitude*(1 - (1/(double)((length-1)/2))*(i-(length-1)/2));
		else output[i]=amplitude;
	input.signal=output;
	input.pulseWidth=length;
	return input;
	
}

public InputData Delta(int length){
	InputData input=new InputData();
	double[] output=new double[length];
	for(int i=0;i<=(length-1)/2-1;i++)
		output[i]=0;
	output[(length-1)/2]=1;
	for(int i=(length-1)/2+1;i<=(length-1)/2-1;i++){
		output[i]=0;
	}
	input.signal=output;
	input.pulseWidth=1;
	input.d=true;
	return input;	
}


public InputData ShiftedDeltaCausal(int length){
	InputData input=new InputData();
	double[] output=new double[length];
	for (int i=0;i<=length-1;i++)
		output[i]=0;
	output[(length-1)/2+length/10]=1;
	input.signal=output;
	input.pulseWidth=1;
	input.d=true;
	return input;	
}

public InputData ShiftedDeltaNonCausal(int length){
	InputData input=new InputData();
	double[] output=new double[length];
	for (int i=0;i<=(length-1);i++)
		output[i]=0;
	output[(length-1)/2-length/10]=1;
	input.signal=output;
	input.pulseWidth=1;
	input.d=true;
	return input;	
}

public InputData ExponentialCausal(int length){
	InputData input=new InputData();
	double[] output=new double[length];
	for (int i=(length-1)/2;i<=3*(length-1)/4-1;i++)
		output[i]=amplitude*Math.pow(2*exponent/3,3*(length-1)/4-i) ;
	for (int i=3*(length-1)/4;i<(length-1);i++)		
		output[i]= amplitude*Math.pow(2*exponent/3,i-3*(length-1)/4);
	input.signal=output;
	input.pulseWidth=(length-1)/2+1;
	return input;	
}

public InputData ExponentialNonCausal(int length){
	InputData input=new InputData();
	double[] output=new double[length];
	for (int i=0;i<=(length-1)/2-1;i++)
		output[i]=amplitude*Math.pow(exponent,(length-1)/2-i) ;
	for (int i=(length-1)/2;i<=(length-1);i++)
		
		output[i]= amplitude*Math.pow(exponent,i-(length-1)/2);
	input.signal=output;
	input.pulseWidth=length;
	return input;
}

public InputData SincCausal(int length){
	InputData input=new InputData();
	double[] output=new double[length];
	
	
	
	
	for (int i=0;i<=(length-1)/4;i++){
		if(i!=0){
		output[i+3*(length-1)/4] =amplitude*Math.sin(2*sinFreq*Math.PI*(i))/(Math.PI*i*2*sinFreq);
		output[3*(length-1)/4 - i] = output[(i + 3*(length-1)/4)];
		}
		else 
			output[i+3*(length-1)/4]=amplitude;
		}
	input.signal=output;
	input.pulseWidth=(length-1)/2+1;
	return input;	
}

public InputData SincNonCausal(int length){
	InputData input=new InputData();
	double[] output=new double[length];
	
	for (int i=0;i<=(length-1)/2;i++){
		if(i!=0){
		output[i+(length-1)/2] =amplitude*Math.sin(sinFreq*Math.PI*i)/(Math.PI*i*sinFreq);
		output[(length-1)/2 - i] = output[(i + (length-1)/2)];
		}
		else 
			output[i+(length-1)/2]=amplitude;	
		
		}
	input.signal=output;
	input.pulseWidth=length;
	return input;	
}


	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
}


