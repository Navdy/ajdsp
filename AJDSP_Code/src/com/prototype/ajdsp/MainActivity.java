package com.prototype.ajdsp;

import java.util.ArrayList;
import java.util.Collections;

import android.os.Bundle;
import com.bugsense.trace.BugSenseHandler;
import com.prototype.ajdsp.PartView.ConnectionData;

import android.app.Activity;
import android.app.KeyguardManager;
import android.app.KeyguardManager.KeyguardLock;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.Point;
import android.view.Menu;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;



public class MainActivity extends Activity implements OnClickListener {
	RelativeLayout layout;
	Button start;
	ImageView imageView;
	int displayWidth;
	int displayHeight;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
		setContentView(R.layout.activity_main);
		KeyguardManager keyguardManager = (KeyguardManager)getSystemService(Activity.KEYGUARD_SERVICE);
		KeyguardLock lock = keyguardManager.newKeyguardLock(KEYGUARD_SERVICE);
		lock.disableKeyguard();
		BugSenseHandler.setup(this, "91aa2337");
		WindowManager mWinMgr = (WindowManager)this.getSystemService(Context.WINDOW_SERVICE);
		Point displaySize=new Point();
		displayHeight=mWinMgr.getDefaultDisplay().getHeight();
		displayWidth= mWinMgr.getDefaultDisplay().getWidth();
       	layout= (RelativeLayout) findViewById(R.id.opening_layout);
		layout.setBackgroundColor(Color.WHITE);
		imageView= (ImageView) findViewById(R.id.logo);
		imageView.setOnClickListener(this);
	}
	@Override
	public void onStop() {
		super.onStop();	
		// lock.disableKeyg();
		finish();

	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		getMenuInflater().inflate(R.menu.activity_main, menu);
		return true;
	}
	@Override
	public void onBackPressed() {
		// do nothing.
	}

	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		if (v == imageView){
			Intent i = new Intent(this, SplashView.class); //Splash View class instead of start view
			startActivity(i);
			PartsController partsController =new PartsController(this,displayWidth,displayHeight);
		}

	}
}
