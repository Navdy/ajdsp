package com.prototype.ajdsp;

import com.prototype.ajdsp.PartObject.PinSignalTypes;
import com.prototype.ajdsp.PartObject.PinTypes;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.view.MotionEvent;
import android.view.View;
import android.view.WindowManager;
import android.view.View.OnTouchListener;
import android.view.Window;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

public class SNRMenu extends Activity implements OnClickListener {
	RelativeLayout layout;
	RelativeLayout topbar;
	Button addParts;
	EditText upSamplerRate;
	TextView snrText;

	Button partList;
	public static int upSamplerNumber=0;
	Bundle blockValues=new Bundle();
	 Bundle inputTopBundle;
	 Bundle inputBottomBundle;
	private OnTouchListener otl = new OnTouchListener() {
     	

		@Override
		public boolean onTouch(View v, MotionEvent event) {
			// TODO Auto-generated method stub
			return false;
		}
      };

	Boolean create=true;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
		//requestWindowFeature(Window.FEATURE_CUSTOM_TITLE);
		setContentView(R.layout.upsampler_menu);
		//getWindow().setFeatureInt(Window.FEATURE_CUSTOM_TITLE, R.layout.row);
		layout=(RelativeLayout)findViewById(R.id.upSamplerMenu);
		layout.setBackgroundColor(Color.WHITE);
		 
        partList=(Button) findViewById(R.id.back_select_parts);
		partList.setOnClickListener(this);
		
		upSamplerRate=(EditText)findViewById(R.id.upSamplerRate);
        snrText=(TextView)findViewById(R.id.upSamplerText);
        snrText.setText("SNR in DB");
        addParts=(Button)findViewById(R.id.add_part);
        addParts.setVisibility(View.GONE); 

       
         upSamplerRate.setOnTouchListener(otl);

		Bundle extras = getIntent().getExtras();
	     
	     
	     if(extras !=null)
		 {
	        blockValues = extras.getBundle("blockValues");
	    
	     
	      
	        inputTopBundle=blockValues.getBundle("leftTopPin");
	        
	        inputBottomBundle=blockValues.getBundle("leftBottomPin");
	        double [] signalMagTop=inputTopBundle.getDoubleArray("signalMag");
	        double [] signalMagBottom=inputBottomBundle.getDoubleArray("signalMag");
	        SNRCalc snrCalc=new SNRCalc();
	       Double snr= snrCalc.snr(signalMagTop,signalMagBottom );
	       upSamplerRate.setText(snr.toString());
	      
			addParts.setText("Update");
			partList.setText("Back");
	    } else{
		
		upSamplerRate.setText("0");
	    }

	}

	@Override
	public void onBackPressed() {
		
			/*Intent i = new Intent(this, StartView.class);
			startActivity(i);*/
		
	}
	@Override
	 public void onStop() {		
			 super.onStop();					
			 finish();
					 }
	
	 
	@Override
	public void onClick(View v) {
		

		if(v==partList){
			
				Intent i = new Intent(this, StartView.class);
				startActivity(i);
			
		}

	}

}
