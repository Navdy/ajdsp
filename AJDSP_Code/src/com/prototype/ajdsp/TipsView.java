package com.prototype.ajdsp;


import android.app.Activity;
import android.content.Intent;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.widget.Button;
import android.widget.RelativeLayout;
import android.widget.TextView;


public class TipsView extends Activity implements OnClickListener{
	
RelativeLayout layout;	
TextView generalIntro;
TextView convDemo;
TextView filterDesign;
TextView tipsText;
Button back;
Button add;
String returnClass;
Bundle inputValues;
	
	
	@Override
    public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		//requestWindowFeature(Window.FEATURE_CUSTOM_TITLE);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		Bundle extras = getIntent().getExtras();
		setContentView(R.layout.tips_view);
		//getWindow().setFeatureInt(Window.FEATURE_CUSTOM_TITLE, R.layout.row);
		layout=(RelativeLayout)findViewById(R.id.tipsPage);
		layout.setBackgroundColor(Color.BLACK);
        layout.setPadding(0, 0, 0, 0);
		
		Bundle inputValues = new Bundle();
		
		
		tipsText = (TextView)findViewById(R.id.tipsText);
		generalIntro = (TextView)findViewById(R.id.introLink);
		generalIntro.setOnClickListener(this);
		convDemo = (TextView)findViewById(R.id.convLink);
		convDemo.setOnClickListener(this);
		filterDesign = (TextView)findViewById(R.id.filtLink);
		filterDesign.setOnClickListener(this);
		
		back=(Button) findViewById(R.id.back_select_parts_black);
		back.setOnClickListener(this);
		

		if(extras !=null)
		{			
			inputValues = extras.getBundle("blockValues");
			returnClass=inputValues.getString("returnClass");
		}
        
	}

	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		if (v == generalIntro){
			String url = "http://www.youtube.com/watch?v=-z7QBbd0es8";
			Intent i = new Intent(Intent.ACTION_VIEW);
			i.setData(Uri.parse(url));
			startActivity(i);
		 }
		else
			if (v == convDemo){
				String url = "http://www.youtube.com/watch?v=AYw4PC61iQ8&list=PLDE6647E29748AADE";
				Intent i = new Intent(Intent.ACTION_VIEW);
				i.setData(Uri.parse(url));
				startActivity(i);
			}
		else
			if (v == filterDesign){
			String url = "http://www.youtube.com/watch?v=O6v7q8vbgPs&list=PLDE6647E29748AADE";
			Intent i = new Intent(Intent.ACTION_VIEW);
			i.setData(Uri.parse(url));
			startActivity(i);	
			}
		else
			if(v == back){
				
				if(returnClass.equalsIgnoreCase("StartView")){
					Intent i = new Intent(this, StartView.class);					   
					startActivity(i);
				}else
					
					if(returnClass.equalsIgnoreCase("SplashView")){
						Intent i = new Intent(this, SplashView.class);					   
						startActivity(i);
					}	
		
			}
	}
	
	
	 @Override
	 public void onStop() {
		 super.onStop();	
				finish();
				
	 }
	 
	 @Override
	    public void onBackPressed() {
	    /*	Intent i = new Intent(this, MainActivity.class);
	        startActivity(i);*/
	    }
}
