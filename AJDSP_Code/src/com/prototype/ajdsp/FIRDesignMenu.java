package com.prototype.ajdsp;

import java.text.DecimalFormat;

import com.prototype.ajdsp.PartObject.PinSignalTypes;
import com.prototype.ajdsp.PartObject.PinTypes;
import com.prototype.ajdsp.PmlMenu.MyOnItemSelectedListener;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.AdapterView.OnItemSelectedListener;

public class FIRDesignMenu extends Activity implements OnClickListener {

	RelativeLayout layout;
	RelativeLayout topbar;
	Button addParts;
	Button back;
	EditText order;
	EditText beta;
	EditText W1;
	EditText W2;
	String orderString;
	Bundle inputValues;
	TextView W1Text;
	TextView W2Text;
	TextView betaText;
	TextView cutOffText;
	Boolean create=true;
	Boolean update=false;



	Spinner filterTypeSpinner;
	Spinner windowTypeSpinner;


	public static int firDesignNumber=0;
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		//requestWindowFeature(Window.FEATURE_CUSTOM_TITLE);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
		setContentView(R.layout.fir_design_layout);
		//getWindow().setFeatureInt(Window.FEATURE_CUSTOM_TITLE, R.layout.row);
		layout=(RelativeLayout)findViewById(R.id.firDesignMenu);
		layout.setBackgroundColor(Color.WHITE);
		


		filterTypeSpinner = (Spinner) findViewById(R.id.firDesignFilterType);
		ArrayAdapter<CharSequence> filterTypeAdapter = ArrayAdapter.createFromResource(
				this, R.array.filter_type_array_kaiser, android.R.layout.simple_spinner_item);
		filterTypeAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
	//	filterTypeSpinner.setBackgroundColor(Color.WHITE);
		filterTypeSpinner.setAdapter(filterTypeAdapter);
		filterTypeSpinner.setOnItemSelectedListener(new FilterTypeOnItemSelectedListener());

		windowTypeSpinner = (Spinner) findViewById(R.id.firDesignWindowType);
		ArrayAdapter<CharSequence> windowTypeAdapter = ArrayAdapter.createFromResource(
				this, R.array.window_type_array, android.R.layout.simple_spinner_item);
		windowTypeAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		//windowTypeSpinner.setBackgroundColor(Color.WHITE);
		windowTypeSpinner.setAdapter(windowTypeAdapter);
		windowTypeSpinner.setOnItemSelectedListener(new WindowTypeOnItemSelectedListener());



		W1=(EditText)findViewById(R.id.W1firDesign);
		W2=(EditText)findViewById(R.id.W2firDesign);
		order=(EditText) findViewById(R.id.firDesignOrder);
		beta=(EditText) findViewById(R.id.firDesignBeta);


		W2Text=(TextView)findViewById(R.id.W2firDesignText);
		W1Text=(TextView)findViewById(R.id.W1firDesignText);
		betaText=(TextView)findViewById(R.id.firDesignBetaText);
		cutOffText=(TextView)findViewById(R.id.cutoffFirDesignText);
		back=(Button) findViewById(R.id.back_select_parts);
		back.setOnClickListener(this);
		TextView title= (TextView)findViewById(R.id.Title);
		title.setText("FIR Design");
	addParts=(Button) findViewById(R.id.add_part);
		addParts.setOnClickListener(this);
		


		Bundle extras = getIntent().getExtras(); 
		   if(extras !=null)
			 {
			 inputValues = extras.getBundle("blockValues");
			 filterTypeSpinner.setSelection(inputValues.getInt("filterType"));
			 windowTypeSpinner.setSelection((int) inputValues.getLong("windowType"));
			 Integer orderValue=inputValues.getInt("order");
			 Double betaValue= inputValues.getDouble("beta");
			 Double W1Value= inputValues.getDouble("W1");
			 Double W2Value=inputValues.getDouble("W2");
			 update=true;
			 create=false;
			 addParts.setText("Update");
			 back.setText("Back");
			 
			 order.setText(orderValue.toString());
			 beta.setText(betaValue.toString());
			 W1.setText(W1Value.toString());
			 W2.setText(W2Value.toString());
			

			 
			 }
		   else{


		filterTypeSpinner.setSelection(0);
		windowTypeSpinner.setSelection(0);

		Integer orderValue=8;
		Double betaValue=3.0;
		//Double W1Value= 0.0;
		//Double W2Value=0.2;


		order.setText(orderValue.toString());
		beta.setText(betaValue.toString());
		//W1.setText(W1Value.toString());
		//W2.setText(W2Value.toString());

		   }

		



	}
	@Override
	 public void onStop() {
		  super.onStop();		
		  finish();				
	 }
	public class FilterTypeOnItemSelectedListener implements OnItemSelectedListener {

		public void onItemSelected(AdapterView<?> parent,
				View view, int pos, long id) {
			W2Text.setVisibility(View.GONE);
			W1Text.setVisibility(View.GONE);
			W2.setVisibility(View.GONE);
			W1.setVisibility(View.GONE);

			switch((int)id){
			case 0:
				W2Text.setVisibility(View.VISIBLE);
				W2.setVisibility(View.VISIBLE);
				if((!update)||(inputValues.getInt("filterType")!=0)){
					W1.setText("0.0");
					W2.setText(".2");
					}else{
						update=false;
					}
				break;

			case 1:
				W1Text.setVisibility(View.VISIBLE);
				W1.setVisibility(View.VISIBLE);
				if((!update)||(inputValues.getInt("filterType")!=1)){
					W1.setText(".2");
					W2.setText("1.0");
					}else{
						update=false;
					}
				cutOffText.setText("Cut Off Frequency(W1,1)");
				break;
			case 2:
				W1Text.setVisibility(View.VISIBLE);
				W1.setVisibility(View.VISIBLE);
				W2Text.setVisibility(View.VISIBLE);
				W2.setVisibility(View.VISIBLE);
				cutOffText.setText("Cut Off Frequency(W1,W2)");			
				if((!update)||(inputValues.getInt("filterType")!=2)){
					W1.setText(".2");
					W2.setText(".4");
					}else{
						update=false;
					}
				break;
			case 3:
				W1Text.setVisibility(View.VISIBLE);
				W1.setVisibility(View.VISIBLE);
				W2Text.setVisibility(View.VISIBLE);
				W2.setVisibility(View.VISIBLE);
				cutOffText.setText("Cut Off Frequency(W1,W2)");	
				if((!update)||(inputValues.getInt("filterType")!=3)){
					W1.setText(".2");
					W2.setText(".4");
					}else{
						update=false;
					}
				break;

			default:
				break;
			}
		}

		public void onNothingSelected(AdapterView parent) {
			// Do nothing.
		}
	}
	public class WindowTypeOnItemSelectedListener implements OnItemSelectedListener {

		public void onItemSelected(AdapterView<?> parent,
				View view, int pos, long id) {
			beta.setVisibility(View.GONE);
			betaText.setVisibility(View.GONE);
			switch((int)id){
			case 5:
				beta.setVisibility(View.VISIBLE);
				betaText.setVisibility(View.VISIBLE);
				break;
			default:
				break;
			}
		}

		public void onNothingSelected(AdapterView parent) {
			// Do nothing.
		}
	}

	@Override
	public void onBackPressed() {
		/*if(create){
			Intent i = new Intent(this, PartList.class);
			startActivity(i);}
		else {
			Intent i = new Intent(this, StartView.class);
			startActivity(i);
		}*/
	}
	private Integer valueInt(String input){
		  if (input.equalsIgnoreCase("")){
			  return 0;
		  }
		  else return Integer.parseInt(input);
	  }
	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		if (v == addParts){
			
			Bundle firDesignInput=new Bundle();


			orderString=order.getText().toString();
		
			if(orderString.length()>3)
			Toast.makeText(getApplicationContext(), "Max Order of the Filter is 64", Toast.LENGTH_LONG).show();

			else if(valueInt(orderString)>64) {			
				Toast.makeText(getApplicationContext(), "Max Order of the Filter is 64", Toast.LENGTH_LONG).show();
			}else{
				String betaString=beta.getText().toString();
				String W1String=W1.getText().toString();
				String W2String=W2.getText().toString();
				int filterType=(int)filterTypeSpinner.getSelectedItemId();
				double w1=value(W1String);
				double w2=value(W2String);
				if(filterType==0)
					w1=0;
				else if(filterType==1)
					w2=1;
if(((filterType==2||filterType==3))&&(w1>w2)){
	Toast.makeText(getApplicationContext(), "Non-Valid inputs.Allowable inputs conform to:W1 <= W2", Toast.LENGTH_LONG).show();
}else if((w1>1)||(w2>1)){
	Toast.makeText(getApplicationContext(), "Cut-off frequencies must be less than 1", Toast.LENGTH_LONG).show();
}else{
				firDesignInput.putInt("filterType", filterType);
				firDesignInput.putLong("windowType", windowTypeSpinner.getSelectedItemId());

				firDesignInput.putInt("order" ,valueInt(orderString));
				firDesignInput.putDouble("beta",value(betaString));
				
				
				firDesignInput.putDouble("W1",w1);
				firDesignInput.putDouble("W2",w2);
				

				

				

				FIRDesignCalc firDesignCalc=new FIRDesignCalc();
				Bundle filterCoeff =firDesignCalc.getFilterCoeff(firDesignInput);
				filterCoeff.putInt("order", valueInt(orderString));
				firDesignInput.putBundle("topPin", filterCoeff);
				
				if(create){
					firDesignNumber=firDesignNumber+1;
				PinSignalTypes firSigType=new PinSignalTypes();
				firSigType.topPin="frequency";
				firDesignInput.putSerializable("pinSignalType", firSigType);
				PinTypes types=new PinTypes();
				types.topPin=true;
				firDesignInput.putSerializable("PinTypes",types);
				firDesignInput.putString("title", "FIR Design" +" "+firDesignNumber);			
				PartsController.createBlock(firDesignInput);
				} else {

					firDesignInput.putString("title", inputValues.getString("title"));
					PartsController.updateBlock(firDesignInput);
				}
				
			    firDesignInput.putStringArray("plotValues", getValues(filterCoeff));
			    firDesignInput.putString("returnClass", "FIRDesignMenu");
				Intent i = new Intent(this, DisplayValues.class);
				i.putExtra("blockValues", firDesignInput);
				startActivity(i);
				}
			}
		}
		else if (v == back){

			if(create){
				Intent i = new Intent(this, PartList.class);
				startActivity(i);}
				 else {
					 Intent i = new Intent(this, StartView.class);
						startActivity(i);
				 }
		}
		
	}
	public String [] getValues(Bundle bundle){		
		int order=bundle.getInt("order");
		
		String [] valueArray= new String[(int)order+3];
		valueArray[0]="Order: "+order;
		valueArray[1]="Coeff: ";
		DecimalFormat threeDForm = new DecimalFormat("#.###");
		double [] bCoeff=bundle.getDoubleArray("filterCoeffB");
		for(int i=2;i<valueArray.length;i++){
			valueArray[i]="b"+(i-2)+": "+threeDForm.format(bCoeff[i-2]);
		}
		return valueArray;
		
	}
	 private Double value(String input){
		  if (input.equalsIgnoreCase("")){
			  return 0.0;
		  }
		  else return Double.parseDouble(input);
	  }
	  

}
