package com.prototype.ajdsp;

import com.prototype.ajdsp.PartObject.PinSignalTypes;
import com.prototype.ajdsp.PartObject.PinTypes;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;


public class FilterCoeffMenu extends Activity implements OnClickListener{
	RelativeLayout mainLayout;
	RelativeLayout topBar;
	EditText a0;
	EditText a1;
	EditText a2;
	EditText a3;
	EditText a4;
	EditText a5;
	EditText a6;
	EditText a7;
	EditText a8;
	EditText a9;
	EditText a10;
	EditText b0;
	EditText b1;
	EditText b2;
	EditText b3;
	EditText b4;
	EditText b5;
	EditText b6;
	EditText b7;
	EditText b8;
	EditText b9;
	EditText b10;
	EditText gain;
	EditText pulseWidth;
	EditText period;
	EditText expBase;
	EditText freq;
	Button add;
	Button partList;
	Button reset;
	Double a0Value=1.0; 
	 Double a1Value=0.0;
	 Double a2Value=0.0;
	 Double a3Value=0.0;
	 Double a4Value=0.0;
	 Double a5Value=0.0;
	 Double a6Value=0.0;
	 Double a7Value=0.0;
	 Double a8Value=0.0;
	 Double a9Value=0.0;
	 Double a10Value=0.0;
	 Double b0Value=1.0;
	 Double b1Value=0.0;
	 Double b2Value=0.0;
	 Double b3Value=0.0;
	 Double b4Value=0.0;
	 Double b5Value=0.0;
	 Double b6Value=0.0;
	 Double b7Value=0.0;
	 Double b8Value=0.0;
	 Double b9Value=0.0;
	 Double b10Value=0.0;
	 Bundle inputBundle;
	 Boolean create=true;
	public static int filterCoeffNumber=0;
	@Override
	  public void onCreate(Bundle savedInstanceState) {
      super.onCreate(savedInstanceState);
      //requestWindowFeature(Window.FEATURE_CUSTOM_TITLE);
      requestWindowFeature(Window.FEATURE_NO_TITLE);
      this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
     setContentView(R.layout.filter_coeff_menu);
     //getWindow().setFeatureInt(Window.FEATURE_CUSTOM_TITLE, R.layout.row);
     mainLayout=(RelativeLayout)findViewById(R.id.filter_coeff_menu_main);
     mainLayout.setBackgroundColor(Color.WHITE);
    
     partList=(Button) findViewById(R.id.back_select_parts_filter_coeff);
		partList.setOnClickListener(this);
		
	add=(Button) findViewById(R.id.add_part_filter_coeff);
		add.setOnClickListener(this);
		reset=(Button) findViewById(R.id.reset_filter_coeff);
		reset.setOnClickListener(this);
		


     a0=(EditText) findViewById(R.id.a0);
     
     a1=(EditText) findViewById(R.id.a1);
     a2=(EditText) findViewById(R.id.a2);
	 a3=(EditText) findViewById(R.id.a3);
	 a4=(EditText) findViewById(R.id.a4);
	 a5=(EditText) findViewById(R.id.a5);
	 a6=(EditText) findViewById(R.id.a6);
     a7=(EditText) findViewById(R.id.a7);
     a8=(EditText) findViewById(R.id.a8);
     a9=(EditText) findViewById(R.id.a9);
     a10=(EditText) findViewById(R.id.a10);
	 b0=(EditText) findViewById(R.id.b0);
	 b1=(EditText) findViewById(R.id.b1);
     b2=(EditText) findViewById(R.id.b2);
     b3=(EditText) findViewById(R.id.b3);
	 b4=(EditText) findViewById(R.id.b4);
	 b5=(EditText) findViewById(R.id.b5);
	 b6=(EditText) findViewById(R.id.b6);
	 b7=(EditText) findViewById(R.id.b7);
	 b8=(EditText) findViewById(R.id.b8);
     b9=(EditText) findViewById(R.id.b9);
     b10=(EditText) findViewById(R.id.b10);
     
    
     
     
     Bundle extras = getIntent().getExtras(); 
     if(extras !=null)
	 {
	inputBundle= extras.getBundle("blockValues");
	Bundle inputValues= inputBundle.getBundle("topPin");
	double[] coEffB=inputValues.getDoubleArray("filterCoeffB");
	double[] coEffA=inputValues.getDoubleArray("filterCoeffA");
	add.setText("Update");
	partList.setText("Back");
	 
	 a0.setText(Double.toString(coEffA[0]));
	 a1.setText(Double.toString(coEffA[1]));
	 a2.setText(Double.toString(coEffA[2]));
	 a3.setText(Double.toString(coEffA[3]));
	 a4.setText(Double.toString(coEffA[4]));
	 a5.setText(Double.toString(coEffA[5]));
	 a6.setText(Double.toString(coEffA[6]));
	 a7.setText(Double.toString(coEffA[7]));
	 a8.setText(Double.toString(coEffA[8]));
	 a9.setText(Double.toString(coEffA[9]));
	 a10.setText(Double.toString(coEffA[10]));
	 b0.setText(Double.toString(coEffB[0]));
	 b1.setText(Double.toString(coEffB[1]));
	 b2.setText(Double.toString(coEffB[2]));
	 b3.setText(Double.toString(coEffB[3]));
	 b4.setText(Double.toString(coEffB[4]));
	 b5.setText(Double.toString(coEffB[5]));
	 b6.setText(Double.toString(coEffB[6]));
	 b7.setText(Double.toString(coEffB[7]));
	 b8.setText(Double.toString(coEffB[8]));
	 b9.setText(Double.toString(coEffB[9]));
	 b10.setText(Double.toString(coEffB[10]));
	 create =false;
	
 }else{
 	
   
	
     
	 a0.setText(a0Value.toString());
	 a1.setText(a1Value.toString());
	 a2.setText(a2Value.toString());
	 a3.setText(a3Value.toString());
	 a4.setText(a4Value.toString());
	 a5.setText(a5Value.toString());
	 a6.setText(a6Value.toString());
	 a7.setText(a7Value.toString());
	 a8.setText(a8Value.toString());
	 a9.setText(a9Value.toString());
	 a10.setText(a10Value.toString());
	 b0.setText(b0Value.toString());
	 b1.setText(b1Value.toString());
	 b2.setText(b2Value.toString());
	 b3.setText(b3Value.toString());
	 b4.setText(b4Value.toString());
	 b5.setText(b5Value.toString());
	 b6.setText(b6Value.toString());
	 b7.setText(b7Value.toString());
	 b8.setText(b8Value.toString());
	 b9.setText(b9Value.toString());
	 b10.setText(b10Value.toString());
 }
 }
	@Override
	 public void onStop() {
		        super.onStop();	
				finish();
				
	 }
    
	@Override
	public void onBackPressed() {
	/*	if(create){
			Intent i = new Intent(this, PartList.class);
			startActivity(i);}
		else {
			Intent i = new Intent(this, StartView.class);
			startActivity(i);
		}*/
	}
	@Override
	public void onClick(View v) {
		if(v==add){
			
			Bundle filterCoeff=new Bundle();
			double [] coeffB=new double[11];
			double [] coeffA=new double[11];
			coeffB[0]=parseDouble(b0.getText().toString());
			coeffB[1]=parseDouble(b1.getText().toString());
			coeffB[2]=parseDouble(b2.getText().toString());
			coeffB[3]=parseDouble(b3.getText().toString());
			coeffB[4]=parseDouble(b4.getText().toString());
			coeffB[5]=parseDouble(b5.getText().toString());
			coeffB[6]=parseDouble(b6.getText().toString());
			coeffB[7]=parseDouble(b7.getText().toString());
			coeffB[8]=parseDouble(b8.getText().toString());
			coeffB[9]=parseDouble(b9.getText().toString());
			coeffB[10]=parseDouble(b10.getText().toString());
			coeffA[0]=parseDouble(a0.getText().toString());
			coeffA[1]=parseDouble(a1.getText().toString());
			coeffA[2]=parseDouble(a2.getText().toString());
			coeffA[3]=parseDouble(a3.getText().toString());
			coeffA[4]=parseDouble(a4.getText().toString());
			coeffA[5]=parseDouble(a5.getText().toString());
			coeffA[6]=parseDouble(a6.getText().toString());
			coeffA[7]=parseDouble(a7.getText().toString());
			coeffA[8]=parseDouble(a8.getText().toString());
			coeffA[9]=parseDouble(a9.getText().toString());
			coeffA[10]=parseDouble(a10.getText().toString());
			
			filterCoeff.putDoubleArray("filterCoeffA", coeffA);
			filterCoeff.putDoubleArray("filterCoeffB", coeffB);
			Bundle filterInput=new Bundle();
			filterInput.putBundle("topPin", filterCoeff);
			if(create){ 
			filterCoeffNumber++;
			
		
			PinTypes types=new PinTypes();
			types.topPin=true;	
			
			filterInput.putSerializable("PinTypes",types);
			
			PinSignalTypes filterCoeffSigType=new PinSignalTypes();
			 filterCoeffSigType.topPin="frequency";
					
		    filterInput.putSerializable("pinSignalType", filterCoeffSigType);
			filterInput.putString("title", "Filter Coeff" +" "+filterCoeffNumber);
			
			PartsController.createBlock(filterInput);
			}
			else{
				filterInput.putString("title", inputBundle.getString("title"));		
				
				PartsController.updateBlock(filterInput);
			}
			Intent i = new Intent(this, StartView.class);
			startActivity(i);
			
		 }else if(v==partList){
			 if(create){
			Intent i = new Intent(this, PartList.class);
			startActivity(i);}
			 else {
				 Intent i = new Intent(this, StartView.class);
					startActivity(i);
			 }
		}else if(v==reset){
			a0.setText(a0Value.toString());
			 a1.setText(a1Value.toString());
			 a2.setText(a2Value.toString());
			 a3.setText(a3Value.toString());
			 a4.setText(a4Value.toString());
			 a5.setText(a5Value.toString());
			 a6.setText(a6Value.toString());
			 a7.setText(a7Value.toString());
			 a8.setText(a8Value.toString());
			 a9.setText(a9Value.toString());
			 a10.setText(a10Value.toString());
			 b0.setText(b0Value.toString());
			 b1.setText(b1Value.toString());
			 b2.setText(b2Value.toString());
			 b3.setText(b3Value.toString());
			 b4.setText(b4Value.toString());
			 b5.setText(b5Value.toString());
			 b6.setText(b6Value.toString());
			 b7.setText(b7Value.toString());
			 b8.setText(b8Value.toString());
			 b9.setText(b9Value.toString());
			 b10.setText(b10Value.toString());
			 
		}
		
	}
	public double parseDouble(String valueEntry){
		double value=0;
		if(!valueEntry.equalsIgnoreCase("")||!valueEntry.equalsIgnoreCase("-"))
			value=Double.parseDouble(valueEntry);
		return value;
	}
}
