package com.prototype.ajdsp;



import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;


import com.prototype.ajdsp.PoleZeroObject.PoleZeroCoordinates;

import android.content.Context;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Rect;
import android.graphics.Paint.Style;

import android.util.AttributeSet;
import android.view.Gravity;
import android.view.MotionEvent;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.view.View;
import android.view.WindowManager;

public class PZ2CoeffView extends SurfaceView  implements SurfaceHolder.Callback{
	private static ZeroPolePositionListener mPositionListener;
	private PZ2CoeffThread _thread;    
	public  List<ConjugatePoleZero> zeroList=Collections.synchronizedList(new ArrayList<ConjugatePoleZero>()); 
	public List<ConjugatePoleZero> poleList=Collections.synchronizedList(new ArrayList<ConjugatePoleZero>());
	
	public static Bitmap zeroBitmap;
	public static Bitmap poleBitmap;
	public static int height;
	public static int width;
	
	private int selectedPole=-1,selectedZero=-1;
	private int zeroIndex=0,poleIndex=0;
	public  int zeroNo=0,poleNo=0;
	public  double[] poleArrayX= new double[20];
	public double [] zeroArrayX= new double[20];
	public  double[] poleArrayY= new double[20];
	public double [] zeroArrayY= new double[20];
	public boolean isFirst=false,isTopSelected=false,isBottomSelected=false;
	private int firstPositionX=0,firstPositionY=0;
	private int touchPositionX=0,touchPositionY=0;
	public static double xCoord=0.0,yCoord=0.0;
	public PoleZeroDeleteDialog poleZeroDelete;
	public boolean poleZeroDeleteChosen=false;
	Paint textPaint=new Paint();
	Paint circlePaint=new Paint();
	Paint rectPaint=new Paint();
	Paint axesPaint=new Paint();
	
	
	private boolean ifMove=false;
	interface ZeroPolePositionListener {
		void onZeropoleListChange(double[] zeroX,double[] zeroY,double[] poleX,double[] poleY);
	}
	public PZ2CoeffView(final Context context) {

		super(context,null,0);   
		zeroBitmap=BitmapFactory.decodeResource(getResources(), R.drawable.zero);
		poleBitmap=BitmapFactory.decodeResource(getResources(), R.drawable.pole);
		getHolder().addCallback(this);
		mPositionListener = null;
		
		
		setFocusable(true);
		setOnLongClickListener(new OnLongClickListener() {
			@Override
			public boolean onLongClick(View v) {
				if(ifMove)
					return false;
				Rect topPartSelector=new Rect();         	  
				Rect bottomPartSelector=new Rect();        


				poleZeroDelete = new PoleZeroDeleteDialog(getContext());
				int selectedZeroUp=-1;
				int selectedPoleUp=-1;
				int zeroIndexUp=0;
				int poleIndexUp=0;
				for(ConjugatePoleZero graphic:zeroList){                       	 
					topPartSelector.set(graphic.topObject.getPoleZeroCoordinates().getLeftX(),graphic.topObject.getPoleZeroCoordinates().getTopY() , graphic.topObject.getPoleZeroCoordinates().getRightX(), graphic.topObject.getPoleZeroCoordinates().getBottomY());
					bottomPartSelector.set(graphic.bottomObject.getPoleZeroCoordinates().getLeftX(),graphic.bottomObject.getPoleZeroCoordinates().getTopY() , graphic.bottomObject.getPoleZeroCoordinates().getRightX(), graphic.bottomObject.getPoleZeroCoordinates().getBottomY());

					if((topPartSelector.contains(touchPositionX, touchPositionY))||(bottomPartSelector.contains(touchPositionX, touchPositionY))){   
						selectedZeroUp=zeroIndexUp;                                   
						zeroIndexUp=0;
						break;
					}
					zeroIndexUp=zeroIndexUp+1;  
				}  


				if(selectedZeroUp==-1){
					for(ConjugatePoleZero graphic:poleList){                     	
						topPartSelector.set(graphic.topObject.getPoleZeroCoordinates().getLeftX(),graphic.topObject.getPoleZeroCoordinates().getTopY() , graphic.topObject.getPoleZeroCoordinates().getRightX(), graphic.topObject.getPoleZeroCoordinates().getBottomY());
						bottomPartSelector.set(graphic.bottomObject.getPoleZeroCoordinates().getLeftX(),graphic.bottomObject.getPoleZeroCoordinates().getTopY() , graphic.bottomObject.getPoleZeroCoordinates().getRightX(), graphic.bottomObject.getPoleZeroCoordinates().getBottomY());
						if((topPartSelector.contains(touchPositionX, touchPositionY))||((bottomPartSelector.contains(touchPositionX, touchPositionY)))){                    			 
							selectedPoleUp=poleIndexUp;                           
							poleIndexUp=0;
							break;
						}
						poleIndexUp=poleIndexUp+1;
					}
				}
				poleZeroDelete.setParameters(selectedZeroUp, selectedPoleUp);

				poleZeroDelete.getWindow().setFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCH_MODAL, WindowManager.LayoutParams.FLAG_NOT_TOUCH_MODAL);

				poleZeroDelete.setCancelable(true);

				poleZeroDelete.show();  

				WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
				lp.copyFrom(poleZeroDelete.getWindow().getAttributes());


				lp.x=(int)(touchPositionX-width/2);
				lp.y=(int)touchPositionY+lp.height;
				lp.gravity=Gravity.TOP;
				lp.flags &= ~WindowManager.LayoutParams.FLAG_DIM_BEHIND;




				poleZeroDeleteChosen=true;
				poleZeroDelete.getWindow().setAttributes(lp);




				return true;
			}

		});
	}

	public PZ2CoeffView(final Context context, final AttributeSet attrs) {
		this(context, attrs, 0);
	}
	public PZ2CoeffView(final Context context, final AttributeSet attrs,
			final int defStyle) {
		super(context, attrs, defStyle);   
		zeroBitmap=BitmapFactory.decodeResource(getResources(), R.drawable.zero);
		poleBitmap=BitmapFactory.decodeResource(getResources(), R.drawable.pole);
		// poleZeroList=new PoleZeroList();
		//poleList= poleZeroList.poleList;
		//zeroList=poleZeroList.zeroList;
		getHolder().addCallback(this);
		mPositionListener = null;
		_thread = new PZ2CoeffThread(getHolder(), this);
		setFocusable(true);
		setOnLongClickListener(new OnLongClickListener() {
			@Override
			public boolean onLongClick(View v) {
				if(ifMove)
					return false;
				Rect topPartSelector=new Rect();         	  
				Rect bottomPartSelector=new Rect();    

				PoleZeroDeleteDialog poleZeroDelete = new PoleZeroDeleteDialog(getContext());
				int selectedZeroUp=-1;
				int selectedPoleUp=-1;
				int zeroIndexUp=0;
				int poleIndexUp=0;
				for(ConjugatePoleZero graphic:zeroList){                       	 
					topPartSelector.set(graphic.topObject.getPoleZeroCoordinates().getLeftX(),graphic.topObject.getPoleZeroCoordinates().getTopY() , graphic.topObject.getPoleZeroCoordinates().getRightX(), graphic.topObject.getPoleZeroCoordinates().getBottomY());
					bottomPartSelector.set(graphic.bottomObject.getPoleZeroCoordinates().getLeftX(),graphic.bottomObject.getPoleZeroCoordinates().getTopY() , graphic.bottomObject.getPoleZeroCoordinates().getRightX(), graphic.bottomObject.getPoleZeroCoordinates().getBottomY());

					if((topPartSelector.contains(touchPositionX, touchPositionY))||(bottomPartSelector.contains(touchPositionX, touchPositionY))){   
						selectedZeroUp=zeroIndexUp;                                   
						zeroIndexUp=0;
						break;
					}
					zeroIndexUp=zeroIndexUp+1;  
				}  


				if(selectedZeroUp==-1){
					for(ConjugatePoleZero graphic:poleList){                     	
						topPartSelector.set(graphic.topObject.getPoleZeroCoordinates().getLeftX(),graphic.topObject.getPoleZeroCoordinates().getTopY() , graphic.topObject.getPoleZeroCoordinates().getRightX(), graphic.topObject.getPoleZeroCoordinates().getBottomY());
						bottomPartSelector.set(graphic.bottomObject.getPoleZeroCoordinates().getLeftX(),graphic.bottomObject.getPoleZeroCoordinates().getTopY() , graphic.bottomObject.getPoleZeroCoordinates().getRightX(), graphic.bottomObject.getPoleZeroCoordinates().getBottomY());
						if((topPartSelector.contains(touchPositionX, touchPositionY))||((bottomPartSelector.contains(touchPositionX, touchPositionY)))){                    			 
							selectedPoleUp=poleIndexUp;                           
							poleIndexUp=0;
							break;
						}
						poleIndexUp=poleIndexUp+1;
					}
				}
				if(selectedZeroUp!=-1&selectedPoleUp!=-1){
					poleZeroDelete.setParameters(selectedZeroUp, selectedPoleUp);
					poleZeroDelete.getWindow().setFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCH_MODAL, WindowManager.LayoutParams.FLAG_NOT_TOUCH_MODAL);

					poleZeroDelete.setCancelable(true);

					poleZeroDelete.show(); 
					WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
					lp.copyFrom(poleZeroDelete.getWindow().getAttributes());


					lp.x=(int)(touchPositionX-width/2);
					lp.y=(int)touchPositionY+lp.height;
					lp.gravity=Gravity.TOP;
					lp.flags &= ~WindowManager.LayoutParams.FLAG_DIM_BEHIND;




					poleZeroDeleteChosen=true;
					poleZeroDelete.getWindow().setAttributes(lp);

				}

				return true;
			}

		});
	}		  

	@Override
	public boolean onTouchEvent(MotionEvent event) {

		synchronized (_thread.getSurfaceHolder()) {
			if (event.getAction() == MotionEvent.ACTION_DOWN) {

				touchPositionX=(int) event.getX();
				touchPositionY=(int) event.getY();
				if(poleZeroDeleteChosen){
					poleZeroDelete.dismiss();
					poleZeroDeleteChosen=false;
				}
			}
else if (event.getAction() == MotionEvent.ACTION_MOVE) {
				
				ifMove=true;
				if(!isFirst){
					firstPositionX=(int) event.getX();
					firstPositionY=(int) event.getY();
					isFirst=true;
				}
				int touchPositionX=(int) event.getX();
				int touchPositionY=(int) event.getY();
				Rect topPartSelector=new Rect();         	  
				Rect bottomPartSelector=new Rect();
				if((touchPositionX<width)&&(touchPositionY<height)&&(touchPositionX>0)&&(touchPositionY>0)){
				if((selectedZero!=-1)&&(selectedPole==-1))
				{
					if(isTopSelected){
						zeroList.get(selectedZero).topObject.getPoleZeroCoordinates().setX(touchPositionX);
						zeroList.get(selectedZero).topObject.getPoleZeroCoordinates().setY(touchPositionY); 
						zeroList.get(selectedZero).bottomObject.getPoleZeroCoordinates().setX(touchPositionX);
						zeroList.get(selectedZero).bottomObject.getPoleZeroCoordinates().setY((height)-touchPositionY); 
					} else if(isBottomSelected){
						zeroList.get(selectedZero).topObject.getPoleZeroCoordinates().setX(touchPositionX);
						zeroList.get(selectedZero).topObject.getPoleZeroCoordinates().setY((height)-touchPositionY); 
						zeroList.get(selectedZero).bottomObject.getPoleZeroCoordinates().setX(touchPositionX);
						zeroList.get(selectedZero).bottomObject.getPoleZeroCoordinates().setY(touchPositionY);                     
					}

				}
				if((selectedPole!=-1)&&(selectedZero==-1)){
					if(isTopSelected){
						poleList.get(selectedPole).topObject.getPoleZeroCoordinates().setX(touchPositionX);
						poleList.get(selectedPole).topObject.getPoleZeroCoordinates().setY(touchPositionY); 
						poleList.get(selectedPole).bottomObject.getPoleZeroCoordinates().setX(touchPositionX);
						poleList.get(selectedPole).bottomObject.getPoleZeroCoordinates().setY((height)-touchPositionY);  
					} else if(isBottomSelected){
						poleList.get(selectedPole).topObject.getPoleZeroCoordinates().setX(touchPositionX);
						poleList.get(selectedPole).topObject.getPoleZeroCoordinates().setY((height)-touchPositionY); 
						poleList.get(selectedPole).bottomObject.getPoleZeroCoordinates().setX(touchPositionX);
						poleList.get(selectedPole).bottomObject.getPoleZeroCoordinates().setY(touchPositionY);

					}
				}
				else if((selectedZero==-1)&&(selectedPole==-1)){
					selectedZero=-1;
					selectedPole=-1;
					zeroIndex=0;
					poleIndex=0;
					for(ConjugatePoleZero graphic:zeroList){                       	 
						topPartSelector.set(graphic.topObject.getPoleZeroCoordinates().getLeftX(),graphic.topObject.getPoleZeroCoordinates().getTopY() , graphic.topObject.getPoleZeroCoordinates().getRightX(), graphic.topObject.getPoleZeroCoordinates().getBottomY());
						bottomPartSelector.set(graphic.bottomObject.getPoleZeroCoordinates().getLeftX(),graphic.bottomObject.getPoleZeroCoordinates().getTopY() , graphic.bottomObject.getPoleZeroCoordinates().getRightX(), graphic.bottomObject.getPoleZeroCoordinates().getBottomY());

						if(topPartSelector.contains(touchPositionX, touchPositionY)){   
							selectedZero=zeroIndex;
							zeroList.get(selectedZero).topObject.getPoleZeroCoordinates().setX(touchPositionX);
							isTopSelected=true;
							zeroList.get(selectedZero).topObject.getPoleZeroCoordinates().setY(touchPositionY); 
							zeroList.get(selectedZero).bottomObject.getPoleZeroCoordinates().setX(touchPositionX);
							zeroList.get(selectedZero).bottomObject.getPoleZeroCoordinates().setY((height)-touchPositionY);                     
							zeroIndex=0;
							break;
						}
						else if(bottomPartSelector.contains(touchPositionX, touchPositionY)){               		
							selectedZero=zeroIndex;
							isBottomSelected=true;
							zeroList.get(selectedZero).topObject.getPoleZeroCoordinates().setX(touchPositionX);
							zeroList.get(selectedZero).topObject.getPoleZeroCoordinates().setY((height)-touchPositionY); 
							zeroList.get(selectedZero).bottomObject.getPoleZeroCoordinates().setX(touchPositionX);
							zeroList.get(selectedZero).bottomObject.getPoleZeroCoordinates().setY(touchPositionY);                     
							zeroIndex=0;
							break;
						}  

						zeroIndex=zeroIndex+1;  
					}  
                     if(selectedZero==-1){


					for(ConjugatePoleZero graphic:poleList){                     	
						topPartSelector.set(graphic.topObject.getPoleZeroCoordinates().getLeftX(),graphic.topObject.getPoleZeroCoordinates().getTopY() , graphic.topObject.getPoleZeroCoordinates().getRightX(), graphic.topObject.getPoleZeroCoordinates().getBottomY());
						bottomPartSelector.set(graphic.bottomObject.getPoleZeroCoordinates().getLeftX(),graphic.bottomObject.getPoleZeroCoordinates().getTopY() , graphic.bottomObject.getPoleZeroCoordinates().getRightX(), graphic.bottomObject.getPoleZeroCoordinates().getBottomY());
						if(topPartSelector.contains(touchPositionX, touchPositionY)){                    			 
							selectedPole=poleIndex;
							isTopSelected=true;
							poleList.get(selectedPole).topObject.getPoleZeroCoordinates().setX(touchPositionX);
							poleList.get(selectedPole).topObject.getPoleZeroCoordinates().setY(touchPositionY); 
							poleList.get(selectedPole).bottomObject.getPoleZeroCoordinates().setX(touchPositionX);
							poleList.get(selectedPole).bottomObject.getPoleZeroCoordinates().setY((height)-touchPositionY);                     
							poleIndex=0;
							break;
						}
						else if(bottomPartSelector.contains(touchPositionX, touchPositionY)){                		
							selectedPole=poleIndex;
							isBottomSelected=true;
							poleList.get(selectedPole).topObject.getPoleZeroCoordinates().setX(touchPositionX);
							poleList.get(selectedPole).topObject.getPoleZeroCoordinates().setY((height)-touchPositionY); 
							poleList.get(selectedPole).bottomObject.getPoleZeroCoordinates().setX(touchPositionX);
							poleList.get(selectedPole).bottomObject.getPoleZeroCoordinates().setY(touchPositionY);                     
							poleIndex=0;
							break;
						} 
						poleIndex=poleIndex+1;
					}
                     }

				}

				if (mPositionListener != null) {

					if(((Math.abs(firstPositionX-(int) event.getX()))>height/200)||(Math.abs(firstPositionY-(int) event.getY()))>height/200){
						if(selectedZero!=-1){
							zeroArrayX[2*selectedZero]=3*(double)(+zeroList.get(selectedZero).topObject.getPoleZeroCoordinates().getX()-(width-width/90)/2)/(height);
							zeroArrayY[2*selectedZero]=3*(double)(-zeroList.get(selectedZero).topObject.getPoleZeroCoordinates().getY()+(height)/2)/(height);

							zeroArrayX[2*selectedZero+1]=3*(double)(+zeroList.get(selectedZero).bottomObject.getPoleZeroCoordinates().getX()-(width-width/90)/2)/(height);
							zeroArrayY[2*selectedZero+1]=3*(double)(-zeroList.get(selectedZero).bottomObject.getPoleZeroCoordinates().getY()+(height)/2)/(height);

						}
						if(selectedPole!=-1){

							poleArrayX[2*selectedPole]=3*(double)(+poleList.get(selectedPole).topObject.getPoleZeroCoordinates().getX()-(width-width/90)/2)/(height);
							poleArrayY[2*selectedPole]=3*(double)(-poleList.get(selectedPole).topObject.getPoleZeroCoordinates().getY()+(height)/2)/(height);

							poleArrayX[2*selectedPole+1]=3*(double)(+poleList.get(selectedPole).bottomObject.getPoleZeroCoordinates().getX()-(width-width/90)/2)/(height);
							poleArrayY[2*selectedPole+1]=3*(double)(-poleList.get(selectedPole).bottomObject.getPoleZeroCoordinates().getY()+(height)/2)/(height);
						}

						mPositionListener.onZeropoleListChange(zeroArrayX,zeroArrayY,poleArrayX,poleArrayY);
						isFirst=false;
						if(selectedZero!=-1){
							if(isTopSelected){
								xCoord=zeroArrayX[2*selectedZero];
								yCoord=zeroArrayY[2*selectedZero];}
							else
								if(isBottomSelected){
									xCoord=zeroArrayX[2*selectedZero+1];
									yCoord=zeroArrayY[2*selectedZero+1];         	    			 
								}
						} else 
							if(selectedPole!=-1){
								if(isTopSelected){
									xCoord=poleArrayX[2*selectedPole];
									yCoord=poleArrayY[2*selectedPole];}
								else
									if(isBottomSelected){
										xCoord=poleArrayX[2*selectedPole+1];
										yCoord=poleArrayY[2*selectedPole+1];         	    			 
									}
							}

					}
				}
				}
			}



			else if(event.getAction() == MotionEvent.ACTION_UP){
				
				selectedPole=-1;
				selectedZero=-1;
				ifMove=false;
			}   
		}
		return super.onTouchEvent(event);
	}



	public void addZero(){
		PoleZeroObject topZeroObject=new PoleZeroObject(zeroBitmap);
		topZeroObject.getPoleZeroCoordinates().setX((width-width/90)/2);
		topZeroObject.getPoleZeroCoordinates().setY(height/48-height/72+(height)/3);
		PoleZeroObject bottomZeroObject=new PoleZeroObject(zeroBitmap);
		bottomZeroObject.getPoleZeroCoordinates().setX((width-width/90)/2);
		bottomZeroObject.getPoleZeroCoordinates().setY(height/48-height/36+2*(height)/3);

		ConjugatePoleZero newZero=new ConjugatePoleZero();
		newZero.topObject=topZeroObject;
		newZero.bottomObject=bottomZeroObject;
		zeroList.add(newZero);    	 
		zeroNo++;
		if (mPositionListener != null) {


			zeroArrayX[2*(zeroNo-1)]=0.0;
			zeroArrayY[2*(zeroNo-1)]=0.5;

			zeroArrayX[2*zeroNo-1]=  0.0;
			zeroArrayY[2*zeroNo-1]=-0.5;



			mPositionListener.onZeropoleListChange(zeroArrayX,zeroArrayY,poleArrayX,poleArrayY);
		}

	}
	public  void addPole(){
		PoleZeroObject topZeroObject=new PoleZeroObject(poleBitmap);
		topZeroObject.getPoleZeroCoordinates().setX((width-width/90)/2);
		topZeroObject.getPoleZeroCoordinates().setY(height/48-height/72+(height)/3);
		PoleZeroObject bottomZeroObject=new PoleZeroObject(poleBitmap);
		bottomZeroObject.getPoleZeroCoordinates().setX((width-width/90)/2);
		bottomZeroObject.getPoleZeroCoordinates().setY(height/48-height/36+2*(height)/3);

		ConjugatePoleZero newPole=new ConjugatePoleZero();
		newPole.topObject=topZeroObject;
		newPole.bottomObject=bottomZeroObject;
		poleList.add(newPole);
		poleNo++; 
		if (mPositionListener != null) {    		    	 

			poleArrayX[2*(poleNo-1)]=0.0;
			poleArrayY[2*(poleNo-1)]=0.5;

			poleArrayX[2*poleNo-1]= 0.0;
			poleArrayY[2*poleNo-1]=-0.5;


			mPositionListener.onZeropoleListChange(zeroArrayX,zeroArrayY,poleArrayX,poleArrayY);
		}

	}

	public static class ConjugatePoleZero implements Serializable{
		/**
		 * 
		 */
		private static final long serialVersionUID = 6699938620975545433L;
		public PoleZeroObject topObject;
		public PoleZeroObject bottomObject;

	}

	@Override
	public void onDraw(Canvas canvas) {
		if(canvas!=null){
		super.onDraw(canvas); 
		canvas.drawColor(Color.WHITE); 
		
		rectPaint.setStyle(Style.STROKE);
		rectPaint.setColor(Color.BLUE);
		
		circlePaint.setColor(Color.BLUE);
		circlePaint.setStyle(Style.STROKE);
		
		axesPaint.setColor(Color.BLACK);
		
		textPaint.setColor(Color.BLACK);
		height=canvas.getHeight();
		width=canvas.getWidth();
		int dim=Math.min(width, height);

		canvas.drawRect(width/180, height/48, width-width/180, height-height/48, rectPaint);
		canvas.drawLine((width-width/90)/2, height/48, (width-width/90)/2, height-height/48, axesPaint);
		canvas.drawLine(width/180,(height)/2 , width-width/180, (height)/2, axesPaint);
		canvas.drawCircle((width-width/90)/2, (height)/2, (dim-width/90)/3, circlePaint);
		canvas.drawText("1.5",width-textPaint.measureText("1.5"), height/48+textPaint.getTextSize(), textPaint);
		canvas.drawText("1.0",width-textPaint.measureText("1.0"), height/48+(height-height/48)/2-(dim-width/90)/3, textPaint);
		canvas.drawText("0.0",width-textPaint.measureText("0.0"), ((height)/2), textPaint);
		canvas.drawText("-1.0",width-textPaint.measureText("-1.0"), height/48+(height)/2+(dim-width/90)/3, textPaint);
		canvas.drawText("-1.5",width-textPaint.measureText("-1.5"), height-height/48, textPaint);
		canvas.drawText("Real", width/180, (height)/2, textPaint);
		canvas.drawText("Imaginary",(width-width/90)/2-textPaint.measureText("Imaginary")/2, height/48+textPaint.getTextSize(), textPaint);

		for (ConjugatePoleZero graphic : zeroList) {
			Bitmap  topBitmap = graphic.topObject.getGraphic();
			Bitmap  bottomBitmap=graphic.bottomObject.getGraphic();

			PoleZeroCoordinates topCoords = graphic.topObject.getPoleZeroCoordinates();
			PoleZeroCoordinates bottomCoords = graphic.bottomObject.getPoleZeroCoordinates();
			canvas.drawBitmap(topBitmap, topCoords.getLeftX(), topCoords.getTopY(), null);
			canvas.drawBitmap(bottomBitmap, bottomCoords.getLeftX(), bottomCoords.getTopY(), null);

		}

		for (ConjugatePoleZero graphic : poleList) {
			Bitmap  topBitmap = graphic.topObject.getGraphic();
			Bitmap  bottomBitmap=graphic.bottomObject.getGraphic();          
			PoleZeroCoordinates topCoords = graphic.topObject.getPoleZeroCoordinates();
			PoleZeroCoordinates bottomCoords = graphic.bottomObject.getPoleZeroCoordinates();
			canvas.drawBitmap(topBitmap, topCoords.getLeftX(), topCoords.getTopY(), null);
			canvas.drawBitmap(bottomBitmap, bottomCoords.getLeftX(), bottomCoords.getTopY(), null);

		}
		}
	}

	@Override
	protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
		super.onMeasure(widthMeasureSpec, heightMeasureSpec);
       String parentClass=  getContext().getClass().getName();
		int parentWidth = MeasureSpec.getSize(widthMeasureSpec);
		int parentHeight = MeasureSpec.getSize(heightMeasureSpec);
		if(parentClass.contains("PZPlacement"))
		this.setMeasuredDimension(
				parentWidth/2, 3*parentHeight/4);
		else 
			this.setMeasuredDimension(
					parentWidth/2, parentHeight);
	}



	public void setPositionListener(ZeroPolePositionListener listener) {
		mPositionListener = listener;
	}
	/*public void setPoleZeroList(Serializable poleZeroInputList){
		poleZeroList=(PoleZeroList) poleZeroInputList;
		zeroList=poleZeroList.zeroList;
		poleList=poleZeroList.poleList;
	}*/
	@Override
	public void surfaceChanged(SurfaceHolder holder, int format, int width, int height) {
		// TODO Auto-generated method stub
	}

	@Override
	public void surfaceCreated(SurfaceHolder holder) {
		//if (!_thread._run) {
		_thread = new PZ2CoeffThread(getHolder(), this);
			_thread.setRunning(true);
			_thread.start();
	//	} else {
		//	_thread.onResume();
		//}
	}

	@Override
	public void surfaceDestroyed(SurfaceHolder holder) {
             //_thread.onPause();
		boolean retry = true;
		_thread.setRunning(false);
		while (retry) {
			try {
				_thread.join();
				retry = false;
			} catch (InterruptedException e) {

			}
		}
	}




}
