package com.prototype.ajdsp;

import java.text.DecimalFormat;

import org.achartengine.*;





import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.view.View.OnClickListener;

import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;



public class PlotPart extends Activity implements OnClickListener{
	LinearLayout layout;
	Button back;
	RelativeLayout topLayout;
	Bundle inputValues;
	static double[] inputArrayLinear;
//	Spinner plotType;
    
	static double[] inputArrayDb;

 Button linear;
	Button db;
	Button cont;
 Button disc;
	PlotView plotView;
	static String[] xAxisValues;
	GraphicalView defaultView;
	static long sigType=0;
	Button showValues;
	public static boolean isLinear=true;
	private static double length=0.0;
	static double[] inputArrayDefault;
	private Handler handlerLinear;
	private Handler handlerDb;
	private Handler handlerCont;
	private Handler handlerDisc;
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		//requestWindowFeature(Window.FEATURE_CUSTOM_TITLE);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
		setContentView(R.layout.plot_view);
		//getWindow().setFeatureInt(Window.FEATURE_CUSTOM_TITLE, R.layout.row);
		layout= (LinearLayout) findViewById(R.id.plot_view);
		layout.setBackgroundColor(Color.WHITE);
		 showValues=(Button)findViewById(R.id.view_values);
         showValues.setOnClickListener(this);
         
         back=(Button) findViewById(R.id.back_to_startView);
 	   	back.setOnClickListener(this);
 	
		

		linear=(Button)findViewById(R.id.select_linear);
		linear.setOnClickListener(this);
		
		db=(Button)findViewById(R.id.select_db);
		db.setOnClickListener(this);
		cont=(Button)findViewById(R.id.select_continuous);
		cont.setOnClickListener(this);
		disc=(Button)findViewById(R.id.select_discrete);
		disc.setOnClickListener(this);
		handlerLinear=new Handler();
		handlerDb=new Handler();
		handlerCont=new Handler();
		handlerDisc=new Handler();
		/*plotType = (Spinner) findViewById(R.id.plotType);
		ArrayAdapter<CharSequence> plotTypeAdapter = ArrayAdapter.createFromResource(
				this, R.array.plot_type, android.R.layout.simple_spinner_item);
		plotTypeAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		plotType.setBackgroundColor(Color.WHITE);
		plotType.setAdapter(plotTypeAdapter);
		plotType.setOnItemSelectedListener(new MyOnItemSelectedListener());
		plotType.setSelection(0);
		*/
		


		Bundle extras = getIntent().getExtras(); 
		if(extras !=null)
		{
			inputValues = extras.getBundle("blockValues");
			length= inputValues.getDouble("pulseWidth");
			if(length==0.0)
				length=256;
			inputArrayLinear= inputValues.getDoubleArray("signal");
			if(inputArrayLinear!=null){
				double[] dbArray=new double[(int)length];   	 
				for(int i=0;i<length;i++){
					if(inputArrayLinear[i]!=0)
						dbArray[i]=20*log10(inputArrayLinear[i]);					
				}
				inputArrayDb=dbArray;
				inputArrayDefault=inputArrayLinear;
				xAxisValues=inputValues.getStringArray("xAxis");
				
			}
			

		}
	
		if(isLinear){
			linear.setBackgroundResource(R.drawable.lightblue_button);
			db.setBackgroundResource(R.drawable.blue_button);
		}
		else{
			linear.setBackgroundResource(R.drawable.blue_button);
			db.setBackgroundResource(R.drawable.lightblue_button);
		}
		if(sigType==1){
			cont.setBackgroundResource(R.drawable.blue_button);
			disc.setBackgroundResource(R.drawable.lightblue_button);
		}
		else {
			disc.setBackgroundResource(R.drawable.blue_button);
			cont.setBackgroundResource(R.drawable.lightblue_button);
		}
		plotView =new PlotView(this);
		plotView.setInputArray(inputArrayDefault,length);
		if(xAxisValues!=null){
			plotView.setXAxisValues(xAxisValues);
		}
		defaultView = plotView.execute(this,sigType);
		layout.addView(defaultView);
	}
	@Override
	public void onBackPressed() {
		
			/*Intent i = new Intent(this, StartView.class);
			startActivity(i);*/
	
	}
	@Override
	 public void onStop() {
		 super.onStop();	
				finish();
				
	 }
	private double log10(double x)
	{if (x==0) return 0;
	else return Math.log(Math.abs(x))/Math.log(10);}
	/*public class MyOnItemSelectedListener implements OnItemSelectedListener {

		public void onItemSelected(AdapterView<?> parent,
				View view, int pos, long id) {
			sigType= plotType.getSelectedItemId();

			switch((int)id){
			case 0:
				plotView.setInputArray(inputArrayDefault,length);

				GraphicalView graphView = plotView.execute(getBaseContext(),sigType);
				layout.removeView(defaultView);
				defaultView=graphView;
				layout.addView(graphView);
				break;

			case 1:
				plotView.setInputArray(inputArrayDefault,length);
				GraphicalView discreteGraphView = plotView.execute(getBaseContext(),sigType);
				layout.removeView(defaultView);
				defaultView=discreteGraphView;
				layout.addView(discreteGraphView);
				break;



			default:
				break;
			}

		}

		public void onNothingSelected(AdapterView parent) {
			// Do nothing.
		}
	}*/
	@Override
	public void onClick(View v) {
		if (v == back){
			sigType=0;
			isLinear=true;
			xAxisValues=null;
			Intent i = new Intent(this, StartView.class);
			startActivity(i);
		}
		if(v==showValues){
			Intent i = new Intent(this, DisplayValues.class);
			Bundle plotValues=new Bundle();
			if(isLinear){		
		       plotValues.putStringArray("plotValues",getValues(inputArrayLinear,length));
		       inputArrayDefault=inputArrayLinear;
			}
			else{
				plotValues.putStringArray("plotValues",getValues(inputArrayDb,length));
				 inputArrayDefault=inputArrayDb;
			}
			plotValues.putString("returnClass", "PlotPart");
			i.putExtra("blockValues", plotValues);
			startActivity(i);
		}

		else		if(v==linear){
			//changePlot.execute("linear");
			
				/*isLinear=true;
				plotView.setInputArray(inputArrayLinear,length);
				GraphicalView graphView = plotView.execute(this,sigType);
				layout.removeView(defaultView);
				defaultView=graphView;
				layout.addView(graphView);
				 inputArrayDefault=inputArrayLinear;
                 linear.setBackgroundResource(R.drawable.lightblue_button);
                 db.setBackgroundResource(R.drawable.blue_button);*/
			  Runnable runnable = new Runnable() {
			      @Override
			      public void run() {
			        
			       
			          
			          handlerLinear.post(new Runnable() {
			            @Override
			            public void run() {
			            	isLinear=true;
							plotView.setInputArray(inputArrayLinear,length);
							changeGraph();
							 inputArrayDefault=inputArrayLinear;
			                 linear.setBackgroundResource(R.drawable.lightblue_button);
			                 db.setBackgroundResource(R.drawable.blue_button);
			                 
			            }
			          });
			        
			      }
			    };
			    new Thread(runnable).start();
			
			}
			else if (v == db){
				//changePlot.execute("db");
				/*isLinear=false;
				plotView.setInputArray(inputArrayDb,length);
				GraphicalView graphView = plotView.execute(this,sigType);
				layout.removeView(defaultView);
				defaultView=graphView;
				layout.addView(graphView);
				 inputArrayDefault=inputArrayDb;
				 db.setBackgroundResource(R.drawable.lightblue_button);
                 linear.setBackgroundResource(R.drawable.blue_button);*/
				
				Runnable runnable = new Runnable() {
				      @Override
				      public void run() {
				        
				       
				          
				          handlerDb.post(new Runnable() {
				            @Override
				            public void run() {
				            	isLinear=false;
								plotView.setInputArray(inputArrayDb,length);
								changeGraph();
								 inputArrayDefault=inputArrayDb;
								 db.setBackgroundResource(R.drawable.lightblue_button);
				                 linear.setBackgroundResource(R.drawable.blue_button);
				            }
				          });
				        
				      }
				    };
				    new Thread(runnable).start();	
			
		}
			else if (v == disc){
			//	changePlot.execute("disc");
				/*sigType=1;
	            plotView.setInputArray(inputArrayDefault,length);
				GraphicalView discreteGraphView = plotView.execute(getBaseContext(),sigType);
				layout.removeView(defaultView);
				defaultView=discreteGraphView;
				layout.addView(discreteGraphView);
				 disc.setBackgroundResource(R.drawable.lightblue_button);
                 cont.setBackgroundResource(R.drawable.blue_button);*/
				Runnable runnable = new Runnable() {
				      @Override
				      public void run() {
				        
				       
				          
				          handlerDisc.post(new Runnable() {
				            @Override
				            public void run() {
				            	sigType=1;
					            plotView.setInputArray(inputArrayDefault,length);
					            changeGraph();
								 disc.setBackgroundResource(R.drawable.lightblue_button);
				                 cont.setBackgroundResource(R.drawable.blue_button);
				            }
				          });
				        
				      }
				    };
				    new Thread(runnable).start();	
				
		}else if (v == cont){	
			//changePlot.execute("cont");
			/*sigType=0;
			plotView.setInputArray(inputArrayDefault,length);
			GraphicalView graphView = plotView.execute(getBaseContext(),sigType);
			layout.removeView(defaultView);
			defaultView=graphView;
			layout.addView(graphView);	
			 cont.setBackgroundResource(R.drawable.lightblue_button);
             disc.setBackgroundResource(R.drawable.blue_button);*/
			Runnable runnable = new Runnable() {
			      @Override
			      public void run() {
			        
			       
			          
			          handlerCont.post(new Runnable() {
			            @Override
			            public void run() {
			            	sigType=0;
			    			plotView.setInputArray(inputArrayDefault,length);
			    			changeGraph();
			    			cont.setBackgroundResource(R.drawable.lightblue_button);
			    	        disc.setBackgroundResource(R.drawable.blue_button);
			    	        
			            }
			          });
			        
			      }
			    };
			    new Thread(runnable).start();	
			
	}
		


	}
	public void changeGraph(){
		synchronized(this){
		GraphicalView graphView = plotView.execute(getBaseContext(),sigType);
		layout.removeView(defaultView);
		defaultView=graphView;
		layout.addView(graphView);	
		}
		 
	}


	/* private class ChangePlot extends AsyncTask<String, Void,String> {
		 
		public Context mContext;
	public 	ChangePlot (Context context){
		mContext=context;
		
	}
		
		    @Override		
		    protected String doInBackground(String... params) {
		
		      
		 
	String input=params[0];
		 

			if(input.equalsIgnoreCase("linear")){
			
			
				isLinear=true;
				plotView.setInputArray(inputArrayLinear,length);
				
			
			}
			else if (input.equalsIgnoreCase("db")){
				 
				isLinear=false;
				plotView.setInputArray(inputArrayDb,length);
				
				 
				
			}
		
			else if (input.equalsIgnoreCase("cont")){
				sigType=1;
	            plotView.setInputArray(inputArrayDefault,length);
				
		}else if (input.equalsIgnoreCase("disc")){	
			sigType=0;
			plotView.setInputArray(inputArrayDefault,length);
			
			
	}
		 
		       
		        
		 
		       
		 
		     return input ;  
		 
		    }
		 
		    protected void onPostExecute(String result) {
				if(result.equalsIgnoreCase("linear")){					
					
					GraphicalView graphView = plotView.execute(mContext,sigType);
					layout.removeView(defaultView);
					defaultView=graphView;
					layout.addView(graphView);
					 inputArrayDefault=inputArrayLinear;
	                 linear.setBackgroundResource(R.drawable.lightblue_button);
	                 db.setBackgroundResource(R.drawable.blue_button);
				
				}
				else if (result.equalsIgnoreCase("db")){
					 
				
					GraphicalView graphView = plotView.execute(mContext,sigType);
					layout.removeView(defaultView);
					defaultView=graphView;
					layout.addView(graphView);
					 inputArrayDefault=inputArrayDb;
					 db.setBackgroundResource(R.drawable.lightblue_button);
	                 linear.setBackgroundResource(R.drawable.blue_button);
					 
					
				}
			
				else if (result.equalsIgnoreCase("cont")){
					
					GraphicalView discreteGraphView = plotView.execute(mContext,sigType);
					layout.removeView(defaultView);
					defaultView=discreteGraphView;
					layout.addView(discreteGraphView);
					 disc.setBackgroundResource(R.drawable.lightblue_button);
	                 cont.setBackgroundResource(R.drawable.blue_button);
					
			}else if (result.equalsIgnoreCase("disc")){	
				GraphicalView graphView = plotView.execute(getBaseContext(),sigType);
				layout.removeView(defaultView);
				defaultView=graphView;
				layout.addView(graphView);	
				 cont.setBackgroundResource(R.drawable.lightblue_button);
	             disc.setBackgroundResource(R.drawable.blue_button);
				
				
				
		}
		      }
		 
		 
		 
		  
		 
		    }*/

	private String [] getValues(double[] signalArray,double length){
		String [] values= new String[(int)length];
		DecimalFormat twoDForm = new DecimalFormat("#.###");
		for(int i=0;i<length;i++){
			values[i]="x="+i+", "+"y="+twoDForm.format(signalArray[i]);

		}
		return values;
	}

}
