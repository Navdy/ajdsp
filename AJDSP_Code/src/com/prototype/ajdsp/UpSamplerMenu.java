package com.prototype.ajdsp;

import com.prototype.ajdsp.PartObject.PinSignalTypes;
import com.prototype.ajdsp.PartObject.PinTypes;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

public class UpSamplerMenu extends Activity implements OnClickListener {
	RelativeLayout layout;
	RelativeLayout topbar;
	Button addParts;
	EditText upSamplerRate;

	Button partList;
	public static int upSamplerNumber=0;
	Bundle blockValues=new Bundle();
	 Bundle inputBundle;

	Boolean create=true;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
		//requestWindowFeature(Window.FEATURE_CUSTOM_TITLE);
		setContentView(R.layout.upsampler_menu);
		//getWindow().setFeatureInt(Window.FEATURE_CUSTOM_TITLE, R.layout.row);
		layout=(RelativeLayout)findViewById(R.id.upSamplerMenu);
		layout.setBackgroundColor(Color.WHITE);
		addParts=(Button)findViewById(R.id.add_part);
        addParts.setOnClickListener(this);  
        partList=(Button) findViewById(R.id.back_select_parts);
		partList.setOnClickListener(this);
		
		upSamplerRate=(EditText)findViewById(R.id.upSamplerRate);

		addParts=(Button)findViewById(R.id.add_part);
		addParts.setOnClickListener(this);

		Bundle extras = getIntent().getExtras();
	     
	     
	     if(extras !=null)
		 {
	        blockValues = extras.getBundle("blockValues");
	    	Integer length=blockValues.getInt("upSamplerRate");
	        upSamplerRate.setText(length.toString());
	      
	        inputBundle=blockValues.getBundle("leftPin");
	        
	        create=false;
			addParts.setText("Update");
			partList.setText("Back");
	    } else{
		Integer length=1;
		upSamplerRate.setText(length.toString());
	    }

	}

	@Override
	public void onBackPressed() {
		
			/*Intent i = new Intent(this, StartView.class);
			startActivity(i);*/
		
	}
	@Override
	 public void onStop() {		
			 super.onStop();					
			 finish();
					 }
	
	  private Integer valueInt(String input){
		  if (input.equalsIgnoreCase("")){
			  return 0;
		  }
		  else return Integer.parseInt(input);
	  }
	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		if (v == addParts){

			

			
			Bundle upSamplerData=new Bundle();
			String uSRate=upSamplerRate.getText().toString();
			
			if(uSRate.length()>2){
				Toast.makeText(getApplicationContext(), "Sampling Rate should be between 1 and 10", Toast.LENGTH_LONG).show();
			}else if(valueInt(uSRate)==0||valueInt(uSRate)>10){
				Toast.makeText(getApplicationContext(), "Sampling Rate should be between 1 and 10", Toast.LENGTH_LONG).show();
			}
			else{
			double [] upSamplerArray = new double[256];
			
			Bundle upSamplerOutputBundle=new Bundle();
			upSamplerOutputBundle.putDoubleArray("signalMag",upSamplerArray);
			upSamplerOutputBundle.putDoubleArray("signalPhase",upSamplerArray);
			upSamplerData.putBundle("leftPin", inputBundle);
			upSamplerData.putBundle("rightPin", upSamplerOutputBundle);
			upSamplerData.putInt("upSamplerRate",valueInt(uSRate));
			if(create){
			upSamplerNumber++;
			upSamplerData.putString("title","Up Sampler"+" "+upSamplerNumber); 
			PinTypes upSamplerPinTypes=new PinTypes();
			upSamplerPinTypes.leftPin=true;
			upSamplerPinTypes.rightPin=true;
			upSamplerData.putSerializable("PinTypes",upSamplerPinTypes);
			PinSignalTypes upSamplerSigType=new PinSignalTypes();
			upSamplerData.putSerializable("pinSignalType", upSamplerSigType);
			PartsController.createBlock(upSamplerData);
			} else {
				upSamplerData.putString("title",blockValues.getString("title")); 
				 PartsController.updateBlock(upSamplerData);
			}
			Intent i = new Intent(this, StartView.class);
			startActivity(i);
			}
		}

		if(v==partList){
			if(create){
				Intent i = new Intent(this, PartList.class);
				startActivity(i);}
			else {
				Intent i = new Intent(this, StartView.class);
				startActivity(i);
			}
		}

	}
}


