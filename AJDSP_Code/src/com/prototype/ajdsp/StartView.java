package com.prototype.ajdsp;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import com.prototype.ajdsp.PartView.ConnectionData;

import android.app.Activity;
import android.app.KeyguardManager;
import android.app.KeyguardManager.KeyguardLock;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.PixelFormat;
import android.os.Bundle;
import android.view.Gravity;
import android.view.View;
import android.view.WindowManager;
import android.view.View.OnClickListener;
import android.view.Window;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

public class StartView extends Activity implements OnClickListener {
	RelativeLayout topbar;
	LinearLayout layout;
	ImageView partList;
	SignalGenerator block;
	PartView partView;
	ArrayList<String> titleList=new ArrayList<String>();
	Bundle inputBundle;
	Bitmap partbitmap;
	Bitmap pinbitmap;
	Button delete;
	Button tips;
	Button facebookLogin;
	//ImageView imageView;
	ImageView imageInfo;
	public static int button_height;
	private List<ConnectionData> connectionDataList=Collections.synchronizedList(new ArrayList<ConnectionData>());
	private List<PartObject> _graphics = Collections.synchronizedList(new ArrayList<PartObject>());


	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
		// requestWindowFeature(Window.FEATURE_CUSTOM_TITLE);
		setContentView(R.layout.start_window);

		//topbar.setBackgroundColor(Color.GRAY);
		//topbar.add
		layout= (LinearLayout) findViewById(R.id.start_layout);
		layout.setBackgroundColor(Color.WHITE);
		delete=(Button)findViewById(R.id.delete_all);
		//delete.setText("Del");
		//button_height = delete.getHeight();
		delete.setOnClickListener(this);  
		imageInfo=(ImageView)findViewById(R.id.info_view);
		imageInfo.setOnClickListener(this);
		//facebookLogin=(Button)findViewById(R.id.fb_login);
		//facebookLogin.setOnClickListener(this);



		tips=(Button)findViewById(R.id.tips_button);
		tips.setText("Tips");
		tips.setOnClickListener(this); 
		partList=(ImageView) findViewById(R.id.add_plus);
		// partList.setBackgroundResource(R.drawable.button);
		//partList.setText("+");
		partList.setOnClickListener(this);

	


		//PlotView plot=new PlotView(this);
		_graphics=PartsController.partList;
		connectionDataList=PartsController.connectionList;
		partView = new PartView(this);
		partView.setParameters(_graphics,connectionDataList);
		layout.addView(partView,0);






	}
	@Override

	public void onAttachedToWindow() {

		super.onAttachedToWindow();

		Window window = getWindow();

		window.setFormat(PixelFormat.RGBA_8888);

	}

	@Override
	public void onStop() {
		super.onStop();
		_graphics = Collections.synchronizedList(new ArrayList<PartObject>());
		connectionDataList=Collections.synchronizedList(new ArrayList<ConnectionData>());
		finish();

	}
	@Override
	public void onBackPressed() {

		//Intent i = new Intent(this, PartList.class);
		//startActivity(i);

	}



	@Override
	public void onClick(View v) {
		/** When OK Button is clicked, dismiss the dialog */
		
		if (v == partList){
			Intent i = new Intent(this, PartList.class);
			startActivity(i);
		}
		else if (v == delete){
			if(!PartsController.partList.isEmpty()){
			AllPartDeleteDialog allPartDelete = new AllPartDeleteDialog(this,partView);
			
			allPartDelete.setCancelable(true);
			allPartDelete.setTitle("This will delete all of the blocks. Are you sure?");

			allPartDelete.show();  
			WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
			lp.copyFrom(allPartDelete.getWindow().getAttributes());



			lp.flags &= ~WindowManager.LayoutParams.FLAG_DIM_BEHIND;
		
			allPartDelete.getWindow().setAttributes(lp); 
			}
		}else
		if (v == tips){
		       Intent i = new Intent(this, TipsView.class);
		       Bundle startValues=new Bundle();
		       startValues.putString("returnClass", "StartView");
			   i.putExtra("blockValues", startValues);
		        startActivity(i);
		}
		else
			if (v == imageInfo){
				Intent i = new Intent(this, InfoView.class);//specify the info view class that will be created
				Bundle startValues=new Bundle();
			    startValues.putString("returnClass", "StartView");
				i.putExtra("blockValues", startValues);
				startActivity(i);
		        //PartsController partsController =new PartsController(this);
			}


	}





}
