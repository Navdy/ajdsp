package com.prototype.ajdsp;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

public class PZPlotPart extends Activity implements OnClickListener{
	
	RelativeLayout topLayout;
	RelativeLayout layout;
	Button back;
	Button display;
	static Bundle inputValues;
	double [] poleXCoord={};
	double [] poleYCoord={};
	double [] zeroXCoord={};
	double [] zeroYCoord={};
	@Override
	  public void onCreate(Bundle savedInstanceState) {
		   super.onCreate(savedInstanceState);
		   requestWindowFeature(Window.FEATURE_NO_TITLE);
		   this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
		  // requestWindowFeature(Window.FEATURE_CUSTOM_TITLE);
		   setContentView(R.layout.pz_plot_view);
		   //getWindow().setFeatureInt(Window.FEATURE_CUSTOM_TITLE, R.layout.row);
		   
		    layout= (RelativeLayout) findViewById(R.id.pzplot_view_layout);
	            layout.setBackgroundColor(Color.WHITE);
	            topLayout= (RelativeLayout) findViewById(R.id.menu_top_bar);
	           
	           // int topBarHeight=topLayout.getH
	           // layout.setBackgroundColor(Color.WHITE);
		    
	       // topLayout.setBackgroundColor(Color.GRAY);
		    display=(Button)findViewById(R.id.add_part);
	        display.setOnClickListener(this);  
	        display.setText("Values");
	        back=(Button) findViewById(R.id.back_select_parts);
			back.setOnClickListener(this);
			TextView title= (TextView)findViewById(R.id.Title);
			title.setText("PZ Plot");
		Bundle extras = getIntent().getExtras(); 
		
        if(extras !=null)
   	 {
        	 inputValues = extras.getBundle("blockValues");
        	 if(inputValues!=null){
        	 poleXCoord= inputValues.getDoubleArray("PoleX");
        	 poleYCoord= inputValues.getDoubleArray("PoleY");
        	 zeroXCoord= inputValues.getDoubleArray("ZeroX");
        	 zeroYCoord= inputValues.getDoubleArray("ZeroY");
        	 }
        	 back.setText("Back");
        	
            }
       PZPlotView pzPlotView = (PZPlotView) findViewById(R.id.pz_plot);
      
      // pzPlotView.getSignals(pzPlotView);
       pzPlotView.setBackgroundColor(Color.WHITE);
       //pzPlotView.setDefaultHeight(topBarHeight);
       pzPlotView.setPzCoord(inputValues);
      // layout.addView(pzPlotView);
	    
     
        
   	 }
	@Override
	 public void onStop() {
		 super.onStop();	
				finish();
				
	 }
	@Override
	public void onBackPressed() {
		
			Intent i = new Intent(this, StartView.class);
			startActivity(i);
		
	}
	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		if(v==back){
		Intent i = new Intent(this, StartView.class);
	        startActivity(i);
		}
		else if(v==display){
			Intent i = new Intent(this, DisplayValues.class);
			Bundle plotValues=new Bundle();
			
				plotValues.putStringArray("plotValues",getValues(inputValues));
			
			plotValues.putString("returnClass", "PZPlot");
			i.putExtra("blockValues", plotValues);
			startActivity(i);
		}
		
	}
	private String [] getValues(Bundle inputBundle){
		//String [] values= new String[2*poleXCoord.length];
		DecimalFormat twoDForm = new DecimalFormat("#.###");
		List<Double> poleX=new ArrayList<Double>();
		List<Double> poleY=new ArrayList<Double>();
		List<Double> zeroX=new ArrayList<Double>();
		List<Double> zeroY=new ArrayList<Double>();
		double[] poleXCoordArray= inputValues.getDoubleArray("PoleX");
    	 double[] poleYCoordArray= inputValues.getDoubleArray("PoleY");
    	 double [] zeroXCoordArray= inputValues.getDoubleArray("ZeroX");
    	 double [] zeroYCoordArray= inputValues.getDoubleArray("ZeroY");
		int j=0;
		   for(int i=0;i<poleXCoordArray.length;i++){
				 if( (poleXCoordArray[i]==0)&&( poleYCoordArray[i]==0)&&(zeroXCoordArray[i]==0)&&(zeroYCoordArray[i]==0))
	        		 continue;
				 poleX.add(poleXCoordArray[i]);
				 poleY.add(poleYCoordArray[i]);
				 zeroX.add(zeroXCoordArray[i]);
				 zeroY.add(zeroYCoordArray[i]);
				 
			 // values[i]="Pole:("+twoDForm.format(poleXCoord[i])+")+("+twoDForm.format(poleYCoord[i])+")i";
			  
		   }
		 String [] values= new String[2*poleX.size()];
		 for(int i=0;i<values.length/2;i++){
		   	  values[i]="Pole:("+twoDForm.format(poleX.get(i))+")+("+twoDForm.format(poleY.get(i))+")i";}
		   for(int i=values.length/2;i<values.length;i++){
			   	  values[i]="Zero:("+twoDForm.format(zeroX.get(i-values.length/2))+")+("+twoDForm.format(zeroY.get(i-values.length/2))+")i";
				  
			   }
		   
		   return values;
	}
  }

