
package com.prototype.ajdsp;


import com.prototype.ajdsp.PartObject.PinSignalTypes;
import com.prototype.ajdsp.PartObject.PinTypes;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.view.View.OnClickListener;
import android.widget.ArrayAdapter;
import android.widget.Button;

import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;

public class FFTMenu extends Activity implements OnClickListener{
	RelativeLayout layout;
	RelativeLayout topbar;
	Button addParts;
	Spinner fftSpinner;
	Bundle inputValues=new Bundle();
	

	Button partList;
	public static int fftNumber=0;
	Boolean create=true;
	double [] inputMag;
	double [] inputPhase;


	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		//requestWindowFeature(Window.FEATURE_CUSTOM_TITLE);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
		setContentView(R.layout.fft_menu);
		//getWindow().setFeatureInt(Window.FEATURE_CUSTOM_TITLE, R.layout.row);
		layout=(RelativeLayout)findViewById(R.id.fftMenu);
		layout.setBackgroundColor(Color.WHITE);
		partList=(Button) findViewById(R.id.back_select_parts);
		partList.setOnClickListener(this);
		
	    addParts=(Button) findViewById(R.id.add_part);
		addParts.setOnClickListener(this);
		fftSpinner = (Spinner) findViewById(R.id.fft_length);
		ArrayAdapter<CharSequence> fftTypeAdapter = ArrayAdapter.createFromResource(
				this, R.array.fft_length, android.R.layout.simple_spinner_item);
		fftTypeAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
	//	filterTypeSpinner.setBackgroundColor(Color.WHITE);
		fftSpinner.setAdapter(fftTypeAdapter);
		


		Bundle extras = getIntent().getExtras();

		if(extras !=null)
		{
			inputValues = extras.getBundle("blockValues");
		
		    fftSpinner.setSelection((int) inputValues.getLong("itemSelected"));
			inputMag=inputValues.getBundle("leftPin").getDoubleArray("signalMag");
			inputPhase=inputValues.getBundle("leftPin").getDoubleArray("signalPhase");
			create=false;
			addParts.setText("Update");
			partList.setText("Back");

		}else{
			
			 fftSpinner.setSelection(5);
		}
	}
	@Override
	public void onBackPressed() {
		/*if(create){
			Intent i = new Intent(this, PartList.class);
			startActivity(i);}
		else {
			Intent i = new Intent(this, StartView.class);
			startActivity(i);
		}*/
	    }
	@Override
	 public void onStop() {
		        super.onStop();	
				finish();
				
	 }

@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		if (v == addParts){
			
		
			Bundle fftData=new Bundle();
			double [] fftArray = new double[256];
			Bundle fftOutputBundle=new Bundle();
			fftOutputBundle.putDoubleArray("signalMag",fftArray);
			fftOutputBundle.putDoubleArray("signalPhase",fftArray);
			fftData.putBundle("rightPin", fftOutputBundle);
			fftData.putInt("fftLength", Integer.parseInt(fftSpinner.getSelectedItem().toString()));
			fftData.putLong("itemSelected", fftSpinner.getSelectedItemId());
			if(create){
				fftNumber++;
				Bundle fftInputBundle=new Bundle();
				fftInputBundle.putDoubleArray("signalMag",fftArray);
				fftInputBundle.putDoubleArray("signalPhase",fftArray);
				fftData.putBundle("leftPin", fftInputBundle);
				PinSignalTypes fftSigType=new PinSignalTypes();
				fftSigType.rightPin="time|fft";
				
				fftData.putSerializable("pinSignalType", fftSigType);
				fftData.putString("title","FFT"+" "+fftNumber); 
				PinTypes fftPinTypes=new PinTypes();
				fftPinTypes.leftPin=true;
				fftPinTypes.rightPin=true;
				fftData.putSerializable("PinTypes",fftPinTypes);			    
				PartsController.createBlock(fftData);
			}
			else{
				Bundle fftInputBundle=new Bundle();
				fftInputBundle.putDoubleArray("signalMag",inputMag);
				fftInputBundle.putDoubleArray("signalPhase",inputPhase);
				fftData.putBundle("leftPin", fftInputBundle);
				fftData.putString("title", inputValues.getString("title"));
				PartsController.updateBlock(fftData);
			}
			Intent i = new Intent(this, StartView.class);
			startActivity(i);
			
		}

		if(v==partList){
			if(create){
				Intent i = new Intent(this, PartList.class);
				startActivity(i);}
			else {
				Intent i = new Intent(this, StartView.class);
				startActivity(i);
			}
		}

	}
}
