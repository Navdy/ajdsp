package com.prototype.ajdsp;
/*Copyright (c) 1999 Andreas Spanias, Arizona State University
JDSP Concept by Andreas Spanias

Development Team:
Director: Andreas Spanias
Research Assistants/Associates: Argyris Constantinou, Axel Clausen, Maya Tampi

For questions regarding use of this software write or
call Prof. Andreas Spanias  (spanias@asu.edu,  (480) 965 1837))
*******************************************************************************

DESCRIPTION:  Implements Chebyshev type II filter design

KEY VARIABLES:
INPUTS:
LP - lowpass filter
HP - highpass filter
BP - bandpass filter
filterType - filter type (LP, HP, BP)
wp1 - passband edge frequency
ws1 - stopband edge frequency
wp2 - second passband edge frequency
ws1 - second stopband edge frequency
rp  - passband ripple
rs  - stopband ripple
OUTPUTS:
order - order of the filter
fp1 - first cut-off frequency
fp2 - second cut-off frequency
*/

class Elliptic {

  public static final int LP = 0;
  public static final int HP = 1;
  public static final int BP = 2;

  private int order, nz;
  private int prototype, filterType;
  private float fp1, fp2,fN, rate;
  double tol = 2.2204E-16;
  private double[] pReal;
  private double[] pImag;
  private double[] z;
  private double[] aCoeff;
  private double[] bCoeff;
  private double Wp1, Ws1, Wp2, Ws2, Rp, Rs, WN, BW=1, kk;

  public  Elliptic()
  {
    rate = 8000.0f;
    fN = 0.5f*rate;
  }

  public void setFilterParameters(int ft, double wp1, double ws1, double wp2, double ws2, double rp, double rs)
  {
	  double wa,tempord,epsilon,k1,k;

	  filterType=ft;
	  Wp1=Math.tan(Math.PI*wp1/2);
	  Ws1=Math.tan(Math.PI*ws1/2);
	  Wp2=Math.tan(Math.PI*wp2/2);
	  Ws2=Math.tan(Math.PI*ws2/2);
	  Rs=rs;
	  Rp=rp;
	  // calculates band-pass (fp1-fp2) and order
	  switch (filterType){
	  case LP: wa=Ws1/Wp1;break;
	  case HP: wa=Wp1/Ws1;break;
	  case BP: wa=Math.min(Math.abs((Ws1*Ws1-Wp1*Wp2)/(Ws1*(Wp1-Wp2))),Math.abs((Ws2*Ws2-Wp1*Wp2)/(Ws2*(Wp1-Wp2))));break;
	  default: wa=0;
	  }
	  epsilon=Math.sqrt(Math.pow(10,0.1*Rp)-1);
	  k1=epsilon/Math.sqrt(Math.pow(10,0.1*Rs)-1);
	  k=1/wa;
	  double capk[]  = ellipke(k*k, 1-k*k);
	  double capk1[] = ellipke(k1*k1,1-k1*k1);
	  tempord=(capk[0]*capk1[1])/(capk[1]*capk1[0]);
	  order=(int)(tempord+0.999999);

	  if ((filterType == BP) )
		  order*=2;

	  switch (filterType){
	  case LP: fp1=0;fp2=(float)((2/Math.PI)*Math.atan(Wp1));break;
	  case HP: fp1=(float)((2/Math.PI)*Math.atan(Wp1));fp2=1;break;
	  case BP: fp1=(float)((2/Math.PI)*Math.atan(Wp1));
			   fp2=(float)((2/Math.PI)*Math.atan(Wp2));break;
	  default: fp1=0;fp2=0;
	  }
	  if (order>0 && order<=10)
		  design();
	  else
		  order=-1;

  }
  private double[] ellipke(double m1, double m2)
  {

	  double[] kkk  = new double[2];
	  double[] a0 = new double[2];
	  double[] b0 = new double[2];
	  double[] s0 = new double[2];
	  double[] a1 = new double[2];
	  double[] b1 = new double[2];
	  double[] c1 = new double[2];
	  double[] w1 = new double[2];
	  double[] m  = new double[2];
	  m[0]=m1;m[1]=m2;
	  for (int i=0;i<2;i++)
	  {
		  a0[i]=1;
		  b0[i]=Math.sqrt(1-m[i]);
	      s0[i]=m[i];
	  }
	  double i1=0;
	  double mm=1;
	  while (mm>tol)
	  {
		  for (int i=0;i<2;i++)
		  {
		  a1[i]=(a0[i]+b0[i])/2; b1[i]=Math.sqrt(a0[i]*b0[i]);
		  c1[i]=(a0[i]-b0[i])/2;
		  }
		  i1++;
		  for (int i=0;i<2;i++)
			  w1[i]=Math.pow(2,i1)*Math.pow(c1[i],2);
		  mm=Math.max(w1[0],w1[1]);
		  for (int i=0;i<2;i++)
		  {
			  s0[i]+=w1[i];a0[i]=a1[i];b0[i]=b1[i];
		  }
	  }
	  for (int i=0;i<2;i++)
		  kkk[i]=Math.PI/(2*a1[i]);
	 return kkk;
  }


  private double[][] ellipj(double[] u, double m, int mmax)
  {
	  double[][] scdn = new double[3][mmax];
	  for (int j=0;j<3;j++)
		for (int i=0;i<mmax;i++)
			scdn[j][i]=0;
	  double[][] a = new double[10][mmax];
	  double[][] b = new double[10][mmax];
	  double[][] c = new double[10][mmax];
	  double[]   n = new double[mmax];
	  for (int i=0;i<10;i++)
	  {
		  for (int j=0;j<mmax;j++)
		  {
			  if (i==0)
			  {a[i][j]=1;b[i][j]=Math.sqrt(1-m);c[i][j]=Math.sqrt(m);}
			  else
			  {a[i][j]=0;b[i][j]=0;c[i][j]=0;}
		  }
	  }

	  int i=0;
	  boolean loop=true;
	  for (int k=0;k<mmax;k++)
			  n[k]=0;
	  while (loop==true && i<9)
	  {
		  i++;
		  for (int j=0;j<mmax;j++)
		  {
			  a[i][j]=0.5*(a[i-1][j]+b[i-1][j]);
			  b[i][j]=Math.sqrt(a[i-1][j]*b[i-1][j]);
			  c[i][j]=0.5*(a[i-1][j]-b[i-1][j]);

			  if (c[i][j]<=tol) loop=false;
			  if (Math.abs(c[i][j])<=tol && Math.abs(c[i-1][j])>tol)
				  n[j]=i;
		  }
	  }

	  double[][] phin = new double[i+1][mmax];
	  for (int j=0;j<=i;j++)
	  {
		  for (int k=0;k<mmax;k++)
		  {
			if (j==i)
				phin[j][k]=Math.pow(2,n[k])*a[j][k]*u[k];
		    else
				phin[j][k]=0;
		  }
	  }

	  while (i>0)
	  {
		  i--;
		  for (int j=0;j<mmax;j++)
		  {
			  if (n[j]>=i+1)
				  phin[i][j]=0.5*(Math.asin(c[i+1][j]*Math.sin(Math.IEEEremainder(phin[i+1][j],2*Math.PI))/a[i+1][j])+phin[i+1][j]);
			  else
				  phin[i][j]=phin[i+1][j];
		 }
	  }
	  for (int j=0;j<mmax;j++)
	  {
		  if (m==1)
		  {
			  scdn[0][j]=sinh(u[j])/cosh(u[j]);
			  scdn[1][j]=1/cosh(u[j]);
			  scdn[2][j]=scdn[1][j];
		  }
		  else
		  {
			  scdn[0][j]=Math.sin(Math.IEEEremainder(phin[0][j],2*Math.PI));
			  scdn[1][j]=Math.cos(Math.IEEEremainder(phin[0][j],Math.PI*2));
			  scdn[2][j]=Math.sqrt(1-m*scdn[0][j]*scdn[0][j]);
		  }

	  }

	  return scdn;
  }


  private double cosh(double x)
  {
	  return (Math.exp(x)+Math.exp(-x))/2;
  }
  private double sinh(double x)
  {
	  return (Math.exp(x)-Math.exp(-x))/2;
  }


  public int getOrder()
  {
    return order;
  }

  public double getFreq1() {
    return fp1;
  }

  public double getFreq2() {
    return fp2;
  }

  public double getACoeff(int i) {
    // returns IIR filter numerator coefficient with index i
    return aCoeff[i];
  }

  public double getBCoeff(int i) {
    // returns IIR filter denominator coefficient with index i
    return bCoeff[i];
  }



private void locatePolesAndZeros()
{

    pReal  = new double[order+1];
    pImag  = new double[order+1];
    z      = new double[order+1];
	double w1=4*Math.tan(Math.PI*fp1/2);
	  double w2=4*Math.tan(Math.PI*fp2/2);
	WN=1;
	if (filterType==LP)
		WN=w2;
	else if (filterType==BP)
	{BW=w2-w1;WN=Math.sqrt(w1*w2);}
	else if (filterType==HP)
		WN=w1;

    for(int k = 1; k <= order; k++)
	{
      pReal[k] = 0.0;
      pImag[k] = 0.0;
    }
    int n = order;

	double krat=0;
	if (n==1)
	{
		pReal[1]=-Math.sqrt(1/(Math.pow(10,Rp/10)-1));
		pImag[1]=0;
		z[1]=0;
		kk=-pReal[1];
	}
	else
	{
		double epsilon=Math.sqrt(Math.pow(10,Rp/10)-1);
		double k1=epsilon/Math.sqrt(Math.pow(10,Rs/10)-1);
		double k1p=Math.sqrt(1-k1*k1);
		if (k1p==1)
			return;
		double capk1[]=ellipke(k1*k1,k1p*k1p);
		double wp=1;
		if (Math.abs(1-k1p*k1p)<tol)
			krat=0;
		else
			krat=n*capk1[0]/capk1[1];
		double mt=0,m=0.5,rr;
		double tem[] = new double[2];
		double rmin=10000000;
		double m1;m=0;mt=0;
		for (int j=1;j<=9;j++)
		{
			mt=(double)j/10;
			tem=ellipke(mt,1-mt);
			rr=Math.abs(tem[0]/tem[1]-krat);
			if (rr<=rmin)
			{
				rmin=rr;m=mt;
			}
		}
		for (int i=2;i<=16;i++)
		{
			m1=m;
			for (int j=-9;j<=9;j++)
			{
				mt=(double)j/Math.pow(10,i)+m1;
				if (mt>0)
				{
					tem=ellipke(mt,1-mt);
					rr=Math.abs(tem[0]/tem[1]-krat);
					if (rr<=rmin)
					{
						rmin=rr;m=mt;
					}
				}
			}
		}

		double capk[]=ellipke(m,0);
		wp=1;
		double ws=wp/Math.sqrt(m);

		double[] par1 = new double[n/2+1];
		double[] par2 = new double[n/2+1];
		int k=0;
		for (int j=1-(n%2);j<n;j+=2)
			par1[k++]=j*capk[0]/n;

		int lngth=k;

		double[][] scd = ellipj(par1,m,lngth);


			int ii=1;
		for (int i=0;i<order+1;i++)
			z[i]=0;
		for (int i=0;i<lngth;i++)
		{
			if (scd[0][i]>tol)
			{
				z[ii]=-1/(Math.sqrt(m)*scd[0][i]);
				z[ii+1]=1/(Math.sqrt(m)*scd[0][i]);
				ii+=2;
			}
		}


		double capkt[] = ellipke(1-m,0);
		double r=capkt[0];double capkmin=11111;
		rmin=1000000;
		double scdp[][] = new double[3][1];


		for (int j=1;j<=6;j++)
		{
			capkt[0]=(double)j;
			capkt[1]=0;
			scdp=ellipj(capkt,k1p*k1p,1);
			r=Math.abs(1/epsilon-scdp[0][0]/scdp[1][0]);
			if (r<rmin)
			{
				rmin=r;capkmin=capkt[0];
			}
		}
		double r1=capkmin;
		for (int i=1;i<=16;i++)
		{
			r1=capkmin;
			for (int j=-9;j<=9;j++)
			{
				capkt[0]=(double)j/Math.pow(10,i)+r1;
				capkt[1]=0;
				scdp=ellipj(capkt,k1p*k1p,1);
				r=Math.abs(1/epsilon-scdp[0][0]/scdp[1][0]);
				if (r<rmin)
				{
					rmin=r;capkmin=capkt[0];
				}
			}
		}

		double[] v0 = new double[1];
		v0[0]=capk[0]*capkmin/(order*capk1[0]);
		scdp=ellipj(v0,1-m,1);
		if (order % 2 ==0)
		{
			int jj=1;
			for (int j=0;j<lngth;j++)
			{
				pReal[jj]=-scd[1][j]*scd[2][j]*scdp[0][0]*scdp[1][0]/(1-Math.pow(scd[2][j]*scdp[0][0],2));
				pImag[jj]=-scd[0][j]*scdp[2][0]/(1-Math.pow(scd[2][j]*scdp[0][0],2));
				pReal[jj+1]=pReal[jj];
				pImag[jj+1]=-pImag[jj];
				jj+=2;
			}
		}
		else
		{
			int jj=1;
			for (int j=0;j<lngth;j++)
			{
				if (j==0)
				{
					pReal[order]=-scd[1][j]*scd[2][j]*scdp[0][0]*scdp[1][0]/(1-Math.pow(scd[2][j]*scdp[0][0],2));
					pImag[order]=-scd[0][j]*scdp[2][0]/(1-Math.pow(scd[2][j]*scdp[0][0],2));
				}
				else
				{
					pReal[jj]=-scd[1][j]*scd[2][j]*scdp[0][0]*scdp[1][0]/(1-Math.pow(scd[2][j]*scdp[0][0],2));
					pImag[jj]=-scd[0][j]*scdp[2][0]/(1-Math.pow(scd[2][j]*scdp[0][0],2));
					pReal[jj+1]=pReal[jj];
					pImag[jj+1]=-pImag[jj];
					jj+=2;
				}
			}

		}

	  double prt=-pReal[1],pit=-pImag[1];

	  double prtn=0,pitn;
	  for (int i=2;i<=order;i++)
	  {
		  prtn=prt*(-pReal[i])-pit*(-pImag[i]);
		  pitn=prt*(-pImag[i])+pit*(-pReal[i]);
		  prt=prtn;
		  pit=pitn;
	  }
	  if (order%2==0)
		  nz=order;
	  else
		  nz=order-1;
	  double zt=1;
	  for (int i=1;i<=nz;i++)
		  zt=zt*(-z[i]);
	  kk=prt/(zt*Math.pow(-1,(double)nz/2.0));
	  if (order % 2 ==0)
		  kk=kk/Math.sqrt(1+Math.pow(epsilon,2));



	}



  }

  private double[] realpoly (double r1, double i1, double r2, double i2)
  {
	  double pc[] = new double[3];
	  pc[0] = 1;
	  pc[1] = -(r1+r2);
	  pc[2] = r1*r2-i1*i2;
	  return pc;
  }

  private void design()
  {
	  UtilityFunctions uf = new UtilityFunctions();
	  aCoeff = new double[11];
      bCoeff = new double[11];
	  for (int i=0;i<=10;i++)
	  {aCoeff[i]=0;bCoeff[i]=0;}

	  if (filterType==BP)
	  {
		  if (order==2)
			  order=1;
		  else
			  order=order/2;
	  }
	  locatePolesAndZeros();

	  double a[][] = new double[order][order];
	  double b[][] = new double[order][1];
	  double c[][] = new double[1][order];
	  double d[][] = new double[1][1];
	  double num[] = new double[3];
	  double den[] = new double[3];
	  double wn;
	  int j=0;
	  int cp=0;
	  b[0][0]=1;d[0][0]=1;c[0][0]=0;
	  for (int i=0;i<order;i++)
		  for (int i2=0;i2<order;i2++)
				a[i][i2]=0;
	  for (int i=1;i<order;i++)
			 b[i][0]=1-i%2;

	  if (order % 2 !=0)
	  {
	  	 a[j][0]=pReal[order];d[0][0]=0;c[0][0]=1;
		 j++;
		 for (int i=1;i<order;i++)
			 b[i][0]=0;

	  }


	  for (int i=1;i<nz;i+=2)
	  {
		  num=realpoly(0,z[i],0,z[i+1]);
		  den=realpoly(pReal[i],pImag[i],pReal[i+1],pImag[i+1]);
		  wn=Math.sqrt(Math.sqrt(pReal[i]*pReal[i]+pImag[i]*pImag[i])*Math.sqrt(pReal[i+1]*pReal[i+1]+pImag[i+1]*pImag[i+1]));
	      if (wn==0) wn=1;
		  double[][] a1={{-den[1],-den[2]/wn},{wn,0}};
		  double[] c1={num[1]-den[1],(num[2]/wn-den[2]/wn)};
		  for (int k=0;k<=cp;k++)
			  a[j][k]=c[0][k];
		  cp++;
		  if (order%2==0 && i==1)
			  cp=cp-1;
		  for (int k=cp;k<=cp+1;k++)
			  a[j][k]=a1[0][k-cp];
		  j++;
		  for (int k=cp;k<=cp+1;k++)
			  a[j][k]=a1[1][k-cp];
		  j++;
	      for (int k=cp;k<=cp+1;k++)
			  c[0][k]=c1[k-cp];
		  cp++;
	  }
	  d[0][0]=d[0][0]*kk;
	  c[0][0]=c[0][0]*kk;
	  for (int i=1;i<order;i++)
		  c[0][i]=c[0][i]*kk;

if (filterType==BP) order=order*2;
	  double at[][] = new double[order][order];
	  double bt[][] = new double[order][1];
	  double ct[][] = new double[1][order];
	  double dt[][] = new double[1][1];
	  int n=order;

	  if (filterType==LP)
	  {
		  bt=uf.sxM(b,WN,n,1);
		  at=uf.sxM(a,WN,n,n);
		  for (int i=0;i<order;i++)
			  ct[0][i]=c[0][i];
		  dt[0][0]=d[0][0];
	  }
	  else if (filterType==HP)
	  {
		  double[][] dtemp=uf.MxM(uf.MxM(c,uf.invM(a,n),1,n,n),b,1,n,1);
		  dt[0][0]=d[0][0]-dtemp[0][0];
		  ct=uf.MxM(c,uf.invM(a,n),1,n,n);
		  bt=uf.sxM(uf.MxM(uf.invM(a,n),b,n,n,1),-WN,n,1);
		  at=uf.sxM(uf.invM(a,n),WN,n,n);
	  }
	  else if (filterType==BP)
	  {

		  double q=WN/BW;
		  for (int i=0;i<order;i++)
			  for (j=0;j<order;j++)
				at[i][j]=0;
		  for (int i=0;i<order/2;i++)
		  {
			  bt[i][0]=WN*b[i][0]/q;bt[i+order/2][0]=0;
			  ct[0][i]=c[0][i];ct[0][i+order/2]=0;
		  }
		  for (int i=0;i<order/2;i++)
			  for (j=0;j<order/2;j++)
			  at[i][j]=WN*a[i][j]/q;
		  for (int i=0;i<order/2;i++)
		  {
			  at[i+order/2][i]=-WN;
			  at[i][i+order/2]=WN;
		  }
		  dt[0][0]=d[0][0];

	  }
	  // bilinear transformation
	  double ab[][] = new double[order][order];
	  double bb[][] = new double[order][1];
	  double cb[][] = new double[1][order];
	  double db[][] = new double[1][1];
	  double t=0.5;
	  double r= Math.sqrt(t);
	  double[][] t1 = new double[order][order];
	  double[][] t2 = new double[order][order];
	  for (int i=0;i<order;i++)
	  {
		  for (j=0;j<order;j++)
		  {
			  t1[i][j]=at[i][j]*t/2;
			  t2[i][j]=-at[i][j]*t/2;
			  if (i==j)
			  {t1[i][j]++;t2[i][j]++;}
		  }

		  ab=uf.MxM(uf.invM(t2,order),t1,order,order,order);
		  bb=uf.sxM(uf.MxM(uf.invM(t2,order),bt,order,order,1),t/r,order,1);
		  cb=uf.sxM(uf.MxM(ct,uf.invM(t2,order),1,order,order),r,1,order);
		  db=uf.MxM(uf.MxM(ct,uf.invM(t2,order),1,order,order),bt,1,order,1);
		  db[0][0]=db[0][0]*t/2+dt[0][0];
	  }
	  aCoeff=uf.polyMat(ab,order);
	  bCoeff=uf.polyMat(uf.MmM(ab,uf.MxM(bb,cb,order,1,order),order,order),order);
	  for (int i=0;i<=order;i++)
		  bCoeff[i]=bCoeff[i]+(db[0][0]-1)*aCoeff[i];

  }






}
