package com.prototype.ajdsp;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.prototype.ajdsp.PartObject.PinSignalTypes;
import com.prototype.ajdsp.PartObject.PinTypes;

public class DownSamplerMenu extends Activity implements OnClickListener {
	RelativeLayout layout;
	RelativeLayout topbar;
	Button addParts;
	Button back;
	EditText downSamplerRate;

	Button partList;
	public static int downSamplerNumber=0;
	Bundle blockValues=new Bundle();
	Boolean create=true;
	Bundle inputBundle;
	Bundle leftPinBundle;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		//requestWindowFeature(Window.FEATURE_CUSTOM_TITLE);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
		setContentView(R.layout.downsampler_menu);
		//getWindow().setFeatureInt(Window.FEATURE_CUSTOM_TITLE, R.layout.row);
		layout=(RelativeLayout)findViewById(R.id.downSamplerMenu);
		layout.setBackgroundColor(Color.WHITE);
		
		downSamplerRate=(EditText)findViewById(R.id.downSamplerRate);

		partList=(Button) findViewById(R.id.back_select_parts);
		partList.setOnClickListener(this);
		TextView title= (TextView)findViewById(R.id.Title);
		title.setText("Down Sampler");
		addParts=(Button) findViewById(R.id.add_part);
		addParts.setOnClickListener(this);


		Bundle extras = getIntent().getExtras();
		if(extras !=null)
		{
			blockValues = extras.getBundle("blockValues");
			leftPinBundle=blockValues.getBundle("leftPin");
			Integer length=blockValues.getInt("downSamplerRate");
			downSamplerRate.setText(length.toString());
			addParts.setText("Update");
			inputBundle=blockValues.getBundle("leftPin");
			partList.setText("Back");

			create=false;
		} else{

			Integer length=1;
			downSamplerRate.setText(length.toString());
		}

	}
	@Override
	 public void onStop() {		
			super.onStop();					
			finish();			
	 }
	
	@Override
	public void onBackPressed() {
		/*if(create){
			Intent i = new Intent(this, PartList.class);
			startActivity(i);}
		else {
			Intent i = new Intent(this, StartView.class);
			startActivity(i);
		}*/
	}

	  private Integer valueInt(String input){
		  if (input.equalsIgnoreCase("")){
			  return 0;
		  }
		  else return Integer.parseInt(input);
	  }
	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		if (v == addParts){
			
			Bundle downSamplerData=new Bundle();
			
			String dSRate=downSamplerRate.getText().toString();
			
			if(dSRate.length()>2){
				Toast.makeText(getApplicationContext(), "Sampling Rate should be between 1 and 10", Toast.LENGTH_LONG).show();
			}else if(valueInt(dSRate)==0||valueInt(dSRate)>10){
				Toast.makeText(getApplicationContext(), "Sampling Rate should be between 1 and 10", Toast.LENGTH_LONG).show();
			}
			
			else{
			double [] downSamplerArray = new double[256];
			
			//downSamplerData.putBundle("leftPin", inputBundle);
			Bundle downSamplerOutputBundle=new Bundle();
			downSamplerOutputBundle.putDoubleArray("signalMag",downSamplerArray);
			downSamplerOutputBundle.putDoubleArray("signalPhase",downSamplerArray);
			downSamplerOutputBundle.putDouble("pulseWidth",256.0);
			
			downSamplerData.putBundle("rightPin", downSamplerOutputBundle);
			downSamplerData.putInt("downSamplerRate",valueInt(dSRate));

			if(create){
				downSamplerNumber++;
				downSamplerData.putString("title","Down Sampler"+" "+downSamplerNumber); 
				PinTypes downSamplerPinTypes=new PinTypes();
				downSamplerPinTypes.leftPin=true;
				downSamplerPinTypes.rightPin=true;
				downSamplerData.putSerializable("PinTypes",downSamplerPinTypes);
				PinSignalTypes downSamplerSigType=new PinSignalTypes();
				downSamplerData.putSerializable("pinSignalType", downSamplerSigType);
				downSamplerData.putBundle("leftPin", downSamplerOutputBundle);
				PartsController.createBlock(downSamplerData);
			}
			else {
				downSamplerData.putBundle("leftPin", inputBundle);
				downSamplerData.putString("title",blockValues.getString("title")); 
				PartsController.updateBlock(downSamplerData);
			}
			Intent i = new Intent(this, StartView.class);
			startActivity(i);
			}
		}

		if(v==partList){
			if(create){
				Intent i = new Intent(this, PartList.class);
				startActivity(i);}
			else {
				Intent i = new Intent(this, StartView.class);
				startActivity(i);
			}
		}

	}
}


