package com.prototype.ajdsp;




import java.util.ArrayList;
import java.util.List;

import android.content.Context;

import android.graphics.Canvas;

import android.graphics.Color;
import android.graphics.Paint;

import android.view.MotionEvent;
import android.view.SurfaceHolder;
import android.view.SurfaceView;


public class FreqSampleView extends SurfaceView  implements SurfaceHolder.Callback {
	/**
	 * 
	 */
	private CursorPositionListener mPositionListener;
	private FreqSampleThread _thread;
	private float xpos,ypos;
	private int height,width;
	public static int pointCount=0;
	private double amplitude=0.0;
	private static int noLineSegments;
	public  static List<Float> pointList=new ArrayList<Float>();
	Paint gridPaint = new Paint();
	Paint xLinePaint=new Paint();
	Paint textPaint=new Paint(); 
	Paint line=new Paint();
	Paint point=new Paint();
	interface CursorPositionListener {
		void onPositionChange(double newPosition);
	}
	public FreqSampleView(Context context) {
		super(context);
		getHolder().addCallback(this);
		mPositionListener = null;
		
		setFocusable(true);
	

	}		  

	@Override
	public boolean onTouchEvent(MotionEvent event) {
		synchronized (_thread.getSurfaceHolder()) {
			if (event.getAction() == MotionEvent.ACTION_DOWN) {

				float touchPositionX= event.getX();
				float touchPositionY=event.getY(); 
				if((touchPositionY>(double)height/15)&&(touchPositionY<(double)(ypos*10+height/15))&&(touchPositionX>xpos)&&(touchPositionX<xpos*16)){
					amplitude=(double)(ypos*10+(double)height/15-touchPositionY)/(double)(ypos*10);
					if(pointCount<=noLineSegments){                   	                   
						if(pointCount==0){
							pointList.add(touchPositionX);
							pointList.add(touchPositionY);
							pointList.add(2*(xpos +xpos*8)-touchPositionX);
							pointList.add(touchPositionY);                    	 
							pointCount++;   }
						if((pointCount>0)&&(touchPositionX>pointList.get(pointList.size()-4))&&(touchPositionX<(xpos +(xpos*8)))){
							pointList.add(touchPositionX);
							pointList.add(touchPositionY);
							pointList.add(2*(xpos +xpos*8)-touchPositionX);
							pointList.add(touchPositionY);
							pointCount++; 

						}
					}
					invalidate();
					if (mPositionListener != null) {
						mPositionListener.onPositionChange(amplitude);
					}
				}


			} else if (event.getAction() == MotionEvent.ACTION_MOVE) {
				float touchPositionX= event.getX();
				float touchPositionY=event.getY();  

				if((touchPositionY>(double)height/15)&&(touchPositionY<(double)(ypos*10+height/15))&&(touchPositionX>xpos)&&(touchPositionX<xpos*16)){
					amplitude=(double)(ypos*10+(double)height/15-touchPositionY)/(double)(ypos*10);
				}
				invalidate();
				if (mPositionListener != null) {
					mPositionListener.onPositionChange(amplitude);
				}

			} else if(event.getAction() == MotionEvent.ACTION_UP){



			}

		}
		return true;
	}

	public static class InputCoordinates{
		float xCoordinate;
		float yCoordinate;
	}


	@Override
	public void onDraw(Canvas canvas) {
		if(canvas!=null){
		super.onDraw(canvas);         
		
		xLinePaint.setColor(Color.RED);
		gridPaint.setStyle(Paint.Style.FILL);      
		canvas.drawColor(Color.WHITE);
		width=canvas.getWidth();
		height=canvas.getHeight();

		xpos = width / 17;
		ypos = height/11;
		for (int i = 0; i < 17; i++) {                  
			gridPaint.setColor(Color.BLUE);
			canvas.drawLine(xpos +(xpos*i), height/15, xpos +(xpos*i), (ypos*10)+height/15, gridPaint);              
		}  
		int pass=0;

		for (int i = 0; i < 11; i++) {                         

			canvas.drawLine(xpos, (ypos*pass)+height/15, xpos +(xpos*16), (ypos*pass)+height/15, gridPaint);      
			pass++;
		}     
		gridPaint.setColor(Color.BLACK);
		canvas.drawLine(xpos +(xpos*8), height/15, xpos +(xpos*8), (ypos*10)+height/15, gridPaint);
		 
		textPaint.setStyle(Paint.Style.FILL); 
		textPaint.setColor(Color.BLACK);
		canvas.drawText("0.0", 0, (ypos*10)+height/15, textPaint);
		canvas.drawText("1.0", 0, height/15+textPaint.getTextSize(), textPaint);
		canvas.drawText("|H(k)|", 0,height/15-textPaint.getTextSize(), textPaint);

		canvas.drawLine(xpos, (ypos*10)+3+height/15, xpos, (ypos*10)-3 +height/15, xLinePaint);
		canvas.drawLine(xpos +(xpos*4), (ypos*10)+3+height/15, xpos +(xpos*4), (ypos*10)-3 +height/15, xLinePaint);
		canvas.drawLine(xpos +(xpos*8), (ypos*10)+3+height/15, xpos +(xpos*8), (ypos*10)-3 +height/15, xLinePaint);
		canvas.drawLine(xpos +(xpos*12), (ypos*10)+3+height/15, xpos +(xpos*12), (ypos*10)-3 +height/15, xLinePaint);
		canvas.drawLine(xpos +(xpos*16), (ypos*10)+3+height/15, xpos +(xpos*16), (ypos*10)-3 +height/15, xLinePaint);

		canvas.drawText("0", xpos, height-xLinePaint.getTextSize()/2, xLinePaint);
		canvas.drawText("pi/2", xpos +(xpos*4)-xLinePaint.getTextSize(), height-xLinePaint.getTextSize()/2, xLinePaint);
		canvas.drawText("pi", xpos +(xpos*8)-xLinePaint.getTextSize(), height-xLinePaint.getTextSize()/2, xLinePaint);
		canvas.drawText("3pi/2", xpos +(xpos*12)-xLinePaint.getTextSize(), height-xLinePaint.getTextSize()/2, xLinePaint);
		canvas.drawText("2pi", xpos +(xpos*16)-xLinePaint.getTextSize(), height-xLinePaint.getTextSize()/2, xLinePaint);
		
		point.setColor(Color.RED);
		
		line.setColor(Color.RED);
		line.setStrokeWidth(3);
		if(pointList.size()>0){
			for(int i=0;i<pointList.size();i=i+4){
				canvas.drawCircle((float)pointList.get(i), (float)pointList.get(i+1),4, point);
				canvas.drawCircle((float)(pointList.get(i+2)), (float)pointList.get(i+3),4, point);
			}
			if(pointCount==noLineSegments+1){
				canvas.drawLine((float)pointList.get(0), (ypos*10)+height/15,(float)pointList.get(0), (float)pointList.get(1),line);
				canvas.drawLine((float)(pointList.get(2)), (ypos*10)+height/15,(float)(pointList.get(2)), (float)pointList.get(1),line);
				for(int i=0;i<pointList.size()-4;i=i+4){
					canvas.drawLine((float)pointList.get(i),(float)pointList.get(i+1) ,(float)pointList.get(i+4), (float)pointList.get(i+5),line);
					canvas.drawLine((float)(pointList.get(i+2)),(float)(pointList.get(i+3)) ,(float)(pointList.get(i+6)), (float)(pointList.get(i+7)),line);
				}
				canvas.drawLine((float)pointList.get(pointList.size()-2), (float)pointList.get(pointList.size()-1),(float)pointList.get(pointList.size()-2) ,(ypos*10)+height/15,  line);
				canvas.drawLine((float)(2*(xpos +xpos*8)-pointList.get(pointList.size()-2)), (float)pointList.get(pointList.size()-1),(float)(2*(xpos +xpos*8)-pointList.get(pointList.size()-2)) ,(ypos*10)+height/15,  line);
			}
		}
		}
	}

	public double getAmplitude(){
		return amplitude;

	}
	public void setNoLineSegments(int lineSegments){
		noLineSegments=lineSegments;  
	}
	public void setValues(float[]coordinates){

		for(int i=0;i<coordinates.length;i=i+2){
			pointList.add(i,(float)((coordinates[i]-33)*(16*xpos/250)+xpos));	 
			pointList.add(i+1,(float)((coordinates[i+1]-33)*(16*xpos/250)+xpos));	 
			
		}
		
		
	}
	public List<Float> getValues(){

		List<Float> coordinatePointList=new ArrayList<Float>();
		for(int i=0;i<pointList.size();i=i+2){
			coordinatePointList.add(i, (float) ((250*(pointList.get(i)-xpos))/(16*xpos))+33);
			coordinatePointList.add(i+1, (float)((100*(pointList.get(i+1)- height/15))/(ypos*10))+33);  		  
		}
		return coordinatePointList;
	}
	public void setPositionListener(CursorPositionListener listener) {
		mPositionListener = listener;
	}
	@Override
	public void surfaceChanged(SurfaceHolder holder, int format, int width, int height) {
		// TODO Auto-generated method stub
	}

	@Override
	public void surfaceCreated(SurfaceHolder holder) {
		_thread = new FreqSampleThread(getHolder(), this);
 		  _thread.setRunning(true);
		 if (!_thread._run) {
		
             _thread.start();
   	    } else {
   	      _thread.onResume();
   	  }
         
	}

	@Override
	public void surfaceDestroyed(SurfaceHolder holder) {

	//	boolean retry = true;
		_thread.setRunning(false);
		 _thread.onPause();
		/*while (retry) {
			try {
				_thread.join();
				retry = false;
			} catch (InterruptedException e) {

			}
		}*/
	}






}



