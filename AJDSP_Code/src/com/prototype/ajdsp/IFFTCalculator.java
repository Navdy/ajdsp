package com.prototype.ajdsp;

import android.os.Bundle;

public class IFFTCalculator {
	public int MAXSIZE=256;
	public int ifft_size = MAXSIZE;
	double[] phzQ = new double[MAXSIZE];
	double[] magnQ = new double[MAXSIZE];
	double[] phzI = new double[MAXSIZE];
	double[] magnI = new double[MAXSIZE];
	public  Bundle IFFT(Bundle dataBundle){
		for (int i=0;i<256;i++)
		{
			phzI[i]=0;magnI[i]=0;
		}
		ifft_size=dataBundle.getInt("ifftLength");
		Bundle inputData=dataBundle.getBundle("leftPin");
		double[] inputMag=inputData.getDoubleArray("signalMag");
		double[] inputPhase=new double[ifft_size];
		if(inputData.getDoubleArray("signalPhase")!=null)
			inputPhase=inputData.getDoubleArray("signalPhase");
		FastFourier ifftI= new FastFourier(inputMag, inputPhase, ifft_size,-1);
		FastFourier ifftQ= new FastFourier(inputMag, inputPhase, ifft_size,-1);
		for (int k=0;k<ifft_size;k++)
		{
			magnI[k]=ifftI.getMagnitude(k);						
			phzI[k]=ifftI.getPhase(k);
			magnQ[k]=ifftQ.getMagnitude(k);
			phzQ[k]=ifftQ.getPhase(k);
		}
		Bundle outputBundle=new Bundle();
		outputBundle.putDoubleArray("signalMag", magnI);
		outputBundle.putDoubleArray("signalPhase", phzI);
		outputBundle.putDouble("pulseWidth", ifft_size);
		dataBundle.putBundle("rightPin", outputBundle);
		return dataBundle;
	}

}
