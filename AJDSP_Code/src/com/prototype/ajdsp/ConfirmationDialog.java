package com.prototype.ajdsp;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup.LayoutParams;
import android.widget.Button;
import android.widget.TextView;

public class ConfirmationDialog  extends Dialog implements OnClickListener{
	Button okButton;
	Button cancelButton;
	TextView pinDialogText;
	PartObject graphic;
	Bitmap pinBitmap;
	Bundle _inputBundle;
	String partCheckText;

	public ConfirmationDialog(Context context) {
		super(context);
		
		setContentView(R.layout.confirmation_dialog);
		getWindow().setLayout(LayoutParams.FILL_PARENT, LayoutParams.WRAP_CONTENT);
		pinDialogText = (TextView) findViewById(R.id.confirmation_message);
		okButton = (Button) findViewById(R.id.confirm);
		okButton.setOnClickListener(this);
		cancelButton = (Button) findViewById(R.id.cancel);
		cancelButton.setOnClickListener(this);
		//cancelButton.setBackgroundColor(Color.GRAY);
	}

	
	public void setInputBundle(Bundle inputBundle){
		_inputBundle=inputBundle;
	}
	@Override
	public void onBackPressed() {
	   dismiss();
	}
 public void setPartCheckText(String checkText){
	 partCheckText=checkText;
 }
	@Override
	public void onClick(View v) {
		if (v == okButton){
			PartsController.createBlock(_inputBundle);
			Intent i = new Intent(this.getContext(), StartView.class);
			getContext().startActivity(i);
			dismiss();
		}
		if (v == cancelButton){
			if((partCheckText.contains("Plot"))&&(!partCheckText.contains("PZ Plot")))
				PartList.plotNumber--;
			if(partCheckText.contains("Convolution"))
				PartList.convolutionNumber--;
			if(partCheckText.contains("Filter"))
				PartList.filterNumber--;
			if(partCheckText.contains("Freq Resp"))
				PartList.filterNumber--;
			if(partCheckText.contains("PZ Plot"))
				PartList.pzPlotNumber --;
			if(partCheckText.contains("PZ Placement"))
				PartList.pzPlacementNumber --;
			if(partCheckText.contains("Junction"))
				PartList.junctionNumber --;
			if(partCheckText.contains("SNR"))
				PartList.junctionNumber --;
			dismiss();
		}
	}
}
