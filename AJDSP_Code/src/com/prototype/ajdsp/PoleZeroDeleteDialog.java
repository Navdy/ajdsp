package com.prototype.ajdsp;

import java.util.ArrayList;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.TextView;

import com.prototype.ajdsp.PZPlacementView.ZeroPolePositionListener;
import com.prototype.ajdsp.PartView.ConnectionData;

public class PoleZeroDeleteDialog extends Dialog implements  OnClickListener {

	Button deleteButton;
	Button cancelButton;
	TextView pinDialogText;
	PartObject graphic;
	Bitmap pinBitmap;
	Bundle _inputBundle;
	String partCheckText;
	int partNumber;
	private int selectedZero=-1,selectedPole=-1;
	double x[] = new double[128];
	double phi[] = new double[128];
	double amp[] = new double[128];
	double ang[] = new double[128];
	double ampdB[] = new double[128];

	int maxOrder=64;
	double [] coeffB=new double[maxOrder+1];
	double [] coeffA=new double[maxOrder+1];
	GraphView magnPlot;
	GraphView phasePlot;
	String[] verLabels;
	String[] horLabels;

	private static ZeroPolePositionDeleteListener mPositionListener;
	interface ZeroPolePositionDeleteListener {
		void onZeroPoleListChange(double[] zeroX,double[] zeroY,double[] poleX,double[] poleY);
	}

	public PoleZeroDeleteDialog(Context context) {
		super(context);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.part_delete_dialog);

		mPositionListener = null;
		deleteButton = (Button) findViewById(R.id.partDelete);
		deleteButton.setBackgroundColor(Color.TRANSPARENT);
		deleteButton.setOnClickListener(this);
		
		
	}

	public void setPartNumber(Integer partNo){
		partNumber=partNo;


	}
	public void setInputBundle(Bundle inputBundle){
		_inputBundle=inputBundle;
	}

	@Override
	public void onClick(View v) {
		if (v == deleteButton){
			if(selectedZero!=-1){
				PZPlacementView.zeroList.remove(selectedZero);
				PZPlacementView.zeroArrayX[2*selectedZero]=0;			
				PZPlacementView.zeroArrayX[2*selectedZero+1]=0;
				PZPlacementView.zeroArrayY[2*selectedZero]=0;
				PZPlacementView.zeroArrayY[2*selectedZero+1]=0;
				PZPlacementView.zeroNo--;
				//if(mPositionListener != null)			
					//mPositionListener.onZeroPoleListChange(PZPlacementView.zeroArrayX,PZPlacementView.zeroArrayY,PZPlacementView.poleArrayX,PZPlacementView.poleArrayY);
			}
			else if(selectedPole!=-1){
				PZPlacementView.poleList.remove(selectedPole); 
				PZPlacementView.poleArrayX[2*selectedPole]=0;
				PZPlacementView.poleArrayX[2*selectedPole+1]=0;
				PZPlacementView.poleArrayY[2*selectedPole]=0;
				PZPlacementView.poleArrayY[2*selectedPole+1]=0;
				PZPlacementView.poleNo--;
				//if(mPositionListener != null)			
				// mPositionListener.onZeroPoleListChange(PZPlacementView.zeroArrayX,PZPlacementView.zeroArrayY,PZPlacementView.poleArrayX,PZPlacementView.poleArrayY);
			}
			

			coeffB=CalcNumCoeff(PZPlacementView.zeroArrayX, PZPlacementView.zeroArrayX, PZPlacementView.zeroNo);
			coeffA=CalcDenCoeff(PZPlacementView.poleArrayX, PZPlacementView.poleArrayX, PZPlacementView.poleNo);             
			getAmpPhi();    
			PZPlacementDemo.setPlot(amp, ang);


			dismiss();
		}
		if(v==cancelButton)
			dismiss();
	}
	public  void setParameters(Integer zero,Integer pole){
		selectedZero=zero;
		selectedPole=pole;
	}
	public void getAmpPhi(){

		double  nr =0;double ni=0;double dr=0;double di=0;

		for (int i=0;i<=127;i++)
		{
			double omega= Math.PI/((double)128)*i;
			nr =0;ni=0;dr=0;di=0;

			for (int k=0;k<coeffA.length;k++)
			{
				nr=nr+coeffB[k]*Math.cos((double)(64-k)*omega);
				ni=ni+coeffB[k]*Math.sin((double)(64-k)*omega);
				dr=dr+coeffA[k]*Math.cos((double)(64-k)*omega);
				di=di+coeffA[k]*Math.sin((double)(64-k)*omega);

			}
			if((dr*dr+di*di)!=0){

				double  re= (nr*dr+ni*di)/(dr*dr+di*di);
				double im= (ni*dr-nr*di)/(dr*dr+di*di);

				x[i] = Math.sqrt(re*re+im*im);
				phi[i]=Math.atan2(im,re);


				amp[i] = x[i];
				ang[i] = phi[i]*180/Math.PI;


				if (x[i] >=100000)			               
					x[i]=100000;			               

				if (x[i] <=-100000)		               

					x[i]=-100000;


			}
		}

	}
	@Override
	public void onBackPressed() {
	    dismiss();
	}
	public  void setPositionListener(ZeroPolePositionDeleteListener listener) {
		mPositionListener = listener;
	}
	public double [] CalcNumCoeff(double[] m_dZeroX, double[] m_dZeroY, int zero_no)
	{
		double tempZ2, tempZ1;
		double [] b=new double[maxOrder+1];
		int zCount = 0;
		b[0] = 1.0;
		for (int i = 1; i < 11;i++)
			b[i] = 0.00;
		for(int m = 0;m < zero_no;m++)
		{
			if(m_dZeroY[m] == 0.00)
			{
				zCount++;
				for(int i = zCount;i>=1;i--)
					b[i] += b[i-1]*(-m_dZeroX[m]);
			}
			else
			{
				zCount += 2;
				tempZ2 = m_dZeroX[m]*m_dZeroX[m] + m_dZeroY[m]*m_dZeroY[m];
				tempZ1 = -2*m_dZeroX[m];
				b[zCount] = b[zCount-2]*tempZ2;
				for(int i = zCount-1;i>=2;i--)
				{
					b[i] += b[i-1]*tempZ1 + b[i-2]*tempZ2;
				}
				b[1] += tempZ1;
			}
		}
		for (int i=zCount+1;i<=10; i++)
		{
			b[i] = 0.00;
		}
		return b;
	}
	public double [] CalcDenCoeff(double[] m_dPoleX, double[] m_dPoleY, int pole_no)
	{
		double [] a=new double[maxOrder+1];
		int pCount=0;
		double tempP2, tempP1;
		a[0] = 1.0;
		for (int i = 1; i < 11;i++)
			a[i] = 0.00;
		for(int m = 0;m < pole_no;m++)
		{
			if(m_dPoleY[m] == 0.00)
			{
				pCount++;
				for(int i = pCount;i>=1;i--)
					a[i] += a[i-1]*(-m_dPoleX[m]);
			}
			else
			{
				pCount += 2;
				tempP2 = m_dPoleX[m]*m_dPoleX[m] + m_dPoleY[m]*m_dPoleY[m];
				tempP1 = -2*m_dPoleX[m];
				a[pCount] = a[pCount-2]*tempP2;
				for(int i = pCount-1;i>=2;i--)
				{a[i] += a[i-1]*tempP1 + a[i-2]*tempP2;}
				a[1] += tempP1;
			}
		}
		for (int i=pCount+1;i<=10; i++)
		{a[i] = 0.00;}
		return a;
	}

}





