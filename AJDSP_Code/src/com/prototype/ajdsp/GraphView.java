package com.prototype.ajdsp;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Paint.Align;
import android.util.AttributeSet;
import android.view.View;
import android.view.View.MeasureSpec;

/**
 * GraphView creates a scaled line or bar graph with x and y axis labels. 
 * @author Arno den Hond
 *
 */
public class GraphView extends View {

	


	private Paint paint;
	private Paint textPaint;
	
	private String[] horlabels;
	private String[] verlabels;
	private String title;
	private boolean type;
	private int length;
	private float[] values;

	public GraphView(Context context){	 
	
		super(context);
		
		paint = new Paint();
		textPaint=new Paint();
		textPaint.setColor(Color.BLACK);
		
		
	}
	public GraphView(Context context, AttributeSet attrs) {
	    super(context, attrs);
	  
	   
	    paint = new Paint();
		textPaint=new Paint();
		textPaint.setColor(Color.BLACK);
			
	  }

	  public GraphView( Context context, AttributeSet attrs,
	       int defStyle) {
		  super(context, attrs, defStyle);
		  
		  paint = new Paint();
			textPaint=new Paint();
			textPaint.setColor(Color.BLACK);
		 
			 

	  }
	public void SetParameters(String title, String[] horlabels, String[] verlabels){
		
		if (title == null)
			title = "";
		else
			this.title = title;
		if (horlabels == null)
			this.horlabels = new String[0];
		else
			this.horlabels = horlabels;
		if (verlabels == null)
			this.verlabels = new String[0];
		else
			this.verlabels = verlabels;
	}
	public void setLength(int lengthInput){
		length=lengthInput;
		values=new float[length];
	}
	public void setValues(double[] inputValues){
	for(int i=0;i<inputValues.length;i++)
		this.values[i]=(float) inputValues[i];
	
	
			
	}

	@Override
	protected void onDraw(Canvas canvas) {
		float border = 20;
		float horstart = border * 2;
		float height = getHeight();
		float width = getWidth() - 1;
		float max = getMax();
		float min = getMin();
		float diff = max - min;
		float graphheight = height - (2 * border);
		float graphwidth = width - (2 * border);
		

		textPaint.setTextAlign(Align.LEFT);
		verlabels=getYLabels();
		int vers = verlabels.length - 1;
		for (int i = 0; i < verlabels.length; i++) {
			paint.setColor(Color.BLACK);
			float y = ((graphheight / vers) * i) + border;
			canvas.drawLine(horstart, y, width, y, paint);	
			
			canvas.drawText(verlabels[i], horstart-textPaint.measureText(verlabels[i]), y, textPaint);
		}
		int hors = horlabels.length - 1;
		for (int i = 0; i < horlabels.length; i++) {
			paint.setColor(Color.BLACK);
			float x = ((graphwidth / hors) * i) + horstart;
			canvas.drawLine(x, height - border, x, border, paint);
			paint.setTextAlign(Align.CENTER);
			if (i==horlabels.length-1)
				paint.setTextAlign(Align.RIGHT);
			if (i==0)
				paint.setTextAlign(Align.LEFT);
			paint.setColor(Color.BLACK);
			canvas.drawText(horlabels[i], x, height - 4, textPaint);
		}

		paint.setTextAlign(Align.CENTER);
		canvas.drawText(title, (graphwidth / 2) + horstart, border - 4, textPaint);

		if (max != min) {
			paint.setColor(Color.BLUE);
			 
				float datalength = values.length;
				float colwidth = (width - (2 * border)) / datalength;
				float halfcol = colwidth / 2;
				float lasth = 0;
				for (int i = 0; i < values.length; i++) {
					float val = values[i] - min;
					float rat = val / diff;
					float h = graphheight * rat;
					if (i > 0)
						canvas.drawLine(((i - 1) * colwidth) + (horstart + 1) + halfcol, (border - lasth) + graphheight, (i * colwidth) + (horstart + 1) + halfcol, (border - h) + graphheight, paint);
					lasth = h;
				
			}
			
		}
		
		
		
	}

	private float getMax() {
		float largest = Integer.MIN_VALUE;
		if(values!=null){
		for (int i = 0; i < values.length; i++)
			if (values[i] > largest)
				largest = values[i];
		}
		return largest;
	}

	private float getMin() {
		float smallest = Integer.MAX_VALUE;
		if(values!=null){
		for (int i = 0; i < values.length; i++)
			if (values[i] < smallest)
				smallest = values[i];
		}
		return smallest;
	}
	public String [] getYLabels() {
		 float max=getMax()+1;
		 float maxBy3=max/3;		 
		 float max2By3=2*max/3;
		
		 
		 String [] verLabels={String.format("%3.2f",max),String.format("%3.2f",max2By3),String.format("%3.2f",maxBy3),"0"};
		 return verLabels;
		
	}
	public void setVerLabels(String[] verLabels){
		verlabels=verLabels;
	}
	
	
	 @Override
     protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
         super.onMeasure(widthMeasureSpec, heightMeasureSpec);

         int parentWidth = MeasureSpec.getSize(widthMeasureSpec);
         int parentHeight = MeasureSpec.getSize(heightMeasureSpec);
         this.setMeasuredDimension(
                 parentWidth/2, parentHeight/2);
     }
}
