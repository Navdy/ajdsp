
package com.prototype.ajdsp;


import java.io.Serializable;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.HashSet;
import java.util.List;
import java.util.Set;


import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.Rect;
import android.graphics.Typeface;
import android.os.Bundle;


public class PartObject implements Comparator{
	/**
	 * 
	 */


	/**
	 * Contains the coordinates of the graphic.
	 */

	public class Coordinates {
		private int _x ;
		private int _y ;

		public int getBottomY() {
			return _y + _partbitmap.getHeight() / 2;
		}
		public int getRightX() {
			return _x + _partbitmap.getWidth() / 2;
		}

		public int getLeftX() {
			return _x - _partbitmap.getWidth() / 2;
		}

		public void setX(int value) {
			_x = value;
		}

		public int getTopY() {
			return _y - _partbitmap.getHeight() / 2;
		}

		public void setY(int value) {
			_y = value;
		}

		public String toString() {
			return "Coordinates: (" + _x + "/" + _y + ")";
		}
	}

	private Bitmap _partbitmap;
	private Bitmap _blockbitmap;
	private Bitmap _pinbitmap;
	private Coordinates _coordinates;
	public String _partName;
	private boolean hasConnection=false;
	private Bundle _blockData;
	private String _pinSelected=null; 
	private PinTypes _pinTypes;
	private Bitmap _bigpinbitmap;
	private Set<String> _selectedPinSet=new HashSet<String>();
	public boolean _isdeleted=false;
	public boolean _isRightPinConnected=false;
	public boolean _isLeftPinConnected=false;
	public boolean _isTopPinConnected=false;
	public boolean _isBottomPinConnected=false;
	public boolean _isLeftTopPinConnected=false;
	public boolean _isLeftBottomPinConnected=false;
	public boolean _isRightTopPinConnected=false;
	public boolean _isRightBottomPinConnected=false;
	public  List<ChildObject> _childList=new ArrayList<ChildObject>();
	public  List<ParentObject> _parentList=new ArrayList<ParentObject>();
	public PinSignalTypes _pinSignal;
	private Bitmap part;
	private Canvas comboImage;


	public PartObject(Bitmap partbitmap,Bitmap pinbitmap,String partName,PinTypes pinType,PinSignalTypes signalType) {

		_blockbitmap=partbitmap;
		_pinbitmap=pinbitmap;
		_partName=partName;
		_pinTypes=pinType;
		_bigpinbitmap=pinbitmap;
		_pinSignal=signalType;

		drawPart();


		addPins(pinType);
		_coordinates = new Coordinates();
		drawText();


	}
	
	public void drawPart(){
		part= Bitmap.createBitmap(_blockbitmap.getWidth()+_pinbitmap.getWidth(), _blockbitmap.getHeight()+_pinbitmap.getHeight(), Bitmap.Config.ARGB_8888); 
		comboImage = new Canvas(part);
		comboImage.drawBitmap(_blockbitmap,_pinbitmap.getWidth()/2,_pinbitmap.getHeight()/2,null); 
	}

	public String getPinSelected(){
		return _pinSelected;
	}
	public PinTypes getPinTypes(){
		return _pinTypes;
	}

	public Bundle getPinData(String selectedPin){
		Bundle data= _blockData.getBundle(selectedPin);
		/*Double arrayLength=_blockData.getDouble("arrayLength");
		data.putDouble("arrayLength", arrayLength);*/
		return data;
	}

	public Bundle setPinData(String selectedPin,Bundle bundle){

		_blockData.putBundle(selectedPin, bundle);
		return _blockData;
	}

	public void addChild(PartObject child,String childPin,String partPin){
		ChildObject newChild=new ChildObject();
		newChild.childPart=child;
		newChild.childPin=childPin;
		newChild.parentPin=partPin;
		_childList.add(newChild);
	}
	public void removeChild(PartObject child,String childPin,String partPin){
		int i=0;
		for(ChildObject childObject:_childList){
			if(childObject.childPart._partName.equalsIgnoreCase(child._partName)&&childPin.equalsIgnoreCase(childObject.childPin)&&partPin.equalsIgnoreCase(childObject.parentPin)){
				_childList.remove(i);
				break;
			}
			i++;
		}
				
		
			
		
		
	}

	public void addParent(PartObject parent,String parentPin,String partPin){
		ParentObject newParent=new ParentObject();
		newParent.parentPart=parent;
		newParent.parentPin=parentPin;
		newParent.partPin=partPin;
		_parentList.add(newParent);
	}

	public void updateChildern(){
		for(ChildObject child:_childList){
			Bundle data=getPinData(child.parentPin);
			child.childPart.setPinData(child.childPin,data);
			PartsController.updateBlock(child.childPart._blockData);
		}
	}

 public List<PartObject> getChildList(){
	 List<PartObject> childObjectList=new ArrayList<PartObject>();
	 
	 for(ChildObject childObject:_childList){
		childObjectList.add(childObject.childPart);
		}
	
	return childObjectList;
 }
	public void setBlockData(Bundle blockData){
		_blockData=blockData;
	}

	public Bundle getBlockData(){
		return _blockData;
	}
	
	public static class PinCoordinates{
		float xCoordinate;
		float yCoordinate;
	}

	public static class ChildObject implements Comparator{
		PartObject childPart;
		String childPin;
		String parentPin;
		@Override
		public int compare(Object arg0, Object arg1) {
			ChildObject graphic1=(ChildObject)arg0;
			ChildObject graphic2=(ChildObject)arg1;
			if(graphic1.childPart.equals(graphic2.childPart)&&graphic1.childPin.equalsIgnoreCase(graphic2.childPin)&&graphic1.parentPin.equalsIgnoreCase(graphic2.parentPin))
			return 1;
			
				return 0;
		}
	}
	public static class ParentObject{
		PartObject parentPart;
		String parentPin;
		String partPin;
	}

	public PinCoordinates getRightPinCoordinates(){
		Rect pinPosition=new Rect();
		PinCoordinates center=new PinCoordinates();
		pinPosition.set(_coordinates.getRightX()-_pinbitmap.getWidth(),_coordinates.getTopY()+(_partbitmap.getHeight()-_pinbitmap.getHeight())/2 ,_coordinates.getRightX() , _coordinates.getTopY()+(_partbitmap.getHeight()+_pinbitmap.getHeight())/2);
		center.xCoordinate=pinPosition.exactCenterX();
		center.yCoordinate=pinPosition.exactCenterY();
		return center;
	}

	public PinCoordinates getLeftPinCoordinates(){
		Rect pinPosition=new Rect();
		PinCoordinates center=new PinCoordinates();
		pinPosition.set(_coordinates.getLeftX(),_coordinates.getTopY()+(_partbitmap.getHeight()-_pinbitmap.getHeight())/2 ,_coordinates.getLeftX()+_pinbitmap.getWidth() , _coordinates.getTopY()+(_partbitmap.getHeight()+_pinbitmap.getHeight())/2);
		center.xCoordinate=pinPosition.exactCenterX();
		center.yCoordinate=pinPosition.exactCenterY();
		return center;
	}
	public PinCoordinates getLeftTopPinCoordinates(){
		Rect pinPosition=new Rect();
		PinCoordinates center=new PinCoordinates();
		pinPosition.set(_coordinates.getLeftX(),_coordinates.getTopY(),_coordinates.getLeftX()+_pinbitmap.getWidth() , _coordinates.getTopY()+_pinbitmap.getHeight());
		center.xCoordinate=pinPosition.exactCenterX();
		center.yCoordinate=pinPosition.exactCenterY();
		return center;
	}
	public PinCoordinates getLeftBottomPinCoordinates(){
		Rect pinPosition=new Rect();
		PinCoordinates center=new PinCoordinates();
		pinPosition.set(_coordinates.getLeftX(),_coordinates.getBottomY()-_pinbitmap.getHeight(),_coordinates.getLeftX()+_pinbitmap.getWidth() , _coordinates.getBottomY());
		center.xCoordinate=pinPosition.exactCenterX();
		center.yCoordinate=pinPosition.exactCenterY();
		return center;
	}
	public PinCoordinates getTopPinCoordinates(){
		Rect pinPosition=new Rect();
		PinCoordinates center=new PinCoordinates();
		pinPosition.set((_coordinates.getLeftX()+_coordinates.getRightX()-_pinbitmap.getWidth())/2,_coordinates.getTopY(),(_coordinates.getLeftX()+_coordinates.getRightX()+_pinbitmap.getWidth())/2, _coordinates.getTopY()+_pinbitmap.getHeight());
		center.xCoordinate=pinPosition.exactCenterX();
		center.yCoordinate=pinPosition.exactCenterY();
		return center;
	}
	public PinCoordinates getBottomPinCoordinates(){
		Rect pinPosition=new Rect();
		PinCoordinates center=new PinCoordinates();
		pinPosition.set((_coordinates.getLeftX()+_coordinates.getRightX()-_pinbitmap.getWidth())/2,_coordinates.getBottomY()-_pinbitmap.getHeight(),(_coordinates.getLeftX()+_coordinates.getRightX()+_pinbitmap.getWidth())/2, _coordinates.getBottomY());
		center.xCoordinate=pinPosition.exactCenterX();
		center.yCoordinate=pinPosition.exactCenterY();
		return center;
	}
	public PinCoordinates getRightTopPinCoordinates(){
		Rect pinPosition=new Rect();
		PinCoordinates center=new PinCoordinates();
		pinPosition.set(_coordinates.getRightX()-_pinbitmap.getWidth(),_coordinates.getTopY(),_coordinates.getRightX() , _coordinates.getTopY()+_pinbitmap.getHeight());
		center.xCoordinate=pinPosition.exactCenterX();
		center.yCoordinate=pinPosition.exactCenterY();
		return center;
	}
	public PinCoordinates getRightBottomPinCoordinates(){
		Rect pinPosition=new Rect();
		PinCoordinates center=new PinCoordinates();
		pinPosition.set(_coordinates.getRightX()-_pinbitmap.getWidth(),_coordinates.getBottomY()-_pinbitmap.getHeight(),_coordinates.getRightX() , _coordinates.getBottomY());
		center.xCoordinate=pinPosition.exactCenterX();
		center.yCoordinate=pinPosition.exactCenterY();
		return center;
	}



	public void setHasConnections(){
		hasConnection=true;
	}

	public boolean hasConnections(){
		return hasConnection;
	}

	public boolean isPinSelected(int positionX,int positionY){
		Rect rightPinPosition=new Rect();
		int radius=10;
		rightPinPosition.set(_coordinates.getRightX()-_pinbitmap.getWidth()-radius,_coordinates.getTopY()+(_partbitmap.getHeight()-_pinbitmap.getHeight())/2 -radius,_coordinates.getRightX() +radius, _coordinates.getTopY()+(_partbitmap.getHeight()+_pinbitmap.getHeight())/2+radius);
		Rect leftPinPosition=new Rect();
		leftPinPosition.set(_coordinates.getLeftX()-radius,_coordinates.getTopY()+(_partbitmap.getHeight()-_pinbitmap.getHeight())/2 -radius,_coordinates.getLeftX()+_pinbitmap.getWidth()+radius , _coordinates.getTopY()+(_partbitmap.getHeight()+_pinbitmap.getHeight())/2+radius);
		Rect leftTopPinPosition=new Rect();
		leftTopPinPosition.set(_coordinates.getLeftX()-radius,_coordinates.getTopY()-radius,_coordinates.getLeftX()+_pinbitmap.getWidth()+radius , _coordinates.getTopY()+_pinbitmap.getHeight()+radius);
		Rect leftBottomPinPosition=new Rect();
		leftBottomPinPosition.set(_coordinates.getLeftX(),_coordinates.getBottomY()-_pinbitmap.getHeight(),_coordinates.getLeftX()+_pinbitmap.getWidth() , _coordinates.getBottomY()+radius);
		Rect topPinPosition=new Rect();
		topPinPosition.set((_coordinates.getLeftX()+_coordinates.getRightX()-_pinbitmap.getWidth())/2-radius,_coordinates.getTopY()-radius,(_coordinates.getLeftX()+_coordinates.getRightX()+_pinbitmap.getWidth())/2+radius, _coordinates.getTopY()+_pinbitmap.getHeight()+radius);
		Rect bottomPinPosition=new Rect();
		bottomPinPosition.set((_coordinates.getLeftX()+_coordinates.getRightX()-_pinbitmap.getWidth())/2-radius,_coordinates.getBottomY()-_pinbitmap.getHeight()-radius,(_coordinates.getLeftX()+_coordinates.getRightX()+_pinbitmap.getWidth())/2+radius, _coordinates.getBottomY()+radius);
		Rect rightTopPinPosition=new Rect();
		rightTopPinPosition.set(_coordinates.getRightX()-_pinbitmap.getWidth()-radius,_coordinates.getTopY()-radius,_coordinates.getRightX()+radius , _coordinates.getTopY()+_pinbitmap.getHeight()+radius);
		Rect rightBottomPinPosition=new Rect();
		rightBottomPinPosition.set(_coordinates.getRightX()-_pinbitmap.getWidth()-radius,_coordinates.getBottomY()-_pinbitmap.getHeight()-radius,_coordinates.getRightX()+radius , _coordinates.getBottomY()+radius);
		if((rightPinPosition.contains(positionX,positionY))&&(_pinTypes.rightPin==true)){
			_pinSelected="rightPin";
			return true;
		}
		if((leftPinPosition.contains(positionX,positionY))&&(_pinTypes.leftPin==true)){
			_pinSelected="leftPin";
			return true;
		}
		if((leftTopPinPosition.contains(positionX,positionY))&&((_pinTypes.leftTopPin==true))){
			_pinSelected="leftTopPin";
			return true;
		}
		if((leftBottomPinPosition.contains(positionX,positionY))&&((_pinTypes.leftBottomPin==true))){
			_pinSelected="leftBottomPin";
			return true;
		}
		if((topPinPosition.contains(positionX,positionY))&&((_pinTypes.topPin==true))){
			_pinSelected="topPin";
			return true;
		}
		if((bottomPinPosition.contains(positionX,positionY))&&(_pinTypes.bottomPin==true)){
			_pinSelected="bottomPin";
			return true;
		}
		if((rightTopPinPosition.contains(positionX,positionY))&&((_pinTypes.rightTopPin==true))){
			_pinSelected="rightTopPin";
			return true;
		}
		if((rightBottomPinPosition.contains(positionX,positionY))&&((_pinTypes.rightBottomPin==true))){
			_pinSelected="rightBottomPin";
			return true;
		}

		else
			return false;
	}
	public PinCoordinates getSelectedPinCoordinates(String selectedPin){
		
		if(selectedPin.equalsIgnoreCase("rightPin")){
			_isRightPinConnected=true;
			return getRightPinCoordinates();
		}
		else if(selectedPin.equalsIgnoreCase("leftPin")){
			_isLeftPinConnected=true;
			return getLeftPinCoordinates();
		}
		else if(selectedPin.equalsIgnoreCase("rightTopPin")){
			
			_isRightTopPinConnected=true;
			return getRightTopPinCoordinates();
		}
		else if(selectedPin.equalsIgnoreCase("rightBottomPin")){
			_isRightBottomPinConnected=true;
			return getRightBottomPinCoordinates();
			}
		else if(selectedPin.equalsIgnoreCase("leftTopPin")){
			_isLeftTopPinConnected=true;
			return getLeftTopPinCoordinates();
		}
		else if(selectedPin.equalsIgnoreCase("leftBottomPin")){
			_isLeftBottomPinConnected=true;
			return getLeftBottomPinCoordinates();
		}
		else if(selectedPin.equalsIgnoreCase("topPin")){
			_isTopPinConnected=true;
			return getTopPinCoordinates();
		}
		else if(selectedPin.equalsIgnoreCase("bottomPin")){			
			_isBottomPinConnected=true;
			return getBottomPinCoordinates();
		}
		else return new PinCoordinates();
		
	
	}
public boolean getIsPinConnected(String selectedPin){
		
		if(selectedPin.equalsIgnoreCase("rightPin")){			
			return _isRightPinConnected;
		}
		else if(selectedPin.equalsIgnoreCase("leftPin")){			
			return _isLeftPinConnected;
		}
		else if(selectedPin.equalsIgnoreCase("rightTopPin")){
		     return _isRightTopPinConnected;
		}
		else if(selectedPin.equalsIgnoreCase("rightBottomPin")){
			return _isRightBottomPinConnected;
			}
		else if(selectedPin.equalsIgnoreCase("leftTopPin")){
		     return _isLeftTopPinConnected;
		}
		else if(selectedPin.equalsIgnoreCase("leftBottomPin")){
			return _isLeftBottomPinConnected;
		}
		else if(selectedPin.equalsIgnoreCase("topPin")){
			return _isTopPinConnected;
		}
		else if(selectedPin.equalsIgnoreCase("bottomPin")){		
			
			return _isBottomPinConnected;
		}
		else return false;
		
	
	}
public void setIsPinConnected(String selectedPin,Boolean ifConnected){
	
	if(selectedPin.equalsIgnoreCase("rightPin")){			
	_isRightPinConnected=ifConnected;
	}
	else if(selectedPin.equalsIgnoreCase("leftPin")){			
	 _isLeftPinConnected=ifConnected;
	}
	else if(selectedPin.equalsIgnoreCase("rightTopPin")){
	     _isRightTopPinConnected=ifConnected;
	}
	else if(selectedPin.equalsIgnoreCase("rightBottomPin")){
		_isRightBottomPinConnected=ifConnected;
		}
	else if(selectedPin.equalsIgnoreCase("leftTopPin")){
	    _isLeftTopPinConnected=ifConnected;
	}
	else if(selectedPin.equalsIgnoreCase("leftBottomPin")){
	_isLeftBottomPinConnected=ifConnected;
	}
	else if(selectedPin.equalsIgnoreCase("topPin")){
	_isTopPinConnected=ifConnected;
	}
	else if(selectedPin.equalsIgnoreCase("bottomPin")){		
		
	_isBottomPinConnected=ifConnected;
	}
	
	

}

	public void highlightPin(PinTypes pinTypes,String selectedPin){
		Bitmap largePin=resizePin();
		redrawObject(pinTypes,selectedPin,largePin);    	
		drawText();
	}
	public void backtoSizePin(PinTypes pinTypes){    	
		addPins(pinTypes);    	
		drawText();
	}
	public static class PinTypes implements Serializable{
		private static final long serialVersionUID = 168457949653366016L;
		boolean rightPin=false;
		boolean leftPin=false;
		boolean topPin=false;
		boolean bottomPin=false;
		boolean leftTopPin=false;
		boolean leftBottomPin=false;
		boolean rightTopPin=false;
		boolean rightBottomPin=false;

	}
	public static class PinSignalTypes implements Serializable{
		/**
		 * 
		 */
		 private static final long serialVersionUID = 1L;

		 String rightPin="time";
		 String leftPin="time";
		 String topPin="time";
		 String bottomPin="time";
		 String leftTopPin="time";
		 String leftBottomPin="time";
		 String rightTopPin="time";
		 String rightBottomPin="time";


	}
	private Bitmap resizePin(){  

		int width = _pinbitmap.getWidth();    		
		int height = _pinbitmap.getHeight();    	
		float scaleWidth = 2;    		
		float scaleHeight = 2;    		
		Matrix matrix = new Matrix();    		
		matrix.postScale(scaleWidth, scaleHeight);    		
		return Bitmap.createBitmap(_pinbitmap, 0, 0, width, height, matrix, false);
	}



	public void addPins(PinTypes partPins){

		if(partPins.rightPin&&!partPins.leftPin&&!partPins.topPin&&!partPins.bottomPin&&!partPins.leftBottomPin&&!partPins.leftTopPin&&!partPins.rightBottomPin&&!partPins.rightTopPin){
			comboImage.drawBitmap(_pinbitmap, _blockbitmap.getWidth(), (part.getHeight()-_pinbitmap.getHeight())/2, null);
			_partbitmap=part;

		}else if((partPins.rightPin&&partPins.leftPin&&!partPins.topPin&&!partPins.bottomPin&&!partPins.leftBottomPin&&!partPins.leftTopPin&&!partPins.rightBottomPin&&!partPins.rightTopPin)){

			comboImage.drawBitmap(_pinbitmap, _blockbitmap.getWidth(), (part.getHeight()-_pinbitmap.getHeight())/2, null);
			comboImage.drawBitmap(_pinbitmap, 0, (_blockbitmap.getHeight())/2, null);
			_partbitmap=part;
		}
		else if((!partPins.rightPin&&partPins.leftPin&&!partPins.topPin&&!partPins.bottomPin&&!partPins.leftBottomPin&&!partPins.leftTopPin&&!partPins.rightBottomPin&&!partPins.rightTopPin)){

			comboImage.drawBitmap(_pinbitmap, 0, (_blockbitmap.getHeight()-_pinbitmap.getHeight())/2, null);
			_partbitmap=part;

		}else if((!partPins.rightPin&&!partPins.leftPin&&!partPins.topPin&&!partPins.bottomPin&&!partPins.leftBottomPin&&!partPins.leftTopPin&&!partPins.rightBottomPin&&!partPins.rightTopPin)){
			_partbitmap=part;
		}else if((partPins.rightPin&&!partPins.leftPin&&!partPins.topPin&&!partPins.bottomPin&&partPins.leftBottomPin&&partPins.leftTopPin)){
			comboImage.drawBitmap(_pinbitmap, _blockbitmap.getWidth(), _blockbitmap.getHeight()/2, null);
			comboImage.drawBitmap(_pinbitmap, 0,0, null);
			comboImage.drawBitmap(_pinbitmap, 0, _blockbitmap.getHeight(), null);
			_partbitmap=part;
		}else if((partPins.rightPin&&partPins.leftPin&&partPins.topPin&&partPins.bottomPin&&!partPins.leftBottomPin&&!partPins.leftTopPin&&!partPins.rightBottomPin&&!partPins.rightTopPin)){
			comboImage.drawBitmap(_pinbitmap, _blockbitmap.getWidth(), _blockbitmap.getHeight()/2, null);
			comboImage.drawBitmap(_pinbitmap, _blockbitmap.getWidth()/2,0, null);
			comboImage.drawBitmap(_pinbitmap,  _blockbitmap.getWidth()/2, _blockbitmap.getHeight(), null);
			comboImage.drawBitmap(_pinbitmap, 0, _blockbitmap.getHeight()/2, null);
			_partbitmap=part;
		}else if((!partPins.rightPin&&!partPins.leftPin&&partPins.topPin&&!partPins.bottomPin&&!partPins.leftBottomPin&&!partPins.leftTopPin&&!partPins.rightBottomPin&&!partPins.rightTopPin)){
			comboImage.drawBitmap(_pinbitmap, (_blockbitmap.getWidth())/2,0, null);
			_partbitmap=part;
		}else if((partPins.rightPin&&!partPins.leftPin&&partPins.topPin&&partPins.bottomPin&&!partPins.leftBottomPin&&!partPins.leftTopPin&&!partPins.rightBottomPin&&!partPins.rightTopPin)){
			comboImage.drawBitmap(_pinbitmap, _blockbitmap.getWidth(), _blockbitmap.getHeight()/2, null);
			comboImage.drawBitmap(_pinbitmap, _blockbitmap.getWidth()/2,0, null);
			comboImage.drawBitmap(_pinbitmap,  _blockbitmap.getWidth()/2, _blockbitmap.getHeight(), null);
			_partbitmap=part;
		}else if((!partPins.rightPin&&!partPins.leftPin&&!partPins.topPin&&partPins.bottomPin&&!partPins.leftBottomPin&&!partPins.leftTopPin&&!partPins.rightBottomPin&&!partPins.rightTopPin)){
			comboImage.drawBitmap(_pinbitmap,  _blockbitmap.getWidth()/2, _blockbitmap.getHeight(), null);
			_partbitmap=part;
		}else if((!partPins.rightPin&&partPins.leftPin&&!partPins.topPin&&!partPins.bottomPin&&!partPins.leftBottomPin&&!partPins.leftTopPin&&partPins.rightBottomPin&&partPins.rightTopPin)){
			comboImage.drawBitmap(_pinbitmap,0, _blockbitmap.getHeight()/2, null);
			comboImage.drawBitmap(_pinbitmap, _blockbitmap.getWidth(),0, null);
			comboImage.drawBitmap(_pinbitmap, _blockbitmap.getWidth(), _blockbitmap.getHeight(), null);
			_partbitmap=part;
		} 	
		else if((!partPins.rightPin&&!partPins.leftPin&&partPins.topPin&&partPins.bottomPin&&!partPins.leftBottomPin&&!partPins.leftTopPin&&!partPins.rightBottomPin&&!partPins.rightTopPin)){
			comboImage.drawBitmap(_pinbitmap, (_blockbitmap.getWidth())/2,0, null);
			comboImage.drawBitmap(_pinbitmap,  _blockbitmap.getWidth()/2, _blockbitmap.getHeight(), null);
			_partbitmap=part;
		} else if((!partPins.rightPin&&!partPins.leftPin&&!partPins.topPin&&!partPins.bottomPin&&partPins.leftBottomPin&&partPins.leftTopPin)){
			
			comboImage.drawBitmap(_pinbitmap, 0,0, null);
			comboImage.drawBitmap(_pinbitmap, 0, _blockbitmap.getHeight(), null);
			_partbitmap=part;
		}	


	}
	public void redrawObject(PinTypes partPins,String selectedPin,Bitmap largePin){

		if(partPins.rightPin&&!partPins.leftPin&&!partPins.topPin&&!partPins.bottomPin&&!partPins.leftBottomPin&&!partPins.leftTopPin&&!partPins.rightBottomPin&&!partPins.rightTopPin){

			Bitmap part= Bitmap.createBitmap(_blockbitmap.getWidth()+largePin.getWidth()/2, _blockbitmap.getHeight(), Bitmap.Config.ARGB_8888); 
			Canvas comboImage = new Canvas(part); 
			comboImage.drawBitmap(_blockbitmap, 0f, 0f, null); 
			comboImage.drawBitmap(largePin, _blockbitmap.getWidth()-largePin.getWidth()/2, (_blockbitmap.getHeight()-largePin.getHeight())/2, null);
			_partbitmap=part;

		}else if((partPins.rightPin&&partPins.leftPin&&!partPins.topPin&&!partPins.bottomPin&&!partPins.leftBottomPin&&!partPins.leftTopPin&&!partPins.rightBottomPin&&!partPins.rightTopPin)){

			Bitmap part= Bitmap.createBitmap(_blockbitmap.getWidth()+(_pinbitmap.getWidth()+largePin.getWidth())/2, _blockbitmap.getHeight(), Bitmap.Config.ARGB_8888); 
			Canvas comboImage = new Canvas(part);
			if(selectedPin=="leftPin"){
				comboImage.drawBitmap(_blockbitmap,largePin.getWidth()/2, 0f, null); 
				comboImage.drawBitmap(_pinbitmap, part.getWidth()-_pinbitmap.getWidth(), (_blockbitmap.getHeight()-_pinbitmap.getHeight())/2, null);
				comboImage.drawBitmap(largePin, 0, (_blockbitmap.getHeight()-largePin.getHeight())/2, null);
				_partbitmap=part;
			}else if(selectedPin=="rightPin"){
				comboImage.drawBitmap(_blockbitmap,_pinbitmap.getWidth()/2, 0f, null); 
				comboImage.drawBitmap(largePin, part.getWidth()-largePin.getWidth(), (_blockbitmap.getHeight()-largePin.getHeight())/2, null);
				comboImage.drawBitmap(_pinbitmap, 0, (_blockbitmap.getHeight()-_pinbitmap.getHeight())/2, null);
				_partbitmap=part;
			}

		}
		else if((!partPins.rightPin&&partPins.leftPin&&!partPins.topPin&&!partPins.bottomPin&&!partPins.leftBottomPin&&!partPins.leftTopPin&&!partPins.rightBottomPin&&!partPins.rightTopPin)){

			Bitmap part= Bitmap.createBitmap(_blockbitmap.getWidth()+largePin.getWidth()/2, _blockbitmap.getHeight(), Bitmap.Config.ARGB_8888); 
			Canvas comboImage = new Canvas(part); 
			comboImage.drawBitmap(_blockbitmap,largePin.getWidth()/2, 0f, null); 
			comboImage.drawBitmap(largePin, 0, (_blockbitmap.getHeight()-largePin.getHeight())/2, null);
			_partbitmap=part;

		}else if((!partPins.rightPin&&!partPins.leftPin&&!partPins.topPin&&!partPins.bottomPin&&!partPins.leftBottomPin&&!partPins.leftTopPin&&!partPins.rightBottomPin&&!partPins.rightTopPin)){
			Bitmap part= Bitmap.createBitmap(_blockbitmap.getWidth(), _blockbitmap.getHeight(), Bitmap.Config.ARGB_8888); 
			Canvas comboImage = new Canvas(part); 
			comboImage.drawBitmap(_blockbitmap,0, 0, null); 
			_partbitmap=part;
		}else if((partPins.rightPin&&!partPins.leftPin&&!partPins.topPin&&!partPins.bottomPin&&partPins.leftBottomPin&&partPins.leftTopPin)){


			if(selectedPin=="rightPin"){

				Bitmap part= Bitmap.createBitmap(_blockbitmap.getWidth()+(_pinbitmap.getWidth()+largePin.getWidth())/2, _blockbitmap.getHeight()+_pinbitmap.getHeight(), Bitmap.Config.ARGB_8888); 
				Canvas comboImage = new Canvas(part); 
				comboImage.drawBitmap(_blockbitmap,_pinbitmap.getWidth()/2,_pinbitmap.getHeight()/2,null); 
				comboImage.drawBitmap(largePin,part.getWidth()-largePin.getWidth(), (_pinbitmap.getHeight()+_blockbitmap.getHeight()-largePin.getHeight())/2, null);
				comboImage.drawBitmap(_pinbitmap, 0,0, null);
				comboImage.drawBitmap(_pinbitmap, 0, _blockbitmap.getHeight(), null);
				_partbitmap=part;
			}else if(selectedPin=="leftTopPin"){
				Bitmap part= Bitmap.createBitmap(_blockbitmap.getWidth()+(_pinbitmap.getWidth()+largePin.getWidth())/2, _blockbitmap.getHeight()+(_pinbitmap.getHeight()+largePin.getHeight())/2, Bitmap.Config.ARGB_8888); 
				Canvas comboImage = new Canvas(part); 
				comboImage.drawBitmap(_blockbitmap,largePin.getWidth()/2,largePin.getHeight()/2,null); 
				comboImage.drawBitmap(_pinbitmap,part.getWidth()-_pinbitmap.getWidth(), (-_pinbitmap.getHeight()+_blockbitmap.getHeight()+largePin.getHeight())/2, null);
				comboImage.drawBitmap(largePin, 0,0, null);
				comboImage.drawBitmap(_pinbitmap, (largePin.getWidth()-_pinbitmap.getWidth())/2, part.getHeight()-_pinbitmap.getHeight(), null);
				_partbitmap=part;
			}else if(selectedPin=="leftBottomPin"){
				Bitmap part= Bitmap.createBitmap(_blockbitmap.getWidth()+(_pinbitmap.getWidth()+largePin.getWidth())/2, _blockbitmap.getHeight()+(_pinbitmap.getHeight()+largePin.getHeight())/2, Bitmap.Config.ARGB_8888); 
				Canvas comboImage = new Canvas(part); 
				comboImage.drawBitmap(_blockbitmap,largePin.getWidth()/2,_pinbitmap.getHeight()/2,null); 
				comboImage.drawBitmap(_pinbitmap,part.getWidth()-_pinbitmap.getWidth(), _blockbitmap.getHeight()/2, null);
				comboImage.drawBitmap(_pinbitmap, (largePin.getWidth()-_pinbitmap.getWidth())/2,0, null);
				comboImage.drawBitmap(largePin, 0, part.getHeight()-largePin.getHeight(), null);
				_partbitmap=part;
			}

		}else if((partPins.rightPin&&partPins.leftPin&&partPins.topPin&&partPins.bottomPin&&!partPins.leftBottomPin&&!partPins.leftTopPin&&!partPins.rightBottomPin&&!partPins.rightTopPin)){
			if(selectedPin=="leftPin"){
				Bitmap part= Bitmap.createBitmap(_blockbitmap.getWidth()+(_pinbitmap.getWidth()+largePin.getWidth())/2, _blockbitmap.getHeight()+_pinbitmap.getHeight(), Bitmap.Config.ARGB_8888); 
				Canvas comboImage = new Canvas(part); 
				comboImage.drawBitmap(_blockbitmap,largePin.getWidth()/2,_pinbitmap.getHeight()/2,null); 
				comboImage.drawBitmap(_pinbitmap, part.getWidth()-_pinbitmap.getWidth(), _blockbitmap.getHeight()/2, null);
				comboImage.drawBitmap(_pinbitmap, (-_pinbitmap.getWidth()+_blockbitmap.getWidth()+largePin.getWidth())/2,0, null);
				comboImage.drawBitmap(_pinbitmap,  (-_pinbitmap.getWidth()+_blockbitmap.getWidth()+largePin.getWidth())/2, _blockbitmap.getHeight(), null);
				comboImage.drawBitmap(largePin, 0,(_pinbitmap.getHeight()+_blockbitmap.getHeight()-largePin.getHeight())/2, null);
				_partbitmap=part;
			}else if(selectedPin=="rightPin"){
				Bitmap part= Bitmap.createBitmap(_blockbitmap.getWidth()+(_pinbitmap.getWidth()+largePin.getWidth())/2, _blockbitmap.getHeight()+_pinbitmap.getHeight(), Bitmap.Config.ARGB_8888); 
				Canvas comboImage = new Canvas(part); 
				comboImage.drawBitmap(_blockbitmap,_pinbitmap.getWidth()/2,_pinbitmap.getHeight()/2,null); 
				comboImage.drawBitmap(largePin, part.getWidth()-largePin.getWidth(),(_pinbitmap.getHeight()+_blockbitmap.getHeight()-largePin.getHeight())/2, null);
				comboImage.drawBitmap(_pinbitmap, _blockbitmap.getWidth()/2,0, null);
				comboImage.drawBitmap(_pinbitmap,  _blockbitmap.getWidth()/2, _blockbitmap.getHeight(), null);
				comboImage.drawBitmap(_pinbitmap, 0, _blockbitmap.getHeight()/2, null);
				_partbitmap=part;
			}else if(selectedPin=="topPin"){
				Bitmap part= Bitmap.createBitmap(_blockbitmap.getWidth()+_pinbitmap.getWidth(), _blockbitmap.getHeight()+(largePin.getHeight()+_pinbitmap.getHeight())/2, Bitmap.Config.ARGB_8888); 
				Canvas comboImage = new Canvas(part); 
				comboImage.drawBitmap(_blockbitmap,_pinbitmap.getWidth()/2,largePin.getHeight()/2,null); 
				comboImage.drawBitmap(_pinbitmap, part.getWidth()-_pinbitmap.getWidth(), (-_pinbitmap.getHeight()+_blockbitmap.getHeight()+largePin.getHeight())/2, null);
				comboImage.drawBitmap(largePin, (_pinbitmap.getWidth()+_blockbitmap.getWidth()-largePin.getWidth())/2,0, null);
				comboImage.drawBitmap(_pinbitmap,  _blockbitmap.getWidth()/2, (part.getHeight()-_pinbitmap.getHeight()), null);
				comboImage.drawBitmap(_pinbitmap, 0,(-_pinbitmap.getHeight()+_blockbitmap.getHeight()+largePin.getHeight())/2, null);
				_partbitmap=part;
			}else if(selectedPin=="bottomPin"){
				Bitmap part= Bitmap.createBitmap(_blockbitmap.getWidth()+_pinbitmap.getWidth(), _blockbitmap.getHeight()+(largePin.getHeight()+_pinbitmap.getHeight())/2, Bitmap.Config.ARGB_8888); 
				Canvas comboImage = new Canvas(part); 
				comboImage.drawBitmap(_blockbitmap,_pinbitmap.getWidth()/2,_pinbitmap.getHeight()/2,null); 
				comboImage.drawBitmap(_pinbitmap, part.getWidth()-_pinbitmap.getWidth(), _blockbitmap.getHeight()/2, null);
				comboImage.drawBitmap(_pinbitmap, _blockbitmap.getWidth()/2,0, null);
				comboImage.drawBitmap(largePin,  (_pinbitmap.getWidth()+_blockbitmap.getWidth()-largePin.getWidth())/2, (part.getHeight()-largePin.getHeight()), null);
				comboImage.drawBitmap(_pinbitmap, 0,_blockbitmap.getHeight()/2, null);
				_partbitmap=part;
			}

		}else if((!partPins.rightPin&&!partPins.leftPin&&partPins.topPin&&!partPins.bottomPin&&!partPins.leftBottomPin&&!partPins.leftTopPin&&!partPins.rightBottomPin&&!partPins.rightTopPin)){
			Bitmap part= Bitmap.createBitmap(_blockbitmap.getWidth(), _blockbitmap.getHeight()+largePin.getHeight()/2, Bitmap.Config.ARGB_8888); 
			Canvas comboImage = new Canvas(part); 
			comboImage.drawBitmap(_blockbitmap,0,largePin.getHeight()/2,null); 
			comboImage.drawBitmap(largePin, (_blockbitmap.getWidth()-largePin.getWidth())/2,0, null);
			_partbitmap=part;
		}else if((partPins.rightPin&&!partPins.leftPin&&partPins.topPin&&partPins.bottomPin&&!partPins.leftBottomPin&&!partPins.leftTopPin&&!partPins.rightBottomPin&&!partPins.rightTopPin)){
			if(selectedPin=="rightPin"){
				Bitmap part= Bitmap.createBitmap(_blockbitmap.getWidth()+(largePin.getWidth())/2, _blockbitmap.getHeight()+_pinbitmap.getHeight(), Bitmap.Config.ARGB_8888); 
				Canvas comboImage = new Canvas(part); 
				comboImage.drawBitmap(_blockbitmap,0,_pinbitmap.getHeight()/2,null); 
				comboImage.drawBitmap(largePin, part.getWidth()-largePin.getWidth(),(_pinbitmap.getHeight()+_blockbitmap.getHeight()-largePin.getHeight())/2, null);
				comboImage.drawBitmap(_pinbitmap,(_blockbitmap.getWidth()-_pinbitmap.getWidth())/2,0, null);
				comboImage.drawBitmap(_pinbitmap, (_blockbitmap.getWidth()-_pinbitmap.getWidth())/2, _blockbitmap.getHeight(), null);
				_partbitmap=part;
			}else if(selectedPin=="topPin"){
				Bitmap part= Bitmap.createBitmap(_blockbitmap.getWidth()+_pinbitmap.getWidth()/2, _blockbitmap.getHeight()+(largePin.getHeight()+_pinbitmap.getHeight())/2, Bitmap.Config.ARGB_8888); 
				Canvas comboImage = new Canvas(part); 
				comboImage.drawBitmap(_blockbitmap,0,largePin.getHeight()/2,null); 
				comboImage.drawBitmap(_pinbitmap, part.getWidth()-_pinbitmap.getWidth(), (-_pinbitmap.getHeight()+_blockbitmap.getHeight()+largePin.getHeight())/2, null);
				comboImage.drawBitmap(largePin, (_blockbitmap.getWidth()-largePin.getWidth())/2,0, null);
				comboImage.drawBitmap(_pinbitmap, (_blockbitmap.getWidth()-_pinbitmap.getWidth())/2, (part.getHeight()-_pinbitmap.getHeight()), null);

				_partbitmap=part;
			}else if(selectedPin=="bottomPin"){
				Bitmap part= Bitmap.createBitmap(_blockbitmap.getWidth()+_pinbitmap.getWidth()/2, _blockbitmap.getHeight()+(largePin.getHeight()+_pinbitmap.getHeight())/2, Bitmap.Config.ARGB_8888); 
				Canvas comboImage = new Canvas(part); 
				comboImage.drawBitmap(_blockbitmap,0,_pinbitmap.getHeight()/2,null); 
				comboImage.drawBitmap(_pinbitmap, part.getWidth()-_pinbitmap.getWidth(), _blockbitmap.getHeight()/2, null);
				comboImage.drawBitmap(_pinbitmap,(_blockbitmap.getWidth()-_pinbitmap.getWidth())/2,0, null);
				comboImage.drawBitmap(largePin,  (_blockbitmap.getWidth()-largePin.getWidth())/2, (part.getHeight()-largePin.getHeight()), null);
				_partbitmap=part;
			}

		}else if((!partPins.rightPin&&!partPins.leftPin&&!partPins.topPin&&partPins.bottomPin&&!partPins.leftBottomPin&&!partPins.leftTopPin&&!partPins.rightBottomPin&&!partPins.rightTopPin)){
			Bitmap part= Bitmap.createBitmap(_blockbitmap.getWidth(),_blockbitmap.getHeight()+largePin.getHeight()/2, Bitmap.Config.ARGB_8888); 
			Canvas comboImage = new Canvas(part); 
			comboImage.drawBitmap(_blockbitmap,0,0,null); 
			comboImage.drawBitmap(largePin,  _blockbitmap.getWidth()/2-largePin.getWidth()/2, _blockbitmap.getHeight()-largePin.getHeight()/2, null);
			_partbitmap=part;

		}else if((!partPins.rightPin&&partPins.leftPin&&!partPins.topPin&&!partPins.bottomPin&&!partPins.leftBottomPin&&!partPins.leftTopPin&&partPins.rightBottomPin&&partPins.rightTopPin)){

			if(selectedPin=="leftPin"){
				Bitmap part= Bitmap.createBitmap(_blockbitmap.getWidth()+(_pinbitmap.getWidth()+largePin.getWidth())/2, _blockbitmap.getHeight()+_pinbitmap.getHeight(), Bitmap.Config.ARGB_8888); 
				Canvas comboImage = new Canvas(part); 
				comboImage.drawBitmap(_blockbitmap,largePin.getWidth()/2,_pinbitmap.getHeight()/2,null); 
				comboImage.drawBitmap(largePin,0, (_pinbitmap.getHeight()+_blockbitmap.getHeight()-largePin.getHeight())/2, null);
				comboImage.drawBitmap(_pinbitmap, part.getWidth()-_pinbitmap.getWidth(),0, null);
				comboImage.drawBitmap(_pinbitmap, part.getWidth()-_pinbitmap.getWidth(), _blockbitmap.getHeight(), null);
				_partbitmap=part;
			}else if(selectedPin=="rightTopPin"){
				Bitmap part= Bitmap.createBitmap(_blockbitmap.getWidth()+(_pinbitmap.getWidth()+largePin.getWidth())/2, _blockbitmap.getHeight()+(_pinbitmap.getHeight()+largePin.getHeight())/2, Bitmap.Config.ARGB_8888); 
				Canvas comboImage = new Canvas(part); 
				comboImage.drawBitmap(_blockbitmap,_pinbitmap.getWidth()/2,largePin.getHeight()/2,null); 
				comboImage.drawBitmap(_pinbitmap,0,(-_pinbitmap.getHeight()+_blockbitmap.getHeight()+largePin.getHeight())/2, null);
				comboImage.drawBitmap(largePin, part.getWidth()-largePin.getWidth(),0, null);
				comboImage.drawBitmap(_pinbitmap, _blockbitmap.getWidth(), part.getHeight()-_pinbitmap.getHeight(), null);
				_partbitmap=part;
			}else if(selectedPin=="rightBottomPin"){
				Bitmap part= Bitmap.createBitmap(_blockbitmap.getWidth()+(_pinbitmap.getWidth()+largePin.getWidth())/2, _blockbitmap.getHeight()+(_pinbitmap.getHeight()+largePin.getHeight())/2, Bitmap.Config.ARGB_8888); 
				Canvas comboImage = new Canvas(part); 
				comboImage.drawBitmap(_blockbitmap,_pinbitmap.getWidth()/2,_pinbitmap.getHeight()/2,null); 
				comboImage.drawBitmap(_pinbitmap,0, _blockbitmap.getHeight()/2, null);
				comboImage.drawBitmap(_pinbitmap, _blockbitmap.getWidth(),0, null);
				comboImage.drawBitmap(largePin,part.getWidth()-largePin.getWidth(), part.getHeight()-largePin.getHeight(), null);
				_partbitmap=part;
			}
		}else if((!partPins.rightPin&&!partPins.leftPin&&partPins.topPin&&partPins.bottomPin&&!partPins.leftBottomPin&&!partPins.leftTopPin&&!partPins.rightBottomPin&&!partPins.rightTopPin)){
			if(selectedPin=="topPin"){
				Bitmap part= Bitmap.createBitmap(_blockbitmap.getWidth(), _blockbitmap.getHeight()+(largePin.getHeight()+_pinbitmap.getHeight())/2, Bitmap.Config.ARGB_8888); 
				Canvas comboImage = new Canvas(part); 
				comboImage.drawBitmap(_blockbitmap,0,largePin.getHeight()/2,null); 
			    comboImage.drawBitmap(largePin, (_pinbitmap.getWidth()+_blockbitmap.getWidth()-largePin.getWidth())/2,0, null);
				comboImage.drawBitmap(_pinbitmap,  _blockbitmap.getWidth()/2, (part.getHeight()-_pinbitmap.getHeight()), null);
				_partbitmap=part;
			}else if(selectedPin=="bottomPin"){
				Bitmap part= Bitmap.createBitmap(_blockbitmap.getWidth(), _blockbitmap.getHeight()+(largePin.getHeight()+_pinbitmap.getHeight())/2, Bitmap.Config.ARGB_8888); 
				Canvas comboImage = new Canvas(part); 
				comboImage.drawBitmap(_blockbitmap,0,_pinbitmap.getHeight()/2,null); 
				comboImage.drawBitmap(_pinbitmap, _blockbitmap.getWidth()/2,0, null);
				comboImage.drawBitmap(largePin,  (_pinbitmap.getWidth()+_blockbitmap.getWidth()-largePin.getWidth())/2, (part.getHeight()-largePin.getHeight()), null);
				_partbitmap=part;
			}
			
		} 	else if((!partPins.rightPin&&!partPins.leftPin&&!partPins.topPin&&!partPins.bottomPin&&partPins.leftBottomPin&&partPins.leftTopPin)){


			 if(selectedPin=="leftTopPin"){
				Bitmap part= Bitmap.createBitmap(_blockbitmap.getWidth()+(largePin.getWidth())/2, _blockbitmap.getHeight()+(_pinbitmap.getHeight()+largePin.getHeight())/2, Bitmap.Config.ARGB_8888); 
				Canvas comboImage = new Canvas(part); 
				comboImage.drawBitmap(_blockbitmap,largePin.getWidth()/2,largePin.getHeight()/2,null); 
				comboImage.drawBitmap(largePin, 0,0, null);
				comboImage.drawBitmap(_pinbitmap, (largePin.getWidth()-_pinbitmap.getWidth())/2, part.getHeight()-_pinbitmap.getHeight(), null);
				_partbitmap=part;
			}else if(selectedPin=="leftBottomPin"){
				Bitmap part= Bitmap.createBitmap(_blockbitmap.getWidth()+(largePin.getWidth())/2, _blockbitmap.getHeight()+(_pinbitmap.getHeight()+largePin.getHeight())/2, Bitmap.Config.ARGB_8888); 
				Canvas comboImage = new Canvas(part); 
				comboImage.drawBitmap(_blockbitmap,largePin.getWidth()/2,_pinbitmap.getHeight()/2,null); 
				comboImage.drawBitmap(_pinbitmap, (largePin.getWidth()-_pinbitmap.getWidth())/2,0, null);
				comboImage.drawBitmap(largePin, 0, part.getHeight()-largePin.getHeight(), null);
				_partbitmap=part;
			}

		}


	}

	public void drawText(){
		Paint paint=new Paint();
		paint.setColor(Color.WHITE);	
		int a=_partbitmap.getWidth();
		paint.setTextSize(_partbitmap.getWidth()/7);
		paint.setTypeface(Typeface.DEFAULT_BOLD);
		paint.setTextAlign(Paint.Align.CENTER);
		Rect bounds=new Rect();
		paint.getTextBounds(_partName,0, _partName.length(), bounds);
		
		Canvas c = new Canvas (_partbitmap);
	if(_partName.length()<=10)
		c.drawText(_partName, (_partbitmap.getWidth()/2), ((_partbitmap.getHeight()+bounds.bottom-bounds.top)/2),paint);
	else
	{
		String[] name=_partName.split(" ");
		if(name.length==3)
		name[1]=name[1]+" "+name[2];		
		Rect topBounds=new Rect();
		Rect bottomBounds= new Rect();
		paint.getTextBounds(name[0],0, name[0].length(), topBounds);		
		paint.getTextBounds(name[1],0, name[1].length(), bottomBounds);
		c.drawText(name[0], (_partbitmap.getWidth()/2), (2*_partbitmap.getHeight()/5)+(topBounds.bottom-topBounds.top)/2,paint);
		c.drawText(name[1], (_partbitmap.getWidth()/2), (3*_partbitmap.getHeight()/5)+(bottomBounds.bottom-bottomBounds.top)/2,paint);
				
	}
			

	}

	public Bitmap getGraphic() {
		return _partbitmap;
	}

	public Bitmap getPinBitmap() {
		return _pinbitmap;
	}

	public Coordinates getCoordinates() {
		return _coordinates;
	}

	public String getPartName(){
		return _partName;
	}
	public String getSignalType(String pinSelected){
		if(pinSelected.equalsIgnoreCase("rightPin"))
			return _pinSignal.rightPin;
		else if(pinSelected.equalsIgnoreCase("leftPin"))
			return _pinSignal.leftPin;
		else	if(pinSelected.equalsIgnoreCase("topPin"))
			return _pinSignal.topPin;
		else if(pinSelected.equalsIgnoreCase("bottomPin"))
			return _pinSignal.bottomPin;
		else if(pinSelected.equalsIgnoreCase("leftTopPin"))
			return _pinSignal.leftTopPin;
		else if(pinSelected.equalsIgnoreCase("leftBottomPin"))
			return _pinSignal.leftBottomPin;
		else if(pinSelected.equalsIgnoreCase("rightTopPin"))
			return _pinSignal.rightTopPin;
		else if(pinSelected.equalsIgnoreCase("rightBottomPin"))
			return _pinSignal.rightBottomPin;
		else return null;	

	}

	@Override
	public int compare(Object object1, Object object2) {
		PartObject graphic1=(PartObject)object1;
		PartObject graphic2=(PartObject)object2;
		String partName1=graphic1._partName;
		String partName2=graphic2._partName;
		if(partName1.equalsIgnoreCase(partName2))
			return 1;
		else
			return 0;
	}







}