package com.prototype.ajdsp;

import android.graphics.Canvas;
import android.view.SurfaceHolder;

public class MIDIThread extends Thread{
	private SurfaceHolder _surfaceHolder;
    private MIDIView _midiView;
    private boolean _run = false;
    private Object mPauseLock = new Object();  
    private boolean mPaused;
    public MIDIThread(SurfaceHolder surfaceHolder, MIDIView panel) {
        _surfaceHolder = surfaceHolder;
        _midiView = panel;
    }

    public void setRunning(boolean run) {
        _run = run;
    }

    public SurfaceHolder getSurfaceHolder() {
        return _surfaceHolder;
    }

    @Override
    public void run() {
        Canvas c;
        while (_run) {
            c = null;
            try {
                c = _surfaceHolder.lockCanvas(null);
                synchronized (_surfaceHolder) {
                  _midiView.onDraw(c);
                }
            } finally {
                // do this in a finally so that if an exception is thrown
                // during the above, we don't leave the Surface in an
                // inconsistent state
                if (c != null) {
                    _surfaceHolder.unlockCanvasAndPost(c);
                }
            }
        }
    }
    public void onPause() {
        synchronized (mPauseLock) {
            mPaused = true;
        }
    }
    public void onResume() {
        synchronized (mPauseLock) {
            mPaused = false;
            mPauseLock.notifyAll();
        }
    }
}
