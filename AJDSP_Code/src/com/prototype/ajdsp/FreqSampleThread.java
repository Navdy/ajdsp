/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.prototype.ajdsp;

import java.io.Serializable;

import android.graphics.Canvas;
import android.view.SurfaceHolder;

public class FreqSampleThread extends Thread {
	/**
	 * 
	 */
	
	private SurfaceHolder _surfaceHolder;
    private FreqSampleView _freqSampleView;
    public boolean _run = false;
    private Object mPauseLock = new Object();  
    private boolean mPaused;


    public FreqSampleThread(SurfaceHolder surfaceHolder, FreqSampleView panel) {
        _surfaceHolder = surfaceHolder;
        _freqSampleView = panel;
    }

    public void setRunning(boolean run) {
        _run = run;
    }

    public SurfaceHolder getSurfaceHolder() {
        return _surfaceHolder;
    }

    @Override
    public void run() {
        Canvas c;
        while (_run) {
            c = null;
            try {
                c = _surfaceHolder.lockCanvas(null);
                synchronized (_surfaceHolder) {
                  _freqSampleView.onDraw(c);
                }
                synchronized (mPauseLock) {
                    while (mPaused) {                       
                            mPauseLock.wait();
                        
                    }
                }
            } catch (InterruptedException e) {
            }finally {
                // do this in a finally so that if an exception is thrown
                // during the above, we don't leave the Surface in an
                // inconsistent state
                if (c != null) {
                    _surfaceHolder.unlockCanvasAndPost(c);
                }
            }
        }
        
    }
    
    public void onPause() {
        synchronized (mPauseLock) {
            mPaused = true;
        }
    }
    public void onResume() {
        synchronized (mPauseLock) {
            mPaused = false;
            mPauseLock.notifyAll();
        }
    }
}


