package com.prototype.ajdsp;



import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;

import org.achartengine.GraphicalView;

import com.prototype.ajdsp.MIDIView.ButtonPressedListener;
import com.prototype.ajdsp.PZPlacementView.ZeroPolePositionListener;
import com.prototype.ajdsp.PartObject.PinSignalTypes;
import com.prototype.ajdsp.PartObject.PinTypes;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.media.SoundPool;
import android.media.SoundPool.OnLoadCompleteListener;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.RelativeLayout;
import android.widget.TextView;

public class MIDIDisplay extends Activity implements OnClickListener{
	RelativeLayout layout;
	Button back;
	Button update;
	RelativeLayout topLayout;
	Bundle inputValues;
	MIDIView midiView;
	GraphView timePlot;
	GraphView freqPlot;
	double pi=Math.PI;
	double fs = 8000; 
	double []y;
	static MediaPlayer piano1;
	static MediaPlayer piano2;
	static MediaPlayer piano3;
	static MediaPlayer piano4;
	static MediaPlayer piano5;
	static MediaPlayer piano6;
	static MediaPlayer piano7;
	static MediaPlayer piano8;
	static MediaPlayer piano9;
	static MediaPlayer piano10;
	static MediaPlayer piano11;
	static MediaPlayer piano12;
	static MediaPlayer piano13;
	static MediaPlayer piano14;
	static MediaPlayer piano15;
	static MediaPlayer piano16;
	static MediaPlayer piano17;
	static MediaPlayer piano18;
	static MediaPlayer piano19;
	static MediaPlayer piano20;
	static MediaPlayer piano21;
	static MediaPlayer piano22;
	Handler handler0=new Handler();
	PlayMusic playMusic=new PlayMusic();
	String [] verLabels;
	String [] horLabels;
	String [] horLabelsTime;
	double [] phase=new double[256];
	double [] magn =new double [256];
	Boolean create=true;
	public boolean loaded=false;
	SoundPool soundPool;
	List<double[]> fft=new ArrayList<double[]>();

	public static int midiNumber=0;


	@SuppressLint("NewApi")
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		//requestWindowFeature(Window.FEATURE_CUSTOM_TITLE);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
		setContentView(R.layout.midi);
		//getWindow().setFeatureInt(Window.FEATURE_CUSTOM_TITLE, R.layout.row);
		layout= (RelativeLayout) findViewById(R.id.midi_view);
		layout.setBackgroundColor(Color.WHITE);
		
		midiView=(MIDIView)findViewById(R.id.midi_piano);  
		timePlot=(GraphView) findViewById(R.id.midi_realPlot);
		timePlot.setLength(256);
		freqPlot=(GraphView) findViewById(R.id.midi_fftPlot);
		freqPlot.setLength(256);
		verLabels = new String[] { "great", "ok", "bad" };
		horLabels = new String[] { "0", "1/4pi", "1/2pi", "3/pi","1" };
		horLabelsTime = new String[] { "0", "32", "64", "96","128" };
	
		back=(Button) findViewById(R.id.back_select_parts);
		back.setOnClickListener(this);
		TextView title= (TextView)findViewById(R.id.Title);
		title.setText("MIDI");
		update=(Button) findViewById(R.id.add_part);
		update.setOnClickListener(this);

		timePlot.SetParameters("SignalView",horLabelsTime, verLabels);
		freqPlot.SetParameters("FFT View",horLabels, verLabels);
		//this.setVolumeControlStream(AudioManager.STREAM_MUSIC);
        /*soundPool = new SoundPool(10, AudioManager.STREAM_MUSIC, 0);
        soundPool.setOnLoadCompleteListener(new OnLoadCompleteListener() {
            @Override
            public void onLoadComplete(SoundPool soundPool, int sampleId,
                    int status) {
                loaded = true;
            }
        });*/
		piano1=MediaPlayer.create(this, R.raw.one);
		piano2=MediaPlayer.create(this, R.raw.two);
		piano3=MediaPlayer.create(this, R.raw.three);
		piano4=MediaPlayer.create(this, R.raw.four);
		piano5=MediaPlayer.create(this, R.raw.five);
		piano6=MediaPlayer.create(this, R.raw.six);
		piano7=MediaPlayer.create(this, R.raw.seven);
		piano8=MediaPlayer.create(this, R.raw.eight);
		piano9=MediaPlayer.create(this, R.raw.nine);
		piano10=MediaPlayer.create(this, R.raw.ten);
		piano11=MediaPlayer.create(this, R.raw.eleven);
		piano12=MediaPlayer.create(this, R.raw.twelve);
		piano13=MediaPlayer.create(this, R.raw.thirteen);
		piano14=MediaPlayer.create(this, R.raw.fourteen);
		piano15=MediaPlayer.create(this, R.raw.fifteen);
		piano16=MediaPlayer.create(this, R.raw.sixteen);
		piano17=MediaPlayer.create(this, R.raw.seventeen);
		piano18=MediaPlayer.create(this, R.raw.eighteen);
		piano19=MediaPlayer.create(this, R.raw.nineteen);
		piano20=MediaPlayer.create(this, R.raw.twenty);
		piano21=MediaPlayer.create(this, R.raw.twentyone);
		piano22=MediaPlayer.create(this, R.raw.twentytwo);
		MIDIFrequency freq=new MIDIFrequency();
		freq.initialize();
		fft=freq.frequencyMIDI;
		

		Bundle extras = getIntent().getExtras(); 
		if(extras !=null)
		{
			inputValues = extras.getBundle("blockValues");  
			create=false;
			update.setText("Update");
			back.setText("Back");

		}
		else {
			
			update.setText("Add");
		}
		
		midiView.setPositionListener(new ButtonPressedListener(){
			public void onButtonPressed(int buttonPressed) {
				playMIDI(buttonPressed);
				y = discretize(getFrequency(buttonPressed),1);
				timePlot.setValues(y); 
				verLabels=timePlot.getYLabels();
				timePlot.setVerLabels(verLabels);
				
				


				freqPlot.setValues(getFFT(buttonPressed));

				timePlot.invalidate();
				freqPlot.invalidate();
			}
		});


	}
	@Override
	public void onBackPressed() {
		/*if(create){
			Intent i = new Intent(this, PartList.class);
			startActivity(i);}
		else {
			Intent i = new Intent(this, StartView.class);
			startActivity(i);
		}*/
	}
	private double getFrequency(int keyNo)
	{
		switch(keyNo){
		case 0: return 146.83;
		case 100: return 138.59;
		case 1: return 164.81;
		case 101: return -1;
		case 2: return 174.61;
		case 102: return 185.00;
		case 3: return 196.00;
		case 103: return 207.65;
		case 4: return 220.00;
		case 104: return 233.08;
		case 5: return 246.94;
		case 105: return -1;
		case 6: return 261.63;
		case 106: return 277.18;
		case 7: return 293.67;
		case 107: return 311.13;
		case 8: return 329.63;
		case 108: return -1;
		case 9: return 349.23;
		case 109: return 369.99;
		case 10: return 392.00;
		case 110: return 415.30;
		case 11: return 440.00;
		case 111: return 466.16;
		case 12: return 493.88;
		case 112: return -1;
		case -200: return -200;
		default: return -1;
		}
	}
	private double[] getFFT(int keyNo)
	{
		switch(keyNo){
		case 0: return fft.get(0) ;
		case 100: return fft.get(1);
		case 1: return fft.get(2);
		case 101: return new double[256];
		case 2: return fft.get(3);
		case 102: return fft.get(4);
		case 3: return fft.get(5);
		case 103: return fft.get(6);
		case 4: return fft.get(7);
		case 104: return fft.get(8);
		case 5: return fft.get(9);
		case 105: return new double[256];
		case 6: return fft.get(10);
		case 106: return fft.get(11);
		case 7: return fft.get(12);
		case 107: return fft.get(13);
		case 8: return fft.get(14);
		case 108: return new double[256];
		case 9: return fft.get(15);
		case 109: return fft.get(16);
		case 10: return fft.get(17);
		case 110: return fft.get(18);
		case 11: return fft.get(19);
		case 111: return fft.get(20);
		case 12: return fft.get(21);
		case 112: return new double[256];
		case -200: return new double[256] ;
		default: return new double[256];
		}
	}
	private double[] discretize(double frequency, int framesNo)
	{
		int sampleLength = framesNo*256;
		double dt = 1/fs;
		double cosine[] = new double[sampleLength];

		if (frequency >=0) //for the case of a space
			for (int i = 0; i < sampleLength; i++)
				cosine[i] = Math.cos(2*Math.PI*frequency*i*dt);

		return cosine;
	}
	public void playMIDI(int Counter)
	{
		 AudioManager audioManager = (AudioManager) getSystemService(AUDIO_SERVICE);
         float actualVolume = (float) audioManager
                 .getStreamVolume(AudioManager.STREAM_MUSIC);
         float maxVolume = (float) audioManager
                 .getStreamMaxVolume(AudioManager.STREAM_MUSIC);
         float volume = actualVolume / maxVolume;
         // Is the sound loaded already?
         
		//String[][] fileToPlay = {{"1.wav"},{"2.wav"},{"3.wav"},{"4.wav"},{"5.wav"},{"6.wav"},{"7.wav"},{"8.wav"},{"9.wav"},{"10.wav"}};
		String fileToPlay = "1.wav";
		//String play = fileToPlay[Counter];
		//AudioClip clip = getAudioClip(getCodeBase(), "A.wav");
		if(Counter == 0)  {			
				
	            // soundPool.play(soundPool.load(this, R.raw.one, 1), volume, volume, 1, 0, 1.0f);
	            
	         //piano1.start();
			Runnable runnable = new Runnable() {
			      @Override
			      public void run() {
			        
			       
			          
			          handler0.post(new Runnable() {
			            @Override
			            public void run() {
			            	piano1.start();
			        		
			            }
			          });
			        
			      }
			    };
			    new Thread(runnable).start();	
		
		}

		
		
		
		else if(Counter == 100)
			piano2.start();
		
		
		    
		else if(Counter == 1)
			piano3.start();
		else if(Counter == 2)
			piano4.start();
		else if(Counter == 102)
			piano5.start();
		else if(Counter == 3)
			piano6.start();
		else if(Counter == 103)
			piano7.start();
		else if(Counter == 4)
			piano8.start();
		else if(Counter == 104)
			piano9.start();
		else if(Counter == 5)
			piano10.start();
		else if(Counter == 6)
			piano11.start();
		else if(Counter == 106)
			piano12.start();
		else if(Counter == 7)
			piano13.start();
		else if(Counter == 107)
			piano14.start();
		else if(Counter == 8)
			piano15.start();
		else if(Counter == 9)
			piano16.start();
		else if(Counter == 109)
			piano17.start();
		else if(Counter == 10)
			piano18.start();
		else if(Counter == 110)
			piano19.start();
		else if(Counter == 11)
			piano20.start();
		else if(Counter == 111)
			piano21.start();
		else
			piano22.start();


	
	}
	@Override
	 public void onStop() {
		 super.onStop();	
		finish();
				
	 }
	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		if(v==back){
			if(create){
				Intent i = new Intent(this, PartList.class);
				startActivity(i);}
				 else {
					 Intent i = new Intent(this, StartView.class);
						startActivity(i);
				 }

		}else if (v==update){
			Bundle midiInput=new Bundle();
			PinSignalTypes midiSigType=new PinSignalTypes();		
			midiInput.putSerializable("pinSignalType", midiSigType);
			double [] midiInputArray = new double[256];
			Bundle midiOutputBundle=new Bundle();
			midiOutputBundle.putDoubleArray("signalMag",y);
			midiOutputBundle.putDoubleArray("signalPhase",midiInputArray);
			midiInput.putBundle("rightPin", midiOutputBundle);
			
			if(create){
				midiNumber++;

				midiInput.putString("title","MIDI"+" "+midiNumber); 
				PinTypes midiTypes=new PinTypes();
				midiTypes.rightPin=true;
				midiInput.putSerializable("PinTypes",midiTypes);				
				PartsController.createBlock(midiInput);
			}


			else{
				midiInput.putString("title",inputValues.getString("title"));
				PartsController.updateBlock(midiInput);
			}
			piano1.release();
			piano2.release();
			piano3.release();
			piano4.release();
			piano5.release();
			piano6.release();
			piano7.release();
			piano8.release();
			piano9.release();
			piano10.release();
			piano11.release();
			piano12.release();
			piano13.release();
			piano14.release();
			piano15.release();
			piano16.release();
			piano17.release();
			piano18.release();
			piano19.release();
			piano20.release();
			piano21.release();
			piano22.release();
			
			Intent i = new Intent(this, StartView.class);
			startActivity(i);

		}

	}
	private class PlayMusic extends AsyncTask<Integer, Void,Void> {
		 
		
	  @Override		
		    protected Void doInBackground(Integer... params) {
		
		      
		 
	Integer input=params[0];
		 

			if(input==0){
			
			
				 piano1.start();
				
			
			}
			/*else if (input.equalsIgnoreCase("db")){
				 
				isLinear=false;
				plotView.setInputArray(inputArrayDb,length);
				
				 
				
			}
		
			else if (input.equalsIgnoreCase("cont")){
				sigType=1;
	            plotView.setInputArray(inputArrayDefault,length);
				
		}else if (input.equalsIgnoreCase("disc")){	
			sigType=0;
			plotView.setInputArray(inputArrayDefault,length);
			
			
	}*/
		 
		       
		        
		 
		       
		 
		     return null ;  
		 
		    }
		 
		 
		 
		 
		  
		 
		    }

	private String [] getValues(double[] signalArray,double length){
		String [] values= new String[(int)length];
		DecimalFormat twoDForm = new DecimalFormat("#.###");
		for(int i=0;i<length;i++){
			values[i]="x="+i+", "+"y="+twoDForm.format(signalArray[i]);

		}
		return values;
	}

}
