package com.prototype.ajdsp;



import android.app.Activity;
import android.app.ListActivity;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Color;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.view.View.OnClickListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.RelativeLayout;

public class Mode extends Activity  implements OnClickListener {
	Button discrete;
	Button continuous;
	LinearLayout layout;
	RelativeLayout topBar;
	Button add;
	Button back;
	
	
	 @Override
	    public void onCreate(Bundle savedInstanceState) {
	        super.onCreate(savedInstanceState);
	        requestWindowFeature(Window.FEATURE_NO_TITLE);
	        this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
	        setContentView(R.layout.mode);
	        layout=(LinearLayout)findViewById(R.id.modeLayout);
	        layout.setBackgroundColor(Color.WHITE);
	        add= (Button)findViewById(R.id.add_part); 
	        add.setVisibility(View.GONE);
	        back= (Button)findViewById(R.id.back_select_parts); 
	        back.setOnClickListener(this);
	        back.setText("Back");
	        
	        discrete=(Button)findViewById(R.id.Discrete);
	        discrete.setOnClickListener(this);
	        continuous =(Button)findViewById(R.id.Continuous);
	        continuous.setOnClickListener(this);
	        
	          }
	 @Override
	 public void onStop() {
		 super.onStop();	
		finish();
				
	 }
	 @Override
	 public void onBackPressed() {
		
				/*Intent i = new Intent(this, StartView.class);
				startActivity(i);*/
			
	 }
	   
	 @Override
	 	public void onClick(View v) {
	 		/** When OK Button is clicked, dismiss the dialog */
	 		if (v == discrete){
	 		       Intent i = new Intent(this, InputSignalType.class);
	 		       i.putExtra("signalType", "discrete");
	 		        startActivity(i);
	 		  }
	 		else if (v == continuous){
	 		       Intent i = new Intent(this, InputSignalType.class);
	 		      i.putExtra("signalType", "continuous");
	 		        startActivity(i);
	 		  } else if(v==back){
	 			 Intent i = new Intent(this, StartView.class);
					startActivity(i);
	 		  }
	 		
	 }
	

}
